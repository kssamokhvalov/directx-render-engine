#pragma once
#include "source/math/Vector3.h"
#include "utils/FPSTimer.h"
#include "windows/window.h"
#include "source/math/Sphere.h"
#include "source/math/Matrix4x4.h"
#include "source/math/Vector3.h"
#include "source/math/Ray.h"
#include "source/Mesh.h"
#include "source/dragger/MatrixMover.h"
#include  "utils/ParallelExecutor.h"
#include "render/d3d.h"
#include "render/systems_and_managers/ModelManager.h"
#include "render/systems_and_managers/MeshSystem.h"
#include "render/systems_and_managers/RenderManager.h"
#include "render/systems_and_managers/TextureManager.h"
#include "render/systems_and_managers/TransformSystem.h"
#include "render/systems_and_managers/LightSystem.h"
#include "render/systems_and_managers/ReflectionCapture.h"
#include "render/systems_and_managers/ParticleSystem.h"


class Engine
{
public:

	// called from main.cpp
	static void init()
	{
		// initilizes engine singletons
		engine::D3D::instance().init();
		TransformSystem::instance().init();
		ModelManager::instance().init();
		MeshSystem::instance().init();
		TextureManager::instance().init();
		LightSystem::instance().init();
		RenderManager::instance().init();
		ReflectionCapture::instance().init();
		ParticleSystem::instance().init();
	}

	static void deinit()
	{
		// deinitilizes engine singletons in reverse order
		ParticleSystem::instance().deinit();
		ReflectionCapture::instance().deinit();
		RenderManager::instance().deinit();
		LightSystem::instance().deinit();
		TextureManager::instance().deinit();
		MeshSystem::instance().deinit();
		ModelManager::instance().deinit();
		TransformSystem::instance().deinit();
		engine::D3D::instance().deinit();
	}
private:
};

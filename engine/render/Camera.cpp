#include "Camera.h"
#include "Primitives.h"
#include "../source/math/Ray.h"
#include "systems_and_managers/TransformSystem.h"
#include <math.h>

math::Ray Camera::getRayForPixelPos(int x, int y, int width_window, int height_window)
{
    Vector3 position_Pixel = m_BL + m_BR * (static_cast<float>(x) / width_window)
        + m_TL * (static_cast<float>(height_window - y) / height_window);
    math::Ray ray_cast({ m_position, position_Pixel });
    return ray_cast;
}

void Camera::setPerspective(float fov, float aspect, float near_plane, float far_plane)
{
    m_updatedMatrices = false;

    m_near_plane = near_plane;
    m_far_plane = far_plane;

    m_proj = Matrix4x4::createFrustumPerspectiveMatrix(fov, aspect, near_plane, far_plane);
    m_projInv = m_proj.getInversedMatrix();
}

void Camera::setWorldOffset(const Vector3& offset)
{
    m_updatedMatrices = false;

    m_position = offset;
}

void Camera::addWorldOffset(const Vector3& offset)
{
    m_updatedMatrices = false;


    m_position += offset;
}

void Camera::addRelativeOffset(const Vector3& offset)
{
    updateMatrices();
    m_updatedMatrices = false;

    m_position += right() * offset.x() + top() * offset.y() + forward() * offset.z();
}

void Camera::setWorldAngles(const Angles& angles)
{
    m_updatedMatrices = false;

    m_rotation = Quaternion(Vector3( 0.0f, 0.0f, 1.0f), angles.roll);
    m_rotation *= Quaternion(Vector3(1.0f, 0.0f, 0.0f), angles.pitch);
    m_rotation *= Quaternion(Vector3(0.0f, 1.0f, 0.0f), angles.yaw);

    m_rotation.normalize();
}

void Camera::addWorldAngles(const Angles& angles)
{
    m_updatedMatrices = false;

    m_rotation *= Quaternion(Vector3( 0.f, 0.f, 1.f ), angles.roll); // overwrites
    m_rotation *= Quaternion(Vector3(1.f, 0.f, 0.f ), angles.pitch);
    m_rotation *= Quaternion(Vector3(0.f, 1.f, 0.f ), angles.yaw);

    m_rotation.normalize();
}

void Camera::addRelativeAngles(const Angles& angles)
{
    m_updatedMatrices = false;

    if(m_fixedBottom)
    {
        m_rotation *= Quaternion(right(), angles.pitch); // rotate around Camera's X
        m_rotation *= Quaternion(Vector3(0.f, 1.f, 0.f), angles.yaw); // rotate around World's Y
    }
    else
    {
        m_rotation *= Quaternion(forward(), angles.roll);
        m_rotation *= Quaternion(right(), angles.pitch);
        m_rotation *= Quaternion(top(), angles.yaw);

    }

    m_rotation.normalize();
}

void Camera::updateMatrices()
{
    if (m_updatedMatrices) return;
    m_updatedMatrices = true;


    m_viewInv = Matrix4x4::createTransformationMatrix(m_rotation, m_position);
    TransformSystem::instance().changeCameraMatrix(m_viewInv);
    m_view.invertOrthonormal(m_viewInv);

    m_viewProj = m_view * m_proj;
    m_viewProjInv = m_projInv * m_viewInv;

    m_BL = Matrix4x4::multiply(Vector4(-1.0f, -1.0f, 1.0f, 1.0f), m_projInv).xyzProj();
    m_BR = Matrix4x4::multiply(Vector4(1.0f, -1.0f, 1.0f, 1.0f), m_projInv).xyzProj();
    m_TL = Matrix4x4::multiply(Vector4(-1.0f, 1.0f, 1.0f, 1.0f), m_projInv).xyzProj();
    m_BR -= m_BL;
    m_TL -= m_BL;

    m_BL = Matrix4x4::multiply(Vector4(m_BL, 0.0f), m_viewInv).xyz();
    m_BR = Matrix4x4::multiply(Vector4(m_BR, 0.0f), m_viewInv).xyz();
    m_TL = Matrix4x4::multiply(Vector4(m_TL, 0.0f), m_viewInv).xyz();
}

void Camera::getFarFrustrimVectors(Vector3& BR, Vector3& BL, Vector3& TR, Vector3& TL)
{
    BL = Matrix4x4::multiply(Vector4(-1.0f, -1.0f, 0.0f, 1.0f), m_projInv).xyzProj();
    BR = Matrix4x4::multiply(Vector4(1.0f, -1.0f, 0.0f, 1.0f), m_projInv).xyzProj();
    TR = Matrix4x4::multiply(Vector4(1.0f, 1.0f, 0.0f, 1.0f), m_projInv).xyzProj();
    TL = Matrix4x4::multiply(Vector4(-1.0f, 1.0f, 0.0f, 1.0f), m_projInv).xyzProj();

    BR -= BL;
    TR -= BL;
    TL -= BL;

    BL = Matrix4x4::multiply(Vector4(BL, 0.0f), m_viewInv).xyz();
    BR = Matrix4x4::multiply(Vector4(BR, 0.0f), m_viewInv).xyz();
    TR = Matrix4x4::multiply(Vector4(TR, 0.0f), m_viewInv).xyz();
    TL = Matrix4x4::multiply(Vector4(TL, 0.0f), m_viewInv).xyz();

    BR += BL;
    TR += BL;
    TL += BL;
}


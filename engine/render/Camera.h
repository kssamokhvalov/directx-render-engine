#pragma once
#include "../source/math/Vector3.h"
#include "../source/math/Matrix4x4.h"
#include "../source/math/Quaternion.h"


struct Angles;
namespace math
{
    struct Ray;
}

class Camera
{
public:
    Camera() :
        m_position(0.0f, 0.0f, 0.0f), m_rotation(Vector3(1.0f, 0.0f, 0.0f), 0.0f), m_fixedBottom(true)
    {}

    math::Ray getRayForPixelPos(int x, int y, int width_window, int height_window);
    void setPerspective(float fov, float aspect, float near_plane, float far_plane);
    void setWorldOffset(const Vector3& offset);
    void addWorldOffset(const Vector3& offset);
    void addRelativeOffset(const Vector3& offset);

    void setWorldAngles(const Angles& angles);
    void addWorldAngles(const Angles& angles);
    void addRelativeAngles(const Angles& angles);


    void updateMatrices();

    const Matrix4x4& getViewInv(){ return m_viewInv; }
    const Matrix4x4& getView() { return m_view; }
    const Matrix4x4& getViewProjInv() { return m_viewProjInv; }
    const Matrix4x4& getViewProj() { return m_viewProj; }
    const Matrix4x4& getProjetionInv() { return m_projInv; }
    const Matrix4x4& getProjetion() { return m_proj; }

    const Vector3 right() 	    const { return m_viewInv.rowAsVector3(0); }
    const Vector3 top() 		const { return m_viewInv.rowAsVector3(1); }
    const Vector3 forward() 	const { return m_viewInv.rowAsVector3(2); }
    const Vector3 position()	const { return m_viewInv.rowAsVector3(3); }


    const Vector3& getBL() { return m_BL; }
    const Vector3& getTL() { return m_TL; }
    const Vector3& getBR() { return m_BR; }
    const Vector3& position() { return m_position; }
    bool  needUpdatedMatrices() const { return m_updatedMatrices == false; }

    Quaternion getQuaternion() const { return m_rotation; }
    void getFarFrustrimVectors(Vector3& BR, Vector3& BL, Vector3& TR, Vector3& TL);
private:

    Quaternion m_rotation;
    Matrix4x4 m_view;
    Matrix4x4 m_viewInv;

    Matrix4x4 m_proj;
    Matrix4x4 m_projInv;

    Matrix4x4 m_viewProj;
    Matrix4x4 m_viewProjInv;

    Vector3 m_position;
    Vector3 m_BL;
    Vector3 m_TL;
    Vector3 m_BR;

    float m_near_plane;
    float m_far_plane;

    bool m_fixedBottom;

    bool m_updatedMatrices;
};


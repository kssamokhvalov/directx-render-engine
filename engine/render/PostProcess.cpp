#include "PostProcess.h"

void PostProcess::init()
{
	m_buffer_post_process.EV100 = 0.0f;
	m_buffer_post_process.gamma = 2.2f;

	m_constantBufferPostProcess.init(D3D11_USAGE_DYNAMIC);
	m_constantBufferPostProcess.update(m_buffer_post_process);

	m_vertex_shader.loadVertexShader(L"../engine/shaders/PostProcess/VertexPostProcess.hlsl");
	m_pixel_shader.loadPixelShader(L"../engine/shaders/PostProcess/PixelPostProcess.hlsl");
	m_pixel_shader_FXAA.loadPixelShader(L"../engine/shaders/PostProcess/fxaa.hlsl");

	m_pixel_shader_downsampling.loadPixelShader(L"../engine/shaders/PostProcess/PixelDownsampling.hlsl");
	m_pixel_shader_upsampling.loadPixelShader(L"../engine/shaders/PostProcess/PixelUpsampling.hlsl");
	m_pixel_shader_bloom.loadPixelShader(L"../engine/shaders/PostProcess/PixelBloom.hlsl");
	m_pixel_shader_fog.loadPixelShader(L"../engine/shaders/PostProcess/PixelFog.hlsl");

	initRTandSR(480, 270);
}

void PostProcess::deinit()
{
	m_vertex_shader.release();
	m_pixel_shader.release();
	m_pixel_shader_FXAA.release();
	m_constantBufferPostProcess.deinit();

	m_first_texture.deinit();
	m_first_RT.deinit();
	m_second_texture.deinit();
	m_second_RT.deinit();

	m_HDR_copy.deinit();
	m_bloom_map.deinit();
	m_bloom_map_RT.deinit();

	m_pixel_shader_downsampling.release();
	m_pixel_shader_upsampling.release();
	m_pixel_shader_bloom.release();
	m_pixel_shader_fog.release();
}

void PostProcess::resolve(ShaderResurce* src, RenderTarget* dst)
{
	ID3D11RenderTargetView* const renderTV = dst->getFramebufferView();
	engine::s_devcon->OMSetRenderTargets(1, &renderTV, nullptr);

	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	ID3D11ShaderResourceView* src_ptr = src->getShaderResourceView();

	engine::s_devcon->PSSetShaderResources(19, 1, &src_ptr);

	m_vertex_shader.bind();
	m_pixel_shader.bind();
	m_constantBufferPostProcess.bind(4, TypeShader::TypePixelShader);

	engine::s_devcon->IASetInputLayout(nullptr);
	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	engine::s_devcon->Draw(3, 0);

	ID3D11ShaderResourceView* nullsrv = nullptr;
	engine::s_devcon->PSSetShaderResources(19, 1, &nullsrv);
}

void PostProcess::resolveMS(ShaderResurce* src, RenderTarget* dst)
{
	ID3D11RenderTargetView* const renderTV = dst->getFramebufferView();
	engine::s_devcon->OMSetRenderTargets(1, &renderTV, nullptr);

	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	ID3D11ShaderResourceView* src_ptr = src->getShaderResourceView();

	engine::s_devcon->PSSetShaderResources(12, 1, &src_ptr);

	m_vertex_shader.bind();
	m_pixel_shader.bind();
	m_constantBufferPostProcess.bind(4, TypeShader::TypePixelShader);


	engine::s_devcon->IASetInputLayout(nullptr);
	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	engine::s_devcon->Draw(3, 0);

	ID3D11ShaderResourceView* nullsrv = nullptr;
	engine::s_devcon->PSSetShaderResources(12, 1, &nullsrv);
}

void PostProcess::generateBloomMap(Texture* src, RenderTarget* dst, UINT num_down_steps)
{
	constexpr int PIXEL_OFFSET = 8;

	ID3D11ShaderResourceView* src_ptr = nullptr;

	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	engine::s_devcon->IASetInputLayout(nullptr);
	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	m_vertex_shader.bind();
	m_pixel_shader_downsampling.bind();
	m_constantBufferPostProcess.bind(4, TypeShader::TypePixelShader);


	//---------First step----------
	m_first_RT.clear();
	ID3D11RenderTargetView* renderTV = m_first_RT.getFramebufferView();
	engine::s_devcon->OMSetRenderTargets(1, &renderTV, nullptr);

	
	src->bind(19, TypePixelShader);

	m_buffer_post_process.original_width_render = dst->getWidthRender();
	m_buffer_post_process.original_height_render = dst->getHeightRender();
	m_buffer_post_process.width_render_RT = m_first_RT.getWidthRender() / 2;
	m_buffer_post_process.height_render_RT = m_first_RT.getHeightRender() / 2;


	updateConstantBufferPostProcess();

	D3D11_VIEWPORT viewport = { 0.0f, 0.0f, FLOAT(m_buffer_post_process.width_render_RT + PIXEL_OFFSET), FLOAT(m_buffer_post_process.height_render_RT + PIXEL_OFFSET), 0.0f, 1.0f };
	engine::s_devcon->RSSetViewports(1, &viewport);

	engine::s_devcon->Draw(3, 0);

	ID3D11ShaderResourceView* nullsrv = nullptr;


	//----------Midle steps------------
	for (int step = 2; step <= num_down_steps; step++)
	{
		if (step % 2 == 1)
		{
			m_first_RT.clear();
			renderTV = m_first_RT.getFramebufferView();
			src_ptr = m_second_texture.getTextureView()->ptr();
		}
		else
		{
			m_second_RT.clear();
			renderTV = m_second_RT.getFramebufferView();
			src_ptr = m_first_texture.getTextureView()->ptr();
		}

		engine::s_devcon->OMSetRenderTargets(1, &renderTV, nullptr);
		engine::s_devcon->PSSetShaderResources(19, 1, &src_ptr);

		m_buffer_post_process.width_render_RT = m_first_RT.getWidthRender() / pow(2, step);
		m_buffer_post_process.height_render_RT = m_first_RT.getHeightRender() / pow(2, step);

		updateConstantBufferPostProcess();

		viewport = { 0.0f, 0.0f, FLOAT(m_buffer_post_process.width_render_RT + PIXEL_OFFSET), FLOAT(m_buffer_post_process.height_render_RT + PIXEL_OFFSET), 0.0f, 1.0f };
		engine::s_devcon->RSSetViewports(1, &viewport);

		engine::s_devcon->Draw(3, 0);

		engine::s_devcon->PSSetShaderResources(19, 1, &nullsrv);
	}


	m_pixel_shader_upsampling.bind();


	for (int step = num_down_steps - 1; step >= 1; step--)
	{
		if (step % 2 == 1)
		{
			m_first_RT.clear();
			renderTV = m_first_RT.getFramebufferView();
			src_ptr = m_second_texture.getTextureView()->ptr();
		}
		else
		{
			m_second_RT.clear();
			renderTV = m_second_RT.getFramebufferView();
			src_ptr = m_first_texture.getTextureView()->ptr();
		}
		engine::s_devcon->OMSetRenderTargets(1, &renderTV, nullptr);
		engine::s_devcon->PSSetShaderResources(19, 1, &src_ptr);

		m_buffer_post_process.width_render_RT = m_first_RT.getWidthRender() / pow(2, step);
		m_buffer_post_process.height_render_RT = m_first_RT.getHeightRender() / pow(2, step);

		updateConstantBufferPostProcess();

		viewport = { 0.0f, 0.0f, FLOAT(m_first_RT.getWidthRender() / pow(2, step) + PIXEL_OFFSET), FLOAT(m_first_RT.getHeightRender() / pow(2, step) + PIXEL_OFFSET), 0.0f, 1.0f };
		engine::s_devcon->RSSetViewports(1, &viewport);

		engine::s_devcon->Draw(3, 0);

		engine::s_devcon->PSSetShaderResources(19, 1, &nullsrv);
	}


	//----------Last step------------
	renderTV = dst->getFramebufferView();
	engine::s_devcon->OMSetRenderTargets(1, &renderTV, nullptr);

	if (num_down_steps % 2 == 1)
	{
		src_ptr = m_first_texture.getTextureView()->ptr();
	}
	else
	{
		src_ptr = m_second_texture.getTextureView()->ptr();
	}


	engine::s_devcon->PSSetShaderResources(19, 1, &src_ptr);

	m_buffer_post_process.width_render_RT = m_first_RT.getWidthRender();
	m_buffer_post_process.height_render_RT = m_first_RT.getHeightRender();

	updateConstantBufferPostProcess();

	viewport = { 0.0f, 0.0f, FLOAT(m_first_RT.getWidthRender()), FLOAT(m_first_RT.getHeightRender()), 0.0f, 1.0f };
	engine::s_devcon->RSSetViewports(1, &viewport);

	engine::s_devcon->Draw(3, 0);


	viewport = { 0.0f, 0.0f, FLOAT(dst->getWidthRender()), FLOAT(dst->getHeightRender()), 0.0f, 1.0f };
	engine::s_devcon->RSSetViewports(1, &viewport);

	engine::s_devcon->PSSetShaderResources(19, 1, &nullsrv);

}

void PostProcess::applyBloom(engine::DxResPtr<ID3D11Texture2D> src, RenderTarget* dst)
{
	if (dst->getWidthRender() != m_first_RT.getWidthRender() || dst->getHeightRender() != m_first_RT.getHeightRender())
	{
		initRTandSR(dst->getWidthRender(), dst->getHeightRender());
	}
	engine::s_devcon->CopyResource(m_HDR_copy.getTexture()->ptr(), src.ptr());
	generateBloomMap(&m_HDR_copy, &m_bloom_map_RT, 5);

	applyBloom(dst);
}


void PostProcess::initRTandSR(UINT width_render, UINT height_render)
{
	if (m_first_texture.getTexture()->ptr() != nullptr)
	{
		m_first_texture.deinit();
		m_first_RT.deinit();

		m_second_texture.deinit();
		m_second_RT.deinit();

		m_HDR_copy.deinit();
		m_bloom_map.deinit();
		m_bloom_map_RT.deinit();
	}


	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));

	textureDesc.Width = width_render;
	textureDesc.Height = height_render;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	m_first_texture.init(&textureDesc);
	m_second_texture.init(&textureDesc);
	m_HDR_copy.init(&textureDesc);
	m_bloom_map.init(&textureDesc);

	D3D11_RENDER_TARGET_VIEW_DESC desc_rtv;
	ZeroMemory(&desc_rtv, sizeof(desc_rtv));
	desc_rtv.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	desc_rtv.Format = textureDesc.Format;

	m_first_RT.init(m_first_texture.getTexture()->ptr(), &desc_rtv);
	m_second_RT.init(m_second_texture.getTexture()->ptr(), &desc_rtv);
	m_bloom_map_RT.init(m_bloom_map.getTexture()->ptr(), &desc_rtv);
}

void PostProcess::applyBloom(RenderTarget* dst)
{
	ID3D11RenderTargetView* const renderTV = dst->getFramebufferView();
	engine::s_devcon->OMSetRenderTargets(1, &renderTV, nullptr);

	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	m_HDR_copy.bind(19, TypePixelShader);
	m_bloom_map.bind(27, TypePixelShader);

	m_vertex_shader.bind();
	m_pixel_shader_bloom.bind();
	m_constantBufferPostProcess.bind(4, TypeShader::TypePixelShader);

	engine::s_devcon->IASetInputLayout(nullptr);
	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	engine::s_devcon->Draw(3, 0);

	m_HDR_copy.unbind();
	m_bloom_map.unbind();
}

void PostProcess::applyFXAA(Texture* src, RenderTarget* dst)
{
	ID3D11RenderTargetView* const renderTV = dst->getFramebufferView();
	engine::s_devcon->OMSetRenderTargets(1, &renderTV, nullptr);

	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	src->bind(26, TypePixelShader);

	m_vertex_shader.bind();
	m_pixel_shader_FXAA.bind();
	m_constantBufferPostProcess.bind(4, TypeShader::TypePixelShader);

	engine::s_devcon->IASetInputLayout(nullptr);
	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	engine::s_devcon->Draw(3, 0);

	src->unbind();
}

void PostProcess::applyFog(engine::DxResPtr<ID3D11Texture2D> src, RenderTarget* dst, Texture* depth)
{
	if (dst->getWidthRender() != m_first_RT.getWidthRender() || dst->getHeightRender() != m_first_RT.getHeightRender())
	{
		initRTandSR(dst->getWidthRender(), dst->getHeightRender());
	}

	engine::s_devcon->CopyResource(m_HDR_copy.getTexture()->ptr(), src.ptr());

	ID3D11RenderTargetView* const renderTV = dst->getFramebufferView();
	engine::s_devcon->OMSetRenderTargets(1, &renderTV, nullptr);

	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	m_HDR_copy.bind(19, TypePixelShader);
	depth->bind(18, TypeShader::TypePixelShader);

	m_vertex_shader.bind();
	m_pixel_shader_fog.bind();
	m_constantBufferPostProcess.bind(4, TypeShader::TypePixelShader);

	engine::s_devcon->IASetInputLayout(nullptr);
	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	engine::s_devcon->Draw(3, 0);

	m_HDR_copy.unbind();
	depth->unbind();
}

void PostProcess::setGamma(float gamma)
{
	m_buffer_post_process.gamma = gamma;
	updateConstantBufferPostProcess();
}

void PostProcess::setEV100(float EV100)
{
	m_buffer_post_process.EV100 = EV100;
	updateConstantBufferPostProcess();
}

void PostProcess::updateConstantBufferPostProcess()
{
	m_constantBufferPostProcess.update(m_buffer_post_process);
}

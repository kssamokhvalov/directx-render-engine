#pragma once
#include <memory>
#include "d3d.h"
#include "d3d_utils/PixelShader.h"
#include "d3d_utils/VertexShader.h"
#include "d3d_utils/ShaderResurce.h"
#include "d3d_utils/RenderTarget.h"
#include "d3d_utils/Texture.h"
#include "d3d_utils/UniformBuffer.h"

class PostProcess
{
public:
	struct PostProcessBuffer
	{
		float original_width_render;
		float original_height_render;
		float width_render_RT;
		float height_render_RT;
		float gamma;
		float EV100;
		float _padd[2];
	};

	void init();
	void deinit();
	void resolve(ShaderResurce* src, RenderTarget* dst); 
	void resolveMS(ShaderResurce* src, RenderTarget* dst);
	void generateBloomMap(Texture* src, RenderTarget* dst, UINT num_down_steps);
	void applyBloom(engine::DxResPtr<ID3D11Texture2D> src, RenderTarget* dst);
	void applyFXAA(Texture* src, RenderTarget* dst); //TODO chaneg Texture to ShaderResurce

	void applyFog(engine::DxResPtr<ID3D11Texture2D> src, RenderTarget* dst, Texture* depth);
	void setGamma(float gamma);
	void setEV100(float EV100);

	float getEV100() const { return m_buffer_post_process.EV100; }
	float getGamma() const { return m_buffer_post_process.gamma; }

	
private:
	void updateConstantBufferPostProcess();
	void initRTandSR(UINT width_render, UINT height_render);

	void applyBloom(RenderTarget* dst);

	VertexShader m_vertex_shader;
	PixelShader m_pixel_shader;
	PixelShader m_pixel_shader_FXAA;
	PixelShader m_pixel_shader_downsampling;
	PixelShader m_pixel_shader_upsampling;
	PixelShader m_pixel_shader_bloom;
	PixelShader m_pixel_shader_fog;

	UniformBuffer<PostProcessBuffer> m_constantBufferPostProcess;

	PostProcessBuffer m_buffer_post_process;

	//RTs and SRs for downsampling and upsampling
	Texture m_first_texture;
	RenderTarget m_first_RT;

	Texture m_second_texture;
	RenderTarget m_second_RT;

	Texture m_bloom_map;
	RenderTarget m_bloom_map_RT;

	Texture m_HDR_copy;
};


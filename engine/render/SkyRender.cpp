#include "SkyRender.h"
#include "systems_and_managers/TextureManager.h"

void SkyRender::init()
{
	m_vertex_shader.loadVertexShader(L"../engine/shaders/SkyShaders/VertexSkyShader.hlsl");
	m_pixel_shader.loadPixelShader(L"../engine/shaders/SkyShaders/PixelSkyShader.hlsl");
}

void SkyRender::deinit()
{
	m_vertex_shader.release();
	m_pixel_shader.release();
}



void SkyRender::render()
{
	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	m_sky_cube->bind(0, TypeShader::TypePixelShader);
	m_vertex_shader.bind();
	m_pixel_shader.bind();

	engine::s_devcon->IASetInputLayout(nullptr);
	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	engine::s_devcon->Draw(3, 0);
}

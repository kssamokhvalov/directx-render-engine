#pragma once
#include <memory>
#include "d3d.h"
#include "d3d_utils/PixelShader.h"
#include "d3d_utils/VertexShader.h"
#include "systems_and_managers/TextureManager.h"

class SkyRender
{
public:
	void init();
	void deinit();
	void render();
	void setTextureSky(const std::shared_ptr<Texture>& texture_sky) { m_sky_cube = texture_sky; }
	std::shared_ptr<Texture>& getTextureSky() { return m_sky_cube; }
private:
	std::shared_ptr<Texture> m_sky_cube;

	VertexShader m_vertex_shader;
	PixelShader m_pixel_shader;
};


#include "d3d.h"
#include "../utils/ParallelExecutor.h"
#include "../windows/window.h"
#include "../utils/Time.h"

namespace engine
{
	// global pointers to most used D3D11 objects for convenience:
	ID3D11Device5* s_device = nullptr;
	ID3D11DeviceContext4* s_devcon = nullptr;
	IDXGIFactory5* s_factory = nullptr;


	ID3D11SamplerState* s_pointWrap = nullptr;
	ID3D11SamplerState* s_linearWrap = nullptr;
	ID3D11SamplerState* s_anisotropicWrap = nullptr;
	ID3D11SamplerState* s_linearBorder = nullptr;
	ID3D11SamplerState* s_linearClamp = nullptr;
	ID3D11SamplerState* s_anisotropicClamp = nullptr;

	ID3D11SamplerState* s_linearWrapCmp = nullptr;

	ID3D11DepthStencilState* s_depth_state_write = nullptr;
	ID3D11DepthStencilState* s_depth_state_readonly = nullptr;
	ID3D11DepthStencilState* s_depth_state_off_readonly = nullptr;

	ID3D11RasterizerState* s_raster_state = nullptr;
	ID3D11RasterizerState* s_raster_state_shadow = nullptr;
	ID3D11RasterizerState* s_raster_state_masking = nullptr;

	ID3D11BlendState* s_blend_state = nullptr;
	ID3D11BlendState* s_blend_state_smoke = nullptr;
}


// Say to NVidia or AMD driver to prefer a dedicated GPU instead of an integrated.
// This has effect on laptops.
extern "C"
{
	_declspec(dllexport) uint32_t NvOptimusEnablement = 1;
	_declspec(dllexport) uint32_t AmdPowerXpressRequestHighPerformance = 1;
}

namespace engine
{
	D3D& D3D::instance()
	{
		static D3D s_d3d;
		return s_d3d;
	}

	void D3D::shangeRasterizerStateShadow(int depth_bias, float slope_scaled_depth_bias) 
	{
		D3D11_RASTERIZER_DESC rasterizerDesc{};

		memset(&rasterizerDesc, 0, sizeof(D3D11_RASTERIZER_DESC));


		rasterizerDesc.FillMode = D3D11_FILL_SOLID;
		rasterizerDesc.CullMode = D3D11_CULL_BACK;
		rasterizerDesc.FrontCounterClockwise = FALSE;
		rasterizerDesc.DepthBias = depth_bias;
		rasterizerDesc.DepthBiasClamp = 0.0f;
		rasterizerDesc.SlopeScaledDepthBias = slope_scaled_depth_bias;
		rasterizerDesc.DepthClipEnable = TRUE;
		rasterizerDesc.ScissorEnable = FALSE;
		rasterizerDesc.AntialiasedLineEnable = FALSE;
		rasterizerDesc.MultisampleEnable = TRUE;

		HRESULT result = m_device5->CreateRasterizerState(&rasterizerDesc,
			m_raster_state_shadow.reset());
		ALWAYS_ASSERT(result >= 0);

		s_raster_state_shadow = m_raster_state_shadow.ptr();
	}

	void D3D::initTextureSamplers()
	{
		D3D11_SAMPLER_DESC samplerDesc;

		ZeroMemory(&samplerDesc, sizeof(samplerDesc));

		samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		samplerDesc.MinLOD = 0;
		samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

		m_device5->CreateSamplerState(&samplerDesc, m_pointWrap.reset());

		samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		m_device5->CreateSamplerState(&samplerDesc, m_linearWrap.reset());

		samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
		m_device5->CreateSamplerState(&samplerDesc, m_anisotropicWrap.reset());

		samplerDesc.ComparisonFunc = D3D11_COMPARISON_GREATER_EQUAL;
		samplerDesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT;
		m_device5->CreateSamplerState(&samplerDesc, m_linearWrapCmp.reset());

		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		m_device5->CreateSamplerState(&samplerDesc, m_linearClamp.reset());

		samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
		m_device5->CreateSamplerState(&samplerDesc, m_anisotropicClamp.reset());

		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
		samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		samplerDesc.BorderColor[0] = 0.00244f;
		samplerDesc.BorderColor[1] = 0.00244f;
		samplerDesc.BorderColor[2] = 0.00244f;
		samplerDesc.BorderColor[3] = 0.00244f;
		m_device5->CreateSamplerState(&samplerDesc, m_linearBorder.reset());

		
	}

	void D3D::initDepthState()
	{
		D3D11_DEPTH_STENCIL_DESC depthStencilDesc{};

		memset(&depthStencilDesc, 0, sizeof(D3D11_DEPTH_STENCIL_DESC));

		depthStencilDesc.DepthEnable = true;
		depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		depthStencilDesc.DepthFunc = D3D11_COMPARISON_GREATER_EQUAL;
		depthStencilDesc.StencilEnable = true;
		depthStencilDesc.StencilReadMask = 0xFF;
		depthStencilDesc.StencilWriteMask = 0xFF;

		// Stencil operations if pixel is front-facing
		depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_REPLACE;
		depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		// Stencil operations if pixel is back-facing
		depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_REPLACE;
		depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		HRESULT result = m_device5->CreateDepthStencilState(&depthStencilDesc,
			m_depth_state_write.reset());
		ALWAYS_ASSERT(result >= 0);


		depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
		depthStencilDesc.StencilEnable = true;
		depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_EQUAL;
		depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_EQUAL;
		result = m_device5->CreateDepthStencilState(&depthStencilDesc,
			m_depth_state_readonly.reset());
		ALWAYS_ASSERT(result >= 0);

		depthStencilDesc.DepthEnable = false;
		result = m_device5->CreateDepthStencilState(&depthStencilDesc,
			m_depth_state_off_readonly.reset());
		ALWAYS_ASSERT(result >= 0);
	}

	void D3D::initRasterizerState() 
	{
		D3D11_RASTERIZER_DESC rasterizerDesc{};

		memset(&rasterizerDesc, 0, sizeof(D3D11_RASTERIZER_DESC));


		rasterizerDesc.FillMode = D3D11_FILL_SOLID;
		rasterizerDesc.CullMode = D3D11_CULL_BACK;
		rasterizerDesc.FrontCounterClockwise = FALSE;
		rasterizerDesc.DepthBias = 0;
		rasterizerDesc.DepthBiasClamp = 0.0f;
		rasterizerDesc.SlopeScaledDepthBias = 0.0f;
		rasterizerDesc.DepthClipEnable = TRUE;
		rasterizerDesc.ScissorEnable = FALSE;
		rasterizerDesc.AntialiasedLineEnable = FALSE;
		rasterizerDesc.MultisampleEnable = TRUE;


		HRESULT result = m_device5->CreateRasterizerState(&rasterizerDesc,
			m_raster_state.reset());
		ALWAYS_ASSERT(result >= 0);

		rasterizerDesc.CullMode = D3D11_CULL_NONE;
		result = m_device5->CreateRasterizerState(&rasterizerDesc,
			m_raster_state_masking.reset());
		ALWAYS_ASSERT(result >= 0);

		rasterizerDesc.CullMode = D3D11_CULL_BACK;
		rasterizerDesc.DepthBias = -1;
		rasterizerDesc.SlopeScaledDepthBias = -2.1;

		result = m_device5->CreateRasterizerState(&rasterizerDesc,
			m_raster_state_shadow.reset());
		ALWAYS_ASSERT(result >= 0);


	}

	void D3D::initBlendState()
	{
		D3D11_BLEND_DESC blendState;
		ZeroMemory(&blendState, sizeof(D3D11_BLEND_DESC));
		blendState.AlphaToCoverageEnable = TRUE;
		blendState.RenderTarget[0].BlendEnable = FALSE;
		blendState.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		blendState.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		blendState.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		blendState.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		blendState.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		blendState.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
		blendState.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

		m_device5->CreateBlendState(&blendState, m_blend_state.reset());

		blendState.AlphaToCoverageEnable = FALSE;
		blendState.RenderTarget[0].BlendEnable = TRUE;
		m_device5->CreateBlendState(&blendState, m_blend_state_smoke.reset());
	}

	void D3D::init()
	{
		HRESULT result;

		result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)m_factory.reset());
		ALWAYS_ASSERT(result >= 0 && "CreateDXGIFactory");

		result = m_factory->QueryInterface(__uuidof(IDXGIFactory5), (void**)m_factory5.reset());
		ALWAYS_ASSERT(result >= 0 && "Query IDXGIFactory5");

		
			uint32_t index = 0;
			DxResPtr <IDXGIAdapter1> adapter;
			while (m_factory5->EnumAdapters1(index++, adapter.reset()) != DXGI_ERROR_NOT_FOUND)
			{
				DXGI_ADAPTER_DESC1 desc;
				adapter->GetDesc1(&desc);
				std::wstring descr(desc.Description);
				//TODO LOG << "GPU #" << index << desc.Description;
			}
		// Init D3D Device & Context

		const D3D_FEATURE_LEVEL featureLevelRequested = D3D_FEATURE_LEVEL_11_1;
		D3D_FEATURE_LEVEL featureLevelInitialized = D3D_FEATURE_LEVEL_11_1;
#if defined(_DEBUG)
		// Enable better shader debugging with the graphics debugging tools.
		UINT flags = D3D11_CREATE_DEVICE_DEBUG;
#else
		UINT flags = 0;
#endif
		result = D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, D3D11_CREATE_DEVICE_DEBUG,
			&featureLevelRequested, 1, D3D11_SDK_VERSION, m_device.reset(), &featureLevelInitialized, m_devcon.reset());
		ALWAYS_ASSERT(result >= 0 && "D3D11CreateDevice");
		ALWAYS_ASSERT(featureLevelRequested == featureLevelInitialized && "D3D_FEATURE_LEVEL_11_1");
		result = m_device->QueryInterface(__uuidof(ID3D11Device5), (void**)m_device5.reset());
		ALWAYS_ASSERT(result >= 0 && "Query ID3D11Device5");

		result = m_devcon->QueryInterface(__uuidof(ID3D11DeviceContext4), (void**)m_devcon4.reset());
		ALWAYS_ASSERT(result >= 0 && "Query ID3D11DeviceContext4");
#if defined(_DEBUG)
		result = m_device->QueryInterface(__uuidof(ID3D11Debug), (void**)m_devdebug.reset());
		ALWAYS_ASSERT(result >= 0 && "Query ID3D11Debug");
#endif

		initTextureSamplers();
		initDepthState();

		initRasterizerState();
		initBlendState();

		// Write global pointers

		s_factory = m_factory5.ptr();
		s_device = m_device5.ptr();
		s_devcon = m_devcon4.ptr();

		s_pointWrap = m_pointWrap.ptr();
		s_linearWrap = m_linearWrap.ptr();
		s_anisotropicWrap = m_anisotropicWrap.ptr();
		s_linearBorder = m_linearBorder.ptr();
		s_linearClamp = m_linearClamp.ptr();
		s_anisotropicClamp = m_anisotropicClamp.ptr();

		s_linearWrapCmp = m_linearWrapCmp.ptr();

		s_depth_state_write = m_depth_state_write.ptr();
		s_depth_state_readonly = m_depth_state_readonly.ptr();
		s_depth_state_off_readonly = m_depth_state_off_readonly.ptr();

		s_raster_state = m_raster_state.ptr();
		s_raster_state_shadow = m_raster_state_shadow.ptr();
		s_raster_state_masking = m_raster_state_masking.ptr();

		s_blend_state = m_blend_state.ptr();
		s_blend_state_smoke = m_blend_state_smoke.ptr();
	}

	void D3D::deinit()
	{
		// set objects to nullptr

		s_factory = nullptr;
		s_devcon = nullptr;
		s_device = nullptr;

		s_pointWrap = nullptr;
		s_linearWrap = nullptr;
		s_anisotropicWrap = nullptr;
		s_linearBorder = nullptr;
		s_linearClamp = nullptr;
		s_anisotropicClamp = nullptr;

		s_linearWrapCmp = nullptr;

		s_depth_state_write = nullptr;
		s_depth_state_readonly = nullptr;
		s_depth_state_off_readonly = nullptr;

		s_raster_state = nullptr;
		s_raster_state_shadow = nullptr;
		s_raster_state_masking = nullptr;;
		
		s_blend_state = nullptr;
		s_blend_state_smoke = nullptr;

		m_pointWrap.release();
		m_linearWrap.release();
		m_anisotropicWrap.release();
		m_linearBorder.release();
		m_linearClamp.release();
		m_anisotropicClamp.release();
		m_linearWrapCmp.release();

		m_depth_state_write.release();
		m_depth_state_readonly.release();
		m_depth_state_off_readonly.release();

		m_raster_state.release();
		m_raster_state_shadow.release();
		m_raster_state_masking.release();

		m_blend_state.release();
		m_blend_state_smoke.release();
#if defined(_DEBUG)
		m_devcon4->Flush();
#endif

		m_factory5.release();
		m_device5.release();
		m_devcon4.release();

		m_device.release();
		m_factory.release();
		m_devcon.release();
#if defined(_DEBUG)
		m_devdebug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
		m_devdebug.release();
#endif
	}

}
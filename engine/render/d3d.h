#pragma once

#include "../include/win_def.h"

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <d3d11_4.h>
#include <d3dcompiler.h>
#include "DxRes.h"

#include "primitives.h"

#include "../source/math/Matrix4x4.h"

#pragma comment(lib, "dxgi")
#pragma comment (lib,"d3d11.lib")

class Window;
class Time;

namespace engine
{
	// global pointers to most used D3D11 objects for convenience:
	extern ID3D11Device5* s_device;
	extern ID3D11DeviceContext4* s_devcon;
	extern IDXGIFactory5* s_factory;

	extern ID3D11SamplerState* s_pointWrap;
	extern ID3D11SamplerState* s_linearWrap;
	extern ID3D11SamplerState* s_anisotropicWrap;
	extern ID3D11SamplerState* s_linearBorder;
	extern ID3D11SamplerState* s_linearClamp;
	extern ID3D11SamplerState* s_anisotropicClamp;

	extern ID3D11SamplerState* s_linearWrapCmp;

	extern ID3D11DepthStencilState* s_depth_state_write;
	extern ID3D11DepthStencilState* s_depth_state_readonly;
	extern ID3D11DepthStencilState* s_depth_state_off_readonly;

	extern ID3D11RasterizerState* s_raster_state;
	extern ID3D11RasterizerState* s_raster_state_shadow;
	extern ID3D11RasterizerState* s_raster_state_masking;

	extern ID3D11BlendState* s_blend_state;
	extern ID3D11BlendState* s_blend_state_smoke;
}

#include "../include/win_undef.h"

namespace engine
{
	class D3D // a singletone for accessing global rendering resources
	{
	public:

		D3D(const D3D& other) = delete;

		void init();
		void deinit();

		void shangeRasterizerStateShadow(int depth_bias, float slope_scaled_depth_bias);

		void operator=(const D3D& other) = delete;
		static D3D& instance();
	private:
		D3D() = default;

		void initTextureSamplers();
		void initDepthState();
		void initRasterizerState();
		void initBlendState();

		DxResPtr<IDXGIFactory> m_factory;
		DxResPtr<IDXGIFactory5> m_factory5;
		DxResPtr<ID3D11Device> m_device;
		DxResPtr<ID3D11Device5> m_device5;
		DxResPtr<ID3D11DeviceContext> m_devcon;
		DxResPtr<ID3D11DeviceContext4> m_devcon4;
		DxResPtr<ID3D11Debug> m_devdebug;

		DxResPtr<ID3D11SamplerState> m_pointWrap;
		DxResPtr<ID3D11SamplerState> m_linearWrap;
		DxResPtr<ID3D11SamplerState> m_anisotropicWrap;

		DxResPtr<ID3D11SamplerState> m_linearBorder;

		DxResPtr<ID3D11SamplerState> m_linearClamp;
		DxResPtr<ID3D11SamplerState> m_anisotropicClamp;

		DxResPtr<ID3D11SamplerState> m_linearWrapCmp;

		DxResPtr<ID3D11DepthStencilState> m_depth_state_write;
		DxResPtr<ID3D11DepthStencilState> m_depth_state_readonly;
		DxResPtr<ID3D11DepthStencilState> m_depth_state_off_readonly;

		DxResPtr<ID3D11RasterizerState> m_raster_state;
		DxResPtr<ID3D11RasterizerState> m_raster_state_shadow;
		DxResPtr<ID3D11RasterizerState> m_raster_state_masking;
		DxResPtr<ID3D11BlendState> m_blend_state;
		DxResPtr<ID3D11BlendState> m_blend_state_smoke;
	};


}

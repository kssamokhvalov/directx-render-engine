#include "ComputeShader.h"
#include "../../utils/ParallelExecutor.h"

void ComputeShader::loadComputeShader(std::wstring path)
{
	ID3DBlob* shaderBlob;

	loadShader(path, &shaderBlob, "cs_5_0");

	HRESULT result = engine::s_device->CreateComputeShader(shaderBlob->GetBufferPointer(),
		shaderBlob->GetBufferSize(), nullptr, m_compute_shader.reset());
	ALWAYS_ASSERT(result >= 0);
}

void ComputeShader::bind()
{
	engine::s_devcon->CSSetShader(m_compute_shader, nullptr, 0);
}

void ComputeShader::release()
{
	m_compute_shader.release();
}

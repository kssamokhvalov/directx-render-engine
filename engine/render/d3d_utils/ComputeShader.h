#pragma once
#include "Shader.h"
class ComputeShader :
    public Shader
{
public:
	void loadComputeShader(std::wstring path);
	void bind();
	void release();
private:
	engine::DxResPtr<ID3D11ComputeShader> m_compute_shader;
};


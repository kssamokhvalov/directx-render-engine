#include "DepthTarget.h"
#include "../../utils/ParallelExecutor.h"

void DepthTarget::init(UINT width_render, UINT height_render)
{
	if (m_depthbuffer.ptr() != nullptr)
	{
		m_depthbuffer.release();
		m_depthView.release();
	}

	memset(&m_depthBufferDesc, 0, sizeof(D3D11_TEXTURE2D_DESC));

	m_depthBufferDesc.Width = width_render;
	m_depthBufferDesc.Height = height_render;
	m_depthBufferDesc.MipLevels = 1;
	m_depthBufferDesc.ArraySize = 1;
	m_depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	m_depthBufferDesc.SampleDesc.Count = 1;
	m_depthBufferDesc.SampleDesc.Quality = 0;
	m_depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	m_depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	m_depthBufferDesc.CPUAccessFlags = 0;
	m_depthBufferDesc.MiscFlags = 0;

	HRESULT result = engine::s_device->CreateTexture2D(&m_depthBufferDesc, NULL, m_depthbuffer.reset());
	ALWAYS_ASSERT(result >= 0);

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;

	memset(&depthStencilViewDesc, 0, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	result = engine::s_device->CreateDepthStencilView(m_depthbuffer,
		&depthStencilViewDesc,
		m_depthView.reset());
	ALWAYS_ASSERT(result >= 0);
}

void DepthTarget::init(ID3D11Resource* pResource)
{
	if (m_depthView.ptr() != nullptr)
	{
		m_depthView.release();
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	memset(&depthStencilViewDesc, 0, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	HRESULT result = engine::s_device->CreateDepthStencilView(pResource, &depthStencilViewDesc, m_depthView.reset());
	ALWAYS_ASSERT(result >= 0);
}

void DepthTarget::init(ID3D11Resource* pResource, D3D11_DEPTH_STENCIL_VIEW_DESC* desc)
{
	if (m_depthView.ptr() != nullptr)
	{
		m_depthView.release();
	}

	HRESULT result = engine::s_device->CreateDepthStencilView(pResource, desc, m_depthView.reset());
	ALWAYS_ASSERT(result >= 0);
}

void DepthTarget::deinit()
{
	m_depthbuffer.release();
	m_depthView.release();
}

void DepthTarget::clearDepth()
{
	engine::s_devcon->ClearDepthStencilView(m_depthView, D3D11_CLEAR_DEPTH,
		0.0f, 0);
}

void DepthTarget::clearStencil()
{
	engine::s_devcon->ClearDepthStencilView(m_depthView, D3D11_CLEAR_STENCIL,
		0.0f, 0);
}

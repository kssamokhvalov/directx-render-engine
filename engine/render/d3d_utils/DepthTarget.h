#pragma once
#include "../d3d.h"

class DepthTarget
{
public:
	void init(UINT width_render, UINT height_render);
	void init(ID3D11Resource* pResource);
	void init(ID3D11Resource* pResource, D3D11_DEPTH_STENCIL_VIEW_DESC* desc);
	void deinit();
	ID3D11DepthStencilView* const getdepthView() { return m_depthView.ptr(); }
	void clearDepth();
	void clearStencil();
private:
	engine::DxResPtr<ID3D11DepthStencilView> m_depthView;
	engine::DxResPtr<ID3D11Texture2D> m_depthbuffer;
	D3D11_TEXTURE2D_DESC m_depthBufferDesc;
};


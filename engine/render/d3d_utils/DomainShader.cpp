#include "DomainShader.h"
#include "../../utils/ParallelExecutor.h"

void DomainShader::loadDomainShader(std::wstring path)
{
	ID3DBlob* shaderBlob;

	loadShader(path, &shaderBlob, "ds_5_0");

	HRESULT result = engine::s_device->CreateDomainShader(shaderBlob->GetBufferPointer(),
		shaderBlob->GetBufferSize(), nullptr, m_domain_shader.reset());
	ALWAYS_ASSERT(result >= 0);
}

void DomainShader::bind()
{
	engine::s_devcon->DSSetShader(m_domain_shader, nullptr, 0);
}

void DomainShader::release()
{
	m_domain_shader.release();
}

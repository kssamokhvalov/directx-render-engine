#pragma once
#include "Shader.h"
class DomainShader :
    public Shader
{
public:
	void loadDomainShader(std::wstring path);
	void bind();
	void release();
private:
	engine::DxResPtr<ID3D11DomainShader> m_domain_shader;
};


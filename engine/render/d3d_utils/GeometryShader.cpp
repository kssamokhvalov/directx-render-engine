#include "GeometryShader.h"
#include "../../utils/ParallelExecutor.h"

void GeometryShader::loadGeometryShader(std::wstring path)
{
	ID3DBlob* shaderBlob;

	loadShader(path, &shaderBlob, "gs_5_0");

	HRESULT result = engine::s_device->CreateGeometryShader(shaderBlob->GetBufferPointer(),
		shaderBlob->GetBufferSize(), nullptr, m_geometry_shader.reset());
	ALWAYS_ASSERT(result >= 0);
}

void GeometryShader::bind()
{
	engine::s_devcon->GSSetShader(m_geometry_shader, nullptr, 0);
}

void GeometryShader::release()
{
	m_geometry_shader.release();
}

#pragma once
#include "Shader.h"
class GeometryShader :
    public Shader
{
public:
	void loadGeometryShader(std::wstring path);
	void bind();
	void release();
private:
	engine::DxResPtr<ID3D11GeometryShader> m_geometry_shader;
};


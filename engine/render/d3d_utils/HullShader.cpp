#include "HullShader.h"
#include "../../utils/ParallelExecutor.h"

void HullShader::loadHullShader(std::wstring path)
{
	ID3DBlob* shaderBlob;

	loadShader(path, &shaderBlob, "hs_5_0");

	HRESULT result = engine::s_device->CreateHullShader(shaderBlob->GetBufferPointer(),
		shaderBlob->GetBufferSize(), nullptr, m_hull_shader.reset());
	ALWAYS_ASSERT(result >= 0);
}

void HullShader::bind()
{
	engine::s_devcon->HSSetShader(m_hull_shader, nullptr, 0);
}

void HullShader::release()
{
	m_hull_shader.release();
}

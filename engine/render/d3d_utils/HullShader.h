#pragma once
#include "Shader.h"
class HullShader :
    public Shader
{
public:
	void loadHullShader(std::wstring path);
	void bind();
	void release();
private:
	engine::DxResPtr<ID3D11HullShader> m_hull_shader;
};


#include "IndexBuffer.h"

void IndexBuffer::init(UINT size, D3D11_USAGE usage, uint32_t* data)
{
	m_numIndexs = size;
	if (m_numIndexs > m_capacity || m_usage != usage)
	{
		m_capacity = m_numIndexs;
		D3D11_BUFFER_DESC indexBufferDesc = {};
		indexBufferDesc.ByteWidth = sizeof(uint32_t) * m_numIndexs;
		indexBufferDesc.Usage = usage;
		indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

		D3D11_SUBRESOURCE_DATA indexSubresourceData{};
		indexSubresourceData.pSysMem = data;

		HRESULT result = engine::s_device->CreateBuffer(&indexBufferDesc, &indexSubresourceData, m_index_buffer.reset());
		ALWAYS_ASSERT(result >= 0);
	}
	else
	{
		auto mapping = map();
		memcpy(mapping.pData, data, sizeof(uint32_t) * m_numIndexs);
		unmap();
	}
	m_usage = usage;
}

D3D11_MAPPED_SUBRESOURCE IndexBuffer::map()
{
	D3D11_MAPPED_SUBRESOURCE mappedSubresource;
	engine::s_devcon->Map(m_index_buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	return mappedSubresource;
}

void IndexBuffer::unmap()
{
	engine::s_devcon->Unmap(m_index_buffer, 0);
}

void IndexBuffer::bind()
{
	engine::s_devcon->IASetIndexBuffer(m_index_buffer, DXGI_FORMAT_R32_UINT, 0);
}

void IndexBuffer::deinit()
{
	m_index_buffer.release();
}

#pragma once
#include <stdint.h>
#include <direct.h>
#include "../d3d.h"
#include "../../utils/ParallelExecutor.h"

class IndexBuffer
{
public:
	IndexBuffer() : m_capacity(0), m_numIndexs(0)
	{ }
	void init(UINT size, D3D11_USAGE usage, uint32_t* data);
	D3D11_MAPPED_SUBRESOURCE map();
	void unmap();
	UINT size() { return m_numIndexs; }
	void bind();
	void deinit();
	bool empty() { return m_numIndexs == 0; }
private:
	engine::DxResPtr<ID3D11Buffer> m_index_buffer;
	UINT m_numIndexs;
	UINT m_capacity;
	D3D11_USAGE m_usage;
};



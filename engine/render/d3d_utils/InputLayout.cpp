#include "InputLayout.h"
#include "../../utils/ParallelExecutor.h"

void InputLayout::loadInputLayout(ID3DBlob* const shaderBlob, D3D11_INPUT_ELEMENT_DESC polygonLayout[], size_t size)
{
	HRESULT result = engine::s_device->CreateInputLayout(polygonLayout, size,
		shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), m_input_layout.reset());
	ALWAYS_ASSERT(result >= 0);
}

void InputLayout::bind()
{
	engine::s_devcon->IASetInputLayout(m_input_layout);
}

void InputLayout::release()
{
	m_input_layout.release();
}

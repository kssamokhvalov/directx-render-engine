#pragma once
#include <string>
#include <vector>
#include "../d3d.h"

class InputLayout
{
public:
	void loadInputLayout(ID3DBlob* const shaderBlob, D3D11_INPUT_ELEMENT_DESC polygonLayout[], size_t size);
	void bind();
	void release();
private:
	engine::DxResPtr<ID3D11InputLayout> m_input_layout;
};


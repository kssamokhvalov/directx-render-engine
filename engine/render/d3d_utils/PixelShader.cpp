#include "PixelShader.h"
#include "../../utils/ParallelExecutor.h"

void PixelShader::loadPixelShader(std::wstring path)
{
	ID3DBlob* shaderBlob;

	loadShader(path, &shaderBlob, "ps_5_0");

	HRESULT result = engine::s_device->CreatePixelShader(shaderBlob->GetBufferPointer(),
		shaderBlob->GetBufferSize(), nullptr, m_pixel_shader.reset());
	ALWAYS_ASSERT(result >= 0);
}

void PixelShader::bind()
{
	engine::s_devcon->PSSetShader(m_pixel_shader, nullptr, 0);
}

void PixelShader::release()
{
	m_pixel_shader.release();
}

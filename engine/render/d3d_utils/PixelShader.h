#pragma once
#include "Shader.h"

class PixelShader : public Shader
{
public:
	void loadPixelShader(std::wstring path);
	void bind();
	void release();
private:
	engine::DxResPtr<ID3D11PixelShader> m_pixel_shader;
};


#pragma once
#include <stdint.h>
#include <direct.h>
#include "../d3d.h"
#include "../../utils/ParallelExecutor.h"
#include "UniformBuffer.h"

template<typename T>
class RWStructuredBuffer
{
public:
	RWStructuredBuffer() : m_capacity(0), m_numIndexs(0)
	{ }
	void init(UINT size, D3D11_USAGE usage, int* data)
	{
		m_numIndexs = size;
		if (m_numIndexs > m_capacity || m_usage != usage)
		{
			m_capacity = m_numIndexs;
			D3D11_BUFFER_DESC structuredBufferDesc = {};
			structuredBufferDesc.ByteWidth = (unsigned int)(sizeof(T) * size);
			structuredBufferDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
			structuredBufferDesc.StructureByteStride = sizeof(T);
			structuredBufferDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
			structuredBufferDesc.Usage = D3D11_USAGE_DEFAULT;
			structuredBufferDesc.CPUAccessFlags = 0;

			D3D11_SUBRESOURCE_DATA structuredSubresourceData{};
			structuredSubresourceData.pSysMem = data;

			HRESULT result = engine::s_device->CreateBuffer(&structuredBufferDesc, nullptr, m_structured_buffer.reset());
			ALWAYS_ASSERT(result >= 0);
		}
		else
		{
			auto mapping = map();
			memcpy(mapping.pData, data, sizeof(T) * m_numIndexs);
			unmap();
		}
		m_usage = usage;
	}

	D3D11_MAPPED_SUBRESOURCE map()
	{
		D3D11_MAPPED_SUBRESOURCE mappedSubresource;
		engine::s_devcon->Map(m_structured_buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
		return mappedSubresource;
	}

	void unmap()
	{
		engine::s_devcon->Unmap(m_structured_buffer, 0);
	}

	UINT size() { return m_numIndexs; }


	void deinit()
	{
		m_structured_buffer.release();
	}

	bool empty() { return m_numIndexs == 0; }

	ID3D11Buffer* getBuffer() { return m_structured_buffer.ptr(); }
private:
	engine::DxResPtr<ID3D11Buffer> m_structured_buffer;
	UINT m_numIndexs;
	UINT m_capacity;
	D3D11_USAGE m_usage;
};
#include "RenderTarget.h"
#include "../../utils/ParallelExecutor.h"

void RenderTarget::deinit()
{

    m_framebufferView.release();

}

void RenderTarget::init(ID3D11Resource* pResource)
{
    if (m_framebufferView.ptr() != nullptr)
    {
        m_framebufferView.release();
    }

    D3D11_TEXTURE2D_DESC resourceDesc;
    ID3D11Texture2D* pTextureInterface = nullptr;
    pResource->QueryInterface<ID3D11Texture2D>(&pTextureInterface);
    pTextureInterface->GetDesc(&resourceDesc);
    pTextureInterface->Release();

    m_height_render = resourceDesc.Height;
    m_width_render = resourceDesc.Width;

    HRESULT result = engine::s_device->CreateRenderTargetView(pResource, 0, m_framebufferView.reset());
    ALWAYS_ASSERT(result >= 0);
}

void RenderTarget::init(ID3D11Resource* pResource, D3D11_RENDER_TARGET_VIEW_DESC* desc)
{
    if (m_framebufferView.ptr() != nullptr)
    {
        m_framebufferView.release();
    }

    D3D11_TEXTURE2D_DESC resourceDesc;
    ID3D11Texture2D* pTextureInterface = nullptr;
    pResource->QueryInterface<ID3D11Texture2D>(&pTextureInterface);
    pTextureInterface->GetDesc(&resourceDesc);
    pTextureInterface->Release();

    m_height_render = resourceDesc.Height;
    m_width_render = resourceDesc.Width;

    HRESULT result = engine::s_device->CreateRenderTargetView(pResource, desc, m_framebufferView.reset());
    ALWAYS_ASSERT(result >= 0);
}

void RenderTarget::clear()
{
    float color[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
    engine::s_devcon->ClearRenderTargetView(m_framebufferView, color);
}

#pragma once
#include  "../d3d.h"

class RenderTarget
{
public:

    void init(ID3D11Resource* pResource); 
    void init(ID3D11Resource* pResource, D3D11_RENDER_TARGET_VIEW_DESC* desc);
    void deinit();

    void clear();

    UINT getWidthRender() const { return m_width_render; }
    UINT getHeightRender() const { return m_height_render; }
    ID3D11RenderTargetView* const getFramebufferView() { return m_framebufferView.ptr(); }


private:
    UINT m_width_render;
    UINT m_height_render;

    engine::DxResPtr<ID3D11RenderTargetView> m_framebufferView;
};


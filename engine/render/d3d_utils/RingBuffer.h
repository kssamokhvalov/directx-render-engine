#pragma once
#include "./UnorderedAccessView.h"
#include "./RWStructuredBuffer.h"
#include "./ByteAddressBuffer.h"
#include "./ShaderResurce.h"

template<typename T, int size>
class RingBuffer
{
public:
	struct RangeDescription
	{
		int number;
		int offset;
		int expired;
		D3D11_DRAW_INSTANCED_INDIRECT_ARGS indirect_arg;
	};
	void init()
	{
		m_rw_structured_buffer.init( size, D3D11_USAGE_DEFAULT, nullptr);
		m_range_buffer.init( 1, D3D11_USAGE_DEFAULT, nullptr);

		D3D11_UNORDERED_ACCESS_VIEW_DESC unorderedAccessViewDesc{};
		unorderedAccessViewDesc.Format = DXGI_FORMAT_UNKNOWN;
		unorderedAccessViewDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
		unorderedAccessViewDesc.Buffer.FirstElement = 0;
		unorderedAccessViewDesc.Buffer.Flags = D3D11_BUFFER_UAV_FLAG_COUNTER;
		unorderedAccessViewDesc.Buffer.NumElements = size;

		m_rw_structured_buffer_UAV.init(m_rw_structured_buffer.getBuffer(), &unorderedAccessViewDesc);

		unorderedAccessViewDesc.Buffer.NumElements = sizeof(RangeDescription) / sizeof(UINT32);
		unorderedAccessViewDesc.Format = DXGI_FORMAT_R32_TYPELESS;
		unorderedAccessViewDesc.Buffer.Flags = D3D11_BUFFER_UAV_FLAG_RAW;
		m_range_buffer_UAV.init(m_range_buffer.getBuffer(), &unorderedAccessViewDesc);

		D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
		ZeroMemory(&shaderResourceViewDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
		shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
		shaderResourceViewDesc.Format = DXGI_FORMAT_UNKNOWN;
		shaderResourceViewDesc.Buffer.FirstElement = 0;
		shaderResourceViewDesc.Buffer.NumElements = size;
		m_rw_structured_buffer_SRV.init(m_rw_structured_buffer.getBuffer(), &shaderResourceViewDesc);

		ZeroMemory(&shaderResourceViewDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
		shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
		shaderResourceViewDesc.Format = DXGI_FORMAT_R32_TYPELESS;
		shaderResourceViewDesc.BufferEx.FirstElement = 0;
		shaderResourceViewDesc.BufferEx.NumElements = sizeof(RangeDescription) / sizeof(UINT32);
		shaderResourceViewDesc.BufferEx.Flags = D3D11_BUFFEREX_SRV_FLAG_RAW;
		m_range_buffer_SRV.init(m_range_buffer.getBuffer(), &shaderResourceViewDesc);
	}

	ID3D11Buffer* getRangeBuffer() { return m_range_buffer.getBuffer(); }

	ID3D11UnorderedAccessView* getStructuredBufferUAV() { return m_rw_structured_buffer_UAV.getUnorderedAccessView(); }
	ID3D11UnorderedAccessView* getRangeBufferUAV() { return m_range_buffer_UAV.getUnorderedAccessView(); }

	ShaderResurce& getStructuredBufferSR() { return m_rw_structured_buffer_SRV; }
	ShaderResurce& getRangeBufferSR() { return m_range_buffer_SRV; }

	void deint()
	{
		m_rw_structured_buffer.deinit();
		m_range_buffer.deinit();
		m_rw_structured_buffer_UAV.deinit();
		m_range_buffer_UAV.deinit();
		m_rw_structured_buffer_SRV.deinit();
		m_range_buffer_SRV.deinit();
	}

private:
	RWStructuredBuffer<T> m_rw_structured_buffer;
	ByteAddressBuffer<RangeDescription> m_range_buffer;
	ShaderResurce m_rw_structured_buffer_SRV;
	ShaderResurce m_range_buffer_SRV;
	UnorderedAccessView m_rw_structured_buffer_UAV;
	UnorderedAccessView m_range_buffer_UAV;
};


#include "Shader.h"
#include "../../utils/ParallelExecutor.h"

void Shader::loadShader(std::wstring path, ID3DBlob** blobShader, LPCSTR target)
{
	ID3DBlob* errors = nullptr;
#if defined(_DEBUG)
	// Enable better shader debugging with the graphics debugging tools.
	UINT compileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#else
	UINT compileFlags = 0;
#endif

	HRESULT result = D3DCompileFromFile(path.c_str(), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, "main",
		target, compileFlags, 0, blobShader, &errors);
	std::string err;
	if(errors)
		err =  (char*)errors->GetBufferPointer();
	ALWAYS_ASSERT(result >= 0);

}

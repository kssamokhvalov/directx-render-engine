#pragma once
#include <string>
#include "../d3d.h"

class Shader
{
public:
	void loadShader(std::wstring path, ID3DBlob** blobShader, LPCSTR target);
};


#include "ShaderResurce.h"
#include "../../utils/ParallelExecutor.h"

void ShaderResurce::init(ID3D11Resource* pResource)
{
    if (m_shader_resource_view.ptr() != nullptr)
    {
        m_shader_resource_view.release();
    }

    HRESULT result = engine::s_device->CreateShaderResourceView(pResource, 0, m_shader_resource_view.reset());
    ALWAYS_ASSERT(result >= 0);
}

void ShaderResurce::init(ID3D11Resource* pResource, D3D11_SHADER_RESOURCE_VIEW_DESC* desc)
{
    if (m_shader_resource_view.ptr() != nullptr)
    {
        m_shader_resource_view.release();
    }

    HRESULT result = engine::s_device->CreateShaderResourceView(pResource, desc, m_shader_resource_view.reset());
    ALWAYS_ASSERT(result >= 0);
}

void ShaderResurce::deinit()
{
    m_shader_resource_view.release();
}

void ShaderResurce::bind(UINT slot, TypeShader type_shader)
{
    unbind();
    m_type_shader_binded = type_shader;
    m_slot_binded = slot;
    ID3D11ShaderResourceView* texture_view_ptr = m_shader_resource_view.ptr();
    switch (type_shader)
    {
    case TypeShader::TypeVertexShader:
        engine::s_devcon->VSSetShaderResources(slot, 1, &texture_view_ptr);
        break;
    case TypeShader::TypePixelShader:
        engine::s_devcon->PSSetShaderResources(slot, 1, &texture_view_ptr);
        break;
    case TypeShader::TypeGeometryShader:
        engine::s_devcon->GSSetShaderResources(slot, 1, &texture_view_ptr);
        break;
    case TypeShader::TypeComputerShader:
        engine::s_devcon->CSSetShaderResources(slot, 1, &texture_view_ptr);
        break;
    default:
        break;
    }
}

void ShaderResurce::unbind()
{
    if (m_slot_binded < 0)
        return;

    ID3D11ShaderResourceView* texture_view_ptr = nullptr;
    switch (m_type_shader_binded)
    {
    case TypeShader::TypeVertexShader:
        engine::s_devcon->VSSetShaderResources(m_slot_binded, 1, &texture_view_ptr);
        break;
    case TypeShader::TypePixelShader:
        engine::s_devcon->PSSetShaderResources(m_slot_binded, 1, &texture_view_ptr);
        break;
    case TypeShader::TypeGeometryShader:
        engine::s_devcon->GSSetShaderResources(m_slot_binded, 1, &texture_view_ptr);
        break;
    case TypeShader::TypeComputerShader:
        engine::s_devcon->CSSetShaderResources(m_slot_binded, 1, &texture_view_ptr);
        break;
    default:
        break;
    }

    m_slot_binded = -1;
}

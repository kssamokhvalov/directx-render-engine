#pragma once
#include  "../d3d.h"
#include "UniformBuffer.h"

class ShaderResurce
{
public:
    void init(ID3D11Resource* pResource);
    void init(ID3D11Resource* pResource, D3D11_SHADER_RESOURCE_VIEW_DESC* desc);
    void deinit();
    void bind(UINT slot, TypeShader type_shader);
    void unbind();

    ID3D11ShaderResourceView* const getShaderResourceView() { return m_shader_resource_view.ptr(); }
private:
    TypeShader m_type_shader_binded;
    int m_slot_binded = -1;
    engine::DxResPtr<ID3D11ShaderResourceView> m_shader_resource_view;
};


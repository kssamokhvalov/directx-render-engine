#include "Texture.h"

Texture::~Texture()
{
	if (m_texture.ptr() != nullptr || m_texture_view.ptr() != nullptr)
		deinit();
}

void Texture::initForDepthTarget(UINT width_render, UINT height_render)
{
	D3D11_TEXTURE2D_DESC desc;
	memset(&desc, 0, sizeof(D3D11_TEXTURE2D_DESC));

	desc.Width = width_render;
	desc.Height = height_render;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	desc.Format = DXGI_FORMAT_R24G8_TYPELESS;
	desc.SampleDesc.Count = 1;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = 0;
	desc.MiscFlags = 0;

	ID3D11Texture2D* tex;
	HRESULT result = engine::s_device->CreateTexture2D(&desc, NULL, &tex);
	ALWAYS_ASSERT(result >= 0);
	m_texture.reset(tex);

	D3D11_SHADER_RESOURCE_VIEW_DESC descSRV;

	descSRV.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
	descSRV.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	descSRV.Texture2D.MostDetailedMip = 0;
	descSRV.Texture2D.MipLevels = 1;

	result = engine::s_device->CreateShaderResourceView(m_texture, &descSRV, m_texture_view.reset());
	ALWAYS_ASSERT(result >= 0);
}

void Texture::init(D3D11_TEXTURE2D_DESC* desc, D3D11_SHADER_RESOURCE_VIEW_DESC* descSRV)
{
	ID3D11Texture2D* tex;
	HRESULT result = engine::s_device->CreateTexture2D(desc, NULL, &tex);
	ALWAYS_ASSERT(result >= 0);

	m_texture.reset(tex);

	result = engine::s_device->CreateShaderResourceView(m_texture, descSRV, m_texture_view.reset());
	ALWAYS_ASSERT(result >= 0);
}

void Texture::init(D3D11_TEXTURE2D_DESC* desc)
{
	ID3D11Texture2D* tex;
	HRESULT result = engine::s_device->CreateTexture2D(desc, NULL, &tex);
	ALWAYS_ASSERT(result >= 0);

	m_texture.reset(tex);

	result = engine::s_device->CreateShaderResourceView(m_texture, 0, m_texture_view.reset());
	ALWAYS_ASSERT(result >= 0);
}

void Texture::init(ID3D11Resource* texture)
{
	m_texture.reset(texture);
	HRESULT result = engine::s_device->CreateShaderResourceView(m_texture, 0, m_texture_view.reset());
	ALWAYS_ASSERT(result >= 0);
}

void Texture::init(engine::DxResPtr<ID3D11Resource>* texture)
{
	m_texture.reset(texture->ptr());
	HRESULT result = engine::s_device->CreateShaderResourceView(m_texture, 0, m_texture_view.reset());
	ALWAYS_ASSERT(result >= 0);
}

void Texture::init(engine::DxResPtr<ID3D11ShaderResourceView>* texture_view, engine::DxResPtr<ID3D11Resource>* texture)
{
	m_texture_view = *texture_view;
	m_texture = *texture;
}

void Texture::deinit()
{
	m_texture.release();
	m_texture_view.release();
}

void Texture::bind(UINT slot, TypeShader type_shader)
{
	unbind();
	m_type_shader_binded = type_shader;
	m_slot_binded = slot;
	ID3D11ShaderResourceView* texture_view_ptr = m_texture_view.ptr();
	switch (type_shader)
	{
	case TypeShader::TypeVertexShader:
		engine::s_devcon->VSSetShaderResources(slot, 1, &texture_view_ptr);
		break;
	case TypeShader::TypePixelShader:
		engine::s_devcon->PSSetShaderResources(slot, 1, &texture_view_ptr);
		break;
	case TypeShader::TypeGeometryShader:
		engine::s_devcon->GSSetShaderResources(slot, 1, &texture_view_ptr);
		break;
	case TypeShader::TypeComputerShader:
		engine::s_devcon->CSSetShaderResources(slot, 1, &texture_view_ptr);
		break;
	default:
		break;
	}
}

void Texture::unbind()
{
	if (m_slot_binded < 0)
		return;

	ID3D11ShaderResourceView* texture_view_ptr = nullptr;
	switch (m_type_shader_binded)
	{
	case TypeShader::TypeVertexShader:
		engine::s_devcon->VSSetShaderResources(m_slot_binded, 1, &texture_view_ptr);
		break;
	case TypeShader::TypePixelShader:
		engine::s_devcon->PSSetShaderResources(m_slot_binded, 1, &texture_view_ptr);
		break;
	case TypeShader::TypeGeometryShader:
		engine::s_devcon->GSSetShaderResources(m_slot_binded, 1, &texture_view_ptr);
		break;
	case TypeShader::TypeComputerShader:
		engine::s_devcon->CSSetShaderResources(m_slot_binded, 1, &texture_view_ptr);
		break;
	default:
		break;
	}

	m_slot_binded = -1;
}


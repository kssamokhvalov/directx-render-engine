#pragma once
#include  "../d3d.h"
#include "UniformBuffer.h"

//TODO better create class "ResourceRender" and create class "Texture" of cobbination classes "ResourceRender" and "ShaderResource" (S in SOLID)
class Texture
{
public:
    ~Texture();
    void initForDepthTarget(UINT width_render, UINT height_render);
    void init(D3D11_TEXTURE2D_DESC* desc, D3D11_SHADER_RESOURCE_VIEW_DESC* descSRV);
    void init(D3D11_TEXTURE2D_DESC* desc);
    void init(ID3D11Resource* texture);
    void init(engine::DxResPtr<ID3D11Resource>* texture);
    void init(engine::DxResPtr<ID3D11ShaderResourceView>* texture_view, engine::DxResPtr<ID3D11Resource>* texture);
    void deinit();
    void bind(UINT slot, TypeShader type_shader);
    void unbind();

    engine::DxResPtr<ID3D11ShaderResourceView>* getTextureView() { return &m_texture_view; }
    engine::DxResPtr<ID3D11Resource>* getTexture() { return &m_texture; }
private:
    int m_slot_binded = -1;
    TypeShader m_type_shader_binded;
    engine::DxResPtr<ID3D11ShaderResourceView> m_texture_view;
    engine::DxResPtr<ID3D11Resource> m_texture;
};


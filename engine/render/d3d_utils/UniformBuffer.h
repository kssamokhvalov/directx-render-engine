#pragma once
#include <direct.h>
#include "../d3d.h"
#include "../../utils/ParallelExecutor.h"

enum TypeShader { TypeVertexShader, TypePixelShader, TypeGeometryShader, TypeComputerShader};

template <typename T>
class UniformBuffer
{
public:
	

	void init(D3D11_USAGE usage)
	{

		D3D11_BUFFER_DESC uniformBufferDesc = {};
		uniformBufferDesc.ByteWidth = sizeof(T);
		uniformBufferDesc.Usage = usage;
		uniformBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		uniformBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		HRESULT result = engine::s_device->CreateBuffer(&uniformBufferDesc, NULL, m_uniform_buffer.reset());
		ALWAYS_ASSERT(result >= 0);
	}

	D3D11_MAPPED_SUBRESOURCE map()
	{
		D3D11_MAPPED_SUBRESOURCE mappedSubresource;
		engine::s_devcon->Map(m_uniform_buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
		return mappedSubresource;
	}

	void unmap()
	{
		engine::s_devcon->Unmap(m_uniform_buffer, 0);
	}

	void update(const T& data) 
	{
		D3D11_MAPPED_SUBRESOURCE mappedSubresource;
		engine::s_devcon->Map(m_uniform_buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
		memcpy(mappedSubresource.pData, &data, sizeof(T));
		engine::s_devcon->Unmap(m_uniform_buffer, 0);
	}

	void bind(UINT slot, TypeShader type_shader)
	{
		ID3D11Buffer* uniform_buffer = m_uniform_buffer.ptr();
		switch (type_shader) 
		{
		case TypeShader::TypeVertexShader:
			engine::s_devcon->VSSetConstantBuffers(slot, 1, &uniform_buffer);
			break;
		case TypeShader::TypePixelShader:
			engine::s_devcon->PSSetConstantBuffers(slot, 1, &uniform_buffer);
			break;
		case TypeShader::TypeGeometryShader:
			engine::s_devcon->GSSetConstantBuffers(slot, 1, &uniform_buffer);
			break;
		case TypeShader::TypeComputerShader:
			engine::s_devcon->CSSetConstantBuffers(slot, 1, &uniform_buffer);
			break;
		default:
			break;
		}
		
	}

	void deinit()
	{
		m_uniform_buffer.release();
	}
private:
	engine::DxResPtr<ID3D11Buffer> m_uniform_buffer;

};

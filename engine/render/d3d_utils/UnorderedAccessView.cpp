#include "UnorderedAccessView.h"

void UnorderedAccessView::init(ID3D11Resource* pResource, D3D11_UNORDERED_ACCESS_VIEW_DESC* unorderedAccessViewDesc)
{
	HRESULT result = engine::s_device->CreateUnorderedAccessView(pResource, unorderedAccessViewDesc, m_unordered_access_view.reset());
	ALWAYS_ASSERT(result >= 0);
}

void UnorderedAccessView::deinit()
{
	m_unordered_access_view.release();
}

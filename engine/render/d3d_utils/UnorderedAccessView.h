
#include  "../d3d.h"
#include "UniformBuffer.h"

class UnorderedAccessView
{
public:
    void init(ID3D11Resource* pResource, D3D11_UNORDERED_ACCESS_VIEW_DESC* unorderedAccessViewDesc);
    void deinit();

    ID3D11UnorderedAccessView* const getUnorderedAccessView() { return m_unordered_access_view.ptr(); }
private:
    int m_slot_binded = -1;
    engine::DxResPtr<ID3D11UnorderedAccessView> m_unordered_access_view;
};
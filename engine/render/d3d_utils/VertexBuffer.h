#pragma once
#include <direct.h>
#include "../d3d.h"
#include "../../utils/ParallelExecutor.h"

template <typename T>
class VertexBuffer
{
public:
	VertexBuffer() { m_capacity = 0; }
	void init(UINT size, D3D11_USAGE usage, T* data)
	{
		m_stride = sizeof(T);
		m_numVertexs = size;
		m_offset = 0;

		if (m_numVertexs > m_capacity || m_usage != usage)
		{
			m_capacity = m_numVertexs;
			D3D11_BUFFER_DESC vertexBufferDesc = {};
			vertexBufferDesc.ByteWidth = sizeof(T) * m_numVertexs;
			vertexBufferDesc.Usage = usage;
			vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

			D3D11_SUBRESOURCE_DATA vertexSubresourceData{};
			vertexSubresourceData.pSysMem = data;

			HRESULT result = engine::s_device->CreateBuffer(&vertexBufferDesc, &vertexSubresourceData, m_vertex_buffer.reset());
			ALWAYS_ASSERT(result >= 0);
		}
		else
		{
			auto mapping = map();
			memcpy(mapping.pData, data, sizeof(T) * m_numVertexs);
			unmap();
		}
		m_usage = usage;
	}

	void init(UINT size, D3D11_USAGE usage)
	{
		m_stride = sizeof(T);
		m_numVertexs = size;
		m_offset = 0;

		if (m_numVertexs > m_capacity || m_usage != usage)
		{
			m_capacity = m_numVertexs;
			D3D11_BUFFER_DESC vertexBufferDesc = {};
			vertexBufferDesc.ByteWidth = sizeof(T) * m_numVertexs;
			vertexBufferDesc.Usage = usage;
			vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

			HRESULT result = engine::s_device->CreateBuffer(&vertexBufferDesc, NULL, m_vertex_buffer.reset());
			ALWAYS_ASSERT(result >= 0);
		}
		m_usage = usage;
	}

	D3D11_MAPPED_SUBRESOURCE map()
	{
		D3D11_MAPPED_SUBRESOURCE mappedSubresource;
		engine::s_devcon->Map(m_vertex_buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
		return mappedSubresource;
	}

	void unmap()
	{
		engine::s_devcon->Unmap(m_vertex_buffer, 0);
	}

	UINT size() { return m_numVertexs; }

	void bind()
	{
		ID3D11Buffer* vertex_buffer = m_vertex_buffer.ptr();
		engine::s_devcon->IASetVertexBuffers(0, 1, &vertex_buffer, &m_stride, &m_offset);
	}

	void bind(UINT slot)
	{
		ID3D11Buffer* vertex_buffer = m_vertex_buffer.ptr();
		engine::s_devcon->IASetVertexBuffers(slot, 1, &vertex_buffer, &m_stride, &m_offset);
	}

	void deinit()
	{
		m_vertex_buffer.release();
	}
private:
	engine::DxResPtr<ID3D11Buffer> m_vertex_buffer;
	UINT m_stride;
	UINT m_numVertexs;
	UINT m_offset;
	UINT m_capacity;
	D3D11_USAGE m_usage;
};


#include "VertexShader.h"
#include "../../utils/ParallelExecutor.h"

void VertexShader::loadVertexShader(std::wstring path)
{
	loadShader(path, m_shaderBlob.reset(), "vs_5_0");

	HRESULT result = engine::s_device->CreateVertexShader(m_shaderBlob->GetBufferPointer(),
		m_shaderBlob->GetBufferSize(), nullptr, m_vertex_shader.reset());
	ALWAYS_ASSERT(result >= 0);

}

void VertexShader::bind()
{
	engine::s_devcon->VSSetShader(m_vertex_shader, nullptr, 0);
}

void VertexShader::release()
{
	m_shaderBlob.release();
	m_vertex_shader.release();
}

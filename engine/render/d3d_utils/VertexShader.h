#pragma once
#include "Shader.h"

class VertexShader : public Shader
{
public:
	void loadVertexShader(std::wstring path);
	void bind();
	void release();
	ID3DBlob* const getShaderBlob() { return m_shaderBlob.ptr(); }
private:
	engine::DxResPtr<ID3D11VertexShader> m_vertex_shader;
	engine::DxResPtr<ID3D11InputLayout> m_input_layout;
	engine::DxResPtr<ID3DBlob> m_shaderBlob;
};


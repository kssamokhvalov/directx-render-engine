#pragma once
#include <cmath>

typedef struct Color {
    unsigned char b;
    unsigned char g;
    unsigned char r;
    unsigned char _pad;
} Pixel, Color;

struct Colorf {
    float b;
    float g;
    float r;


    Color getColor() 
    {
        return { static_cast<unsigned char>(fmin(b * 255, 255)),
            static_cast<unsigned char>(fmin(g * 255, 255)),
            static_cast<unsigned char>(fmin(r * 255, 255)) };
    }

    Colorf operator*(const Colorf& second_color)  const
    {
        Colorf ans;
        ans.b = b * second_color.b;
        ans.g = g * second_color.g;
        ans.r = r * second_color.r;
        return ans;
    }

    Colorf operator+(const Colorf& second_color)  const
    {
        Colorf ans;
        ans.b = b + second_color.b;
        ans.g = g + second_color.g;
        ans.r = r + second_color.r;
        return ans;
    }

    Colorf operator*(float value) const
    {
        Colorf ans;
        ans.b = b * value;
        ans.g = g * value;
        ans.r = r * value;
        return ans;
    }
    Colorf operator+=(const Colorf& second_color)
    {
        b += second_color.b;
        g += second_color.g;
        r += second_color.r;
        return *this;
    }
    Colorf operator*=(const Colorf& second_color)
    {
        b *= second_color.b;
        g *= second_color.g;
        r *= second_color.r;
        return *this;
    }
};

struct Angles {
    float roll;
    float pitch;
    float yaw;
};

struct float4 {
    float x;
    float y;
    float z;
    float w;
};


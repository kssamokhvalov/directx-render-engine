#include "DissolutionGroup.h"
#include "../systems_and_managers/ModelManager.h"
#include "../systems_and_managers/TextureManager.h"
#include "../../source/Entity.h"
#include <algorithm>
#include <cstddef>

constexpr static int HAS_ALBEDO = 1;
constexpr static int HAS_NORMAL = 1 << 1;
constexpr static int HAS_ROUGHNESS = 1 << 2;
constexpr static int HAS_METALNESS = 1 << 3;

bool DissolutionGroup::findIntersection(const math::Ray& ray, internal::ObjRef& objref, math::Intersection& outNearest)
{
	return utility::findIntersection(*this, ray, objref, outNearest, internal::IntersectedType::Dissolution);
}

void DissolutionGroup::remove(ModelInstanceID& entity)
{
	for (int i = 0; i < entity.meshInstanceIDs.size(); i++)
	{
		MeshInstanceID::IdComponent& idComponent = entity.meshInstanceIDs[i].id_component_main;
		m_perModel[idComponent.id_model].perMesh[i].perMaterial[idComponent.id_material].instances.erase(idComponent.id_instance);
	}
	updateInstanceBuffers();
}

void DissolutionGroup::render(bool deferred_render)
{
	if (m_instanceBuffer.size() == 0)
		return;

	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


	m_vertex_shader.bind();
	if (deferred_render)
		m_pixel_shader_deferred_render.bind();
	else
		m_pixel_shader.bind();
	m_input_layout.bind();

	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	m_instanceBuffer.bind(1);
	m_meshData.bind(2, TypeShader::TypeVertexShader);
	m_materialData.bind(6, TypePixelShader);

	m_noise->bind(13, TypePixelShader);

	uint32_t renderedInstances = 0;
	for (const auto& perModel : m_perModel)
	{

		perModel.model->bindBuffers();

		for (uint32_t meshIndex = 0; meshIndex < perModel.perMesh.size(); ++meshIndex)
		{
			const engine::Mesh& mesh = perModel.model->meshes[meshIndex];
			const auto& meshRange = perModel.model->m_ranges[meshIndex];

			// You have to upload a Mesh-to-Model transformation matrix retrieved from model file via Assimp
			m_meshData.update(mesh.instances[0]);// ... update shader local per-mesh uniform buffer

			for (uint32_t materialIndex = 0; materialIndex < perModel.perMesh[meshIndex].perMaterial.size(); materialIndex++)
			{
				const PerMaterial& perMaterial = perModel.perMesh[meshIndex].perMaterial[materialIndex];
				if (perMaterial.instances.size() == 0) continue;

				if (perMaterial.material.data.state & HAS_ALBEDO)
					perMaterial.material.albedo->bind(1, TypeShader::TypePixelShader);

				if (perMaterial.material.data.state & HAS_NORMAL)
					perMaterial.material.normal->bind(2, TypeShader::TypePixelShader);

				if (perMaterial.material.data.state & HAS_ROUGHNESS)
					perMaterial.material.roughness->bind(3, TypeShader::TypePixelShader);

				if (perMaterial.material.data.state & HAS_METALNESS)
					perMaterial.material.metalness->bind(4, TypeShader::TypePixelShader);

				m_materialData.update(perMaterial.material.data);

				uint32_t numInstances = 0;
				for (size_t i = 0; i < perMaterial.instances.size(); ++i)
				{
					if (perMaterial.instances.occupied(i))
						numInstances++;
				}
				if (perModel.model->indexBuffer().empty() == false)
				{
					engine::s_devcon->DrawIndexedInstanced(meshRange.indexNum, numInstances, meshRange.indexOffset, meshRange.vertexOffset, renderedInstances);
				}
				else {
					engine::s_devcon->DrawInstanced(meshRange.vertexNum, numInstances, meshRange.vertexOffset, renderedInstances);
				}
				renderedInstances += numInstances;
			}

		}
	}


}

void DissolutionGroup::renderDepth2D()
{
	if (m_instanceBuffer.size() == 0)
		return;

	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


	m_input_layout_2D_shadow.bind();
	m_vertex_shader_2D_shadow.bind();
	m_pixel_shader_shadow.bind();
	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	m_instanceBuffer.bind(1);
	m_meshData.bind(2, TypeShader::TypeVertexShader);
	m_noise->bind(13, TypePixelShader);

	uint32_t renderedInstances = 0;
	for (const auto& perModel : m_perModel)
	{

		perModel.model->bindBuffers();

		for (uint32_t meshIndex = 0; meshIndex < perModel.perMesh.size(); ++meshIndex)
		{
			const engine::Mesh& mesh = perModel.model->meshes[meshIndex];
			const auto& meshRange = perModel.model->m_ranges[meshIndex];

			// You have to upload a Mesh-to-Model transformation matrix retrieved from model file via Assimp
			m_meshData.update(mesh.instances[0]);// ... update shader local per-mesh uniform buffer

			for (uint32_t materialIndex = 0; materialIndex < perModel.perMesh[meshIndex].perMaterial.size(); materialIndex++)
			{
				const PerMaterial& perMaterial = perModel.perMesh[meshIndex].perMaterial[materialIndex];
				if (perMaterial.instances.size() == 0) continue;

				uint32_t numInstances = 0;
				for (size_t i = 0; i < perMaterial.instances.size(); ++i)
				{
					if (perMaterial.instances.occupied(i))
						numInstances++;
				}
				if (perModel.model->indexBuffer().empty() == false)
				{
					engine::s_devcon->DrawIndexedInstanced(meshRange.indexNum, numInstances, meshRange.indexOffset, meshRange.vertexOffset, renderedInstances);
				}
				else {
					engine::s_devcon->DrawInstanced(meshRange.vertexNum, numInstances, meshRange.vertexOffset, renderedInstances);
				}
				renderedInstances += numInstances;
			}

		}
	}
}

void DissolutionGroup::renderDepthCubemaps(const Vector3& light_pos)
{
	if (m_instanceBuffer.size() == 0)
		return;

	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


	m_vertex_shader_cube_map_shadow.bind();
	m_input_layout_cube_map_shadow.bind();
	m_geometry_shader_cube_map_shadow.bind();
	m_pixel_shader_shadow.bind();

	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);


	m_instanceBuffer.bind(1);
	m_meshData.bind(2, TypeShader::TypeVertexShader);
	m_shadowBuffer.bind(2, TypeShader::TypeGeometryShader);
	m_noise->bind(13, TypePixelShader);
	m_shadowBuffer.update({ light_pos });

	uint32_t renderedInstances = 0;
	for (const auto& perModel : m_perModel)
	{

		perModel.model->bindBuffers();

		for (uint32_t meshIndex = 0; meshIndex < perModel.perMesh.size(); ++meshIndex)
		{
			const engine::Mesh& mesh = perModel.model->meshes[meshIndex];
			const auto& meshRange = perModel.model->m_ranges[meshIndex];

			// You have to upload a Mesh-to-Model transformation matrix retrieved from model file via Assimp
			m_meshData.update(mesh.instances[0]);// ... update shader local per-mesh uniform buffer

			for (uint32_t materialIndex = 0; materialIndex < perModel.perMesh[meshIndex].perMaterial.size(); materialIndex++)
			{
				const PerMaterial& perMaterial = perModel.perMesh[meshIndex].perMaterial[materialIndex];
				if (perMaterial.instances.size() == 0) continue;

				uint32_t numInstances = uint32_t(perMaterial.instances.sizeOccupied());
				if (perModel.model->indexBuffer().empty() == false)
				{
					engine::s_devcon->DrawIndexedInstanced(meshRange.indexNum, numInstances, meshRange.indexOffset, meshRange.vertexOffset, renderedInstances);
				}
				else {
					engine::s_devcon->DrawInstanced(meshRange.vertexNum, numInstances, meshRange.vertexOffset, renderedInstances);
				}
				renderedInstances += numInstances;
			}

		}
	}
}

void DissolutionGroup::init()
{
	m_vertex_shader.loadVertexShader(L"../engine/shaders/DissolutionShaders/VertexDissolutionShader.hlsl");
	{
		D3D11_INPUT_ELEMENT_DESC polygonLayout[] =
		{
			{"POSITION",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(engine::Mesh::Vertex, pos), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"NORMAL",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(engine::Mesh::Vertex, normal), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"TEXCOORD",  0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(engine::Mesh::Vertex, tc), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"TANGENT",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(engine::Mesh::Vertex, tangent), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"BITANGENT",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(engine::Mesh::Vertex, bitangent), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"MW",  1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 32, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  4, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 48, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"TIME",  0, DXGI_FORMAT_R32G32_FLOAT, 1, 64, D3D11_INPUT_PER_INSTANCE_DATA, 1}
		};

		m_input_layout.loadInputLayout(m_vertex_shader.getShaderBlob(), polygonLayout, std::size(polygonLayout));
	}

	m_vertex_shader_2D_shadow.loadVertexShader(L"../engine/shaders/DissolutionShaders/DepthOnlyShaders/VertexShaderDepthOnly.hlsl");
	{
		D3D11_INPUT_ELEMENT_DESC polygonLayout[] =
		{
			{"POSITION",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(engine::Mesh::Vertex, pos), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"NORMAL",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(engine::Mesh::Vertex, normal), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"TEXCOORD",  0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(engine::Mesh::Vertex, tc), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"MW",  1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 32, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  4, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 48, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"TIME",  0, DXGI_FORMAT_R32G32_FLOAT, 1, 64, D3D11_INPUT_PER_INSTANCE_DATA, 1}
		};

		m_input_layout_2D_shadow.loadInputLayout(m_vertex_shader_2D_shadow.getShaderBlob(), polygonLayout, std::size(polygonLayout));
	}

	m_vertex_shader_cube_map_shadow.loadVertexShader(L"../engine/shaders/DissolutionShaders/DepthOnlyShaders/VertexShaderDepthOnlyCubeMap.hlsl");
	m_geometry_shader_cube_map_shadow.loadGeometryShader(L"../engine/shaders/DissolutionShaders/DepthOnlyShaders/GeometryShaderDepthOnly.hlsl");
	{
		D3D11_INPUT_ELEMENT_DESC polygonLayout[] =
		{
			{"POSITION",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(engine::Mesh::Vertex, pos), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"NORMAL",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(engine::Mesh::Vertex, normal), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"TEXCOORD",  0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(engine::Mesh::Vertex, tc), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"MW",  1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 32, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  4, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 48, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"TIME",  0, DXGI_FORMAT_R32G32_FLOAT, 1, 64, D3D11_INPUT_PER_INSTANCE_DATA, 1}
		};

		m_input_layout_cube_map_shadow.loadInputLayout(m_vertex_shader_cube_map_shadow.getShaderBlob(), polygonLayout, std::size(polygonLayout));
	}
	m_pixel_shader_deferred_render.loadPixelShader(L"../engine/shaders/DissolutionShaders/DeferredRender/PixelDissolutionShader.hlsl");
	m_pixel_shader.loadPixelShader(L"../engine/shaders/DissolutionShaders/PixelDissolutionShader.hlsl");
	m_pixel_shader_shadow.loadPixelShader(L"../engine/shaders/DissolutionShaders/DepthOnlyShaders/PixelDepthOnlyShader.hlsl");
	m_noise = TextureManager::instance().getTexture("assets/textures/noise/Noise_21.dds");
	m_meshData.init(D3D11_USAGE_DYNAMIC);
	m_materialData.init(D3D11_USAGE_DYNAMIC);
	m_shadowBuffer.init(D3D11_USAGE_DYNAMIC);
}

void DissolutionGroup::deinit()
{
	m_instanceBuffer.deinit();
	m_meshData.deinit();
	m_shadowBuffer.deinit();
	m_materialData.deinit();
	m_vertex_shader.release();
	m_pixel_shader.release();
	m_input_layout.release();
	m_pixel_shader_shadow.release();
	m_pixel_shader_deferred_render.release();

	m_input_layout_2D_shadow.release();
	m_vertex_shader_2D_shadow.release();
	m_input_layout_cube_map_shadow.release();
	m_geometry_shader_cube_map_shadow.release();
	m_vertex_shader_cube_map_shadow.release();
	m_noise.reset();
}

void DissolutionGroup::updateInstanceBuffers()
{
	uint32_t totalInstances = 0;
	for (auto& perModel : m_perModel)
		for (auto& perMesh : perModel.perMesh)
			for (const auto& perMaterial : perMesh.perMaterial)
				totalInstances += uint32_t(perMaterial.instances.sizeOccupied());

	if (totalInstances == 0)
		return;

	m_instanceBuffer.init(totalInstances, D3D11_USAGE_DYNAMIC);

	auto mapping = m_instanceBuffer.map();
	InstanceGPU* dst = static_cast<InstanceGPU*>(mapping.pData);

	uint32_t copiedNum = 0;
	for (const auto& perModel : m_perModel)
	{
		for (uint32_t meshIndex = 0; meshIndex < perModel.perMesh.size(); ++meshIndex)
		{
			const engine::Mesh& mesh = perModel.model->meshes[meshIndex];

			for (const auto& perMaterial : perModel.perMesh[meshIndex].perMaterial)
			{
				auto& instances = perMaterial.instances;

				uint32_t numModelInstances = instances.sizeMax();
				for (uint32_t index = 0; index < numModelInstances; ++index)
				{

#ifdef LIGHT_CALCULATION_WORLD_SPACE
					dst[copiedNum++] = { TransformSystem::instance().getMatrix(instances[index].id_modelToWorld), instances[index].start_time, instances[index].duration };
#else
					dst[copiedNum++] = { TransformSystem::instance().getCameraCenteredMatrix(instances[index].id_modelToWorld), instances[index].start_time, instances[index].duration };
#endif // LIGHT_CALCULATION_WORLD_SPACE

				}
			}
		}
	}

	m_instanceBuffer.unmap();

}

void DissolutionGroup::add(const std::shared_ptr<Model>& model, const Material& material, const InstanceCPU& instance, ModelInstanceID& entity)
{
	auto finded_model = std::find_if(m_perModel.begin(), m_perModel.end(), [&](const PerModel& perModel)
		{ return perModel.model == model; });

	if (finded_model == m_perModel.end())
	{

		PerModel pushedPerModel;
		pushedPerModel.model = model;
		pushedPerModel.perMesh.resize(model->meshes.size());
		for (int i = 0; i < model->meshes.size(); i++)
		{
			MeshInstanceID meshInstanceID{};
			pushedPerModel.perMesh[i].perMaterial.resize(1);
			pushedPerModel.perMesh[i].perMaterial[0].material = material;
			meshInstanceID.id_component_main.id_instance = pushedPerModel.perMesh[i].perMaterial[0].instances.insert(instance);
			meshInstanceID.id_component_main.id_material = 0;
			meshInstanceID.id_component_main.id_shading = internal::IntersectedType::Dissolution;
			meshInstanceID.id_component_main.id_model = m_perModel.size();
			entity.meshInstanceIDs.push_back(meshInstanceID);

		}
		m_perModel.push_back(pushedPerModel);
	}
	else
	{
		for (int i = 0; i < model->meshes.size(); i++)
		{
			auto finded_material = std::find_if(finded_model->perMesh[i].perMaterial.begin(), finded_model->perMesh[i].perMaterial.end(), [&](const PerMaterial& perMaterial)
				{ return perMaterial.material.albedo == material.albedo; });

			size_t id_material = 0;
			if (finded_material == finded_model->perMesh[i].perMaterial.end())
			{
				finded_model->perMesh[i].perMaterial.push_back(PerMaterial{});

				id_material = finded_model->perMesh[i].perMaterial.size() - 1;
			}
			else
			{
				id_material = std::distance(finded_model->perMesh[i].perMaterial.begin(), finded_material);
			}

			MeshInstanceID meshInstanceID{};
			meshInstanceID.id_component_main.id_instance = finded_model->perMesh[i].perMaterial[id_material].instances.insert(instance);
			finded_model->perMesh[i].perMaterial[id_material].material = material;
			meshInstanceID.id_component_main.id_shading = internal::IntersectedType::Dissolution;
			meshInstanceID.id_component_main.id_model = std::distance(m_perModel.begin(), finded_model);
			meshInstanceID.id_component_main.id_material = id_material;
			entity.meshInstanceIDs.push_back(meshInstanceID);
		}
	}
	updateInstanceBuffers();
}

void DissolutionGroup::add(const std::shared_ptr<Model>& model, std::vector<Material> materials, const InstanceCPU& instance, ModelInstanceID& entity)
{
	DEV_ASSERT(model->meshes.size() == materials.size());

	auto finded_model = std::find_if(m_perModel.begin(), m_perModel.end(), [&](const PerModel& perModel)
		{ return perModel.model == model; });

	if (finded_model == m_perModel.end())
	{

		PerModel pushedPerModel;
		pushedPerModel.model = model;
		pushedPerModel.perMesh.resize(model->meshes.size());
		for (int i = 0; i < model->meshes.size(); i++)
		{
			MeshInstanceID meshInstanceID{};
			pushedPerModel.perMesh[i].perMaterial.resize(1);
			pushedPerModel.perMesh[i].perMaterial[0].material = materials[i];
			meshInstanceID.id_component_main.id_instance = pushedPerModel.perMesh[i].perMaterial[0].instances.insert(instance);
			meshInstanceID.id_component_main.id_material = 0;
			meshInstanceID.id_component_main.id_shading = internal::IntersectedType::Dissolution;
			meshInstanceID.id_component_main.id_model = m_perModel.size();
			entity.meshInstanceIDs.push_back(meshInstanceID);

		}
		m_perModel.push_back(pushedPerModel);
	}
	else
	{

		for (int i = 0; i < model->meshes.size(); i++)
		{
			auto finded_material = std::find_if(finded_model->perMesh[i].perMaterial.begin(), finded_model->perMesh[i].perMaterial.end(), [&](const PerMaterial& perMaterial)
				{ return perMaterial.material.albedo == materials[i].albedo; });

			size_t id_material = 0;
			if (finded_material == finded_model->perMesh[i].perMaterial.end())
			{
				finded_model->perMesh[i].perMaterial.push_back(PerMaterial{});

				id_material = finded_model->perMesh[i].perMaterial.size() - 1;
			}
			else
			{
				id_material = std::distance(finded_model->perMesh[i].perMaterial.begin(), finded_material);
			}

			MeshInstanceID meshInstanceID{};
			meshInstanceID.id_component_main.id_instance = finded_model->perMesh[i].perMaterial[id_material].instances.insert(instance);
			finded_model->perMesh[i].perMaterial[id_material].material = materials[i];
			meshInstanceID.id_component_main.id_shading = internal::IntersectedType::Dissolution;
			meshInstanceID.id_component_main.id_model = std::distance(m_perModel.begin(), finded_model);
			meshInstanceID.id_component_main.id_material = id_material;
			entity.meshInstanceIDs.push_back(meshInstanceID);
		}
	}
	updateInstanceBuffers();
}
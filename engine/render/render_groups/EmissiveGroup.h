#pragma once
#include "../../source/math/Matrix4x4.h"
#include "../primitives.h"
#include "../../source/Model.h"
#include <vector>
#include <memory>
#include "../d3d_utils/PixelShader.h"
#include "../d3d_utils/VertexShader.h"
#include "../d3d_utils/HullShader.h"
#include "../d3d_utils/DomainShader.h"
#include "../d3d_utils/GeometryShader.h"
#include "../d3d_utils/InputLayout.h"
#include "../d3d_utils/VertexBuffer.h"
#include "../d3d_utils/UniformBuffer.h"
#include "../../source/Entity.h"
#include "../../utils/IntersectionFinder.h"


struct MeshInstanceID;

class EmissiveGroup
{
public:
    struct InstanceIDs {
        engine::ID id_modelToWorld;
        Vector4 emission;
        size_t id_entity;
    };

    struct Instance {
        Matrix4x4 modelToWorld;
        Vector4 emission;
        size_t id_entity;
    };
    struct PerMaterial {
        SolidVector<InstanceIDs> instances;
    };
    struct PerMesh {
        std::vector<PerMaterial> perMaterial;
    };
    struct PerModel {
        std::shared_ptr<Model> model;
        std::vector<PerMesh> perMesh;
    };

    friend bool utility::findIntersection(EmissiveGroup& group, const math::Ray& ray, internal::ObjRef& objref, math::Intersection& outNearest, internal::IntersectedType intersected_type);
    bool findIntersection(const math::Ray& ray, internal::ObjRef& objref, math::Intersection& outNearest);

    void remove(ModelInstanceID& entity);

    void render(bool deferred_render);
    void init();
    void deinit();
    void updateInstanceBuffers();
    void addEmissiveInstance(const std::shared_ptr<Model>& model, const InstanceIDs& instance, ModelInstanceID& entity);
    std::vector<PerModel>& getPerModel() { return m_perModel; }
private:
    std::vector<PerModel> m_perModel;
    VertexBuffer<Instance> m_instanceBuffer;
    UniformBuffer<Matrix4x4> m_meshData;
    VertexShader m_vertex_shader;
    PixelShader m_pixel_shader;
    PixelShader m_pixel_shader_deferred_render;
    InputLayout m_input_layout;
};
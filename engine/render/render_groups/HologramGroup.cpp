#include "HologramGroup.h"
#include "../systems_and_managers/ModelManager.h"
#include "../../source/Entity.h"
#include <algorithm>
#include <cstddef>

bool HologramGroup::findIntersection(const math::Ray& ray, internal::ObjRef& objref, math::Intersection& outNearest)
{
	return utility::findIntersection(*this, ray, objref, outNearest, internal::IntersectedType::Hologram);
}

void HologramGroup::remove(ModelInstanceID& entity)
{
	for (int i = 0; i < entity.meshInstanceIDs.size(); i++)
	{
		MeshInstanceID::IdComponent& idComponent = entity.meshInstanceIDs[i].id_component_main;
		m_perModel[idComponent.id_model].perMesh[i].perMaterial[idComponent.id_material].instances.erase(idComponent.id_instance);
	}
	updateInstanceBuffers();
}

void HologramGroup::render(bool deferred_render)
{
	if (m_instanceBuffer.size() == 0)
		return;

	engine::s_devcon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);

	m_vertex_shader.bind();
	m_vertex_shader.bind();
	if (deferred_render)
		m_pixel_shader_deferred_render.bind();
	else
		m_pixel_shader.bind();
	m_input_layout.bind();

	m_hull_shader.bind();
	m_domain_shader.bind();
	m_geometry_shader.bind();

	m_instanceBuffer.bind(1);
	m_meshData.bind(2, TypeShader::TypeVertexShader);

	uint32_t renderedInstances = 0;
	for (const auto& perModel : m_perModel)
	{
		perModel.model->bindBuffers();

		for (uint32_t meshIndex = 0; meshIndex < perModel.perMesh.size(); ++meshIndex)
		{
			const engine::Mesh& mesh = perModel.model->meshes[meshIndex];
			const auto& meshRange = perModel.model->m_ranges[meshIndex];

			// You have to upload a Mesh-to-Model transformation matrix retrieved from model file via Assimp
			m_meshData.update(mesh.instances[0]);// ... update shader local per-mesh uniform buffer

			for (const auto& perMaterial : perModel.perMesh[meshIndex].perMaterial)
			{
				if (perMaterial.instances.size() == 0) continue;

				uint32_t numInstances = uint32_t(perMaterial.instances.sizeOccupied());

				if (perModel.model->indexBuffer().empty() == false)
				{
					engine::s_devcon->DrawIndexedInstanced(meshRange.indexNum, numInstances, meshRange.indexOffset, meshRange.vertexOffset, renderedInstances);
				}
				else {
					engine::s_devcon->DrawInstanced(meshRange.vertexNum, numInstances, meshRange.vertexOffset, renderedInstances);
				}
				renderedInstances += numInstances;
			}

		}
	}


}

void HologramGroup::init()
{
	m_vertex_shader.loadVertexShader(L"../engine/shaders/HologramShaders/VertexHologramShader.hlsl");

	{
		D3D11_INPUT_ELEMENT_DESC polygonLayout[] =
		{
			{"POSITION",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(engine::Mesh::Vertex, pos), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"NORMAL",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(engine::Mesh::Vertex, normal), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"MW",  1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 32, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  4, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 48, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"COLOR",  0, DXGI_FORMAT_R32G32B32_FLOAT, 1, 64, D3D11_INPUT_PER_INSTANCE_DATA, 1}
		};

		m_input_layout.loadInputLayout(m_vertex_shader.getShaderBlob(), polygonLayout, std::size(polygonLayout));
	}

	m_pixel_shader.loadPixelShader(L"../engine/shaders/HologramShaders/PixelHologramShader.hlsl");
	m_pixel_shader_deferred_render.loadPixelShader(L"../engine/shaders/HologramShaders/DeferredRender/PixelHologramShader.hlsl");
	m_hull_shader.loadHullShader(L"../engine/shaders/HologramShaders/HullHologramShader.hlsl");
	m_domain_shader.loadDomainShader(L"../engine/shaders/HologramShaders/DomainHologramShader.hlsl");
	m_geometry_shader.loadGeometryShader(L"../engine/shaders/HologramShaders/GeometryHologramShader.hlsl");
	m_meshData.init(D3D11_USAGE_DYNAMIC);
}

void HologramGroup::deinit()
{
	m_instanceBuffer.deinit();
	m_meshData.deinit();
	m_hull_shader.release();
	m_domain_shader.release();
	m_geometry_shader.release();
	m_vertex_shader.release();
	m_pixel_shader.release();
	m_pixel_shader_deferred_render.release();
	m_input_layout.release();
}

void HologramGroup::updateInstanceBuffers()
{
	uint32_t totalInstances = 0;
	for (auto& perModel : m_perModel)
		for (auto& perMesh : perModel.perMesh)
			for (const auto& perMaterial : perMesh.perMaterial)
				totalInstances += uint32_t(perMaterial.instances.sizeOccupied());

	if (totalInstances == 0)
		return;

	m_instanceBuffer.init(totalInstances, D3D11_USAGE_DYNAMIC);

	auto mapping = m_instanceBuffer.map();
	Instance* dst = static_cast<Instance*>(mapping.pData);

	uint32_t copiedNum = 0;
	for (const auto& perModel : m_perModel)
	{
		for (uint32_t meshIndex = 0; meshIndex < perModel.perMesh.size(); ++meshIndex)
		{
			const engine::Mesh& mesh = perModel.model->meshes[meshIndex];

			for (const auto& perMaterial : perModel.perMesh[meshIndex].perMaterial)
			{
				auto& instances = perMaterial.instances;

				uint32_t numModelInstances = instances.sizeMax();
				for (uint32_t index = 0; index < numModelInstances; ++index)
				{
					if (instances.occupied(index) == false)
						continue;
#ifdef LIGHT_CALCULATION_WORLD_SPACE
					dst[copiedNum++] = { TransformSystem::instance().getMatrix(instances[index].id_modelToWorld), instances[index].color };
#else
					dst[copiedNum++] = { TransformSystem::instance().getCameraCenteredMatrix(instances[index].id_modelToWorld), instances[index].color };
#endif // LIGHT_CALCULATION_WORLD_SPACE
					
				}
			}
		}
	}

	m_instanceBuffer.unmap();

}

void HologramGroup::addHologramInstance(const std::shared_ptr<Model>& model, const InstanceIDs& instance, ModelInstanceID& entity)
{
	auto finded_model = std::find_if(m_perModel.begin(), m_perModel.end(), [&](const PerModel& perModel)
		{ return perModel.model == model; });

	if (finded_model == m_perModel.end())
	{
		
		PerModel pushedPerModel;
		pushedPerModel.model = model;
		pushedPerModel.perMesh.resize(model->meshes.size());
		for (int i = 0; i < model->meshes.size(); i++)
		{
			MeshInstanceID meshInstanceID{};
			pushedPerModel.perMesh[i].perMaterial.resize(1);
			meshInstanceID.id_component_main.id_instance = pushedPerModel.perMesh[i].perMaterial[0].instances.insert(instance);
			meshInstanceID.id_component_main.id_material = 0;
			meshInstanceID.id_component_main.id_shading = internal::IntersectedType::Hologram;
			meshInstanceID.id_component_main.id_model = m_perModel.size();
			entity.meshInstanceIDs.push_back(meshInstanceID);

		}
		m_perModel.push_back(pushedPerModel);
	}
	else
	{
		for (int i = 0; i < model->meshes.size(); i++)
		{
			MeshInstanceID meshInstanceID{};
			meshInstanceID.id_component_main.id_instance = finded_model->perMesh[i].perMaterial[0].instances.insert(instance);
			meshInstanceID.id_component_main.id_shading = internal::IntersectedType::Hologram;
			meshInstanceID.id_component_main.id_model = std::distance(m_perModel.begin(), finded_model);
			meshInstanceID.id_component_main.id_material = 0;
			entity.meshInstanceIDs.push_back(meshInstanceID);
		}
	}
	updateInstanceBuffers();
}



#pragma once
#include <vector>
#include <memory>
#include "../../source/math/Matrix4x4.h"
#include "../primitives.h"
#include "../../source/Model.h"
#include "../d3d_utils/PixelShader.h"
#include "../d3d_utils/VertexShader.h"
#include "../d3d_utils/HullShader.h"
#include "../d3d_utils/DomainShader.h"
#include "../d3d_utils/GeometryShader.h"
#include "../d3d_utils/InputLayout.h"
#include "../d3d_utils/VertexBuffer.h"
#include "../d3d_utils/UniformBuffer.h"
#include "../../source/Entity.h"
#include "../../utils/IntersectionFinder.h"
#include "../systems_and_managers/TextureManager.h"

struct MeshInstanceID;

class IncinerationGroup
{
public:

    struct ShadowBuffer
    {
        Vector3 light_pos;
        float _pad;
    };

    struct MaterialData
    {
        Vector3 albedo;
        float roughness;
        Vector3 normal;
        float metalness;
        int state;
        float _pad[3];
    };

    struct Material {
        std::shared_ptr<Texture> albedo;
        std::shared_ptr<Texture> normal;
        std::shared_ptr<Texture> roughness;
        std::shared_ptr<Texture> metalness;
        MaterialData data;
    };

    struct InstanceCPU {
        engine::ID id_modelToWorld;
        Vector4 tint;
        Vector4 emission_color;
        Vector3 start_pos;
        float start_time;
        float velocity;
        float max_radius;
        size_t id_entity;
    };

    struct InstanceGPU {
        Matrix4x4 modelToWorld;
        Vector4 tint;
        Vector4 emission_color;
        Vector3 start_pos;
        float start_time;
        float velocity;
        float max_radius;
        float _pad[2];
    };

    struct PerMaterial {
        Material material;
        SolidVector<InstanceCPU> instances;
    };
    struct PerMesh {
        std::vector<PerMaterial> perMaterial;
    };
    struct PerModel {
        std::shared_ptr<Model> model;
        std::vector<PerMesh> perMesh;
    };

    friend bool utility::findIntersection(IncinerationGroup& group, const math::Ray& ray, internal::ObjRef& objref, math::Intersection& outNearest, internal::IntersectedType intersected_type);
    bool findIntersection(const math::Ray& ray, internal::ObjRef& objref, math::Intersection& outNearest);

    void remove(ModelInstanceID& entity);

    void render(bool deferred_render);
    void renderDepth2D();
    void renderDepthCubemaps(const Vector3& light_pos);
    void init();
    void deinit();


    void updateInstanceBuffers();
    void add(const std::shared_ptr<Model>& model, const Material& material, const InstanceCPU& instance, ModelInstanceID& entity);
    void add(const std::shared_ptr<Model>& model, std::vector<Material> materials, const InstanceCPU& instance, ModelInstanceID& entity);

    std::vector<PerModel>& getPerModel() { return m_perModel; }
private:

    std::shared_ptr<Texture> m_noise;
    std::vector<PerModel> m_perModel;
    VertexBuffer<InstanceGPU> m_instanceBuffer;
    UniformBuffer<Matrix4x4> m_meshData;
    UniformBuffer<ShadowBuffer> m_shadowBuffer;
    UniformBuffer<MaterialData> m_materialData;
    VertexShader m_vertex_shader;

    PixelShader m_pixel_shader;
    PixelShader m_pixel_shader_deferred_render;
    InputLayout m_input_layout;

    VertexShader m_vertex_shader_2D_shadow;
    InputLayout m_input_layout_2D_shadow;
    PixelShader m_pixel_shader_shadow;

    VertexShader m_vertex_shader_cube_map_shadow;
    InputLayout m_input_layout_cube_map_shadow;
    GeometryShader m_geometry_shader_cube_map_shadow;
};

#include "TextureOnlyGroup.h"
#include "../systems_and_managers/ModelManager.h"
#include "../systems_and_managers/TextureManager.h"
#include "../../source/Entity.h"
#include <algorithm>
#include <cstddef>

bool TextureOnlyGroup::findIntersection(const math::Ray& ray, internal::ObjRef& objref, math::Intersection& outNearest)
{
	//return utility::findIntersection(*this, ray, objref, outNearest, internal::IntersectedType::Emessive);
	return false;
}

void TextureOnlyGroup::render()
{
	if (m_instanceBuffer.size() == 0)
		return;

	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


	m_vertex_shader.bind();
	m_pixel_shader.bind();
	m_input_layout.bind();

	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	m_instanceBuffer.bind(1);
	m_meshData.bind(2, TypeShader::TypeVertexShader);

	uint32_t renderedInstances = 0;
	for (const auto& perModel : m_perModel)
	{

		perModel.model->bindBuffers();

		for (uint32_t meshIndex = 0; meshIndex < perModel.perMesh.size(); ++meshIndex)
		{
			const engine::Mesh& mesh = perModel.model->meshes[meshIndex];
			const auto& meshRange = perModel.model->m_ranges[meshIndex];

			// You have to upload a Mesh-to-Model transformation matrix retrieved from model file via Assimp
			m_meshData.update(mesh.instances[0]);// ... update shader local per-mesh uniform buffer

			for (const auto& perMaterial : perModel.perMesh[meshIndex].perMaterial)
			{
				if (perMaterial.instances.empty()) continue;

				perMaterial.material.albedo->bind(1, TypeShader::TypePixelShader);
				uint32_t numInstances = uint32_t(perMaterial.instances.size());
				if (perModel.model->indexBuffer().empty() == false)
				{
					engine::s_devcon->DrawIndexedInstanced(meshRange.indexNum, numInstances, meshRange.indexOffset, meshRange.vertexOffset, renderedInstances);
				}
				else {
					engine::s_devcon->DrawInstanced(meshRange.vertexNum, numInstances, meshRange.vertexOffset, renderedInstances);
				}
				renderedInstances += numInstances;
			}

		}
	}


}

void TextureOnlyGroup::init()
{
	m_vertex_shader.loadVertexShader(L"../engine/shaders/TextureOnlyShaders/VertexTextureOnlyShader.hlsl");

	{
		D3D11_INPUT_ELEMENT_DESC polygonLayout[] =
		{
			{"POSITION",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(engine::Mesh::Vertex, pos), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"NORMAL",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(engine::Mesh::Vertex, normal), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"TEXCOORD",  0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(engine::Mesh::Vertex, tc), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"MW",  1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 32, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  4, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 48, D3D11_INPUT_PER_INSTANCE_DATA, 1}
		};

		m_input_layout.loadInputLayout(m_vertex_shader.getShaderBlob(), polygonLayout, std::size(polygonLayout));
	}

	m_pixel_shader.loadPixelShader(L"../engine/shaders/TextureOnlyShaders/PixelTextureOnlyShader.hlsl");
	m_meshData.init(D3D11_USAGE_DYNAMIC);
}

void TextureOnlyGroup::deinit()
{
	m_instanceBuffer.deinit();
	m_meshData.deinit();
	m_vertex_shader.release();
	m_pixel_shader.release();
	m_input_layout.release();
}

void TextureOnlyGroup::updateInstanceBuffers()
{
	uint32_t totalInstances = 0;
	for (auto& perModel : m_perModel)
		for (auto& perMesh : perModel.perMesh)
			for (const auto& perMaterial : perMesh.perMaterial)
				totalInstances += uint32_t(perMaterial.instances.size());

	if (totalInstances == 0)
		return;

	m_instanceBuffer.init(totalInstances, D3D11_USAGE_DYNAMIC);

	auto mapping = m_instanceBuffer.map();
	Instance* dst = static_cast<Instance*>(mapping.pData);

	uint32_t copiedNum = 0;
	for (const auto& perModel : m_perModel)
	{
		for (uint32_t meshIndex = 0; meshIndex < perModel.perMesh.size(); ++meshIndex)
		{
			const engine::Mesh& mesh = perModel.model->meshes[meshIndex];

			for (const auto& perMaterial : perModel.perMesh[meshIndex].perMaterial)
			{
				auto& instances = perMaterial.instances;

				uint32_t numModelInstances = instances.size();
				for (uint32_t index = 0; index < numModelInstances; ++index)
				{
#ifdef LIGHT_CALCULATION_WORLD_SPACE
					dst[copiedNum++] = { TransformSystem::instance().getMatrix(instances[index].id_modelToWorld) };
#else
					dst[copiedNum++] = { TransformSystem::instance().getCameraCenteredMatrix(instances[index].id_modelToWorld) };
#endif // LIGHT_CALCULATION_WORLD_SPACE
				}
			}
		}
	}

	m_instanceBuffer.unmap();

}

void TextureOnlyGroup::add(const std::shared_ptr<Model>& model, const Material& material, const InstanceIDs& instance, MeshInstanceID& entity)
{
	auto finded_model = std::find_if(m_perModel.begin(), m_perModel.end(), [&](const PerModel& perModel)
		{ return perModel.model == model; });

	if (finded_model == m_perModel.end())
	{

		PerModel pushedPerModel;
		pushedPerModel.model = model;
		pushedPerModel.perMesh.resize(model->meshes.size());
		for (int i = 0; i < model->meshes.size(); i++)
		{
			pushedPerModel.perMesh[i].perMaterial.resize(1);
			pushedPerModel.perMesh[i].perMaterial[0].material = material;
			pushedPerModel.perMesh[i].perMaterial[0].instances.push_back(instance);

		}
		m_perModel.push_back(pushedPerModel);
		entity.id_component_main.id_model = m_perModel.size() - 1;
		entity.id_component_main.id_material = 0;
		entity.id_component_main.id_instance = 0;
	}
	else
	{
		for (int i = 0; i < model->meshes.size(); i++)
		{
			auto finded_material = std::find_if(finded_model->perMesh[i].perMaterial.begin(), finded_model->perMesh[i].perMaterial.end(), [&](const PerMaterial& perMaterial)
				{ return perMaterial.material.albedo == material.albedo; });

			size_t id_material = 0;
			if (finded_material == finded_model->perMesh[i].perMaterial.end())
			{
				finded_model->perMesh[i].perMaterial.push_back(PerMaterial{});

				id_material = finded_model->perMesh[i].perMaterial.size() - 1;
			}
			else
			{
				id_material = std::distance(finded_model->perMesh[i].perMaterial.begin(), finded_material);
			}

			finded_model->perMesh[i].perMaterial[id_material].instances.push_back(instance);
			finded_model->perMesh[i].perMaterial[id_material].material = material;

			entity.id_component_main.id_model = std::distance(m_perModel.begin(), finded_model);
			entity.id_component_main.id_material = id_material;
			entity.id_component_main.id_instance = finded_model->perMesh[i].perMaterial[id_material].instances.size() - 1;
		}
	}
	updateInstanceBuffers();
}

void TextureOnlyGroup::add(const std::shared_ptr<Model>& model, std::vector<Material> materials, const InstanceIDs& instance, MeshInstanceID& entity)
{
	DEV_ASSERT(model->meshes.size() == materials.size());

	auto finded_model = std::find_if(m_perModel.begin(), m_perModel.end(), [&](const PerModel& perModel)
		{ return perModel.model == model; });

	if (finded_model == m_perModel.end())
	{

		PerModel pushedPerModel;
		pushedPerModel.model = model;
		pushedPerModel.perMesh.resize(model->meshes.size());
		for (int i = 0; i < model->meshes.size(); i++)
		{
			pushedPerModel.perMesh[i].perMaterial.resize(1);
			pushedPerModel.perMesh[i].perMaterial[0].material = { TextureManager::instance().getTexture(model->albedo_texture_paths[i]) };
			pushedPerModel.perMesh[i].perMaterial[0].instances.push_back(instance);

		}
		m_perModel.push_back(pushedPerModel);
		entity.id_component_main.id_model = m_perModel.size() - 1;
		entity.id_component_main.id_material = 0;
		entity.id_component_main.id_instance = 0;
	}
	else
	{

		for (int i = 0; i < model->meshes.size(); i++)
		{
			auto finded_material = std::find_if(finded_model->perMesh[i].perMaterial.begin(), finded_model->perMesh[i].perMaterial.end(), [&](const PerMaterial& perMaterial)
				{ return perMaterial.material.albedo == materials[i].albedo; });

			size_t id_material = 0;
			if (finded_material == finded_model->perMesh[i].perMaterial.end())
			{
				finded_model->perMesh[i].perMaterial.push_back(PerMaterial{});

				id_material = finded_model->perMesh[i].perMaterial.size() - 1;
			}
			else
			{
				id_material = std::distance(finded_model->perMesh[i].perMaterial.begin(), finded_material);
			}

			finded_model->perMesh[i].perMaterial[id_material].instances.push_back(instance);
			finded_model->perMesh[i].perMaterial[id_material].material = materials[i];

			entity.id_component_main.id_model = std::distance(m_perModel.begin(), finded_model);
			entity.id_component_main.id_material = id_material;
			entity.id_component_main.id_instance = finded_model->perMesh[i].perMaterial[id_material].instances.size() - 1;
		}
	}
	updateInstanceBuffers();
}

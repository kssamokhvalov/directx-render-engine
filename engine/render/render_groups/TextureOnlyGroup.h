#pragma once
#include "../../source/math/Matrix4x4.h"
#include "../primitives.h"
#include "../../source/Model.h"
#include <vector>
#include <memory>
#include "../d3d_utils/PixelShader.h"
#include "../d3d_utils/VertexShader.h"
#include "../d3d_utils/HullShader.h"
#include "../d3d_utils/DomainShader.h"
#include "../d3d_utils/GeometryShader.h"
#include "../d3d_utils/InputLayout.h"
#include "../d3d_utils/VertexBuffer.h"
#include "../d3d_utils/UniformBuffer.h"
#include "../../source/Entity.h"
#include "../../utils/IntersectionFinder.h"
#include "../systems_and_managers/TextureManager.h"

struct MeshInstanceID;

class TextureOnlyGroup
{
public:
    struct Material {
        std::shared_ptr<Texture> albedo;
    };

    struct InstanceIDs {
        engine::ID id_modelToWorld;
        size_t id_entity;
    };

    struct Instance {
        Matrix4x4 modelToWorld;
    };

    struct PerMaterial {
        Material material;
        std::vector<InstanceIDs> instances;
    };
    struct PerMesh {
        std::vector<PerMaterial> perMaterial;
    };
    struct PerModel {
        std::shared_ptr<Model> model;
        std::vector<PerMesh> perMesh;
    };

    friend bool utility::findIntersection(class TextureOnlyGroup& group, const math::Ray& ray, internal::ObjRef& objref, math::Intersection& outNearest, internal::IntersectedType intersected_type);
    bool findIntersection(const math::Ray& ray, internal::ObjRef& objref, math::Intersection& outNearest);

    void render();
    void init();
    void deinit();
    void updateInstanceBuffers();
    void add(const std::shared_ptr<Model>& model, const Material& material, const InstanceIDs& instance, MeshInstanceID& entity);
    void add(const std::shared_ptr<Model>& model, std::vector<Material> materials, const InstanceIDs& instance, MeshInstanceID& entity);

    std::vector<PerModel>& getPerModel() { return m_perModel; }
private:
    std::vector<PerModel> m_perModel;
    VertexBuffer<Instance> m_instanceBuffer;
    UniformBuffer<Matrix4x4> m_meshData;
    VertexShader m_vertex_shader;
    PixelShader m_pixel_shader;
    InputLayout m_input_layout;
    HullShader m_hull_shader;
    DomainShader m_domain_shader;
    GeometryShader m_geometry_shader;
};
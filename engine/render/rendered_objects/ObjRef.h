#pragma once

namespace internal
{
	enum IntersectedType { Hologram, NormalVis, TextureOnly, Opaque, Dissolution, Emessive, Incineration, NUM };
	struct ObjRef
	{
		int id;
		IntersectedType type;
		void reset()
		{
			id = 0;
			type = IntersectedType::NUM;
		}
	};
}


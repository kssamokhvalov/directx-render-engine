#include "DecalSystem.h"
#include "ModelManager.h"
#include "../../source/Entity.h"
#include "../../source/Mesh.h"
#include <algorithm>
#include <cstddef>

constexpr static int HAS_ALBEDO = 1;
constexpr static int HAS_NORMAL = 1 << 1;
constexpr static int HAS_ROUGHNESS = 1 << 2;
constexpr static int HAS_METALNESS = 1 << 3;

void DecalSystem::init()
{
	m_vertex_shader.loadVertexShader(L"../engine/shaders/DecalShaders/VertexDecalShader.hlsl");
	{
		D3D11_INPUT_ELEMENT_DESC polygonLayout[] =
		{
			{"POSITION",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(engine::Mesh::Vertex, pos), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"NORMAL",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(engine::Mesh::Vertex, normal), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"MW",  1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 32, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  4, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 48, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  5, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 4 * 16, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  6, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 5 * 16, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  7, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 6 * 16, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  8, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 7 * 16, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"ID_OBJECT",  0, DXGI_FORMAT_R32_UINT, 1, 8 * 16, D3D11_INPUT_PER_INSTANCE_DATA, 1}
		};

		m_input_layout.loadInputLayout(m_vertex_shader.getShaderBlob(), polygonLayout, std::size(polygonLayout));
	}

	//m_pixel_shader.loadPixelShader(L"../engine/shaders/OpaqueShaders/PixelOpaqueShader.hlsl");
	m_pixel_shader_deferred_render.loadPixelShader(L"../engine/shaders/DecalShaders/DeferredRender/PixelDecalShader.hlsl");
	//m_meshData.init(D3D11_USAGE_DYNAMIC);
	m_materialData.init(D3D11_USAGE_DYNAMIC);
	m_cube = ModelManager::instance().getUnitCube();
}

void DecalSystem::deinit()
{
	m_materialData.deinit();
	m_vertex_shader.release();
	m_pixel_shader_deferred_render.release();
	m_input_layout.release();
	m_instanceBuffer.deinit();

	m_cube.reset();
}

void DecalSystem::render(bool deferred_render)
{
	if (m_instanceBuffer.size() == 0)
		return;

	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


	m_vertex_shader.bind();
	if (deferred_render)
		m_pixel_shader_deferred_render.bind();
	//else
		//m_pixel_shader.bind();
	m_input_layout.bind();

	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	m_instanceBuffer.bind(1);
	m_materialData.bind(6, TypePixelShader);

	uint32_t renderedInstances = 0;

	m_cube->bindBuffers();
	const auto& meshRange = m_cube->m_ranges[0];
	for (const auto& perMaterial : m_perMaterial)
	{
		if (perMaterial.instances.empty()) continue;

		if (perMaterial.material.data.state & HAS_ALBEDO)
			perMaterial.material.albedo->bind(1, TypeShader::TypePixelShader);

		if (perMaterial.material.data.state & HAS_NORMAL)
			perMaterial.material.normal->bind(2, TypeShader::TypePixelShader);

		if (perMaterial.material.data.state & HAS_ROUGHNESS)
			perMaterial.material.roughness->bind(3, TypeShader::TypePixelShader);

		if (perMaterial.material.data.state & HAS_METALNESS)
			perMaterial.material.metalness->bind(4, TypeShader::TypePixelShader);

		m_materialData.update(perMaterial.material.data);

		uint32_t numInstances = uint32_t(perMaterial.instances.size());
		if (m_cube->indexBuffer().empty() == false)
		{
			engine::s_devcon->DrawIndexedInstanced(meshRange.indexNum, numInstances, meshRange.indexOffset, meshRange.vertexOffset, renderedInstances);
		}
		else {
			engine::s_devcon->DrawInstanced(meshRange.vertexNum, numInstances, meshRange.vertexOffset, renderedInstances);
		}
		renderedInstances += numInstances;
	}
}

void DecalSystem::add(const Material& material, const InstanceIDs& instance, MeshInstanceID& entity)
{
	auto finded_material = std::find_if(m_perMaterial.begin(), m_perMaterial.end(), [&](const PerMaterial& perMaterial)
		{ return perMaterial.material.albedo == material.albedo; });

	size_t id_material = 0;
	if (finded_material == m_perMaterial.end())
	{
		m_perMaterial.push_back(PerMaterial{});

		id_material = m_perMaterial.size() - 1;
	}
	else
	{
		id_material = std::distance(m_perMaterial.begin(), finded_material);
	}

	m_perMaterial[id_material].instances.push_back(instance);
	m_perMaterial[id_material].material = material;

	entity.id_component_main.id_model = 0; //TODO
	entity.id_component_main.id_material = id_material;
	entity.id_component_main.id_instance = m_perMaterial[id_material].instances.size() - 1;

	updateInstanceBuffers();
}

void DecalSystem::updateInstanceBuffers()
{
	uint32_t totalInstances = 0;
	for (const auto& perMaterial : m_perMaterial)
		totalInstances += uint32_t(perMaterial.instances.size());

	if (totalInstances == 0)
		return;

	m_instanceBuffer.init(totalInstances, D3D11_USAGE_DYNAMIC);

	auto mapping = m_instanceBuffer.map();
	DecalInstance* dst = static_cast<DecalInstance*>(mapping.pData);

	uint32_t copiedNum = 0;
	for (const auto& perMaterial : m_perMaterial)
	{
		auto& instances = perMaterial.instances;

		uint32_t numModelInstances = instances.size();
		for (uint32_t index = 0; index < numModelInstances; ++index)
		{

#ifdef LIGHT_CALCULATION_WORLD_SPACE
			dst[copiedNum++] = { TransformSystem::instance().getMatrix(instances[index].id_decalToWorld),
			TransformSystem::instance().getMatrix(instances[index].id_worldToDecal) };
#else
			dst[copiedNum++] = { TransformSystem::instance().getCameraCenteredMatrix(instances[index].id_decalToWorld),
								 TransformSystem::instance().getCameraCenteredMatrix(instances[index].id_decalToWorld).getInversedMatrix(),
								 static_cast<uint32_t>(instances[index].id_entity_attached_model) };
#endif // LIGHT_CALCULATION_WORLD_SPACE

		}
	}


	m_instanceBuffer.unmap();
}

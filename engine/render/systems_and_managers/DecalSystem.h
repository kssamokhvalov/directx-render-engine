#pragma once
#include "TransformSystem.h"
#include "../../source/Model.h"
#include "../d3d_utils/PixelShader.h"
#include "../d3d_utils/VertexShader.h"
#include "../d3d_utils/VertexBuffer.h"
#include "../d3d_utils/InputLayout.h"
#include "../d3d_utils/Texture.h"
#include "../../source/Entity.h"

class DecalSystem
{
public:
    struct MaterialData
    {
        Vector3 albedo;
        float roughness;
        Vector3 normal;
        float metalness;
        int state;
        float _pad[3];
    };

    struct Material {
        std::shared_ptr<Texture> albedo;
        std::shared_ptr<Texture> normal;
        std::shared_ptr<Texture> roughness;
        std::shared_ptr<Texture> metalness;
        MaterialData data;
    };

    struct InstanceIDs {
        engine::ID id_decalToWorld;
        size_t id_entity;
        size_t id_entity_attached_model;
    };

    struct DecalInstance
    {
        Matrix4x4 decalToWorld;
        Matrix4x4 worldToDecal;
        uint32_t id_entity_attached_model;
    };

    struct PerMaterial {
        Material material;
        std::vector<InstanceIDs> instances;
    };

    void init();
    void deinit();
    void render(bool deferred_render);

    void add(const Material& material, const InstanceIDs& instance, MeshInstanceID& entity);
    void updateInstanceBuffers();
private:
    UniformBuffer<MaterialData> m_materialData;
    std::vector<PerMaterial> m_perMaterial;
	VertexShader m_vertex_shader;
	PixelShader m_pixel_shader_deferred_render;
	InputLayout m_input_layout;
	VertexBuffer<DecalInstance> m_instanceBuffer;

    std::shared_ptr<Model> m_cube;
};


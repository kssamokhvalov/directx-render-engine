#include "LightSystem.h"
#include "MeshSystem.h"
#include "ModelManager.h"
#include "../../source/math/Matrix4x4.h"

LightSystem& LightSystem::instance()
{
	static LightSystem s_light_system;
	return s_light_system;
}

void LightSystem::bind(int batch_number)
{
	D3D11_MAPPED_SUBRESOURCE mappedSubresource = m_light_sources_const_buffer.map();
	unsigned char* pData = reinterpret_cast<unsigned char*>(mappedSubresource.pData);

	int32_t numSpotLights = m_spot_lights.size() - batch_number * MAX_NUM_SPOT_LIGHTS;
	if (numSpotLights < 1)
	{
		numSpotLights = 0;
	}
	else
	{
		numSpotLights = numSpotLights > MAX_NUM_SPOT_LIGHTS ? MAX_NUM_SPOT_LIGHTS : numSpotLights;
		memcpy(pData + offsetof(LightSources, spotLights), m_spot_lights.data() + batch_number * MAX_NUM_SPOT_LIGHTS, sizeof(SpotLight) * numSpotLights);
	}

	int32_t numSpotLightsWithMask = m_spot_lights_with_mask.size() - batch_number * MAX_NUM_SPOT_LIGHTS_WITH_MASK;
	if (numSpotLightsWithMask < 1)
	{
		numSpotLightsWithMask = 0;
	}
	else
	{
		numSpotLightsWithMask = numSpotLightsWithMask > MAX_NUM_SPOT_LIGHTS_WITH_MASK ? MAX_NUM_SPOT_LIGHTS_WITH_MASK : numSpotLightsWithMask;
		memcpy(pData + offsetof(LightSources, spotLightsWithMask), m_spot_lights_with_mask.data() + batch_number * MAX_NUM_SPOT_LIGHTS_WITH_MASK, sizeof(SpotLightWithMask) * numSpotLightsWithMask);
	}

	int32_t numPointLights = m_point_lights.size() - batch_number * MAX_NUM_POINT_LIGHTS;
	if (numPointLights < 1)
	{
		numPointLights = 0;
	}
	else
	{
		numPointLights = numPointLights > MAX_NUM_POINT_LIGHTS ? MAX_NUM_POINT_LIGHTS : numPointLights;
		memcpy(pData + offsetof(LightSources, pointLights), m_point_lights.data() + batch_number * MAX_NUM_POINT_LIGHTS, sizeof(PointLight) * numPointLights);
	}

	int32_t numDirectionalLights = m_directional_lights.size() - batch_number * MAX_NUM_DIRETIONAL_LIGHTS;
	if (numDirectionalLights < 1)
	{
		numDirectionalLights = 0;
	}
	else
	{
		numDirectionalLights = numDirectionalLights > MAX_NUM_DIRETIONAL_LIGHTS ? MAX_NUM_DIRETIONAL_LIGHTS : numDirectionalLights;
		memcpy(pData + offsetof(LightSources, directionalLights), m_directional_lights.data() + batch_number * MAX_NUM_DIRETIONAL_LIGHTS, sizeof(DirectionalLight) * numDirectionalLights);
	}

	memcpy(pData + offsetof(LightSources, numSpotLights), &numSpotLights, sizeof(int32_t));
	memcpy(pData + offsetof(LightSources, numSpotLightsWithMask), &numSpotLightsWithMask, sizeof(int32_t));
	memcpy(pData + offsetof(LightSources, numPointLights), &numPointLights, sizeof(int32_t));
	memcpy(pData + offsetof(LightSources, numDirectionalLights), &numDirectionalLights, sizeof(int32_t));
	memcpy(pData + offsetof(LightSources, ambiendLight), &m_ambiend_light, sizeof(Vector4));
	
	m_light_sources_const_buffer.unmap();
	m_light_sources_const_buffer.bind(3, TypeShader::TypePixelShader);

	m_flash_light_mask->bind(5, TypeShader::TypePixelShader);
}

void LightSystem::update()
{
	for (int i = 0; i < m_point_lights.size(); ++i)
	{
		Matrix4x4 lightToModel = TransformSystem::instance().getMatrix(m_point_lights.at(i).id_matirx_local);
#ifdef LIGHT_CALCULATION_WORLD_SPACE
		m_point_lights.at(i).position = Matrix4x4::multiply(lightToModel.rowAsVector4(3), TransformSystem::instance().getMatrix(m_point_lights.at(i).id_matirx)).xyz();
#else
		m_point_lights.at(i).position = Matrix4x4::multiply(lightToModel.rowAsVector4(3), TransformSystem::instance().getCameraCenteredMatrix(m_point_lights.at(i).id_matirx)).xyz();
#endif // LIGHT_CALCULATION_WORLD_SPACE
	}
	for (int i = 0; i < m_spot_lights.size(); ++i)
	{
		Matrix4x4 lightToModel = TransformSystem::instance().getMatrix(m_spot_lights.at(i).id_matirx_local);
#ifdef LIGHT_CALCULATION_WORLD_SPACE
		Matrix4x4 modelToWorld = TransformSystem::instance().getMatrix(m_spot_lights_with_mask.at(i).id_matirx);
#else
		Matrix4x4 modelToWorld = TransformSystem::instance().getCameraCenteredMatrix(m_spot_lights_with_mask.at(i).id_matirx);
#endif // LIGHT_CALCULATION_WORLD_SPACE
		m_spot_lights.at(i).matrix_light_to_world = lightToModel * modelToWorld;

	}
	for (int i = 0; i < m_spot_lights_with_mask.size(); ++i)
	{
		Matrix4x4 lightToModel = TransformSystem::instance().getMatrix(m_spot_lights_with_mask.at(i).id_matirx_local);
#ifdef LIGHT_CALCULATION_WORLD_SPACE
		Matrix4x4 modelToWorld = TransformSystem::instance().getMatrix(m_spot_lights_with_mask.at(i).id_matirx);
#else
		Matrix4x4 modelToWorld = TransformSystem::instance().getCameraCenteredMatrix(m_spot_lights_with_mask.at(i).id_matirx);
#endif // LIGHT_CALCULATION_WORLD_SPACE
		m_spot_lights_with_mask.at(i).matrix_light_to_world = lightToModel * modelToWorld;
		m_spot_lights_with_mask.at(i).matrix_world_to_light = m_spot_lights_with_mask.at(i).matrix_light_to_world.getInversedMatrix();
	}
}

void LightSystem::init()
{
	m_light_sources_const_buffer.init(D3D11_USAGE_DYNAMIC);
	m_flash_light_mask = TextureManager::instance().getTexture("assets/textures/flash_light/mask_fixed.dds");
	m_ambiend_light = Vector4(0.02f, 0.02f, 0.02f, 1.0f);
}

void LightSystem::deinit()
{
	m_light_sources_const_buffer.deinit();
}

void LightSystem::addPointLight(engine::ID id_matrix, engine::ID id_matrix_local, const Vector4& emission, float radius)
{
	Matrix4x4 lightToModel = TransformSystem::instance().getMatrix(id_matrix_local);
#ifdef LIGHT_CALCULATION_WORLD_SPACE
	Matrix4x4 modelToWorld = TransformSystem::instance().getMatrix(id_matrix);
#else
	Matrix4x4 modelToWorld = TransformSystem::instance().getCameraCenteredMatrix(id_matrix);
#endif // LIGHT_CALCULATION_WORLD_SPACE
	m_point_lights.push_back({ Matrix4x4::multiply(lightToModel.rowAsVector4(3), modelToWorld).xyz(), radius, emission, id_matrix, id_matrix_local});

}

void LightSystem::addSpotLight(engine::ID id_matrix, engine::ID id_matrix_local, float p, const Vector4& emission, float radius)
{
	Matrix4x4 lightToModel = TransformSystem::instance().getMatrix(id_matrix_local);
#ifdef LIGHT_CALCULATION_WORLD_SPACE
	Matrix4x4 modelToWorld = TransformSystem::instance().getMatrix(id_matrix);
#else
	Matrix4x4 modelToWorld = TransformSystem::instance().getCameraCenteredMatrix(id_matrix);
#endif // LIGHT_CALCULATION_WORLD_SPACE
	m_spot_lights.push_back({ lightToModel * modelToWorld, emission, p, radius, id_matrix, id_matrix_local });
}


void LightSystem::addSpotLightWithMask(engine::ID id_matrix, engine::ID id_matrix_local, float p, const Vector4& emission, float radius)
{
	Matrix4x4 lightToModel = TransformSystem::instance().getMatrix(id_matrix_local);
#ifdef LIGHT_CALCULATION_WORLD_SPACE
	Matrix4x4 modelToWorld = TransformSystem::instance().getMatrix(id_matrix);
#else
	Matrix4x4 modelToWorld = TransformSystem::instance().getCameraCenteredMatrix(id_matrix);
#endif // LIGHT_CALCULATION_WORLD_SPACE
	Matrix4x4 lightMaskProj = Matrix4x4::createFrustumPerspectiveMatrix(2 * acosf(pow(0.0001f, 1 / p)), 1.0f, 0.1f, 1000.0f);  //TODO avoid acos
	engine::ID id_lightMaskProj = TransformSystem::instance().addMatrix(lightMaskProj);

	Matrix4x4 lightToWorld= lightToModel * modelToWorld;
	Matrix4x4 worldToLight = lightToWorld.getInversedMatrix();

	m_spot_lights_with_mask.push_back({ worldToLight, lightToWorld, lightMaskProj, emission, p, radius, id_matrix, id_lightMaskProj, id_matrix_local });
}

void LightSystem::addDirectionalLight(const Vector3& direction, float I, const Vector3& color, float solid_angle, float cos_alpha)
{
	Vector4 emission = Vector4(color, 1.0f) * I;
	Matrix4x4 worldToLight = Matrix4x4::createRotationMatrix(direction.normalized());
	Matrix4x4 lightProj = Matrix4x4::createZeroMatrix();
	engine::ID id_matrix = TransformSystem::instance().addMatrix(worldToLight);
	engine::ID id_lightMaskProj = TransformSystem::instance().addMatrix(lightProj);
	m_directional_lights.push_back({ worldToLight, lightProj, direction, solid_angle, emission, cos_alpha, id_matrix, id_lightMaskProj});
}

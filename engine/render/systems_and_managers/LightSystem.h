#pragma once
#include "../../source/math/Vector3.h"
#include "../d3d_utils/UniformBuffer.h"
#include "TextureManager.h"
#include "TransformSystem.h" //TODO

// all const values must be equal to const values in globals.hlsl
const int MAX_NUM_SPOT_LIGHTS = 10; 
const int MAX_NUM_POINT_LIGHTS = 10;
const int MAX_NUM_DIRETIONAL_LIGHTS = 2;
const int MAX_NUM_SPOT_LIGHTS_WITH_MASK = 1;



class LightSystem
{
public:
    //TODO maybe better divide structs of lights: cpu_light and gpu_light
    struct SpotLight
    {
        Matrix4x4 matrix_light_to_world;
        Vector4 emission;
        float p;
        float radius;
        uint32_t id_matirx; //TODO it is padding, but temporal used for matrix id;
        uint32_t id_matirx_local; //TODO it is padding, but temporal used for matrix id. Matrix for inside model 
    };

    struct SpotLightWithMask
    {
        Matrix4x4 matrix_world_to_light;
        Matrix4x4 matrix_light_to_world;
        Matrix4x4 matrix_mask_proj;
        Vector4 emission;
        float p;
        float radius;
        uint32_t id_matirx; //TODO it is padding, but temporal used for matrix id;
        uint32_t id_lightMaskProj;//TODO it is padding, but temporal used for matrix id;
        uint32_t id_matirx_local; //TODO it is padding, but temporal used for matrix id. Matrix for inside model 
        float _pad[3];
    };

    struct PointLight
    {
        Vector3 position;
        float radius;    
        Vector4 emission;
        uint32_t id_matirx;    //TODO it is padding, but temporal used for matrix id;
        uint32_t id_matirx_local;    //TODO it is padding, but temporal used for matrix id;
        float _pad[2];
    };

    struct DirectionalLight
    {
        Matrix4x4 matrix_world_to_light;
        Matrix4x4 matrix_proj;
        Vector3 direction;
        float solid_angle;
        Vector4 emission;
        float cosAlpha;
        uint32_t id_matirx;    //TODO it is padding, but temporal used for matrix id;
        uint32_t id_lightProj;    //TODO it is padding, but temporal used for matrix id;
        float _pad;
    };

    struct LightSources
    {
        SpotLight spotLights[MAX_NUM_SPOT_LIGHTS];
        SpotLightWithMask spotLightsWithMask[MAX_NUM_SPOT_LIGHTS_WITH_MASK];
        PointLight pointLights[MAX_NUM_POINT_LIGHTS];
        DirectionalLight directionalLights[MAX_NUM_DIRETIONAL_LIGHTS];
        int32_t numSpotLights;
        int32_t numSpotLightsWithMask;
        int32_t numPointLights;
        int32_t numDirectionalLights;
        Vector4 ambiendLight;
    };

    LightSystem(const LightSystem& other) = delete;
    void operator=(const LightSystem& other) = delete;

    static LightSystem& instance();

    void bind(int batch_number);
    void update();
    void init();
    void deinit();
    void addPointLight(engine::ID id_matrix, engine::ID id_matrix_local, const Vector4& emission, float radius);
    void addSpotLight(engine::ID id_matrix, engine::ID id_matrix_local, float p, const Vector4& emission, float radius);
    void addSpotLightWithMask(engine::ID id_matrix, engine::ID id_matrix_local, float p, const Vector4& emission, float radius);
    void addDirectionalLight(const Vector3& direction, float I, const Vector3& color, float solid_angle, float cos_alpha);

    size_t getNumDirectionalLight() const { return m_directional_lights.size(); }
    std::vector<DirectionalLight>& getDirectionalLights() { return m_directional_lights; }
    std::vector<SpotLightWithMask>& getSpotWithMaskLights() { return m_spot_lights_with_mask; }
    std::vector<PointLight>& getPointLights() { return m_point_lights; }
private:
    LightSystem() = default;

    UniformBuffer<LightSources> m_light_sources_const_buffer;
    std::vector<SpotLight> m_spot_lights;
    std::vector<SpotLightWithMask> m_spot_lights_with_mask;
    std::vector<PointLight> m_point_lights;
    std::vector<DirectionalLight> m_directional_lights;
    
    std::shared_ptr<Texture> m_flash_light_mask;

    Vector4 m_ambiend_light;
};


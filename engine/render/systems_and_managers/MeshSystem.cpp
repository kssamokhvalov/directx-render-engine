#include "MeshSystem.h"
#include "../../utils/ParallelExecutor.h"
#include "../../windows/window.h"
#include "../../source/math/Intersection.h"
#include "../rendered_objects/ObjRef.h"
#include "../../source/dragger/MatrixMover.h"
#include "../../source/math/Ray.h"
#include "ModelManager.h"
#include "../../source/Entity.h"
#include "../../utils/SolidVector.h"
#include "TransformSystem.h"


#include <direct.h>
#pragma comment (lib,"D3DCompiler.lib")


MeshSystem& MeshSystem::instance()
{
	static MeshSystem s_mesh_system;
	return s_mesh_system;
}

void MeshSystem::render(bool deferred_render)
{
	m_hologram.render(deferred_render);
	m_emissives.render(deferred_render);
	m_textureOnly.render();
	//TODO try change render order
	if(m_render_normalVis == true)
		m_normalVis.render();
	

}

void MeshSystem::renderPBR(bool deferred_render)
{
	m_opaques.render(deferred_render);
}

void MeshSystem::renderTranslucency(bool deferred_render)
{
	m_dissolution.render(deferred_render);
	m_incinerations.render(deferred_render);
}

void MeshSystem::renderDecals(bool deferred_render)
{
	m_decal_system.render(deferred_render);
}

void MeshSystem::renderDepth2D()
{
	m_opaques.renderDepth2D();
	m_dissolution.renderDepth2D();
	m_incinerations.renderDepth2D();
}

void MeshSystem::renderDepthCubemaps(const Vector3& light_pos)
{
	m_opaques.renderDepthCubemaps(light_pos);
	m_dissolution.renderDepthCubemaps(light_pos);
	m_incinerations.renderDepthCubemaps(light_pos);
}

bool MeshSystem::intersects(const math::Ray& ray, const Matrix4x4& obj, internal::ObjRef& outRef, const engine::Mesh& mesh, math::Intersection& outNearest) const
{
	Matrix4x4 objInv = obj.getInversedMatrix();
	math::Ray ray_obj{ Matrix4x4::multiply(Vector4(ray.origin, 1.0f), objInv).xyz(),
		Matrix4x4::multiply(Vector4(ray.direction, 0.0f), objInv).xyz() };

	math::Intersection inter_box;
	inter_box.t = outNearest.t;
	if (mesh.octree.intersect(ray_obj, inter_box))
	{
		if (inter_box.t < outNearest.t)
		{
			outNearest.t = inter_box.t;
			outNearest.normal = Matrix4x4::multiply(Vector4(inter_box.normal, 0.0f), obj).xyz();
			outNearest.pos = Matrix4x4::multiply(Vector4(ray_obj.getPoint(inter_box.t), 1), obj).xyz();
			return true;
		}
	}
	return false;
}

bool MeshSystem::findIntersection(const math::Ray& ray, internal::ObjRef& objref, math::Intersection& outNearest)
{
	outNearest.reset();

	m_hologram.findIntersection(ray, objref, outNearest);
	//m_textureOnly.findIntersection(ray, objref, outNearest);
	m_opaques.findIntersection(ray, objref, outNearest);
	m_emissives.findIntersection(ray, objref, outNearest);
	m_dissolution.findIntersection(ray, objref, outNearest);
	m_incinerations.findIntersection(ray, objref, outNearest);

	return std::isfinite(outNearest.t);
}

bool MeshSystem::findIntersection(const math::Ray& ray, math::Intersection& outNearest, std::unique_ptr<math::MatrixMover>& matrixMover)
{
	internal::ObjRef objref;
	objref.reset();

	findIntersection(ray, objref, outNearest);

	matrixMover.reset(new math::MatrixMover(ray.getPoint(outNearest.t), outNearest.t, objref.type));
	ModelInstanceID* entity = &MeshSystem::instance().getInstances()[objref.id];

	//TODO need use CRT here
	switch (objref.type)
	{
	case internal::IntersectedType::Hologram:
	{
		matrixMover->getMatricesID().resize(entity->meshInstanceIDs.size());

		for (uint32_t meshIndex = 0; meshIndex < entity->meshInstanceIDs.size(); ++meshIndex)
		{
			MeshInstanceID& meshInstanceID = entity->meshInstanceIDs[meshIndex];
			matrixMover->getMatricesID()[meshIndex] = m_hologram.getPerModel()[meshInstanceID.id_component_main.id_model].perMesh[meshIndex].perMaterial[meshInstanceID.id_component_main.id_material].instances[meshInstanceID.id_component_main.id_instance].id_modelToWorld;
		}
		break;
	}
	case internal::IntersectedType::Opaque:
	{
		matrixMover->getMatricesID().resize(entity->meshInstanceIDs.size());

		for (uint32_t meshIndex = 0; meshIndex < entity->meshInstanceIDs.size(); ++meshIndex)
		{
			MeshInstanceID& meshInstanceID = entity->meshInstanceIDs[meshIndex];
			matrixMover->getMatricesID()[meshIndex] = m_opaques.getPerModel()[meshInstanceID.id_component_main.id_model].perMesh[meshIndex].perMaterial[meshInstanceID.id_component_main.id_material].instances[meshInstanceID.id_component_main.id_instance].id_modelToWorld;
		}
		break;
	}
	case internal::IntersectedType::Dissolution:
	{
		matrixMover->getMatricesID().resize(entity->meshInstanceIDs.size());

		for (uint32_t meshIndex = 0; meshIndex < entity->meshInstanceIDs.size(); ++meshIndex)
		{
			MeshInstanceID& meshInstanceID = entity->meshInstanceIDs[meshIndex];
			matrixMover->getMatricesID()[meshIndex] = m_dissolution.getPerModel()[meshInstanceID.id_component_main.id_model].perMesh[meshIndex].perMaterial[meshInstanceID.id_component_main.id_material].instances[meshInstanceID.id_component_main.id_instance].id_modelToWorld;
		}
		break;
	}
	case internal::IntersectedType::Incineration:
	{
		matrixMover->getMatricesID().resize(entity->meshInstanceIDs.size());

		for (uint32_t meshIndex = 0; meshIndex < entity->meshInstanceIDs.size(); ++meshIndex)
		{
			MeshInstanceID& meshInstanceID = entity->meshInstanceIDs[meshIndex];
			matrixMover->getMatricesID()[meshIndex] = m_incinerations.getPerModel()[meshInstanceID.id_component_main.id_model].perMesh[meshIndex].perMaterial[meshInstanceID.id_component_main.id_material].instances[meshInstanceID.id_component_main.id_instance].id_modelToWorld;
		}
		break;
	}
	case internal::IntersectedType::Emessive:
	{
		matrixMover->getMatricesID().resize(entity->meshInstanceIDs.size());

		for (uint32_t meshIndex = 0; meshIndex < entity->meshInstanceIDs.size(); ++meshIndex)
		{
			MeshInstanceID& meshInstanceID = entity->meshInstanceIDs[meshIndex];
			matrixMover->getMatricesID()[meshIndex] = m_emissives.getPerModel()[meshInstanceID.id_component_main.id_model].perMesh[meshIndex].perMaterial[meshInstanceID.id_component_main.id_material].instances[meshInstanceID.id_component_main.id_instance].id_modelToWorld;
		}
		break;
	}
	case internal::IntersectedType::NUM:
		matrixMover.reset();
		break;
	default:
		break;
	}

	return objref.type != internal::IntersectedType::NUM;
}



void MeshSystem::addNormalVisInstance(const std::shared_ptr<Model>& model, Matrix4x4& modelToWorld, const Colorf& color)
{
	engine::ID id_matrix = TransformSystem::instance().addMatrix(modelToWorld);

	size_t index_entity = m_entitys.insert(MeshInstanceID());
	m_normalVis.addNormalVisInstance(model, { id_matrix, index_entity }, m_instances[index_entity]);
}

void MeshSystem::addHologramInstance(const std::shared_ptr<Model>& model, Matrix4x4& modelToWorld, const Colorf& color)
{
	engine::ID id_matrix = TransformSystem::instance().addMatrix(modelToWorld);

	size_t index_entity = m_instances.insert(ModelInstanceID());
	m_hologram.addHologramInstance(model, { id_matrix, color, index_entity }, m_instances[index_entity]);
	m_normalVis.addNormalVisInstance(model, { id_matrix, index_entity }, m_instances[index_entity]);
}

void MeshSystem::addOpaqueInstance(const std::shared_ptr<Model>& model, const OpaqueGroup::Material& material, Matrix4x4& modelToWorld)
{
	engine::ID id_matrix = TransformSystem::instance().addMatrix(modelToWorld);
	size_t index_entity = m_instances.insert(ModelInstanceID());
	m_opaques.add(model, material, { id_matrix, index_entity }, m_instances[index_entity]);
	m_normalVis.addNormalVisInstance(model, { id_matrix, index_entity }, m_instances[index_entity]);
}

void MeshSystem::addOpaqueInstance(const std::shared_ptr<Model>& model, std::vector<OpaqueGroup::Material> materials, Matrix4x4& modelToWorld)
{
	DEV_ASSERT(model->meshes.size() == materials.size());
	engine::ID id_matrix = TransformSystem::instance().addMatrix(modelToWorld);

	size_t index_entity = m_instances.insert(ModelInstanceID());
	m_opaques.add(model, materials, { id_matrix, index_entity }, m_instances[index_entity]);
	m_normalVis.addNormalVisInstance(model, { id_matrix, index_entity }, m_instances[index_entity]);
}

void MeshSystem::addOpaqueInstance(const std::shared_ptr<Model>& model, const OpaqueGroup::Material& material, engine::ID id_matrix)
{
	size_t index_entity = m_instances.insert(ModelInstanceID());
	m_opaques.add(model, material, { id_matrix, index_entity }, m_instances[index_entity]);
	m_normalVis.addNormalVisInstance(model, { id_matrix, index_entity }, m_instances[index_entity]);
}

void MeshSystem::addOpaqueInstance(const std::shared_ptr<Model>& model, std::vector<OpaqueGroup::Material> materials, engine::ID id_matrix)
{
	DEV_ASSERT(model->meshes.size() == materials.size());

	size_t index_entity = m_instances.insert(ModelInstanceID());
	m_opaques.add(model, materials, { id_matrix, index_entity }, m_instances[index_entity]);
	m_normalVis.addNormalVisInstance(model, { id_matrix, index_entity }, m_instances[index_entity]);
}

void MeshSystem::addDissolutionInstance(const std::shared_ptr<Model>& model, const DissolutionGroup::Material& material, Matrix4x4& modelToWorld, float start_time, float duration)
{
	engine::ID id_matrix = TransformSystem::instance().addMatrix(modelToWorld);
	size_t index_entity = m_instances.insert(ModelInstanceID());
	m_dissolution.add(model, material, { id_matrix, start_time, duration, index_entity }, m_instances[index_entity]);
	m_normalVis.addNormalVisInstance(model, { id_matrix, index_entity }, m_instances[index_entity]);
}

void MeshSystem::addDissolutionInstance(const std::shared_ptr<Model>& model, std::vector<DissolutionGroup::Material> materials, Matrix4x4& modelToWorld, float start_time, float duration)
{
	DEV_ASSERT(model->meshes.size() == materials.size());
	engine::ID id_matrix = TransformSystem::instance().addMatrix(modelToWorld);

	size_t index_entity = m_instances.insert(ModelInstanceID());
	m_dissolution.add(model, materials, { id_matrix, start_time, duration, index_entity }, m_instances[index_entity]);
	m_normalVis.addNormalVisInstance(model, { id_matrix, index_entity }, m_instances[index_entity]);
}

void MeshSystem::addIncinerationInstance(const std::shared_ptr<Model>& model, const IncinerationGroup::Material& material, Matrix4x4& modelToWorld, Vector4 tint, Vector4 emission_color, Vector3 start_pos_global, float start_time, float velocity, float max_radius)
{
	engine::ID id_matrix = TransformSystem::instance().addMatrix(modelToWorld);

	Matrix4x4 worldToModel = modelToWorld.getInversedMatrix();
	Vector3 start_pos = Matrix4x4::multiply(Vector4(start_pos_global, 1.0f), worldToModel).xyz();

	size_t index_entity = m_instances.insert(ModelInstanceID());
	m_incinerations.add(model, material, { id_matrix, tint, emission_color, start_pos, start_time, velocity, max_radius, index_entity }, m_instances[index_entity]);
	m_normalVis.addNormalVisInstance(model, { id_matrix, index_entity }, m_instances[index_entity]);
}

void MeshSystem::addIncinerationInstance(const std::shared_ptr<Model>& model, std::vector<IncinerationGroup::Material> materials, Matrix4x4& modelToWorld, Vector4 tint, Vector4 emission_color, Vector3 start_pos_global, float start_time, float velocity, float max_radius)
{
	DEV_ASSERT(model->meshes.size() == materials.size());
	engine::ID id_matrix = TransformSystem::instance().addMatrix(modelToWorld);

	Matrix4x4 worldToModel = modelToWorld.getInversedMatrix();
	Vector3 start_pos = Matrix4x4::multiply(Vector4(start_pos_global, 1.0f), worldToModel).xyz();

	size_t index_entity = m_instances.insert(ModelInstanceID());
	m_incinerations.add(model, materials, { id_matrix, tint, emission_color, start_pos, start_time, velocity, max_radius, index_entity }, m_instances[index_entity]);
	m_normalVis.addNormalVisInstance(model, { id_matrix, index_entity }, m_instances[index_entity]);
}

void MeshSystem::addIncinerationInstance(const std::shared_ptr<Model>& model, const IncinerationGroup::Material& material, engine::ID id_matrix, Vector4 tint, Vector4 emission_color, Vector3 start_pos_global, float start_time, float velocity, float max_radius)
{
	Matrix4x4 worldToModel = TransformSystem::instance().getMatrix(id_matrix).getInversedMatrix();
	Vector3 start_pos = Matrix4x4::multiply(Vector4(start_pos_global, 1.0f), worldToModel).xyz();

	size_t index_entity = m_instances.insert(ModelInstanceID());
	m_incinerations.add(model, material, { id_matrix, tint, emission_color, start_pos, start_time, velocity, max_radius, index_entity }, m_instances[index_entity]);
	m_normalVis.addNormalVisInstance(model, { id_matrix, index_entity }, m_instances[index_entity]);
}

void MeshSystem::addIncinerationInstance(const std::shared_ptr<Model>& model, std::vector<IncinerationGroup::Material> materials, engine::ID id_matrix, Vector4 tint, Vector4 emission_color, Vector3 start_pos_global, float start_time, float velocity, float max_radius)
{
	DEV_ASSERT(model->meshes.size() == materials.size());

	Matrix4x4 worldToModel = TransformSystem::instance().getMatrix(id_matrix).getInversedMatrix();
	Vector3 start_pos = Matrix4x4::multiply(Vector4(start_pos_global, 1.0f), worldToModel).xyz();

	size_t index_entity = m_instances.insert(ModelInstanceID());
	m_incinerations.add(model, materials, { id_matrix, tint, emission_color, start_pos, start_time, velocity, max_radius, index_entity }, m_instances[index_entity]);
	m_normalVis.addNormalVisInstance(model, { id_matrix, index_entity }, m_instances[index_entity]);
}

void MeshSystem::addEmissiveInstance(const std::shared_ptr<Model>& model, engine::ID id_matrix, const Vector4& color)
{
	size_t index_entity = m_instances.insert(ModelInstanceID());
	m_emissives.addEmissiveInstance(model, { id_matrix, color, index_entity }, m_instances[index_entity]);
	m_normalVis.addNormalVisInstance(model, { id_matrix, index_entity }, m_instances[index_entity]);
}

void MeshSystem::addDecalInstance(const DecalSystem::Material& material, Matrix4x4& decalToWorld, size_t id_model_attached)
{
	engine::ID id_matrix = TransformSystem::instance().addMatrix(decalToWorld);
	size_t index_entity = m_entitys.insert(MeshInstanceID());
	m_decal_system.add(material, { id_matrix, index_entity, id_model_attached }, m_entitys[index_entity]);

}

void MeshSystem::remove(engine::ID id)
{
	
	ModelInstanceID* entity = &MeshSystem::instance().getInstances()[id];
	//TODO need use CRT here
	switch (entity->meshInstanceIDs[0].id_component_main.id_shading)
	{
	case internal::IntersectedType::Hologram:
	{
		m_hologram.remove(*entity);
		m_normalVis.remove(*entity);
		break;
	}
	case internal::IntersectedType::Opaque:
	{
		m_opaques.remove(*entity);
		m_normalVis.remove(*entity);
		break;
	}
	case internal::IntersectedType::Dissolution:
	{
		m_dissolution.remove(*entity);
		m_normalVis.remove(*entity);
		break;
	}
	case internal::IntersectedType::Emessive:
	{
		m_emissives.remove(*entity);
		m_normalVis.remove(*entity);
		break;
	}
	case internal::IntersectedType::Incineration:
	{
		m_incinerations.remove(*entity);
		m_normalVis.remove(*entity);
		break;
	}
	case internal::IntersectedType::NUM:
		break;
	default:
		break;
	}

	return;
}

void MeshSystem::init()
{
	m_incinerations.init();
	m_hologram.init();
	m_normalVis.init();
	m_textureOnly.init();
	m_opaques.init();
	m_dissolution.init();
	m_emissives.init();
	m_decal_system.init();
	m_render_normalVis = false;
}

void MeshSystem::deinit()
{
	m_normalVis.deinit();
	m_hologram.deinit();
	m_textureOnly.deinit();
	m_dissolution.deinit();
	m_opaques.deinit();
	m_emissives.deinit();
	m_decal_system.deinit();
	m_incinerations.deinit();
}

void MeshSystem::update()
{
	m_hologram.updateInstanceBuffers();
	m_normalVis.updateInstanceBuffers();
	m_textureOnly.updateInstanceBuffers();
	m_opaques.updateInstanceBuffers();
	m_dissolution.updateInstanceBuffers();
	m_emissives.updateInstanceBuffers();
	m_decal_system.updateInstanceBuffers();
	m_incinerations.updateInstanceBuffers();
}

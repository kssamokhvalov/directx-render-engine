#pragma once

#include "../../utils/time.h"
#include "../d3d.h"
#include "../d3d_utils/VertexBuffer.h"
#include "../d3d_utils/IndexBuffer.h"
#include "../render_groups/HologramGroup.h"
#include "../render_groups/NormalVisGroup.h"
#include "../render_groups/TextureOnlyGroup.h"
#include "../render_groups/OpaqueGroup.h"
#include "../render_groups/DissolutionGroup.h"
#include "../render_groups/EmissiveGroup.h"
#include "../render_groups/IncinerationGroup.h"
#include "./DecalSystem.h"

class Window;
struct ParallelExecutor;
struct MeshInstanceID;

namespace math
{
	class MatrixMover;
	struct Intersection;
}

namespace internal { struct ObjRef; }

class MeshSystem
{
public:
	
	struct Vertex {
		float coordinats[3];
	};


	MeshSystem(const MeshSystem& other) = delete;
	void operator=(const MeshSystem& other) = delete;

	static MeshSystem& instance();

	void render(bool deferred_render);
	void renderPBR(bool deferred_render);
	void renderTranslucency(bool deferred_render);
	void renderDecals(bool deferred_render);
	void renderDepth2D();
	void renderDepthCubemaps(const Vector3& light_pos);

	bool findIntersection(const math::Ray& ray, internal::ObjRef& objref, math::Intersection& outNearest);
	bool findIntersection(const math::Ray& ray, math::Intersection& outNearest, std::unique_ptr<math::MatrixMover>& matrixMover);

	
	void addHologramInstance(const std::shared_ptr<Model>& model, Matrix4x4& modelToWorld, const Colorf& color);
	void addOpaqueInstance(const std::shared_ptr<Model>& model, const OpaqueGroup::Material& material, Matrix4x4& modelToWorld);
	void addOpaqueInstance(const std::shared_ptr<Model>& model, std::vector<OpaqueGroup::Material> materials, Matrix4x4& modelToWorld);
	void addOpaqueInstance(const std::shared_ptr<Model>& model, const OpaqueGroup::Material& material, engine::ID id_matrix);
	void addOpaqueInstance(const std::shared_ptr<Model>& model, std::vector<OpaqueGroup::Material> materials, engine::ID id_matrix);
	void addDissolutionInstance(const std::shared_ptr<Model>& model, const DissolutionGroup::Material& material, Matrix4x4& modelToWorld, float start_time, float duration);
	void addDissolutionInstance(const std::shared_ptr<Model>& model, std::vector<DissolutionGroup::Material> materials, Matrix4x4& modelToWorld, float start_time, float duration);
	void addIncinerationInstance(const std::shared_ptr<Model>& model, const IncinerationGroup::Material& material, Matrix4x4& modelToWorld, Vector4 tint, Vector4 emission_color, Vector3 start_pos_global, float start_time, float velocity, float max_radius);
	void addIncinerationInstance(const std::shared_ptr<Model>& model, std::vector<IncinerationGroup::Material> materials, Matrix4x4& modelToWorld, Vector4 tint, Vector4 emission_color, Vector3 start_pos_global, float start_time, float velocity, float max_radius);
	void addIncinerationInstance(const std::shared_ptr<Model>& model, const IncinerationGroup::Material& material, engine::ID id_matrix, Vector4 tint, Vector4 emission_color, Vector3 start_pos_global, float start_time, float velocity, float max_radius);
	void addIncinerationInstance(const std::shared_ptr<Model>& model, std::vector<IncinerationGroup::Material> materials, engine::ID id_matrix, Vector4 tint, Vector4 emission_color, Vector3 start_pos_global, float start_time, float velocity, float max_radius);
	void addEmissiveInstance(const std::shared_ptr<Model>& model, engine::ID id_matrix, const Vector4& color);
	void addDecalInstance(const DecalSystem::Material& material, Matrix4x4& decalToWorld, size_t id_model_attached);

	void remove(engine::ID id);

	void init();
	void deinit();
	void update();
 	void setRenderNormalVis(bool state) { m_render_normalVis = state; }
	void toggleRenderNormalVis() { m_render_normalVis = !m_render_normalVis; }
	HologramGroup& getHologramInstances() { return m_hologram; }
	NormalVisGroup& getNormalVisInstances() { return m_normalVis; }
	TextureOnlyGroup& getTextureOnlyInstances() { return m_textureOnly; }
	OpaqueGroup& getOpaqueInstances() { return m_opaques; }
	DissolutionGroup& getDissolutioInstances() { return m_dissolution; }
	IncinerationGroup& getIncinerationInstances() { return m_incinerations; }
	SolidVector<MeshInstanceID>& getEntitys() { return m_entitys; }
	SolidVector<ModelInstanceID>& getInstances() { return m_instances; }
private:
	MeshSystem() = default; 


	void addNormalVisInstance(const std::shared_ptr<Model>& model, Matrix4x4& modelToWorld, const Colorf& color);
	bool intersects(const math::Ray& ray, const Matrix4x4& obj, internal::ObjRef& outRef, const engine::Mesh& mesh, math::Intersection& outNearest) const;

	HologramGroup m_hologram;
	NormalVisGroup m_normalVis;
	TextureOnlyGroup m_textureOnly;
	OpaqueGroup m_opaques;
	DissolutionGroup m_dissolution;
	EmissiveGroup m_emissives;
	IncinerationGroup m_incinerations;

	DecalSystem m_decal_system;

	SolidVector<MeshInstanceID> m_entitys;
	SolidVector<ModelInstanceID> m_instances;

	bool m_render_normalVis;
};


#include "ModelManager.h"
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <functional>

#include "../../source/math/Vector3.h"
#include "../../source/math/Matrix4x4.h"
#include "../../source/math/Triangle.h"
#include "../../utils/ParallelExecutor.h"

void ModelManager::init()
{
}

void ModelManager::deinit()
{
	for (const auto model : m_loaded_models)
	{
		model.second->deinit();
	}
	m_loaded_models.clear();
}

std::shared_ptr<Model> ModelManager::getModel(const std::string& path)
{
	m_loaded_models.find(path);
	auto search = m_loaded_models.find(path);
	if (search != m_loaded_models.end())
		return search->second;
	else
		return assimpLoader(path);
}

std::shared_ptr<Model> ModelManager::getUnitCube()
{
	auto search = m_loaded_models.find(DefaultModels[UNIT_CUBE]);
	if (search != m_loaded_models.end())
		return search->second;
	else
	{
		initUnitCube();
		return m_loaded_models.at(DefaultModels[UNIT_CUBE]);
	}
}

std::shared_ptr<Model> ModelManager::getUnitSphere()
{
	auto search = m_loaded_models.find(DefaultModels[UNIT_SPHERE]);
	if (search != m_loaded_models.end())
		return search->second;
	else
	{
		initUnitSphere();
		return m_loaded_models.at(DefaultModels[UNIT_SPHERE]);
	}
}

std::shared_ptr<Model> ModelManager::getUnitSphereFlat()
{
	auto search = m_loaded_models.find(DefaultModels[UNIT_SPHERE_FLAT]);
	if (search != m_loaded_models.end())
		return search->second;
	else
	{
		initUnitSphereFlat();
		return m_loaded_models.at(DefaultModels[UNIT_SPHERE_FLAT]);
	}
}

ModelManager& ModelManager::instance()
{
	static ModelManager s_model_manager;
	return s_model_manager;
}

void ModelManager::addModel(const std::string& path, const Model&& model)
{
	//std::shared_ptr <Model> model_ptr(new Model(model));
	//m_loaded_models.emplace(path, model_ptr);
}

std::string ModelManager::replaceFilePath(const std::string& path1, const std::string& path2)
{
	size_t slashIndex = path1.find_last_of('\\');
	if (slashIndex == std::string::npos) {
		return changeFileExtension(path2);
	}

	return path1.substr(0, slashIndex) + "\\" + changeFileExtension(path2);
}

std::string ModelManager::changeFileExtension(const std::string& filename)
{
	size_t dotIndex = filename.find_last_of('.');
	if (dotIndex == std::string::npos) {
		return filename;
	}

	size_t slashIndex = filename.find_last_of('\\');
	if (slashIndex == std::string::npos) {
		return filename;
	}

	std::string extension = filename.substr(dotIndex + 1);
	if (extension != "TGA" && extension != "tga") {
		return filename;
	}

	return "dds" + filename.substr(slashIndex, dotIndex - slashIndex) + ".DDS";
}

std::shared_ptr<Model> ModelManager::assimpLoader(const std::string& path)
{
	// Load aiScene

	uint32_t flags = uint32_t(aiProcess_Triangulate | aiProcess_GenBoundingBoxes | aiProcess_ConvertToLeftHanded | aiProcess_CalcTangentSpace);
	// aiProcess_Triangulate - ensure that all faces are triangles and not polygonals, otherwise triangulare them
	// aiProcess_GenBoundingBoxes - automatically compute bounding box, though we could do that manually
	// aiProcess_ConvertToLeftHanded - Assimp assumes left-handed basis orientation by default, convert for Direct3D
	// aiProcess_CalcTangentSpace - computes tangents and bitangents, they are used in advanced lighting

	Assimp::Importer importer;
	const aiScene* assimpScene = importer.ReadFile(path, flags);
	DEV_ASSERT(assimpScene);

	uint32_t numMeshes = assimpScene->mNumMeshes;
	uint32_t numMaterials = assimpScene->mNumMaterials;

	// Load vertex data

	std::shared_ptr <Model> model(new Model);
	model->name = path;
	model->box = {};
	model->meshes.resize(numMeshes);
	model->albedo_texture_paths.resize(numMaterials);

	static_assert(sizeof(Vector3) == sizeof(aiVector3D), "Vector3 is not equal to aiVector3D");
	static_assert(sizeof(math::Triangle) == 3 * sizeof(uint32_t), "math::Triangle is not equal to 3 * uint32_t");

	for (uint32_t i = 0; i < numMeshes; ++i)
	{
		auto& srcMesh = assimpScene->mMeshes[i];
		auto& dstMesh = model->meshes[i];

		dstMesh.name = srcMesh->mName.C_Str();
		dstMesh.box.min = reinterpret_cast<Vector3&>(srcMesh->mAABB.mMin);
		dstMesh.box.max = reinterpret_cast<Vector3&>(srcMesh->mAABB.mMax);


		dstMesh.vertices.resize(srcMesh->mNumVertices);
		dstMesh.triangles.resize(srcMesh->mNumFaces);

		for (uint32_t v = 0; v < srcMesh->mNumVertices; ++v)
		{
			engine::Mesh::Vertex& vertex = dstMesh.vertices[v];
			vertex.pos = reinterpret_cast<Vector3&>(srcMesh->mVertices[v]);
			vertex.tc = reinterpret_cast<engine::Mesh::Float2&>(srcMesh->mTextureCoords[0][v]);
			vertex.normal = reinterpret_cast<Vector3&>(srcMesh->mNormals[v]);
			vertex.tangent = reinterpret_cast<Vector3&>(srcMesh->mTangents[v]);
			vertex.bitangent = reinterpret_cast<Vector3&>(srcMesh->mBitangents[v]) * -1.f; // Flip V
		}

		for (uint32_t f = 0; f < srcMesh->mNumFaces; ++f)
		{
			const auto& face = srcMesh->mFaces[f];
			DEV_ASSERT(face.mNumIndices == 3);
			dstMesh.triangles[f] = *reinterpret_cast<math::Triangle*>(face.mIndices);
		}

		dstMesh.updateOctree();
	}

	
	for (uint32_t i = 0; i < numMaterials; ++i)
	{
		aiString Path;
		assimpScene->mMaterials[i]->GetTexture(aiTextureType_DIFFUSE, 0, &Path, NULL, NULL, NULL, NULL, NULL);
		model->albedo_texture_paths[i] = replaceFilePath(path, Path.data);
	}

	// Recursively load mesh instances (meshToModel transformation matrices)

	std::function<void(aiNode*)> loadInstances;
	loadInstances = [&loadInstances, &model](aiNode* node)
	{
		const Matrix4x4 nodeToParent = reinterpret_cast<const Matrix4x4&>(node->mTransformation.Transpose());
		const Matrix4x4 parentToNode = nodeToParent.getInversedMatrix();

		// The same node may contain multiple meshes in its space, referring to them by indices
		for (uint32_t i = 0; i < node->mNumMeshes; ++i)
		{
			uint32_t meshIndex = node->mMeshes[i];
			model->meshes[meshIndex].instances.push_back(nodeToParent);
			model->meshes[meshIndex].instancesInv.push_back(parentToNode);
		}

		for (uint32_t i = 0; i < node->mNumChildren; ++i)
			loadInstances(node->mChildren[i]);
	};

	loadInstances(assimpScene->mRootNode);
	model->init();
	m_loaded_models.emplace(path, model);
	return model;
}

void ModelManager::initUnitCube()
{
	const uint32_t SIDES = 6;
	const uint32_t TRIS_PER_SIDE = 2;
	const uint32_t VERT_PER_SIZE = 3 * TRIS_PER_SIDE;

	std::shared_ptr <Model> model(new Model);
	model->name = "UNIT_CUBE";
	model->box = engine::BoundingBox::empty();

	model->meshes.push_back(engine::Mesh());
	engine::Mesh& mesh = model->meshes[model->meshes.size() - 1];
	mesh.name = "UNIT_CUBE";
	mesh.box = model->box;
	mesh.instances = { Matrix4x4::identity() };
	mesh.instancesInv = { Matrix4x4::identity() };

	mesh.vertices.resize(VERT_PER_SIZE * SIDES);
	engine::Mesh::Vertex* vertex = mesh.vertices.data();

	int sideMasks[6][3] =
	{
		{ 2, 1, 0 },
		{ 0, 1, 2 },
		{ 2, 1, 0 },
		{ 0, 1, 2 },
		{ 0, 2, 1 },
		{ 0, 2, 1 }
	};

	float sideSigns[6][3] =
	{
		{ +1, +1, +1 },
		{ -1, +1, +1 },
		{ -1, +1, -1 },
		{ +1, +1, -1 },
		{ +1, -1, -1 },
		{ +1, +1, +1 }
	};

	for (int side = 0; side < SIDES; ++side)
	{
		float left =  - 1.f;
		float right = 1.f;
		float bottom =  - 1.f;
		float top = 1.f;

		Vector3 quad[4] =
		{
			Vector3(left, bottom, 1.f),
			Vector3(left, top, 1.f),
			Vector3(right, bottom, 1.f),
			Vector3(right, top, 1.f)
		};

		vertex[0] = vertex[1] = vertex[2] = vertex[3] = engine::Mesh::Vertex::initial();

		auto setPos = [sideMasks, sideSigns](int side, engine::Mesh::Vertex& dst, const Vector3& pos)
		{
			dst.pos[sideMasks[side][0]] = pos.x() * sideSigns[side][0];
			dst.pos[sideMasks[side][1]] = pos.y() * sideSigns[side][1];
			dst.pos[sideMasks[side][2]] = pos.z() * sideSigns[side][2];
			dst.pos /= 2;
			dst.tc = { (pos.x() + 1) / 2, (pos.y() + 1) / 2 };
		};

		setPos(side, vertex[0], quad[0]);
		setPos(side, vertex[1], quad[1]);
		setPos(side, vertex[2], quad[2]);

		{
			Vector3 AB = vertex[1].pos - vertex[0].pos;
			Vector3 AC = vertex[2].pos - vertex[0].pos;

			engine::Mesh::Float2 deltaUV1 = vertex[1].tc - vertex[0].tc;
			engine::Mesh::Float2 deltaUV2 = vertex[2].tc - vertex[0].tc;

			float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);

			Vector3 tangent = (AB * deltaUV2.y - AC * deltaUV1.y) * r;
			Vector3 bitangent = (AC * deltaUV1.x - AB * deltaUV2.x) * r;

			vertex[0].normal = vertex[1].normal = vertex[2].normal = AB.cross(AC).normalized(); //right-handedness

			vertex[0].tangent = vertex[1].tangent = vertex[2].tangent = tangent;
			vertex[0].bitangent = vertex[1].bitangent = vertex[2].bitangent = -bitangent;
		}

		vertex += 3;

		setPos(side, vertex[0], quad[1]);
		setPos(side, vertex[1], quad[3]);
		setPos(side, vertex[2], quad[2]);

		{
			Vector3 AB = vertex[1].pos - vertex[0].pos;
			Vector3 AC = vertex[2].pos - vertex[0].pos;

			engine::Mesh::Float2 deltaUV1 = vertex[1].tc - vertex[0].tc;
			engine::Mesh::Float2 deltaUV2 = vertex[2].tc - vertex[0].tc;

			float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);

			Vector3 tangent = (AB * deltaUV2.y - AC * deltaUV1.y) * r;
			Vector3 bitangent = (AC * deltaUV1.x - AB * deltaUV2.x) * r;

			vertex[0].normal = vertex[1].normal = vertex[2].normal = AB.cross(AC).normalized(); // right-handedness

			vertex[0].tangent = vertex[1].tangent = vertex[2].tangent = tangent;
			vertex[0].bitangent = vertex[1].bitangent = vertex[2].bitangent = -bitangent;
		}

		vertex += 3;
	}

	

	mesh.init();
	mesh.updateOctree();
	model->init();
	m_loaded_models.emplace(DefaultModels[UNIT_CUBE], model);

}

void ModelManager::initUnitSphere()
{
	const uint32_t SIDES = 6;
	const uint32_t GRID_SIZE = 12;
	const uint32_t TRIS_PER_SIDE = GRID_SIZE * GRID_SIZE * 2;
	const uint32_t VERT_PER_SIZE = (GRID_SIZE + 1) * (GRID_SIZE + 1);

	std::shared_ptr <Model> model(new Model);
	model->name = "UNIT_SPHERE";
	model->box = engine::BoundingBox::empty();

	model->meshes.push_back(engine::Mesh());
	engine::Mesh& mesh = model->meshes[model->meshes.size() - 1];
	mesh.name = "UNIT_SPHERE";
	mesh.box = model->box;
	mesh.instances = { Matrix4x4::identity() };
	mesh.instancesInv = { Matrix4x4::identity() };

	mesh.vertices.resize(VERT_PER_SIZE* SIDES);
	engine::Mesh::Vertex* vertex = mesh.vertices.data();

	int sideMasks[6][3] =
	{
		{ 2, 1, 0 },
		{ 0, 1, 2 },
		{ 2, 1, 0 },
		{ 0, 1, 2 },
		{ 0, 2, 1 },
		{ 0, 2, 1 }
	};

	float sideSigns[6][3] =
	{
		{ +1, +1, +1 },
		{ -1, +1, +1 },
		{ -1, +1, -1 },
		{ +1, +1, -1 },
		{ +1, -1, -1 },
		{ +1, +1, +1 }
	};

	for (int side = 0; side < SIDES; ++side)
	{
		for (int row = 0; row < GRID_SIZE + 1; ++row)
		{
			for (int col = 0; col < GRID_SIZE + 1; ++col)
			{
				Vector3 v;
				v.ref_x() = col / float(GRID_SIZE) * 2.f - 1.f;
				v.ref_y() = row / float(GRID_SIZE) * 2.f - 1.f;
				v.ref_z() = 1.f;

				vertex[0] = engine::Mesh::Vertex::initial();

				vertex[0].pos[sideMasks[side][0]] = v.x() * sideSigns[side][0];
				vertex[0].pos[sideMasks[side][1]] = v.y() * sideSigns[side][1];
				vertex[0].pos[sideMasks[side][2]] = v.z() * sideSigns[side][2];
				vertex[0].normal = vertex[0].pos = vertex[0].pos.normalized();

				vertex += 1;
			}
		}
	}

	mesh.triangles.resize(TRIS_PER_SIDE * SIDES);
	auto* triangle = mesh.triangles.data();

	for (int side = 0; side < SIDES; ++side)
	{
		uint32_t sideOffset = VERT_PER_SIZE * side;

		for (int row = 0; row < GRID_SIZE; ++row)
		{
			for (int col = 0; col < GRID_SIZE; ++col)
			{
				triangle[0].indices[0] = sideOffset + (row + 0) * (GRID_SIZE + 1) + col + 0;
				triangle[0].indices[1] = sideOffset + (row + 1) * (GRID_SIZE + 1) + col + 0;
				triangle[0].indices[2] = sideOffset + (row + 0) * (GRID_SIZE + 1) + col + 1;

				triangle[1].indices[0] = sideOffset + (row + 1) * (GRID_SIZE + 1) + col + 0;
				triangle[1].indices[1] = sideOffset + (row + 1) * (GRID_SIZE + 1) + col + 1;
				triangle[1].indices[2] = sideOffset + (row + 0) * (GRID_SIZE + 1) + col + 1;

				triangle += 2;
			}
		}
	}

	mesh.updateOctree();

	mesh.init();
	model->init();
	m_loaded_models.emplace(DefaultModels[UNIT_SPHERE], model);
}

void ModelManager::initUnitSphereFlat()
{
	const uint32_t SIDES = 6;
	const uint32_t GRID_SIZE = 12;
	const uint32_t TRIS_PER_SIDE = GRID_SIZE * GRID_SIZE * 2;
	const uint32_t VERT_PER_SIZE = 3 * TRIS_PER_SIDE;

	std::shared_ptr <Model> model(new Model);
	model->name = "UNIT_SPHERE_FLAT";
	model->box = engine::BoundingBox::empty();

	model->meshes.push_back(engine::Mesh());
	engine::Mesh& mesh = model->meshes[model->meshes.size() - 1];
	mesh.name = "UNIT_SPHERE_FLAT";
	mesh.box = model->box;
	mesh.instances = { Matrix4x4::identity() };
	mesh.instancesInv = { Matrix4x4::identity() };

	mesh.vertices.resize(VERT_PER_SIZE * SIDES);
	engine::Mesh::Vertex* vertex = mesh.vertices.data();

	int sideMasks[6][3] =
	{
		{ 2, 1, 0 },
		{ 0, 1, 2 },
		{ 2, 1, 0 },
		{ 0, 1, 2 },
		{ 0, 2, 1 },
		{ 0, 2, 1 }
	};

	float sideSigns[6][3] =
	{
		{ +1, +1, +1 },
		{ -1, +1, +1 },
		{ -1, +1, -1 },
		{ +1, +1, -1 },
		{ +1, -1, -1 },
		{ +1, +1, +1 }
	};

	for (int side = 0; side < SIDES; ++side)
	{
		for (int row = 0; row < GRID_SIZE; ++row)
		{
			for (int col = 0; col < GRID_SIZE; ++col)
			{
				float left = (col + 0) / float(GRID_SIZE) * 2.f - 1.f;
				float right = (col + 1) / float(GRID_SIZE) * 2.f - 1.f;
				float bottom = (row + 0) / float(GRID_SIZE) * 2.f - 1.f;
				float top = (row + 1) / float(GRID_SIZE) * 2.f - 1.f;

				Vector3 quad[4] =
				{
					Vector3( left, bottom, 1.f ),
					Vector3( left, top, 1.f ),
					Vector3( right, bottom, 1.f ),
					Vector3( right, top, 1.f )
				};

				vertex[0] = vertex[1] = vertex[2] = vertex[3] = engine::Mesh::Vertex::initial();

				auto setPos = [sideMasks, sideSigns](int side, engine::Mesh::Vertex& dst, const Vector3& pos)
				{
					dst.pos[sideMasks[side][0]] = pos.x() * sideSigns[side][0];
					dst.pos[sideMasks[side][1]] = pos.y() * sideSigns[side][1];
					dst.pos[sideMasks[side][2]] = pos.z() * sideSigns[side][2];
					dst.pos = dst.pos.normalized();
				};

				setPos(side, vertex[0], quad[0]);
				setPos(side, vertex[1], quad[1]);
				setPos(side, vertex[2], quad[2]);

				{
					Vector3 AB = vertex[1].pos - vertex[0].pos;
					Vector3 AC = vertex[2].pos - vertex[0].pos;
					vertex[0].normal = vertex[1].normal = vertex[2].normal = AB.cross(AC).normalized(); // right-handedness
				}

				vertex += 3;

				setPos(side, vertex[0], quad[1]);
				setPos(side, vertex[1], quad[3]);
				setPos(side, vertex[2], quad[2]);

				{
					Vector3 AB = vertex[1].pos - vertex[0].pos;
					Vector3 AC = vertex[2].pos - vertex[0].pos;
					vertex[0].normal = vertex[1].normal = vertex[2].normal = AB.cross(AC).normalized(); // right-handedness
				}

				vertex += 3;
			}
		}
	}

	mesh.updateOctree();

	mesh.init();
	model->init();
	m_loaded_models.emplace(DefaultModels[UNIT_SPHERE_FLAT], model);

}


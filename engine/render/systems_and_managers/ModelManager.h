#pragma once
#include <unordered_map>
#include <string>
#include <memory>
#include "../../source/Model.h"

class ModelManager
{
public:
	ModelManager(const ModelManager& other) = delete;

	void init();
	void deinit();

	void operator=(const ModelManager& other) = delete;
	std::shared_ptr<Model> getModel(const std::string& path);
	std::shared_ptr<Model> getUnitCube();
	std::shared_ptr<Model> getUnitSphere();
	std::shared_ptr<Model> getUnitSphereFlat();
	static ModelManager& instance();
private:
	ModelManager() = default;

	enum DefaultModels
	{
		UNIT_CUBE = 0,
		UNIT_SPHERE,
		UNIT_SPHERE_FLAT
	};

	void addModel(const std::string& path, const Model&& model);
	std::string replaceFilePath(const std::string& path1, const std::string& path2);
	std::string changeFileExtension(const std::string& filename);
	std::shared_ptr<Model> assimpLoader(const std::string& path);

	void initUnitCube();
	void initUnitSphere();
	void initUnitSphereFlat();

	std::unordered_map <std::string, std::shared_ptr<Model>> m_loaded_models;

	std::array<std::string, 3> DefaultModels =
	{
		"unitCube",
		"unitSphere",
		"unitSphereFlat"
	};
};


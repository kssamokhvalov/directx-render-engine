#include "NoiseCapture.h"
#include "../d3d.h"
#include "../d3d_utils/ComputeShader.h"
#include "../d3d_utils/UnorderedAccessView.h"
#include "./TextureManager.h"

std::shared_ptr<Texture> NoiseCapture::computePerlinNoise3D(const std::string& path)
{
	//Loading shaders
	ComputeShader compute_shader;
	compute_shader.loadComputeShader(L"../engine/shaders/utils/ComputePerlinShader.hlsl");

	UINT resolution = 256;

	//Init texture
	D3D11_TEXTURE3D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));

	textureDesc.Width = resolution;
	textureDesc.Height = resolution;
	textureDesc.Depth = resolution;
	textureDesc.MipLevels = 1;
	textureDesc.Format = DXGI_FORMAT_R32_FLOAT;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
	textureDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	textureDesc.MiscFlags = 0;

	ID3D11Texture3D* tex;
	engine::s_device->CreateTexture3D(&textureDesc, NULL, &tex);

	//Init RenderTaret
	UnorderedAccessView tex_UAV;
	D3D11_UNORDERED_ACCESS_VIEW_DESC desc;
	desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE3D;
	desc.Format = DXGI_FORMAT_R32_FLOAT;
	desc.Texture3D.MipSlice = 0;
	desc.Texture3D.FirstWSlice = 0;
	desc.Texture3D.WSize  = -1;
	tex_UAV.init(tex, &desc);

	//Render
	compute_shader.bind();

	ID3D11UnorderedAccessView* const UAV = tex_UAV.getUnorderedAccessView();
	ID3D11UnorderedAccessView* const UAV_NULL = nullptr;
	UINT value = (-1);
	engine::s_devcon->CSSetUnorderedAccessViews(0, 1, &UAV, &value);

	engine::s_devcon->Dispatch(resolution / 4, resolution / 4, resolution / 4);

	

	//Saving texture
	std::shared_ptr<Texture> perlin_noise(new Texture);
	perlin_noise->init(tex);

	std::wstring w_path(path.begin(), path.end());
	TextureManager::instance().save(w_path, *perlin_noise.get(), TextureManager::FileFormat::BC4_UNSIGNED, false);

	//Deinit
	tex_UAV.deinit();
	compute_shader.release();

	return perlin_noise;
}


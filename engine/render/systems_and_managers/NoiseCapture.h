#pragma once
#include <memory>
#include "../d3d_utils/Texture.h"


class NoiseCapture
{
public:
	static std::shared_ptr<Texture> computePerlinNoise3D(const std::string& path);
private:
};


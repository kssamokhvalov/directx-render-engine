#include "ParticleSystem.h"
#include "TextureManager.h"
#include "TransformSystem.h"
#include <algorithm>


void ParticleSystem::init()
{
	m_BBF = TextureManager::instance().getTexture("assets/textures/smoke/smoke_BotBF.dds");
	m_RLT = TextureManager::instance().getTexture("assets/textures/smoke/smoke_RLT.dds");
	m_EMVA = TextureManager::instance().getTexture("assets/textures/smoke/smoke_EMVA.dds");
	m_spark = TextureManager::instance().getTexture("assets/textures/spark/spark.dds");
	m_vertex_shader.loadVertexShader(L"../engine/shaders/ParticalShaders/VertexParticalShader.hlsl");
	{
		D3D11_INPUT_ELEMENT_DESC polygonLayout[] =
		{
			{"COLOR",  0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(ParticleCPU, tint), D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"COLOR",  1, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(ParticleCPU, emission_color), D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"SPEED",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(ParticleCPU, speed), D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"POSITION",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(ParticleCPU, position), D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"ROTATION",  0, DXGI_FORMAT_R32_FLOAT, 0, offsetof(ParticleCPU, rotation_angle), D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"START_TIME",  0, DXGI_FORMAT_R32_FLOAT, 0, offsetof(ParticleCPU, start_time), D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"DURATION",  0, DXGI_FORMAT_R32_FLOAT, 0, offsetof(ParticleCPU, life_time), D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"SIZE",  0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(ParticleCPU, size_x), D3D11_INPUT_PER_INSTANCE_DATA, 1}
		};
		m_input_layout.loadInputLayout(m_vertex_shader.getShaderBlob(), polygonLayout, std::size(polygonLayout));
	}

	m_pixel_shader.loadPixelShader(L"../engine/shaders/ParticalShaders/PixelParticalShader.hlsl");
	m_update_prticals_shader.loadComputeShader(L"../engine/shaders/GPUParticalShaders/UpdateParticalsGPUShader.hlsl");
	m_update_range_shader.loadComputeShader(L"../engine/shaders/GPUParticalShaders/ComputeUpdateRangeBufferShader.hlsl");

	m_vertex_shader_GPU_particles.loadVertexShader(L"../engine/shaders/GPUParticalShaders/VertexParticalShader.hlsl");
	m_pixel_shader_GPU_particles.loadPixelShader(L"../engine/shaders/GPUParticalShaders/PixelParticalShader.hlsl");

	m_vertex_shader_GPU_particles_light.loadVertexShader(L"../engine/shaders/GPUParticalShaders/ParticalLight/VertexParticalLightShader.hlsl");
	m_pixel_shader_GPU_particles_light.loadPixelShader(L"../engine/shaders/GPUParticalShaders/ParticalLight/PixelParticalLightShader.hlsl");

	m_GPU_particals.init();
}

void ParticleSystem::deinit()
{
	m_BBF.reset();
	m_RLT.reset();
	m_EMVA.reset();
	m_spark.reset();

	m_GPU_particals.deint();

	m_vertex_shader_GPU_particles.release();
	m_pixel_shader_GPU_particles.release();

	m_vertex_shader_GPU_particles_light.release();
	m_pixel_shader_GPU_particles_light.release();

	m_vertex_shader.release();
	m_pixel_shader.release();
	m_input_layout.release();
	m_update_prticals_shader.release();
	m_update_range_shader.release();
	m_instanceBuffer.deinit();
}

void ParticleSystem::addSmokeEmitter(Vector3 position, float spawn_rate_per_second, float spawn_circle_radius, float life_time_particle, Vector4 spawned_particle_color, Vector3 emission_color)
{
	m_emitters.push_back({ position, spawned_particle_color, emission_color, spawn_rate_per_second,  spawn_circle_radius,  life_time_particle,  -1.0f / spawn_rate_per_second});

	updateInstanceBuffers();
}

void ParticleSystem::addSmokeEmitter(Vector3 position, float spawn_rate_per_second, float spawn_circle_radius, float life_time_particle, Vector4 spawned_particle_color, Vector3 emission_color, engine::ID id_modelToWorld)
{
	m_emitters.push_back({ position, spawned_particle_color, emission_color, spawn_rate_per_second,  spawn_circle_radius,  life_time_particle,  -1.0f / spawn_rate_per_second, id_modelToWorld});

	updateInstanceBuffers();
}

void ParticleSystem::update(float curr_time)
{
	for (auto& emitter : m_emitters)
	{
		emitter.position = TransformSystem::instance().getCameraCenteredMatrix(emitter.id_modelToWorld).rowAsVector3(3);
		for (int i = 0; i < emitter.particles.size(); ++i)
		{
			ParticleCPU& particle = emitter.particles[i];

			if (particle.start_time + particle.life_time < curr_time)
			{
				TransformSystem::instance().TransformSystem::instance().deleteMatrix(particle.id_modelToWorld);
				emitter.particles.erase(emitter.particles.begin() + i);
				continue;
			}

			particle.position = TransformSystem::instance().getCameraCenteredMatrix(particle.id_modelToWorld).rowAsVector3(3) +
				particle.speed * (curr_time - particle.start_time) / particle.life_time;

			if (particle.start_time + SHORT_PERIOD * particle.life_time > curr_time)
			{
				particle.tint.ref_w() = 1.0f - (particle.start_time + SHORT_PERIOD * particle.life_time - curr_time) / (SHORT_PERIOD * particle.life_time);
				continue;
			}

			if (particle.start_time + (1 - SHORT_PERIOD) * particle.life_time < curr_time)
			{
				particle.tint.ref_w() = 1.0f - (curr_time - (particle.start_time + (1 - SHORT_PERIOD) * particle.life_time)) / (SHORT_PERIOD * particle.life_time);
				continue;
			}

			particle.tint.ref_w() = 1.0f;
		}

		if (emitter.time_last_spawned_particle + 1.0f / emitter.spawn_rate_per_second < curr_time)
		{
			Vector3 position = Vector3((double)rand() / (RAND_MAX), 0.0f, (double)rand() / (RAND_MAX));
			position.makeUnitVector();
			position *= emitter.spawn_circle_radius;
			position += TransformSystem::instance().getMatrix(emitter.id_modelToWorld).rowAsVector3(3);
			Matrix4x4 modelToWorld = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, 0.0f, 0.0f, 0.0f), position);
			engine::ID id_matrix = TransformSystem::instance().addMatrix(modelToWorld);

			Vector3 speed = Vector3(VERTICAL_SPEED * (double)rand() / (RAND_MAX), VERTICAL_SPEED, VERTICAL_SPEED * (double)rand() / (RAND_MAX));

			emitter.particles.push_back({ emitter.spawned_particle_color,
				emitter.emission_color,
				position,
				speed,
				1.0f,
				SIZE_PARTICLE,
				SIZE_PARTICLE,
				curr_time,
				emitter.life_time_particle,
				id_matrix });

			emitter.time_last_spawned_particle = curr_time;
		}

		std::sort(emitter.particles.begin(), emitter.particles.end(), [](const ParticleCPU& first, const ParticleCPU& second)
			{
				return first.position.lenght() > second.position.lenght();
			});
	}


	updateInstanceBuffers();
}

void ParticleSystem::updateInstanceBuffers()
{
	uint32_t totalInstances = 0;
	for (auto& perEmitter : m_emitters)
		totalInstances += uint32_t(perEmitter.particles.size());

	if (totalInstances == 0)
		return;

	m_instanceBuffer.init(totalInstances, D3D11_USAGE_DYNAMIC);

	auto mapping = m_instanceBuffer.map();
	ParticleCPU* dst = static_cast<ParticleCPU*>(mapping.pData);

	uint32_t copiedNum = 0;
	for (auto& perEmitter : m_emitters)
	{
		auto& instances = perEmitter.particles;

		uint32_t numModelInstances = instances.size();
		for (uint32_t index = 0; index < numModelInstances; ++index)
		{
			instances[index].position = TransformSystem::instance().getCameraCenteredMatrix(instances[index].id_modelToWorld).rowAsVector3(3);
			dst[copiedNum++] = instances[index];
		}
	}

	m_instanceBuffer.unmap();
}

void ParticleSystem::updateParticlesGPU()
{
	m_update_prticals_shader.bind();
	ID3D11UnorderedAccessView* const UAVs[2] = { m_GPU_particals.getStructuredBufferUAV(),
												m_GPU_particals.getRangeBufferUAV()};
	ID3D11UnorderedAccessView* const UAVs_NULL[2] = { nullptr,
												nullptr };
	UINT value = (-1);
	engine::s_devcon->CSSetUnorderedAccessViews(0, 2, UAVs, &value);
	engine::s_devcon->Dispatch(1, 1, 1);

	m_update_range_shader.bind();
	engine::s_devcon->Dispatch(1, 1, 1);

	engine::s_devcon->CSSetUnorderedAccessViews(0, 2, UAVs_NULL, nullptr);
}

void ParticleSystem::render()
{
	if (m_instanceBuffer.size() == 0)
		return;

	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


	m_vertex_shader.bind();
	m_pixel_shader.bind();
	m_input_layout.bind();

	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	m_instanceBuffer.bind(0);

	m_BBF->bind(14, TypePixelShader);
	m_RLT->bind(15, TypePixelShader);
	m_EMVA->bind(16, TypePixelShader);

	uint32_t renderedInstances = 0;
	for (const auto& perEmitter : m_emitters)
	{
		uint32_t numInstances = uint32_t(perEmitter.particles.size());
		engine::s_devcon->DrawInstanced(6 , numInstances, 0, renderedInstances);
		renderedInstances += numInstances;
	}
}

void ParticleSystem::renderParticlesGPU()
{
	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	m_vertex_shader_GPU_particles.bind();
	m_pixel_shader_GPU_particles.bind();

	engine::s_devcon->IASetInputLayout(nullptr);
	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	m_GPU_particals.getStructuredBufferSR().bind(27, TypeVertexShader);
	m_GPU_particals.getRangeBufferSR().bind(28, TypeVertexShader);

	m_spark->bind(27, TypePixelShader);

	using RangeDescription = RingBuffer::RangeDescription;

	m_vertex_shader_GPU_particles_light.bind();
	m_pixel_shader_GPU_particles_light.bind();
	engine::s_devcon->DrawInstancedIndirect(m_GPU_particals.getRangeBuffer(), offsetof(RangeDescription, indirect_arg));

	m_vertex_shader_GPU_particles.bind();
	m_pixel_shader_GPU_particles.bind();
	engine::s_devcon->DrawInstancedIndirect(m_GPU_particals.getRangeBuffer(), offsetof(RangeDescription, indirect_arg));
	
	m_GPU_particals.getStructuredBufferSR().unbind();
	m_GPU_particals.getRangeBufferSR().unbind();
	m_spark->unbind();
}

ParticleSystem& ParticleSystem::instance()
{
	static ParticleSystem s_partcal_system;
	return s_partcal_system;
}

#pragma once
#include <memory>
#include "../d3d.h"
#include "../d3d_utils/PixelShader.h"
#include "../d3d_utils/VertexShader.h"
#include "../d3d_utils/ShaderResurce.h"
#include "../d3d_utils/ComputeShader.h"
#include "../d3d_utils/UniformBuffer.h"
#include "../d3d_utils/VertexBuffer.h"
#include "../d3d_utils/InputLayout.h"
#include "../d3d_utils/Texture.h"
#include "../d3d_utils/RingBuffer.h"
#include "../../source/math/Vector3.h"
#include "../../source/math/Vector4.h"
#include "../../global.h"
#include "../../utils/ringbuffer.hpp"

class ParticleSystem
{
public:

	struct ParticleCPU
	{
		Vector4 tint;
		Vector3 emission_color;
		Vector3 position;
		Vector3 speed;
		float rotation_angle;
		float size_x;
		float size_y;
		float start_time;
		float life_time;
		engine::ID id_modelToWorld;
	};


	struct ParticleGPU
	{
		Vector4 tint;
		Vector3 emission_color;
		Vector3 position;
		Vector3 speed;
		float start_time;
		float life_time;
		UINT32 active;
	};



	struct Emitter
	{
		Vector3 position;
		Vector4 spawned_particle_color;
		Vector3 emission_color;
		float spawn_rate_per_second;
		float spawn_circle_radius;
		float life_time_particle;
		float time_last_spawned_particle;
		engine::ID id_modelToWorld;
		std::vector<ParticleCPU> particles;
	};

	using RingBuffer = RingBuffer<ParticleGPU, 32>;

	const float SHORT_PERIOD = 0.10f;
	const float VERTICAL_SPEED = 0.35f;
	const float SIZE_PARTICLE = 4.35f;

	ParticleSystem(const ParticleSystem& other) = delete;

	void init();
	void deinit();

	void addSmokeEmitter(Vector3 position,
		float spawn_rate_per_second,
		float spawn_circle_radius,
		float life_time_particle,
		Vector4 spawned_particle_color,
		Vector3 emission_color);

	void addSmokeEmitter(Vector3 position, 
		float spawn_rate_per_second, 
		float spawn_circle_radius, 
		float life_time_particle, 
		Vector4 spawned_particle_color, 
		Vector3 emission_color, 
		engine::ID id_modelToWorld);

	void update(float curr_time);
	void updateInstanceBuffers();
	void updateParticlesGPU();
	void render();
	void renderParticlesGPU();
	RingBuffer& getGPUParticals() { return m_GPU_particals; }
	static ParticleSystem& instance();
private:
	ParticleSystem() = default;

	std::shared_ptr<Texture> m_BBF;
	std::shared_ptr<Texture> m_RLT;
	std::shared_ptr<Texture> m_EMVA;
	std::vector<Emitter> m_emitters;

	std::shared_ptr<Texture> m_spark;

	VertexShader m_vertex_shader;
	PixelShader m_pixel_shader;
	InputLayout m_input_layout;
	VertexBuffer<ParticleCPU> m_instanceBuffer;

	VertexShader m_vertex_shader_GPU_particles;
	PixelShader m_pixel_shader_GPU_particles;

	VertexShader m_vertex_shader_GPU_particles_light;
	PixelShader m_pixel_shader_GPU_particles_light;

	RingBuffer m_GPU_particals;
	ComputeShader m_update_prticals_shader;
	ComputeShader m_update_range_shader;
};


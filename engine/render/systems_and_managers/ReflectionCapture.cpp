#include "ReflectionCapture.h"
#include "RenderManager.h"

void ReflectionCapture::init()
{
}

void ReflectionCapture::deinit()
{
}

ReflectionCapture& ReflectionCapture::instance()
{
	static ReflectionCapture s_reflection_capture;
	return s_reflection_capture;
}

std::string ReflectionCapture::appendToFileName(const std::string& file_path, const std::string& append_str)
{
	std::string fileName, ext;
	size_t dotPos = file_path.find_last_of('.');
	if (dotPos != std::string::npos) {
		fileName = file_path.substr(0, dotPos);
		ext = file_path.substr(dotPos);
	}
	else {
		fileName = file_path;
	}
	return fileName + append_str + ext;
}


std::shared_ptr<Texture> ReflectionCapture::computeDiffuseIrradianCubeMap(std::shared_ptr<Texture> original_tex, const std::string& path)
{
	//Loading shaders
	VertexShader vertex_shader;
	PixelShader pixel_shader;
	vertex_shader.loadVertexShader(L"../engine/shaders/DiffuseIrradianceShaders/VertexDiffuseIrradianceShader.hlsl");
	pixel_shader.loadPixelShader(L"../engine/shaders/DiffuseIrradianceShaders/PixelDiffuseIrradianceShader.hlsl");

	//Init texture
	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));

	textureDesc.Width = 8;
	textureDesc.Height = 8;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 6;
	textureDesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	textureDesc.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE;

	ID3D11Texture2D* tex;
	engine::s_device->CreateTexture2D(&textureDesc, NULL, &tex);

	D3D11_VIEWPORT viewport = { 0.0f, 0.0f, textureDesc.Width, textureDesc.Height, 0.0f, 1.0f };
	engine::s_devcon->RSSetViewports(1, &viewport);

	//Init ReflectionCaptureBuffer
	ReflectionCaptureBuffer RCBuffer;
	RCBuffer.width_tex = textureDesc.Width;
	RCBuffer.height_tex = textureDesc.Height;
	UniformBuffer<ReflectionCaptureBuffer>  RCBuffer_GPU;
	RCBuffer_GPU.init(D3D11_USAGE_DYNAMIC);
	RCBuffer_GPU.bind(5, TypeShader::TypeVertexShader);
	RCBuffer_GPU.bind(5, TypeShader::TypePixelShader);

	//Init RenderTarets
	RenderTarget cube_map_face_RTs[6];

	D3D11_RENDER_TARGET_VIEW_DESC desc;
	desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
	desc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	desc.Texture2DArray.ArraySize = 1;
	desc.Texture2DArray.MipSlice = 0;

	//Render
	RenderManager::instance().setupBeforeRender(); //sets samplers

	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	original_tex->bind(0, TypeShader::TypePixelShader);
	vertex_shader.bind();
	pixel_shader.bind();

	engine::s_devcon->IASetInputLayout(nullptr);
	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	for (int i = 0; i < 6; ++i)
	{
		desc.Texture2DArray.FirstArraySlice = i;
		cube_map_face_RTs[i].init(tex, &desc);

		ID3D11RenderTargetView* const renderTV = cube_map_face_RTs[i].getFramebufferView();
		engine::s_devcon->OMSetRenderTargets(1, &renderTV, nullptr);

		RCBuffer.BL = DIRECTIONS[i * 3];
		RCBuffer.TL = DIRECTIONS[i * 3 + 1];
		RCBuffer.BR = DIRECTIONS[i * 3 + 2];
		RCBuffer_GPU.update(RCBuffer);

		engine::s_devcon->Draw(3, 0);
	}

	//Saving texture
	std::shared_ptr<Texture> reflection_map(new Texture);
	reflection_map->init(tex);
	
	std::string extended_path = appendToFileName(path, DIFFUSE_IRRADIAN_EXPANDING);
	std::wstring w_path(extended_path.begin(), extended_path.end());
	TextureManager::instance().save(w_path, *reflection_map.get(), TextureManager::FileFormat::BC6_UNSIGNED, false);

	//Deinit
	vertex_shader.release();
	pixel_shader.release();
	RCBuffer_GPU.deinit();
	for (int i = 0; i < 6; ++i)
	{
		cube_map_face_RTs[i].deinit();
	}

	return reflection_map;
}


std::shared_ptr<Texture> ReflectionCapture::computeSpecularIrradianCubeMap(std::shared_ptr<Texture> original_tex, const std::string& path)
{
	//Loading shaders
	VertexShader vertex_shader;
	PixelShader pixel_shader;
	vertex_shader.loadVertexShader(L"../engine/shaders/SpecularIrradianceShaders/VertexSpecularIrradianceShader.hlsl");
	pixel_shader.loadPixelShader(L"../engine/shaders/SpecularIrradianceShaders/PixelSpecularIrradianceShader.hlsl");

	//Init texture
	D3D11_TEXTURE2D_DESC resourceDesc;
	ID3D11Texture2D* pTextureInterface = nullptr;
	original_tex->getTexture()->ptr()->QueryInterface<ID3D11Texture2D>(&pTextureInterface);
	pTextureInterface->GetDesc(&resourceDesc);
	pTextureInterface->Release();

	UINT height_tex = resourceDesc.Height;
	UINT width_tex = resourceDesc.Width;
	float mip_levels = 1 + ceil(fmaxf(log2f(height_tex), log2f(width_tex)));

	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));

	textureDesc.Width = width_tex;
	textureDesc.Height = height_tex;
	textureDesc.MipLevels = mip_levels;
	textureDesc.ArraySize = 6;
	textureDesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	textureDesc.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE;

	ID3D11Texture2D* tex;
	engine::s_device->CreateTexture2D(&textureDesc, NULL, &tex);

	//Init ReflectionCaptureBuffer
	ReflectionCaptureBuffer RCBuffer;
	UniformBuffer<ReflectionCaptureBuffer>  constantBufferReflectionCapture;
	constantBufferReflectionCapture.init(D3D11_USAGE_DYNAMIC);
	constantBufferReflectionCapture.bind(5, TypeShader::TypeVertexShader);
	constantBufferReflectionCapture.bind(5, TypeShader::TypePixelShader);
	
	//Init RenderTarets
	RenderTarget cube_map_face_RTs[6];

	D3D11_RENDER_TARGET_VIEW_DESC desc;
	desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
	desc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	desc.Texture2DArray.ArraySize = 1;

	//Render
	RenderManager::instance().setupBeforeRender(); //sets samplers

	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	original_tex->bind(0, TypeShader::TypePixelShader);
	vertex_shader.bind();
	pixel_shader.bind();

	engine::s_devcon->IASetInputLayout(nullptr);
	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	RCBuffer.width_tex = width_tex;
	RCBuffer.height_tex = height_tex;

	for (int i = 0; i < 6; ++i)
	{
		for (int mipLvl = 0; mipLvl < mip_levels; ++mipLvl)
		{

			desc.Texture2DArray.MipSlice = mipLvl;
			desc.Texture2DArray.FirstArraySlice = i;
			cube_map_face_RTs[i].init(tex, &desc);

			ID3D11RenderTargetView* const renderTV = cube_map_face_RTs[i].getFramebufferView();
			engine::s_devcon->OMSetRenderTargets(1, &renderTV, nullptr);

			D3D11_VIEWPORT viewport = { 0.0f, 0.0f,  width_tex / powf(2.0f, mipLvl), height_tex / powf(2.0f, mipLvl), 0.0f, 1.0f };
			engine::s_devcon->RSSetViewports(1, &viewport);

			RCBuffer.BL = DIRECTIONS[i * 3];
			RCBuffer.TL = DIRECTIONS[i * 3 + 1];
			RCBuffer.BR = DIRECTIONS[i * 3 + 2];
			RCBuffer.roughness = mipLvl / (mip_levels - 1);
			constantBufferReflectionCapture.update(RCBuffer);

			engine::s_devcon->Draw(3, 0);
			cube_map_face_RTs[i].deinit();
		}
	}

	//Saving texture
	std::shared_ptr<Texture> reflection_map(new Texture);
	reflection_map->init(tex);

	std::string extended_path = appendToFileName(path, SPECULAR_IRRADIAN_EXPANDING);
	std::wstring w_path(extended_path.begin(), extended_path.end());
	TextureManager::instance().save(w_path, *reflection_map.get(), TextureManager::FileFormat::BC6_UNSIGNED, false);

	//Deinit
	vertex_shader.release();
	pixel_shader.release();
	constantBufferReflectionCapture.deinit();

	return reflection_map;
}


std::shared_ptr<Texture> ReflectionCapture::computeSpecularReflectanceCubeMap(const std::string& path)
{
	//Loading shaders
	VertexShader vertex_shader;
	PixelShader pixel_shader;
	vertex_shader.loadVertexShader(L"../engine/shaders/SpecularReflectanceShaders/VertexSpecularReflectanceShader.hlsl");
	pixel_shader.loadPixelShader(L"../engine/shaders/SpecularReflectanceShaders/PixelSpecularReflectanceShader.hlsl");

	UINT height_tex = 1024;
	UINT width_tex = 1024;

	//Init texture
	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));

	textureDesc.Width = width_tex;
	textureDesc.Height = height_tex;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R32G32_FLOAT;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	textureDesc.MiscFlags = 0;

	ID3D11Texture2D* tex;
	engine::s_device->CreateTexture2D(&textureDesc, NULL, &tex);

	//Init ReflectionCaptureBuffer
	ReflectionCaptureBuffer RCBuffer;
	RCBuffer.width_tex = width_tex;
	RCBuffer.height_tex = height_tex;

	UniformBuffer<ReflectionCaptureBuffer>  constantBufferReflectionCapture;
	constantBufferReflectionCapture.init(D3D11_USAGE_DYNAMIC);
	constantBufferReflectionCapture.bind(5, TypeShader::TypeVertexShader);
	constantBufferReflectionCapture.bind(5, TypeShader::TypePixelShader);
	constantBufferReflectionCapture.update(RCBuffer);

	//Init RenderTaret
	RenderTarget tex_RT;
	D3D11_RENDER_TARGET_VIEW_DESC desc;
	desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	desc.Format = DXGI_FORMAT_R32G32_FLOAT;
	desc.Texture2D.MipSlice = 0;
	tex_RT.init(tex, &desc);

	//Render
	RenderManager::instance().setupBeforeRender(); //sets samplers

	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	vertex_shader.bind();
	pixel_shader.bind();

	engine::s_devcon->IASetInputLayout(nullptr);
	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	ID3D11RenderTargetView* const renderTV = tex_RT.getFramebufferView();
	engine::s_devcon->OMSetRenderTargets(1, &renderTV, nullptr);

	D3D11_VIEWPORT viewport = { 0.0f, 0.0f, RCBuffer.width_tex, RCBuffer.height_tex, 0.0f, 1.0f };
	engine::s_devcon->RSSetViewports(1, &viewport);

	engine::s_devcon->Draw(3, 0);
	tex_RT.deinit();

	//Saving texture
	std::shared_ptr<Texture> reflection_map(new Texture);
	reflection_map->init(tex);

	std::string extended_path = appendToFileName(path, SPECULAR_REFLECTANCE_EXPANDING);
	std::wstring w_path(extended_path.begin(), extended_path.end());
	TextureManager::instance().save(w_path, *reflection_map.get(), TextureManager::FileFormat::BC5_UNSIGNED, false);

	//Deinit
	vertex_shader.release();
	pixel_shader.release();
	constantBufferReflectionCapture.deinit();
	tex_RT.deinit();

	return reflection_map;
}

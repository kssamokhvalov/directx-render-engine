#pragma once
#include <memory>
#include "../d3d.h"
#include "../d3d_utils/PixelShader.h"
#include "../d3d_utils/VertexShader.h"
#include "../d3d_utils/ShaderResurce.h"
#include "../d3d_utils/RenderTarget.h"
#include "../d3d_utils/UniformBuffer.h"
#include "../d3d_utils/Texture.h"

class ReflectionCapture
{
public:
	struct ReflectionCaptureBuffer
	{
		Vector3 BL;
		float null0; // must  be alwase equal null
		Vector3 TL;
		float null1;  // must  be alwase equal null
		Vector3 BR;
		float null2; // must  be alwase equal null
		float width_tex;
		float height_tex;
		float roughness;
		float _pad;
	};

	const std::string DIFFUSE_IRRADIAN_EXPANDING = "_DiffuseIrradian";
	const std::string SPECULAR_IRRADIAN_EXPANDING = "_SpecularIrradian";
	const std::string SPECULAR_REFLECTANCE_EXPANDING = "_SpecularReflectance";

	std::shared_ptr<Texture> computeDiffuseIrradianCubeMap(std::shared_ptr<Texture> original_tex, const std::string& path);
	std::shared_ptr<Texture> computeSpecularIrradianCubeMap(std::shared_ptr<Texture> original_tex, const std::string& path);
	std::shared_ptr<Texture> computeSpecularReflectanceCubeMap(const std::string& path);
	void init();
	void deinit();

	ReflectionCapture(const ReflectionCapture& other) = delete;
	void operator=(const ReflectionCapture& other) = delete;

	static std::string appendToFileName(const std::string& file_path, const std::string& append_str);

	static ReflectionCapture& instance();
private:
	

	ReflectionCapture() = default;

	const Vector3 DIRECTIONS[18] = { Vector3(1.0f,-1.0f, 1.0f),  Vector3(1.0f, 3.0f, 1.0f), Vector3(1.0f, -1.0f, -3.0f),		//+X
								   Vector3(-1.0f,-1.0f, -1.0f),  Vector3(-1.0f, 3.0f, -1.0f), Vector3(-1.0f, -1.0f, 3.0f),		//-X
								   Vector3(-1.0f, 1.0f, 1.0f),  Vector3(-1.0f, 1.0f, -3.0f), Vector3(3.0f, 1.0f, 1.0f),			//+Y
								   Vector3(-1.0f,-1.0f, -1.0f),  Vector3(-1.0f, -1.0f, 3.0f), Vector3(3.0f, -1.0f, -1.0f),		//-Y
								   Vector3(-1.0f,-1.0f, 1.0f),  Vector3(-1.0f, 3.0f, 1.0f), Vector3(3.0f, -1.0f, 1.0f),			//+Z
								   Vector3(1.0f, -1.0f, -1.0f), Vector3(1.0f, 3.0f, -1.0f), Vector3(-3.0f, -1.0f, -1.0f) };		//-Z
};


#include "RenderManager.h"
#include  "../d3d.h"
#include "MeshSystem.h"
#include "ParticleSystem.h"
#include "../d3d_utils/RenderTarget.h"

constexpr static int EN_DIFFUSE = 1;
constexpr static int EN_SPECLAR = 1 << 1;
constexpr static int EN_IBL = 1 << 2;
constexpr static int EN_ROUGHNESS_OVERWRITING = 1 << 3;
constexpr static int EN_MSAA = 1 << 4;

void RenderManager::init(UINT width_render, UINT height_render)
{
	initConstantBuffer();

	m_player_camera.updateMatrices();

	m_per_view;

	m_per_view.null0 = 0;
	m_per_view.null1 = 0;
	m_per_view.null2 = 0;

	m_per_view.worldToViewProj = m_player_camera.getViewProj();
	m_per_view.projection = m_player_camera.getProjetion();
	m_per_view.mipLvl = 1;

	m_vertex_shader.loadVertexShader(L"../engine/shaders/DeferredRenderShaders/VertexDeferredShader.hlsl");
	m_PBR_pixel_shader.loadPixelShader(L"../engine/shaders/DeferredRenderShaders/PixelDeferredPBRShader.hlsl");
	m_NoLight_pixel_shader.loadPixelShader(L"../engine/shaders/DeferredRenderShaders/PixelDeferredNoLightShader.hlsl");
	updatePerView(m_per_view);

	m_sky.init();
	m_post_process.init();
	m_shadow_manager.init();
	m_volumetric_fog.init();
	setResolution(width_render, height_render);
}

void RenderManager::deinit()
{
	m_HDR_MS_buffer.release();
	m_HDR_MS_RT.deinit();
	m_HDR_MS_SR.deinit();

	m_HDR_buffer.release();
	m_HDR_RT.deinit();
	m_HDR_SR.deinit();
	m_LDR.deinit();
	m_LDR_RT.deinit();
	m_HDR_copy.deinit();

	m_vertex_shader.release();
	m_PBR_pixel_shader.release();
	m_NoLight_pixel_shader.release();

	m_shadow_manager.deint();
	m_post_process.deinit();
	m_volumetric_fog.deinit();
	m_sky.deinit();

	m_depth_target_MS.deinit();
	m_depth_texture_MS.deinit();
	m_depth_buffer_MS_copy.deinit();

	m_constantBufferPerFrame.deinit();
	m_constantBufferPerView.deinit();

	m_depth_target.deinit();
	m_depth_texture.deinit();
	m_depth_buffer_copy.deinit();
	m_stencil_buffer_SR.deinit();

	m_irradiance.reset();
	m_reflection.reset();
	m_reflectance.reset();

	m_gb_albedo.deinit();
	m_gb_rough_metal.deinit();
	m_gb_normals.deinit();
	m_gb_emmision.deinit();
	m_gb_objectID.deinit();

	for (int i = 0; i < 5; ++i)
	{
		m_GBuffers_RT[i].deinit();
	}

	m_gb_normals_copy.deinit();
	m_gb_objectID_copy.deinit();
}

void RenderManager::updatePerFrame()
{
	PerFrame perFrame;

	perFrame.time = m_time;
	perFrame.time_prev_frame = m_time_prev_frame;
	perFrame.resolution = { static_cast<float> (m_width_render),
		static_cast<float> (m_height_render),
		1.0f / static_cast<float> (m_width_render),
		1.0f / static_cast<float> (m_height_render) };

	perFrame.flags = m_flags;
	perFrame.roughnessSlider = m_roughnessSlider;
	perFrame.qualitySubpix = m_qualitySubpix;
	perFrame.qualityEdgeThreshold = m_qualityEdgeThreshold;
	perFrame.qualityEdgeThresholdMin = m_qualityEdgeThresholdMin;
	perFrame.coffAbsorption = m_coffAbsorption;
	perFrame.asymmetryParameter = m_asymmetryParameter;

	m_constantBufferPerFrame.update(perFrame);
}

void RenderManager::updatePerView(const PerView& perView)
{
	m_constantBufferPerView.update(perView);
}

void RenderManager::render(RenderTarget* renderTarget)
{
	m_flags |= EN_MSAA;

	renderTarget->clear();
	m_depth_target_MS.clearDepth();
	m_HDR_MS_RT.clear();
	updatePerFrame();
	engine::D3D::instance().shangeRasterizerStateShadow(m_depth_bias, m_slope_scaled_depth_bias);
	updateCameraMatrices();
	

	setupBeforeRender();

	m_irradiance->bind(6, TypeShader::TypePixelShader);
	m_reflection->bind(7, TypeShader::TypePixelShader);
	m_reflectance->bind(8, TypeShader::TypePixelShader);

	engine::s_devcon->OMSetDepthStencilState(engine::s_depth_state_write, 0);

	engine::s_devcon->RSSetState(engine::s_raster_state_shadow);
	m_shadow_manager.processShadows();
	LightSystem::instance().bind(0);

	updatePerView(m_per_view);

	D3D11_VIEWPORT viewport = { 0.0f, 0.0f, FLOAT(renderTarget->getWidthRender()), FLOAT(renderTarget->getHeightRender()), 0.0f, 1.0f };
	engine::s_devcon->RSSetViewports(1, &viewport);

	ID3D11RenderTargetView* const renderTV = m_HDR_MS_RT.getFramebufferView();
	engine::s_devcon->OMSetRenderTargets(1, &renderTV, m_depth_target_MS.getdepthView());
	engine::s_devcon->RSSetState(engine::s_raster_state);

	float blendFactor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	UINT sampleMask = 0xffffffff;
	
	engine::s_devcon->OMSetBlendState(engine::s_blend_state_smoke, blendFactor, sampleMask);
	m_shadow_manager.bindShadows();  //between the generation of texture shadows and their bind, it should take some time (as it comes out of practice, perhaps there is a better option)
	MeshSystem::instance().render(false);
	engine::s_devcon->OMSetDepthStencilState(engine::s_depth_state_write, 0);
	MeshSystem::instance().renderPBR(false);
	engine::s_devcon->OMSetDepthStencilState(engine::s_depth_state_readonly, 0);
	m_sky.render();
	engine::s_devcon->CopyResource(m_depth_buffer_MS_copy.getTexture()->ptr(), m_depth_texture_MS.getTexture()->ptr());

	//engine::s_devcon->OMSetDepthStencilState(engine::s_depth_state_write, 1);  //TODO It's not right but partricles looks better

	ParticleSystem::instance().update(m_time);
	m_depth_buffer_MS_copy.bind(17, TypeShader::TypePixelShader);
	engine::s_devcon->OMSetDepthStencilState(engine::s_depth_state_readonly, 0);
	ParticleSystem::instance().render();

	engine::s_devcon->OMSetDepthStencilState(engine::s_depth_state_write, 0);  //TODO It's not right but partricles looks better
	engine::s_devcon->OMSetBlendState(engine::s_blend_state, blendFactor, sampleMask);
	engine::s_devcon->RSSetState(engine::s_raster_state_masking);
	MeshSystem::instance().renderTranslucency(false);

	engine::s_devcon->OMSetBlendState(nullptr, blendFactor, sampleMask);
	m_post_process.resolveMS(&m_HDR_MS_SR, renderTarget);
}

void RenderManager::deferredRender(RenderTarget* renderTarget)
{
	m_flags &= ~EN_MSAA;
	//clear targets
	m_LDR_RT.clear();
	renderTarget->clear();
	m_HDR_RT.clear();
	for (int i = 0; i < 5; ++i)
	{
		m_GBuffers_RT[i].clear();
	}
	m_depth_target.clearDepth();
	m_depth_target.clearStencil();

	updatePerFrame();
	engine::D3D::instance().shangeRasterizerStateShadow(m_depth_bias, m_slope_scaled_depth_bias);
	updateCameraMatrices();


	setupBeforeRender();

	m_irradiance->bind(6, TypeShader::TypePixelShader);
	m_reflection->bind(7, TypeShader::TypePixelShader);
	m_reflectance->bind(8, TypeShader::TypePixelShader);

	

	engine::s_devcon->RSSetState(engine::s_raster_state_shadow);
	m_shadow_manager.processShadows();
	LightSystem::instance().bind(0);

	updatePerView(m_per_view);

	D3D11_VIEWPORT viewport = { 0.0f, 0.0f, FLOAT(renderTarget->getWidthRender()), FLOAT(renderTarget->getHeightRender()), 0.0f, 1.0f };
	engine::s_devcon->RSSetViewports(1, &viewport);

	float blendFactor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	UINT sampleMask = 0xffffffff;

	//----------Fill GBuffer------------
	ID3D11RenderTargetView* const renderTVs[5] = { m_GBuffers_RT[0].getFramebufferView(),
		m_GBuffers_RT[1].getFramebufferView(),
		m_GBuffers_RT[2].getFramebufferView(),
		m_GBuffers_RT[3].getFramebufferView(),
	    m_GBuffers_RT[4].getFramebufferView() };
	engine::s_devcon->OMSetRenderTargets(5, renderTVs, m_depth_target.getdepthView());
	engine::s_devcon->RSSetState(engine::s_raster_state);
	engine::s_devcon->OMSetBlendState(nullptr, blendFactor, sampleMask);
	engine::s_devcon->OMSetDepthStencilState(engine::s_depth_state_write, DEF_STENCIL_REF_VALUE);
	MeshSystem::instance().render(true);
	engine::s_devcon->OMSetDepthStencilState(engine::s_depth_state_write, PBR_STENCIL_REF_VALUE);
	MeshSystem::instance().renderPBR(true);
	
	ID3D11UnorderedAccessView* const UAVs[2] = { ParticleSystem::instance().getGPUParticals().getStructuredBufferUAV(),
												 ParticleSystem::instance().getGPUParticals().getRangeBufferUAV()};
	UINT value = (-1);
	engine::s_devcon->OMSetRenderTargetsAndUnorderedAccessViews(5, renderTVs, m_depth_target.getdepthView(), 5, 2, UAVs, &value);
	engine::s_devcon->RSSetState(engine::s_raster_state_masking);
	MeshSystem::instance().renderTranslucency(true);

	engine::s_devcon->CopyResource(m_depth_buffer_copy.getTexture()->ptr(), m_depth_texture.getTexture()->ptr());
	engine::s_devcon->CopyResource(m_gb_normals_copy.getTexture()->ptr(), m_gb_normals.getTexture()->ptr());
	engine::s_devcon->CopyResource(m_gb_objectID_copy.getTexture()->ptr(), m_gb_objectID.getTexture()->ptr());

	m_depth_buffer_copy.bind(18, TypeShader::TypePixelShader);
	m_gb_normals_copy.bind(22, TypeShader::TypePixelShader);
	m_gb_objectID_copy.bind(24, TypePixelShader);
	engine::s_devcon->OMSetBlendState(engine::s_blend_state, blendFactor, sampleMask);
	engine::s_devcon->OMSetDepthStencilState(engine::s_depth_state_readonly, PBR_STENCIL_REF_VALUE);
	MeshSystem::instance().renderDecals(true);



	engine::s_devcon->RSSetState(engine::s_raster_state);
	m_shadow_manager.bindShadows();  //between the generation of texture shadows and their bind, it should take some time (as it comes out of practice, perhaps there is a better option)
	engine::s_devcon->OMSetBlendState(engine::s_blend_state_smoke, blendFactor, sampleMask);
	ID3D11RenderTargetView* const renderTV = m_HDR_RT.getFramebufferView();
	engine::s_devcon->OMSetRenderTargets(1, &renderTV, m_depth_target.getdepthView());
	
	engine::s_devcon->OMSetDepthStencilState(engine::s_depth_state_readonly, SKY_STENCIL_REF_VALUE);
	m_sky.render();
	engine::s_devcon->CopyResource(m_depth_buffer_copy.getTexture()->ptr(), m_depth_texture.getTexture()->ptr());

	updateParticalsGPU();

	ParticleSystem::instance().update(m_time);
	m_depth_buffer_copy.bind(18, TypeShader::TypePixelShader);
	engine::s_devcon->OMSetRenderTargets(1, &renderTV, m_depth_target.getdepthView());
	engine::s_devcon->OMSetDepthStencilState(engine::s_depth_state_off_readonly, DEF_STENCIL_REF_VALUE);
	deferredRenderNoLight();
	engine::s_devcon->OMSetDepthStencilState(engine::s_depth_state_off_readonly, PBR_STENCIL_REF_VALUE);
	deferredRenderPBR();

	m_post_process.applyFog(m_HDR_buffer, &m_HDR_RT, &m_depth_buffer_copy);

	engine::s_devcon->OMSetDepthStencilState(engine::s_depth_state_write, 0);
	engine::s_devcon->CopyResource(m_HDR_copy.getTexture()->ptr(), m_HDR_buffer.ptr());
	m_HDR_copy.bind(19, TypePixelShader);
	m_depth_buffer_copy.bind(18, TypeShader::TypePixelShader);
	engine::s_devcon->RSSetState(engine::s_raster_state_masking);

	m_volumetric_fog.render();

	engine::s_devcon->CopyResource(m_depth_buffer_copy.getTexture()->ptr(), m_depth_texture.getTexture()->ptr());
	engine::s_devcon->RSSetState(engine::s_raster_state);

	engine::s_devcon->OMSetDepthStencilState(engine::s_depth_state_readonly, 0);
	engine::s_devcon->OMSetRenderTargets(1, &renderTV, nullptr);
	ParticleSystem::instance().render();
	renderParticlesGPU();

	engine::s_devcon->OMSetBlendState(nullptr, blendFactor, sampleMask);
	m_post_process.applyBloom(m_HDR_buffer, &m_HDR_RT);
	m_post_process.resolve(&m_HDR_SR, &m_LDR_RT);
	m_post_process.applyFXAA(&m_LDR, renderTarget);
}

void RenderManager::setupBeforeRender()
{
	engine::s_devcon->PSSetSamplers(0, 1, &engine::s_pointWrap);
	engine::s_devcon->PSSetSamplers(1, 1, &engine::s_linearWrap);
	engine::s_devcon->PSSetSamplers(2, 1, &engine::s_anisotropicWrap);
	engine::s_devcon->PSSetSamplers(3, 1, &engine::s_linearBorder);
	engine::s_devcon->PSSetSamplers(4, 1, &engine::s_linearClamp);
	engine::s_devcon->PSSetSamplers(5, 1, &engine::s_anisotropicClamp);
	engine::s_devcon->PSSetSamplers(6, 1, &engine::s_linearWrapCmp);

	engine::s_devcon->CSSetSamplers(0, 1, &engine::s_pointWrap);
	engine::s_devcon->CSSetSamplers(1, 1, &engine::s_linearWrap);
	engine::s_devcon->CSSetSamplers(2, 1, &engine::s_anisotropicWrap);
	engine::s_devcon->CSSetSamplers(3, 1, &engine::s_linearBorder);
	engine::s_devcon->CSSetSamplers(4, 1, &engine::s_linearClamp);
	engine::s_devcon->CSSetSamplers(5, 1, &engine::s_anisotropicClamp);
	engine::s_devcon->CSSetSamplers(6, 1, &engine::s_linearWrapCmp);

	m_constantBufferPerFrame.bind(0, TypeShader::TypeVertexShader);
	m_constantBufferPerFrame.bind(0, TypeShader::TypePixelShader);
	m_constantBufferPerFrame.bind(0, TypeShader::TypeGeometryShader);
	m_constantBufferPerFrame.bind(0, TypeShader::TypeComputerShader);
	m_constantBufferPerView.bind(1, TypeShader::TypeVertexShader);
	m_constantBufferPerView.bind(1, TypeShader::TypePixelShader);
	m_constantBufferPerView.bind(1, TypeShader::TypeGeometryShader);
	m_constantBufferPerView.bind(1, TypeShader::TypeComputerShader);
}

void RenderManager::setResolution(UINT width_render, UINT height_render)
{
	m_width_render = width_render;
	m_height_render = height_render;

	//Init direction texture
	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));

	textureDesc.Width = width_render;
	textureDesc.Height = height_render;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = MAX_NUM_DIRETIONAL_LIGHTS;
	textureDesc.Format = DXGI_FORMAT_R24G8_TYPELESS;
	textureDesc.SampleDesc.Count = 4;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	D3D11_SHADER_RESOURCE_VIEW_DESC descSRV;

	descSRV.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
	descSRV.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DMS;
	descSRV.Texture2DArray.ArraySize = MAX_NUM_DIRETIONAL_LIGHTS;
	descSRV.Texture2DArray.MipLevels = 1;
	descSRV.Texture2DArray.MostDetailedMip = 0;
	descSRV.Texture2DArray.FirstArraySlice = 0;

	m_depth_texture_MS.init(&textureDesc, &descSRV);
	m_depth_buffer_MS_copy.init(&textureDesc, &descSRV);

	textureDesc.SampleDesc.Count = 1;
	descSRV.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	m_depth_texture.init(&textureDesc, &descSRV);
	m_depth_buffer_copy.init(&textureDesc, &descSRV);
	descSRV.Format = DXGI_FORMAT_X24_TYPELESS_G8_UINT;
	m_stencil_buffer_SR.init(m_depth_buffer_copy.getTexture()->ptr(), &descSRV);

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	m_depth_target_MS.init(m_depth_texture_MS.getTexture()->ptr(), &depthStencilViewDesc);

	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	m_depth_target.init(m_depth_texture.getTexture()->ptr(), &depthStencilViewDesc);

	initRTandSR();
	initRTandSR_MS();
	initGBuffers();
}

void RenderManager::updateCameraMatrices()
{
	if (m_player_camera.needUpdatedMatrices())
	{
		m_player_camera.updateMatrices();

		updatePerViewBuffer();
	}
}

void RenderManager::updatePerViewBuffer()
{
	m_per_view.worldToViewProj = m_player_camera.getViewProj();
	m_per_view.worldToViewProjInv = m_player_camera.getViewProjInv();
	m_per_view.worldToView = m_player_camera.getView();
	m_per_view.worldToViewInv = m_player_camera.getViewInv();
	m_per_view.projection = m_player_camera.getProjetion();
	m_per_view.projectionInv = m_player_camera.getProjetionInv();
	m_per_view.shadow_texel = m_shadow_manager.getSizeTexel();
	m_per_view.BL = m_player_camera.getBL();
	m_per_view.BR = m_player_camera.getBL() + m_player_camera.getBR() * 2;
	m_per_view.TL = m_player_camera.getBL() + m_player_camera.getTL() * 2;

	updatePerView(m_per_view);

#ifdef LIGHT_CALCULATION_WORLD_SPACE
#else
	MeshSystem::instance().update();
	LightSystem::instance().update();
	ParticleSystem::instance().updateInstanceBuffers();
	m_volumetric_fog.updateInstanceBuffers();
#endif // LIGHT_CALCULATION_WORLD_SPACE
}


void RenderManager::setReflection(const std::shared_ptr<Texture>& reflection)
{
	D3D11_TEXTURE2D_DESC resourceDesc;
	ID3D11Texture2D* pTextureInterface = nullptr;
	reflection->getTexture()->ptr()->QueryInterface<ID3D11Texture2D>(&pTextureInterface);
	pTextureInterface->GetDesc(&resourceDesc);
	pTextureInterface->Release();

	m_reflection = reflection;
	m_per_view.mipLvl = resourceDesc.MipLevels;
}

void RenderManager::updateSettigns(float roughnessSlider, int depth_bias, float slope_scaled_depth_bias, bool en_diffuse, bool en_specular, bool en_IBL, bool en_roughness_overwriting,
	float qualitySubpix, float qualityEdgeThreshold, float qualityEdgeThresholdMin, float coffAbsorption, float asymmetryParameter)
{
	m_flags = 0;
	if (en_diffuse)
		m_flags = m_flags | EN_DIFFUSE;
	if (en_specular)
		m_flags = m_flags | EN_SPECLAR;
	if (en_IBL)
		m_flags = m_flags | EN_IBL;
	if (en_roughness_overwriting)
		m_flags = m_flags | EN_ROUGHNESS_OVERWRITING;
	//m_flags |= EN_MSAA;
	m_roughnessSlider = roughnessSlider;
	m_depth_bias = depth_bias;
	m_slope_scaled_depth_bias = slope_scaled_depth_bias;
	m_qualitySubpix = qualitySubpix;
	m_qualityEdgeThreshold = qualityEdgeThreshold;
	m_qualityEdgeThresholdMin = qualityEdgeThresholdMin;
	m_coffAbsorption = coffAbsorption;
	m_asymmetryParameter = asymmetryParameter;
}

RenderManager& RenderManager::instance()
{
	static RenderManager s_render_manager;
	return s_render_manager;
}

void RenderManager::initConstantBuffer()
{
	m_constantBufferPerFrame.init(D3D11_USAGE_DYNAMIC);
	m_constantBufferPerView.init(D3D11_USAGE_DYNAMIC);
}

void RenderManager::initRTandSR_MS()
{
	if (m_HDR_MS_buffer.ptr() != nullptr)
	{
		m_HDR_MS_buffer.release();
		m_HDR_MS_RT.deinit();
		m_HDR_MS_SR.deinit();
	}


	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));

	textureDesc.Width = m_width_render;
	textureDesc.Height = m_height_render;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	textureDesc.SampleDesc.Count = 4;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	engine::s_device->CreateTexture2D(&textureDesc, NULL, m_HDR_MS_buffer.reset());

	D3D11_RENDER_TARGET_VIEW_DESC desc_rtv;
	ZeroMemory(&desc_rtv, sizeof(desc_rtv));
	desc_rtv.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DMS;
	desc_rtv.Format = textureDesc.Format;

	m_HDR_MS_RT.init(m_HDR_MS_buffer, &desc_rtv);

	D3D11_SHADER_RESOURCE_VIEW_DESC desc_srv;
	ZeroMemory(&desc_srv, sizeof(desc_srv));

	desc_srv.Format = textureDesc.Format;
	desc_srv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DMS;
	desc_srv.Texture2D.MostDetailedMip = 0;
	desc_srv.Texture2D.MipLevels = 1;

	m_HDR_MS_SR.init(m_HDR_MS_buffer, &desc_srv);
}

void RenderManager::initRTandSR()
{
	if (m_HDR_buffer.ptr() != nullptr)
	{
		m_HDR_buffer.release();
		m_HDR_RT.deinit();
		m_HDR_SR.deinit();

		m_LDR.deinit();
		m_LDR_RT.deinit();
	}


	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));

	textureDesc.Width = m_width_render;
	textureDesc.Height = m_height_render;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	engine::s_device->CreateTexture2D(&textureDesc, NULL, m_HDR_buffer.reset());
	m_LDR.init(&textureDesc);
	m_HDR_copy.init(&textureDesc);

	D3D11_RENDER_TARGET_VIEW_DESC desc_rtv;
	ZeroMemory(&desc_rtv, sizeof(desc_rtv));
	desc_rtv.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	desc_rtv.Format = textureDesc.Format;

	m_HDR_RT.init(m_HDR_buffer, &desc_rtv);
	m_LDR_RT.init(m_LDR.getTexture()->ptr(), &desc_rtv);

	D3D11_SHADER_RESOURCE_VIEW_DESC desc_srv;
	ZeroMemory(&desc_srv, sizeof(desc_srv));

	desc_srv.Format = textureDesc.Format;
	desc_srv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	desc_srv.Texture2D.MostDetailedMip = 0;
	desc_srv.Texture2D.MipLevels = 1;

	m_HDR_SR.init(m_HDR_buffer, &desc_srv);
}

void RenderManager::initGBuffers()
{

	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));

	textureDesc.Width = m_width_render;
	textureDesc.Height = m_height_render;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	D3D11_RENDER_TARGET_VIEW_DESC desc_rtv;
	ZeroMemory(&desc_rtv, sizeof(desc_rtv));
	desc_rtv.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	desc_rtv.Format = textureDesc.Format;


	m_gb_albedo.init(&textureDesc);
	m_gb_rough_metal.init(&textureDesc);

	m_GBuffers_RT[0].init(m_gb_albedo.getTexture()->ptr(), &desc_rtv);
	m_GBuffers_RT[1].init(m_gb_rough_metal.getTexture()->ptr(), &desc_rtv);

	textureDesc.Format = DXGI_FORMAT_R16G16B16A16_SNORM;
	m_gb_normals.init(&textureDesc);
	m_gb_normals_copy.init(&textureDesc);
	desc_rtv.Format = textureDesc.Format;
	m_GBuffers_RT[2].init(m_gb_normals.getTexture()->ptr(), &desc_rtv);

	textureDesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	m_gb_emmision.init(&textureDesc);
	desc_rtv.Format = textureDesc.Format;
	m_GBuffers_RT[3].init(m_gb_emmision.getTexture()->ptr(), &desc_rtv);

	textureDesc.Format = DXGI_FORMAT_R32_UINT;
	m_gb_objectID.init(&textureDesc);
	m_gb_objectID_copy.init(&textureDesc);
	desc_rtv.Format = textureDesc.Format;
	m_GBuffers_RT[4].init(m_gb_objectID.getTexture()->ptr(), &desc_rtv);
}

void RenderManager::deferredRenderPBR()
{
	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


	m_vertex_shader.bind();
	m_PBR_pixel_shader.bind();

	m_gb_albedo.bind(20, TypePixelShader);
	m_gb_rough_metal.bind(21, TypePixelShader);
	m_gb_normals.bind(22, TypePixelShader);
	m_gb_objectID.bind(24, TypePixelShader);

	engine::s_devcon->IASetInputLayout(nullptr);
	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	engine::s_devcon->Draw(3, 0);

	m_gb_albedo.unbind();
	m_gb_rough_metal.unbind();
	m_gb_normals.unbind();
	m_gb_objectID.unbind();
}

void RenderManager::deferredRenderNoLight()
{
	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


	m_vertex_shader.bind();
	m_NoLight_pixel_shader.bind();

	m_gb_albedo.bind(20, TypePixelShader);
	m_gb_rough_metal.bind(21, TypePixelShader);
	m_gb_normals.bind(22, TypePixelShader);
	m_gb_emmision.bind(23, TypePixelShader);
	m_gb_objectID.bind(24, TypePixelShader);

	engine::s_devcon->IASetInputLayout(nullptr);
	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	engine::s_devcon->Draw(3, 0);

	m_gb_albedo.unbind();
	m_gb_rough_metal.unbind();
	m_gb_normals.unbind();
	m_gb_emmision.unbind();
	m_gb_objectID.unbind();
}

void RenderManager::updateParticalsGPU()
{
	m_depth_buffer_copy.bind(18, TypeComputerShader);
	m_gb_normals.bind(22, TypeComputerShader);

	ParticleSystem::instance().updateParticlesGPU();

	m_depth_buffer_copy.unbind();
	m_gb_normals.unbind();
}

void RenderManager::renderParticlesGPU()
{
	m_gb_normals.bind(22, TypePixelShader);

	ParticleSystem::instance().renderParticlesGPU();

	m_gb_normals.unbind();
}

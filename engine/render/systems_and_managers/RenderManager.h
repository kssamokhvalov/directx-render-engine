#pragma once
#include "../../utils/Time.h"
#include "../Camera.h"
#include "../d3d_utils/UniformBuffer.h"
#include "../d3d_utils/DepthTarget.h"
#include "../d3d_utils/ShaderResurce.h"
#include "../SkyRender.h"
#include "../PostProcess.h"
#include "LightSystem.h"
#include "../d3d.h"
#include "./ShadowManager.h"
#include "./DecalSystem.h"
#include "./VolumetricFog.h"

class Window;
class Scene;
class RenderTarget;

class RenderManager
{
public:
	struct PerFrame
	{
		float4 resolution;
		float time;
		float time_prev_frame;
		int32_t flags;
		float roughnessSlider;
		float qualitySubpix;
		float qualityEdgeThreshold;
		float qualityEdgeThresholdMin;
		float coffAbsorption;
		float asymmetryParameter;
		float _padd[3];
	};

	struct PerView
	{
		Matrix4x4 worldToViewProj;
		Matrix4x4 worldToViewProjInv;
		Matrix4x4 worldToView;
		Matrix4x4 worldToViewInv;
		Matrix4x4 projection;
		Matrix4x4 projectionInv;
		float _pad[2];
		float shadow_texel;
		float mipLvl;
		Vector3 BL;
		float null0; // must  be alwase equal null
		Vector3 TL;
		float null1;  // must  be alwase equal null
		Vector3 BR;
		float null2; // must  be alwase equal null
	};

	RenderManager(const RenderManager& other) = delete;
	void operator=(const RenderManager& other) = delete;

	void init(UINT width_render = 500, UINT height_render = 480);
	void deinit();
	void updatePerFrame();
	void updatePerView(const PerView& perView);
	void render(RenderTarget* renderTarget);
	void deferredRender(RenderTarget* renderTarget);
	void setupBeforeRender();

	int getWidthRender() const { return m_width_render; }
	int getHeightRender() const { return m_height_render; }
	void setResolution(UINT width_render, UINT height_render);
	void updateCameraMatrices();
	void updatePerViewBuffer();
	
	Camera* const getCamera() { return &m_player_camera; }
	SkyRender* const getSkyRender() { return &m_sky; }
	PostProcess* const getPostPocess() { return &m_post_process; }
	VolumetricFog* const getVolumetricFog() { return &m_volumetric_fog; }

	void setIrradiance(const std::shared_ptr<Texture>& irradiance) { m_irradiance = irradiance; }
	void setReflection(const std::shared_ptr<Texture>& reflection);
	void setReflectance(const std::shared_ptr<Texture>& reflectance) { m_reflectance = reflectance; }

	void setTime(float time) 
	{ 
		m_time_prev_frame = m_time;
		m_time = time; 
	}

	void updateSettigns(float roughnessSlider, int depth_bias, float slope_scaled_depth_bias, bool en_diffuse, bool en_specular, bool en_IBL, bool en_roughness_overwriting,
		float qualitySubpix, float qualityEdgeThreshold, float qualityEdgeThresholdMin, float coffAbsorption, float asymmetryParameter);

	PerView getPerView() const { return m_per_view; }

	static RenderManager& instance();
private:
	RenderManager() = default;

	void initConstantBuffer();
	void initRTandSR_MS();
	void initRTandSR();
	void initGBuffers();

	void deferredRenderPBR();
	void deferredRenderNoLight();
	void updateParticalsGPU();
	void renderParticlesGPU();

	const  UINT PBR_STENCIL_REF_VALUE = 1;
	const  UINT SKY_STENCIL_REF_VALUE = 0;
	const  UINT DEF_STENCIL_REF_VALUE = 2;

	float m_time{};
	float m_time_prev_frame;
	Camera m_player_camera;

	PerFrame m_per_frame;
	PerView m_per_view;

	UniformBuffer<PerFrame> m_constantBufferPerFrame;
	UniformBuffer<PerView> m_constantBufferPerView;

	Texture m_depth_texture;
	DepthTarget m_depth_target;
	Texture m_depth_buffer_copy;
	ShaderResurce m_stencil_buffer_SR;

	Texture m_depth_texture_MS;
	DepthTarget m_depth_target_MS;
	Texture m_depth_buffer_MS_copy;

	UINT m_width_render;
	UINT m_height_render;

	SkyRender m_sky;

	PostProcess m_post_process;
	ShadowManager m_shadow_manager;
	VolumetricFog m_volumetric_fog;

	//-------Multi sampling-------
	//TODO maybe better HDR_buffer change on Texture
	engine::DxResPtr<ID3D11Texture2D> m_HDR_MS_buffer;
	D3D11_TEXTURE2D_DESC m_HDR_MS_bufferDesc;
	RenderTarget m_HDR_MS_RT;
	ShaderResurce m_HDR_MS_SR;

//TODO maybe better HDR_buffer change on Texture
	engine::DxResPtr<ID3D11Texture2D> m_HDR_buffer;
	D3D11_TEXTURE2D_DESC m_HDR_bufferDesc;
	RenderTarget m_HDR_RT;
	ShaderResurce m_HDR_SR;
	Texture m_HDR_copy;


	Texture m_LDR;
	RenderTarget m_LDR_RT;

	std::shared_ptr<Texture> m_irradiance;
	std::shared_ptr<Texture> m_reflection;
	std::shared_ptr<Texture> m_reflectance;

	//GBuffers
	Texture m_gb_albedo;
	Texture m_gb_rough_metal;
	Texture m_gb_normals;
	Texture m_gb_emmision;
	Texture m_gb_objectID;
	RenderTarget m_GBuffers_RT[5];

	Texture m_gb_normals_copy;
	Texture m_gb_objectID_copy;

	VertexShader m_vertex_shader;
	PixelShader m_PBR_pixel_shader;
	PixelShader m_NoLight_pixel_shader;

	//PerFrame
	int32_t m_flags;
	float m_roughnessSlider;
	int m_depth_bias;
	float m_slope_scaled_depth_bias;

	float m_qualitySubpix;
	float m_qualityEdgeThreshold;
	float m_qualityEdgeThresholdMin;

	float m_coffAbsorption;
	float m_asymmetryParameter;
};


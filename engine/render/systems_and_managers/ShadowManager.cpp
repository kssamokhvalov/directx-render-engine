#include "ShadowManager.h"
#include "RenderManager.h"
#include "MeshSystem.h"


void ShadowManager::init()
{

	//Init direction texture
	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));

	textureDesc.Width = SHADOW_RESOLUTION;
	textureDesc.Height = SHADOW_RESOLUTION;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = MAX_NUM_DIRETIONAL_LIGHTS;
	textureDesc.Format = DXGI_FORMAT_R24G8_TYPELESS;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	D3D11_SHADER_RESOURCE_VIEW_DESC descSRV;

	descSRV.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
	descSRV.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
	descSRV.Texture2DArray.ArraySize = MAX_NUM_DIRETIONAL_LIGHTS;
	descSRV.Texture2DArray.MipLevels = 1;
	descSRV.Texture2DArray.MostDetailedMip = 0;
	descSRV.Texture2DArray.FirstArraySlice = 0;

	m_directional_shadows.init(&textureDesc, &descSRV);


	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));

	descDSV.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DARRAY;
	descDSV.Texture2DArray.ArraySize = 1;
	descDSV.Texture2DArray.MipSlice = 0;

	for (int i = 0; i < MAX_NUM_DIRETIONAL_LIGHTS; ++i)
	{
		descDSV.Texture2DArray.FirstArraySlice = i;
		m_directional_shadows_DTs[i].init(m_directional_shadows.getTexture()->ptr(), &descDSV);
	}

	textureDesc.ArraySize = MAX_NUM_SPOT_LIGHTS_WITH_MASK;
	descSRV.Texture2DArray.ArraySize = MAX_NUM_SPOT_LIGHTS_WITH_MASK;

	m_spot_with_mask_shadows.init(&textureDesc, &descSRV);

	for (int i = 0; i < MAX_NUM_SPOT_LIGHTS_WITH_MASK; ++i)
	{
		descDSV.Texture2DArray.FirstArraySlice = i;
		m_spot_with_mask_shadows_DTs[i].init(m_spot_with_mask_shadows.getTexture()->ptr(), &descDSV);
	}

	//init CubeArray

	textureDesc.ArraySize = 6 * MAX_NUM_POINT_LIGHTS;
	textureDesc.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE;

	descSRV.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBEARRAY;
	descSRV.TextureCubeArray.NumCubes = MAX_NUM_POINT_LIGHTS;
	descSRV.TextureCubeArray.MipLevels = 1;
	descSRV.TextureCubeArray.First2DArrayFace = 0;
	descSRV.TextureCubeArray.MostDetailedMip = 0;

	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DARRAY;
	descDSV.Texture2DArray.ArraySize = 6;


	m_point_shadows.init(&textureDesc, &descSRV);

	for (int i = 0; i < MAX_NUM_POINT_LIGHTS; ++i)
	{
		descDSV.Texture2DArray.FirstArraySlice = i * 6;
		m_point_shadows_DTs[i].init(m_point_shadows.getTexture()->ptr(), &descDSV);
	}
}

void ShadowManager::deint()
{
	for (int i = 0; i < MAX_NUM_DIRETIONAL_LIGHTS; ++i)
	{
		m_directional_shadows_DTs[i].deinit();
	}

	for (int i = 0; i < MAX_NUM_SPOT_LIGHTS_WITH_MASK; ++i)
	{
		m_spot_with_mask_shadows_DTs[i].deinit();
	}

	for (int i = 0; i < MAX_NUM_POINT_LIGHTS; ++i)
	{
		m_point_shadows_DTs[i].deinit();
	}

	m_directional_shadows.deinit();
	m_spot_with_mask_shadows.deinit();
	m_point_shadows.deinit();
}

void ShadowManager::processShadows()
{
	
	unbindShadows();

	D3D11_VIEWPORT viewport = { 0.0f, 0.0f, FLOAT(SHADOW_RESOLUTION), FLOAT(SHADOW_RESOLUTION), 0.0f, 1.0f };
	engine::s_devcon->RSSetViewports(1, &viewport);
	engine::s_devcon->OMSetDepthStencilState(engine::s_depth_state_write, 0);

	createPointShadows();
	createDirectionlShadows();
	createSpotShadows();

}

void ShadowManager::bindShadows()
{
	m_directional_shadows.bind(9, TypeShader::TypePixelShader);
	m_spot_with_mask_shadows.bind(10, TypeShader::TypePixelShader);
	m_point_shadows.bind(11, TypeShader::TypePixelShader);
}

void ShadowManager::unbindShadows()
{
	m_directional_shadows.unbind();
	m_spot_with_mask_shadows.unbind();
	m_point_shadows.unbind();
}

void ShadowManager::createDirectionlShadows()
{

	std::vector<LightSystem::DirectionalLight>& directionl_lights = LightSystem::instance().getDirectionalLights();
	size_t num_directionl_lights = directionl_lights.size();

	Vector3 BL, BR, TR, TL;
	RenderManager::instance().getCamera()->getFarFrustrimVectors(BR, BL, TR, TL);

	RenderManager::PerView perView = RenderManager::instance().getPerView();

	auto findAllMinimals = [&](Vector3 BR, Vector3 BL, Vector3 TR, Vector3 TL, Vector3 cam_pos, float& min_z, float& max_z, float& min_x, float& min_y, float& max_x, float& max_y)
	{
		min_z = fminf(BR.z(), BL.z());
		min_z = fminf(min_z, TR.z());
		min_z = fminf(min_z, TL.z());
		min_z = fminf(min_z, cam_pos.z());

		max_z = fmaxf(BR.z(), BL.z());
		max_z = fmaxf(max_z, TR.z());
		max_z = fmaxf(max_z, TL.z());
		max_z = fmaxf(max_z, cam_pos.z());

		min_x = fminf(BR.x(), BL.x());
		min_x = fminf(min_x, TR.x());
		min_x = fminf(min_x, TL.x());
		min_x = fminf(min_x, cam_pos.x());

		min_y = fminf(BR.y(), BL.y());
		min_y = fminf(min_y, TR.y());
		min_y = fminf(min_y, TL.y());
		min_y = fminf(min_y, cam_pos.y());

		max_x = fmaxf(BR.x(), BL.x());
		max_x = fmaxf(max_x, TR.x());
		max_x = fmaxf(max_x, TL.x());
		max_x = fmaxf(max_x, cam_pos.x());
				 
		max_y = fmaxf(BR.y(), BL.y());
		max_y = fmaxf(max_y, TR.y());
		max_y = fmaxf(max_y, TL.y());
		max_y = fmaxf(max_y, cam_pos.y());
	};

	for (int i = 0; i < num_directionl_lights; ++i)
	{
		float min_z = 0.0f, max_z = 0.0f, min_x = 0.0f, min_y = 0.0f, max_x = 0.0f, max_y = 0.0f;

		Matrix4x4 world_to_light = directionl_lights.at(i).matrix_world_to_light;

#ifdef LIGHT_CALCULATION_WORLD_SPACE
		Vector3 pos_cam_in_light = Matrix4x4::multiply(Vector4(RenderManager::instance().getCamera()->position(), 0.0f), world_to_light).xyz();
#else
		Vector3 pos_cam_in_light(0.0f, 0.0f, 0.0f);
#endif // LIGHT_CALCULATION_WORLD_SPACE
		

		findAllMinimals(Matrix4x4::multiply(Vector4(BR, 0.0f), world_to_light).xyz() / 2.0f,
			Matrix4x4::multiply(Vector4(BL, 0.0f), world_to_light).xyz() / 2.0f,
			Matrix4x4::multiply(Vector4(TR, 0.0f), world_to_light).xyz() / 2.0f,
			Matrix4x4::multiply(Vector4(TL, 0.0f), world_to_light).xyz() / 2.0f,
			pos_cam_in_light,
			min_z, max_z,
			min_x, min_y,
			max_x, max_y);

		min_z -= MARGIN;

		float diff_x = max_x - min_x;
		float diff_y = max_y - min_y;

		float shadow_distance = fmaxf(diff_y, diff_x);
		m_size_texel = shadow_distance / SHADOW_RESOLUTION;

		min_x = int(min_x / m_size_texel) * m_size_texel;
		min_y = int(min_y / m_size_texel) * m_size_texel;

		Matrix4x4 orthographic = Matrix4x4::createFrustumOrthographicMatrix(
			min_x + shadow_distance,
			min_x,					
			min_y + shadow_distance,
			min_y,					
			min_z,
			max_z);

		directionl_lights[i].matrix_proj = orthographic;

		perView.worldToView = world_to_light;
		perView.projection = orthographic;
		perView.worldToViewProj = world_to_light * orthographic;

		RenderManager::instance().updatePerView(perView);

		m_directional_shadows_DTs[i].clearDepth();
		ID3D11RenderTargetView* renderTV = nullptr;
		engine::s_devcon->OMSetRenderTargets(1, &renderTV, m_directional_shadows_DTs[i].getdepthView());

		MeshSystem::instance().renderDepth2D();
	}
}

void ShadowManager::createSpotShadows()
{
	std::vector<LightSystem::SpotLightWithMask>& spot_with_mask_lights = LightSystem::instance().getSpotWithMaskLights();
	size_t num_spot_with_mask_lights = spot_with_mask_lights.size();

	RenderManager::PerView perView = RenderManager::instance().getPerView();

	for (int i = 0; i < num_spot_with_mask_lights; ++i)
	{

		perView.worldToView = spot_with_mask_lights.at(i).matrix_world_to_light;
		perView.projection = spot_with_mask_lights[i].matrix_mask_proj;
		perView.worldToViewProj = spot_with_mask_lights.at(i).matrix_world_to_light * perView.projection;

		RenderManager::instance().updatePerView(perView);

		m_spot_with_mask_shadows_DTs[i].clearDepth();
		ID3D11RenderTargetView* renderTV = nullptr;
		engine::s_devcon->OMSetRenderTargets(1, &renderTV, m_spot_with_mask_shadows_DTs[i].getdepthView());

		MeshSystem::instance().renderDepth2D();
	}
}

void ShadowManager::createPointShadows()
{
	std::vector<LightSystem::PointLight>& point_lights = LightSystem::instance().getPointLights();
	size_t num_point_lights = point_lights.size();

	for (int i = 0; i < num_point_lights; ++i)
	{
		m_point_shadows_DTs[i].clearDepth();
		ID3D11RenderTargetView* renderTV = nullptr;
		engine::s_devcon->OMSetRenderTargets(1, &renderTV, m_point_shadows_DTs[i].getdepthView());

		MeshSystem::instance().renderDepthCubemaps(point_lights.at(i).position);
	}

}

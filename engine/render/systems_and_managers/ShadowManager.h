#pragma once
#include <memory>
#include "../d3d.h"
#include "../d3d_utils/VertexShader.h"
#include "../d3d_utils/ShaderResurce.h"
#include "../d3d_utils/GeometryShader.h"
#include "../d3d_utils/Texture.h"
#include "../d3d_utils/DepthTarget.h"
#include "LightSystem.h"

class ShadowManager
{
public:
    void init();
    void deint();

    void processShadows();
    void bindShadows();
    void unbindShadows();

    const int SHADOW_RESOLUTION = 2048;
    const float MARGIN = 40.0f;

    float getSizeTexel() { return m_size_texel; }
private:
    void createDirectionlShadows();
    void createSpotShadows();
    void createPointShadows();

    float m_size_texel;

    Texture m_directional_shadows;
    Texture m_spot_with_mask_shadows;
    Texture m_point_shadows;

    DepthTarget m_directional_shadows_DTs[MAX_NUM_DIRETIONAL_LIGHTS];
    DepthTarget m_spot_with_mask_shadows_DTs[MAX_NUM_SPOT_LIGHTS_WITH_MASK];
    DepthTarget m_point_shadows_DTs[MAX_NUM_POINT_LIGHTS];
};


#include "TextureManager.h"
#include "ReflectionCapture.h"
#include "TextureManager.h"
#include <DDSTextureLoader/DDSTextureLoader11.h>
#include <DirectXTex.h>

#include "../../utils/ParallelExecutor.h"
#include "./NoiseCapture.h"

void TextureManager::init()
{
}

void TextureManager::deinit()
{
	for (const auto texture : m_loaded_textures)
	{
		texture.second->deinit();
	}
	m_loaded_textures.clear();
}

bool TextureManager::getTexture(std::shared_ptr<Texture>& texture, const std::string& path)
{
	m_loaded_textures.find(path);
	auto search = m_loaded_textures.find(path);
	if (search != m_loaded_textures.end())
	{
		texture = search->second;
		return true;
	}
	else
	{
		return DDSTextureLoader(texture, path);
	}
		
}

std::shared_ptr<Texture> TextureManager::getTexture(const std::string& path)
{
	m_loaded_textures.find(path);
	auto search = m_loaded_textures.find(path);
	if (search != m_loaded_textures.end())
		return search->second;
	else
		return DDSTextureLoader(path);
}

void TextureManager::save(std::wstring path, Texture& gpuTexture, FileFormat format, bool generateMips)
{
	DirectX::ScratchImage scratchImage;
	HRESULT hr = DirectX::CaptureTexture(engine::s_device, engine::s_devcon, gpuTexture.getTexture()->ptr(), scratchImage);

	const DirectX::ScratchImage* imagePtr = &scratchImage;

	DirectX::ScratchImage mipchain;
	if (generateMips)
	{
		DirectX::GenerateMipMaps(*scratchImage.GetImage(0, 0, 0), DirectX::TEX_FILTER_DEFAULT, 0, mipchain);
		imagePtr = &mipchain;
	}

	DirectX::ScratchImage compressed;
	if (DirectX::IsCompressed(DXGI_FORMAT(format)))
	{
		HRESULT result;
		if (FileFormat::BC6_UNSIGNED <= format && format <= FileFormat::BC7_SRGB)
			result = DirectX::Compress(engine::s_device, imagePtr->GetImages(), imagePtr->GetImageCount(), imagePtr->GetMetadata(),
				DXGI_FORMAT(format), DirectX::TEX_COMPRESS_PARALLEL, 1.f, compressed);
		else
			result = DirectX::Compress(imagePtr->GetImages(), imagePtr->GetImageCount(), imagePtr->GetMetadata(),
				DXGI_FORMAT(format), DirectX::TEX_COMPRESS_PARALLEL, 1.f, compressed);

		DEV_ASSERT(result >= 0);
		imagePtr = &compressed;
	}

	DirectX::SaveToDDSFile(imagePtr->GetImages(), imagePtr->GetImageCount(), imagePtr->GetMetadata(), DirectX::DDS_FLAGS(0), path.c_str());

}

std::shared_ptr<Texture> TextureManager::getDiffuseIrradianCubeMap(const std::string& path)
{
	
	std::string path_mdf = ReflectionCapture::appendToFileName(path, ReflectionCapture::instance().DIFFUSE_IRRADIAN_EXPANDING);

	std::shared_ptr<Texture> texture(new Texture);
	if (getTexture(texture, path_mdf))
	{
		return texture;
	}
	else
	{
		auto texture_sky = TextureManager::instance().getTexture(path);
		texture = ReflectionCapture::instance().computeDiffuseIrradianCubeMap(texture_sky, path);
		m_loaded_textures.emplace(path_mdf, texture);
		return texture;
	}
}

std::shared_ptr<Texture> TextureManager::getSpecularIrradianCubeMap(const std::string& path)
{

	std::string path_mdf = ReflectionCapture::appendToFileName(path, ReflectionCapture::instance().SPECULAR_IRRADIAN_EXPANDING);

	std::shared_ptr<Texture> texture(new Texture);
	if (getTexture(texture, path_mdf))
	{
		return texture;
	}
	else
	{
		auto texture_sky = TextureManager::instance().getTexture(path);
		texture = ReflectionCapture::instance().computeSpecularIrradianCubeMap(texture_sky, path);
		m_loaded_textures.emplace(path_mdf, texture);
		return texture;
	}
}

std::shared_ptr<Texture> TextureManager::getSpecularReflectanceCubeMap(const std::string& path)
{
	std::string path_mdf = ReflectionCapture::appendToFileName(path, ReflectionCapture::instance().SPECULAR_REFLECTANCE_EXPANDING);

	std::shared_ptr<Texture> texture(new Texture);
	if (getTexture(texture, path_mdf))
	{
		return texture;
	}
	else
	{
		auto texture_sky = TextureManager::instance().getTexture(path);
		texture = ReflectionCapture::instance().computeSpecularReflectanceCubeMap(path);
		m_loaded_textures.emplace(path_mdf, texture);
		return texture;
	}
}

std::shared_ptr<Texture> TextureManager::getSimplexNoise3D(const std::string& path)
{
	std::shared_ptr<Texture> texture(new Texture);
	if (getTexture(texture, path))
	{
		return texture;
	}
	else
	{
		texture = ReflectionCapture::instance().computeSpecularReflectanceCubeMap(path);
		m_loaded_textures.emplace(path, texture);
		return texture;
	}
}


TextureManager& TextureManager::instance()
{
	static TextureManager s_model_manager;
	return s_model_manager;
}

std::shared_ptr<Texture> TextureManager::DDSTextureLoader(const std::string& path)
{
	std::wstring wpath = std::wstring(path.begin(), path.end());
	std::shared_ptr<Texture> texture(new Texture);

	DirectX::CreateDDSTextureFromFile(engine::s_device, wpath.c_str(), texture->getTexture()->reset(), texture->getTextureView()->reset());

	m_loaded_textures.emplace(path, texture);

	return texture;
}

bool TextureManager::DDSTextureLoader(std::shared_ptr<Texture>& texture, const std::string& path)
{
	std::wstring wpath = std::wstring(path.begin(), path.end());

	HRESULT result;
	result = DirectX::CreateDDSTextureFromFile(engine::s_device, wpath.c_str(), texture->getTexture()->reset(), texture->getTextureView()->reset());

	if (SUCCEEDED(result))
	{
		m_loaded_textures.emplace(path, texture);
		return true;
	}
	else
	{
		return false;
	}
	
}

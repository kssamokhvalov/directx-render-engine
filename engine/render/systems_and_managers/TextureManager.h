#pragma once
#include <unordered_map>
#include <string>
#include <memory>
#include "../d3d.h"
#include "../DxRes.h"
#include "../d3d_utils/Texture.h"

class TextureManager
{
public:

	enum class FileFormat
	{
		NONE,
		PNG,
		TGA,
		HDR,
		BC1_LINEAR = DXGI_FORMAT_BC1_UNORM,			// RGB, 1 bit Alpha
		BC1_SRGB = DXGI_FORMAT_BC1_UNORM_SRGB,		// RGB, 1-bit Alpha, SRGB
		BC3_LINEAR = DXGI_FORMAT_BC3_UNORM,			// RGBA
		BC3_SRGB = DXGI_FORMAT_BC3_UNORM_SRGB,		// RGBA, SRGB
		BC4_UNSIGNED = DXGI_FORMAT_BC4_UNORM,		// GRAY, unsigned
		BC4_SIGNED = DXGI_FORMAT_BC4_SNORM,			// GRAY, signed
		BC5_UNSIGNED = DXGI_FORMAT_BC5_UNORM,		// RG, unsigned
		BC5_SIGNED = DXGI_FORMAT_BC5_SNORM,			// RG, signed
		BC6_UNSIGNED = DXGI_FORMAT_BC6H_UF16,		// RGB HDR, unsigned
		BC6_SIGNED = DXGI_FORMAT_BC6H_SF16,			// RGB HDR, signed
		BC7_LINEAR = DXGI_FORMAT_BC7_UNORM,			// RGBA Advanced
		BC7_SRGB = DXGI_FORMAT_BC7_UNORM_SRGB,		// RGBA Advanced, SRGB
	};

	TextureManager(const TextureManager& other) = delete;

	void init();
	void deinit();

	void operator=(const TextureManager& other) = delete;
	bool getTexture(std::shared_ptr<Texture>& texture, const std::string& path);
	std::shared_ptr<Texture> getTexture(const std::string& path);
	void save(std::wstring path, Texture& gpuTexture, FileFormat format, bool generateMips);

	std::shared_ptr<Texture> getDiffuseIrradianCubeMap(const std::string& path);
	std::shared_ptr<Texture> getSpecularIrradianCubeMap(const std::string& path);
	std::shared_ptr<Texture> getSpecularReflectanceCubeMap(const std::string& path);
	std::shared_ptr<Texture> getSimplexNoise3D(const std::string& path);

	static TextureManager& instance();
private:
	TextureManager() = default;

	std::shared_ptr<Texture> DDSTextureLoader(const std::string& path);
	bool DDSTextureLoader(std::shared_ptr<Texture>& texture, const std::string& path);
	std::unordered_map <std::string, std::shared_ptr<Texture>> m_loaded_textures;
};
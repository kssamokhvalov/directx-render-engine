#include "TransformSystem.h"

TransformSystem& TransformSystem::instance()
{
	static TransformSystem s_mesh_system;
	return s_mesh_system;
}

void TransformSystem::init()
{
	addCameraMatrix(Matrix4x4::identity());
}

void TransformSystem::deinit()
{
}

engine::ID TransformSystem::addMatrix(const Matrix4x4& matrix)
{
	return m_matrixes.insert(matrix);
}

engine::ID TransformSystem::addCameraMatrix(const Matrix4x4& matrix)
{
	return m_id_camera_matrix = addMatrix(matrix);
}

void TransformSystem::changeCameraMatrix(const Matrix4x4& matrix)
{
	changeMatrix(matrix, m_id_camera_matrix);
}

void TransformSystem::changeMatrix(const Matrix4x4& matrix, engine::ID id)
{
	m_matrixes[id] = matrix;
}

Matrix4x4 TransformSystem::getMatrix(engine::ID id)
{
	return m_matrixes[id];
}

void TransformSystem::deleteMatrix(engine::ID id)
{
	m_matrixes.erase(id);
}

Matrix4x4 TransformSystem::getCameraCenteredMatrix(engine::ID id)
{
	Matrix4x4 matix = getMatrix(id);
	Vector4 T = Vector4(matix.rowAsVector3(3) - getCameraMatrix().rowAsVector3(3), 1.0f);
	matix.setRow(3, T);
	return matix;
}

Matrix4x4 TransformSystem::getCameraMatrix()
{
	return getMatrix(m_id_camera_matrix);
}

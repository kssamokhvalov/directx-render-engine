#pragma once
#include "../../utils/SolidVector.h"
#include "../../source/math/Matrix4x4.h"
class TransformSystem
{
public:
	TransformSystem(const TransformSystem& other) = delete;
	void operator=(const TransformSystem& other) = delete;

	static TransformSystem& instance();

	void init();
	void deinit();

	engine::ID addMatrix(const Matrix4x4& matrix);
	engine::ID addCameraMatrix(const Matrix4x4& matrix);
	void changeCameraMatrix(const Matrix4x4& matrix);
	void changeMatrix(const Matrix4x4& matrix, engine::ID id);
	Matrix4x4 getMatrix(engine::ID id);
	void deleteMatrix(engine::ID id);
	Matrix4x4 getCameraCenteredMatrix(engine::ID id);
	Matrix4x4 getCameraMatrix();
private:
	TransformSystem() = default;
	SolidVector<Matrix4x4> m_matrixes;
	engine::ID m_id_camera_matrix;
};


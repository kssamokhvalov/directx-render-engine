#include "VolumetricFog.h"
#include "ModelManager.h"

void VolumetricFog::init()
{
	m_vertex_shader.loadVertexShader(L"../engine/shaders/VolumetricFog/VertexVolumetricFog.hlsl");
	{
		D3D11_INPUT_ELEMENT_DESC polygonLayout[] =
		{
			{"POSITION",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(engine::Mesh::Vertex, pos), D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"MW",  1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 32, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  4, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 48, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  5, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 4 * 16, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  6, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 5 * 16, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  7, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 6 * 16, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MW",  8, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 7 * 16, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MIN",  0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 8 * 16, D3D11_INPUT_PER_INSTANCE_DATA, 1},
			{"MAX",  0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 9 * 16, D3D11_INPUT_PER_INSTANCE_DATA, 1}
		};

		m_input_layout.loadInputLayout(m_vertex_shader.getShaderBlob(), polygonLayout, std::size(polygonLayout));
	}

	m_pixel_shader.loadPixelShader(L"../engine/shaders/VolumetricFog/PixelVolumetricFog.hlsl");
	m_cube = ModelManager::instance().getUnitCube();
}

void VolumetricFog::deinit()
{
	m_vertex_shader.release();
	m_pixel_shader.release();
	m_input_layout.release();
	m_instanceBuffer.deinit();

	m_simplex_noise.reset();
	m_cube.reset();
}

void VolumetricFog::render()
{
	if (m_instanceBuffer.size() == 0)
		return;

	engine::s_devcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


	m_vertex_shader.bind();
	m_pixel_shader.bind();
	m_input_layout.bind();

	engine::s_devcon->HSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->DSSetShader(nullptr, nullptr, 0);
	engine::s_devcon->GSSetShader(nullptr, nullptr, 0);

	m_instanceBuffer.bind(1);
	m_simplex_noise->bind(27, TypePixelShader);


	uint32_t renderedInstances = 0;

	m_cube->bindBuffers();
	const auto& meshRange = m_cube->m_ranges[0];
	for (const auto& perMaterial : m_perMaterial)
	{
		uint32_t numInstances = uint32_t(perMaterial.instances.size());
		if (m_cube->indexBuffer().empty() == false)
		{
			engine::s_devcon->DrawIndexedInstanced(meshRange.indexNum, numInstances, meshRange.indexOffset, meshRange.vertexOffset, renderedInstances);
		}
		else {
			engine::s_devcon->DrawInstanced(meshRange.vertexNum, numInstances, meshRange.vertexOffset, renderedInstances);
		}
		renderedInstances += numInstances;
	}
}

void VolumetricFog::add(const Material& material, const InstanceIDs& instance, MeshInstanceID& entity)
{
	if (m_perMaterial.size() == 0)
	{
		m_perMaterial.push_back(PerMaterial{});
	}

	m_perMaterial[0].instances.push_back(instance);
	m_perMaterial[0].material = material;

	entity.id_component_main.id_model = 0; //TODO
	entity.id_component_main.id_material = 0;
	entity.id_component_main.id_instance = m_perMaterial[0].instances.size() - 1;

	updateInstanceBuffers();
}

void VolumetricFog::updateInstanceBuffers()
{
	uint32_t totalInstances = 0;
	for (const auto& perMaterial : m_perMaterial)
		totalInstances += uint32_t(perMaterial.instances.size());

	if (totalInstances == 0)
		return;

	m_instanceBuffer.init(totalInstances, D3D11_USAGE_DYNAMIC);

	auto mapping = m_instanceBuffer.map();
	VolumetricFogInstance* dst = static_cast<VolumetricFogInstance*>(mapping.pData);

	uint32_t copiedNum = 0;
	for (const auto& perMaterial : m_perMaterial)
	{
		auto& instances = perMaterial.instances;

		uint32_t numModelInstances = instances.size();
		for (uint32_t index = 0; index < numModelInstances; ++index)
		{

#ifdef LIGHT_CALCULATION_WORLD_SPACE
			dst[copiedNum++] = { TransformSystem::instance().getMatrix(instances[index].id_decalToWorld),
			TransformSystem::instance().getMatrix(instances[index].id_worldToDecal) };
#else
			dst[copiedNum++] = { TransformSystem::instance().getCameraCenteredMatrix(instances[index].id_fogToWorld),
								 TransformSystem::instance().getCameraCenteredMatrix(instances[index].id_fogToWorld).getInversedMatrix(),
								 m_cube->box.min, 0.0f,
								 m_cube->box.max };
#endif // LIGHT_CALCULATION_WORLD_SPACE

		}
	}


	m_instanceBuffer.unmap();
}

#pragma once
#include "TransformSystem.h"
#include "../../source/Model.h"
#include "../d3d_utils/PixelShader.h"
#include "../d3d_utils/VertexShader.h"
#include "../d3d_utils/VertexBuffer.h"
#include "../d3d_utils/InputLayout.h"
#include "../d3d_utils/Texture.h"
#include "../../source/Entity.h"

class VolumetricFog
{
public:

    struct Material {
    };

    struct InstanceIDs {
        engine::ID id_fogToWorld;
        size_t id_entity;
    };

    struct VolumetricFogInstance
    {
        Matrix4x4 fogToWorld;
        Matrix4x4 worldToFog;
        //Bounding  box
        Vector3 min;
        float _pad1;
        Vector3 max;
        float _pad2;
    };

    struct PerMaterial {
        Material material;
        std::vector<InstanceIDs> instances;
    };

    void init();
    void deinit();
    void render();

    void add(const Material& material, const InstanceIDs& instance, MeshInstanceID& entity);
    void updateInstanceBuffers();
    void setNoise(std::shared_ptr<Texture> noise) { m_simplex_noise = noise; }
private:
    std::vector<PerMaterial> m_perMaterial;
    VertexShader m_vertex_shader;
    PixelShader m_pixel_shader;
    InputLayout m_input_layout;
    VertexBuffer<VolumetricFogInstance> m_instanceBuffer;

    std::shared_ptr<Model> m_cube;
    std::shared_ptr<Texture> m_simplex_noise;
};


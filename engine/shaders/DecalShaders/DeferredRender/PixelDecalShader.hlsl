#include "../../globals.hlsl"
#include "../../utils.hlsl"

#define EN_DIFFUSE 1
#define EN_SPECLAR 2
#define EN_IBL 4
#define EN_ROUGHNESS_OVERWRITING 8


struct VS_Output
{
    float4 position : SV_POSITION;
    float4 positionView : POSITION;
    float4x4 decalToWorld : MATRIX_ONE;
    float4x4 worldToDecal : MATRIX_TWO;
    uint id_object_attached_model : ID_OBJECT;
};

struct PS_OUTPUT
{
    float4 albedo: SV_Target0;
    float4 rougness_metalness: SV_Target1;
    float4 normals: SV_Target2;
    float4 emmision : SV_Target3;
    uint id_objects: SV_Target4;
};


PS_OUTPUT main(VS_Output input) : SV_TARGET
{

    uint id_attached_object = g_gb_objectID.Load(float3(input.position.xy, 0)).x;
    if(input.id_object_attached_model != id_attached_object)
        discard;

    float3 positionView;

    positionView.x = input.position.x / g_mainResolution.x;
    positionView.y = (g_mainResolution.y - input.position.y) / g_mainResolution.y; // swap coordinats y
    positionView.z = g_DepthBufferCopy.Load(float3(input.position.xy, 0)).x;
    positionView.xy = 2.0f * positionView.xy - 1.0f;

#ifdef LIGHT_CALCULATION_WORLD_SPACE
#else
    float4x4 worldToViewInv = g_worldToViewInv();
    float4x4 projectionInv = g_projectionInv();
#endif 

#ifdef LIGHT_CALCULATION_WORLD_SPACE
    float4 position = mul(float4(positionView, 1.0f), g_worldToViewProjInv());
    positionView = position.xyz / position.w;
#else
    float4 position = mul(float4(positionView, 1.0f), projectionInv);
    position.xyz = position.xyz / position.w;
    position.xyz = mul(float4(position.xyz, 0.0f), worldToViewInv).xyz;
#endif 

    positionView = mul(float4(position.xyz, 1.0f), input.worldToDecal);
    positionView *= 2.0f;    //TODO cube mesh have size from -0.5 to 0.5
    if (abs(positionView.x) > 1.0f || abs(positionView.y) > 1.0f || abs(positionView.z) > 1.0f)
        discard;

    float2 tc = 0.5f * positionView.xy + 0.5f;

    tc.y = 1.0f - tc.y;

    PS_OUTPUT output;

    if (g_hasAlbedoTexture() == true)
        output.albedo.xyz = g_albedoTexture.Sample(g_anisotropicWrap, tc);
    else
        output.albedo.xyz = g_texture_albedo;

    float3 gN = unpackOctahedron(g_gb_normal.Load(float3(input.position.xy, 0)).ba);

    //normal test
    if(mul(float4(gN, 0.0f), input.worldToDecal).z > 0.0f)  //TODO
        discard;

    float3 T = input.decalToWorld._m00_m10_m20;
    T = normalize(T - gN * dot(gN, T));
    float3 B = normalize(cross(gN, T));
    float3x3 TBN = float3x3(T, B, gN);

    float3 normal;
    float alphaNormal;
    if (g_hasNormalTexture() == true)
    {
        normal = g_normalTexture.Sample(g_anisotropicWrap, tc).xyz;
        alphaNormal = g_normalTexture.Sample(g_anisotropicWrap, tc).a;
    }
    else
    {
        alphaNormal = 1.0f;
        normal = g_texture_normal;
    }
    float3 N = unpackOctahedron(g_gb_normal.Load(float3(input.position.xy, 0)).rg);
    normal = normal * 2.0f - 1.0f;
    normal = normalize(mul(normal, TBN));
    normal = alphaNormal * normal + N * (1.0f - alphaNormal);
    output.normals = g_gb_normal.Load(float3(input.position.xy, 0));
    output.normals.rg = packOctahedron(normal);

    if (g_flags & EN_ROUGHNESS_OVERWRITING)
    {
        output.rougness_metalness.x = g_roughnessSlider;
    }
    else
    {
        if (g_hasRoughnessTexture() == true)
            output.rougness_metalness.x = g_roughnessTexture.Sample(g_anisotropicWrap, tc);
        else
            output.rougness_metalness.x = g_texture_roughness;

    }


    if (g_hasMetalnessTexture() == true)
        output.rougness_metalness.y = g_metalnessTexture.Sample(g_anisotropicWrap, tc);
    else
        output.rougness_metalness.y = g_texture_metalness;


    output.albedo.a = alphaNormal;
    output.rougness_metalness.a = alphaNormal;
    output.emmision.a = alphaNormal;

    return output;
}
#include "../globals.hlsl"

struct VS_Input
{
    float3 pos : POSITION;
    float3 normal : NORMAL;

    float4 decalToWorld1 : MW1;
    float4 decalToWorld2 : MW2;
    float4 decalToWorld3 : MW3;
    float4 decalToWorld4 : MW4;

    float4 worldToDecal1 : MW5;
    float4 worldToDecal2 : MW6;
    float4 worldToDecal3 : MW7;
    float4 worldToDecal4 : MW8;

    uint id_object_attached_model : ID_OBJECT;
};

struct VS_Output
{
    float4 position : SV_POSITION;
    float4 positionView : POSITION;
    float4x4 decalToWorld : MATRIX_ONE;
    float4x4 worldToDecal : MATRIX_TWO;
    uint id_object_attached_model : ID_OBJECT;
};

VS_Output main(VS_Input input)
{
    VS_Output output;
    output.position = float4(input.pos, 1.0f);
#ifdef LIGHT_CALCULATION_WORLD_SPACE
#else
    float4x4 worldToView = g_worldToView();
    float4x4 projection = g_projection();
#endif 
    output.decalToWorld = float4x4(input.decalToWorld1, input.decalToWorld2, input.decalToWorld3, input.decalToWorld4);

    output.position = mul(output.position, output.decalToWorld);
    output.positionView = output.position;

#ifdef LIGHT_CALCULATION_WORLD_SPACE
    output.position = mul(output.position, g_worldToViewProj());
#else
    output.position = mul(float4(output.position.xyz, 0.0f), worldToView);
    output.position = mul(float4(output.position.xyz, 1.0f), projection);
#endif 
    output.worldToDecal = float4x4(input.worldToDecal1, input.worldToDecal2, input.worldToDecal3, input.worldToDecal4);
    output.id_object_attached_model = input.id_object_attached_model;
    return output;
}
#include "../globals.hlsl"
#include "../utils.hlsl"

struct VS_Output
{
	float4 position : SV_POSITION;
};

float4 main(VS_Output input) : SV_TARGET
{
	return float4(g_gb_emmision.Load(float3(input.position.xy, 0)).rgb, 1.0f);
}
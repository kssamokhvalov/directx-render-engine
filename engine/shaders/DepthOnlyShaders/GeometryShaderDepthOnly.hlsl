#include "../globals.hlsl"

struct GS_Output
{
    float4 position : SV_POSITION;
    uint slice : SV_RenderTargetArrayIndex;
};

struct VS_Output
{
    float4 position : SV_POSITION;
};

cbuffer Light : register(b2)
{
    float3 light_position;
    float _pad_b2;
}

[maxvertexcount(3 * 6)]
void main(
    triangle VS_Output input[3]  : SV_POSITION,
    inout TriangleStream< GS_Output > output
)
{

    const float3x3 MATRIX_FACES[6] = { { float3(0.0f, 0.0f, 1.0f), float3(0.0f, 1.0f, 0.0f), float3(-1.0f, 0.0f, 0.0f) },	    //+X
                               { float3(0.0f, 0.0f, -1.0f), float3(0.0f, 1.0f, 0.0f), float3(1.0f, 0.0f, 0.0f) }, 		        //-X
                               { float3(1.0f, 0.0f, 0.0f), float3(0.0f, 0.0f, 1.0f), float3(0.0f, -1.0f, 0.0f) }, 		        //+Y
                               { float3(1.0f, 0.0f, 0.0f), float3(0.0f, 0.0f, -1.0f), float3(0.0f, 1.0f, 0.0f) },		        //-Y
                               { float3(1.0f, 0.0f, 0.0f), float3(0.0f, 1.0f, 0.0f), float3(0.0f, 0.0f, 1.0f) }, 		        //+Z
                               { float3(-1.0f, 0.0f, 0.0f), float3(0.0f, 1.0f, 0.0f), float3(0.0f, 0.0f, -1.0f) } };		    //-Z
    
    const float4x4 projection = { float4(1.00079664403f, 0.0f,           0.0f,                                                                            0.0f),
                                  float4(0.0f,           1.00079664403f, 0.0f,                                                                            0.0f),
                                  float4(0.0f,           0.0f,           NEAR_PLANE_SHADOW / (NEAR_PLANE_SHADOW - FAR_PLANE_SHADOW),                      1.0f),
                                  float4(0.0f,           0.0f,           -FAR_PLANE_SHADOW * NEAR_PLANE_SHADOW / (NEAR_PLANE_SHADOW - FAR_PLANE_SHADOW),  0.0f) };

    float4x4 worldToLight = { float4(0.0f, 0.0f, 0.0f, 0.0f),
        float4(0.0f, 0.0f, 0.0f, 0.0f),
        float4(0.0f, 0.0f, 0.0f, 0.0f),
        float4(0.0f, 0.0f, 0.0f, 1.0f) };


    for (uint i = 0; i < 6; i++)
    {
        worldToLight._m00_m01_m02 = MATRIX_FACES[i]._m00_m01_m02;
        worldToLight._m10_m11_m12 = MATRIX_FACES[i]._m10_m11_m12;
        worldToLight._m20_m21_m22 = MATRIX_FACES[i]._m20_m21_m22;

        float3 T = mul(-light_position, MATRIX_FACES[i]);
        worldToLight._m30_m31_m32 = T;

        for (uint k = 0; k < 3; k++)
        {
            GS_Output element;
            element.position = mul(input[k].position, worldToLight);
            element.position = mul(element.position, projection);
            element.slice = i;
            output.Append(element);
        }

        output.RestartStrip();
    }
}
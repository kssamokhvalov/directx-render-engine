#include "../globals.hlsl"

struct VS_Input
{
    float3 pos : POSITION;
    float3 normal : NORMAL;
    float4 modelToWorld1 : MW1;
    float4 modelToWorld2 : MW2;
    float4 modelToWorld3 : MW3;
    float4 modelToWorld4 : MW4;
};

struct VS_Output
{
    float4 position : SV_POSITION;
};

cbuffer MeshToModel : register(b2)
{
    float4 meshToModel1;
    float4 meshToModel2;
    float4 meshToModel3;
    float4 meshToModel4;
}

VS_Output main(VS_Input input)
{
    VS_Output output;
    output.position = float4(input.pos, 1.0f);
    float4x4 meshToModel = float4x4(meshToModel1, meshToModel2, meshToModel3, meshToModel4);
#ifdef LIGHT_CALCULATION_WORLD_SPACE
#else
    float4x4 worldToLight = g_worldToView(); // TODO maybe better create new function
    float4x4 projection = g_projection();
#endif 
    float4x4 modelToWorld = float4x4(input.modelToWorld1, input.modelToWorld2, input.modelToWorld3, input.modelToWorld4);

    output.position = mul(output.position, meshToModel);
    output.position = mul(output.position, modelToWorld);

#ifdef LIGHT_CALCULATION_WORLD_SPACE
    output.position = mul(output.position, g_worldToViewProj());
#else
    output.position = mul(float4(output.position.xyz, 1.0f), worldToLight);
    output.position = mul(float4(output.position.xyz, 1.0f), projection);
#endif 
    return output;
}
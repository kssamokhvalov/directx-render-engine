#include "../utils.hlsl"
#include "../globals.hlsl"

static const int NUM_SAMPLES = 1024;



struct VS_Output
{
	float4 position : SV_POSITION;
	float3 normal : POSITION;
};

float3 main(VS_Output input) : SV_TARGET
{
	const float3x3 R = basisFromDir(normalize(input.normal));

	float3 ans_color = float3(0.0f, 0.0f, 0.0f);
	float NoL = 0.0f;

	uint width;
	uint height;

	g_skyBox.GetDimensions(width, height);

	float resolution = width;
	float pdf = 1 / (2 * PI); 
	float saTexel = 4.0f * PI / (6.0f * resolution * resolution);
	float saSample = 1.0f / (float(NUM_SAMPLES) * pdf);
	float mipLevel = g_roughness == 0.0 ? 0.0 : 0.5 * log2(saSample / saTexel);
	for (int i = 0; i < NUM_SAMPLES; ++i)
	{
		float3 L = randomHemisphere(NoL, i, NUM_SAMPLES);
		L = mul(L, R);
		float3 Ei = g_skyBox.SampleLevel(g_linearWrap, L, mipLevel);

		ans_color += Ei * NoL * (1 - fresnel(NoL, float3(TYPICALLY_F0, TYPICALLY_F0, TYPICALLY_F0))) / PI;
	}
	return 2 * PI * ans_color / NUM_SAMPLES;
}


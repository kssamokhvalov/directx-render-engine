#include "../../globals.hlsl"
#include "../../utils.hlsl"

struct PS_Input
{
    float4 position : SV_POSITION;
    float2 tc : TEXCOORD;
    nointerpolation float start_time : TIME1;
    nointerpolation float duration : TIME2;
};

float main(PS_Input input) : SV_Depth
{
    //Masking
    float noise = g_noise.Sample(g_anisotropicWrap, input.tc);
    float alpha = (g_time - input.start_time) / input.duration + noise;
    alpha = derivatives(alpha, 0.95f, 0.1f);

    if(alpha < 1.0f)
        discard;

	return input.position.z;
}
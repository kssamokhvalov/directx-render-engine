#include "../../globals.hlsl"

struct VS_Input
{
    float3 pos : POSITION;
    float3 normal : NORMAL;
    float2 tc : TEXCOORD;
    float4 modelToWorld1 : MW1;
    float4 modelToWorld2 : MW2;
    float4 modelToWorld3 : MW3;
    float4 modelToWorld4 : MW4;
    float2 time_animation : TIME;
};

struct VS_Output
{
    float4 position : SV_POSITION;
    float2 tc : TEXCOORD;
    nointerpolation float start_time : TIME1;
    nointerpolation float duration : TIME2;
};

cbuffer MeshToModel : register(b2)
{
    float4 meshToModel1;
    float4 meshToModel2;
    float4 meshToModel3;
    float4 meshToModel4;
}

VS_Output main(VS_Input input)
{
    VS_Output output;
    output.position = float4(input.pos, 1.0f);
    float4x4 meshToModel = float4x4(meshToModel1, meshToModel2, meshToModel3, meshToModel4);
    float4x4 modelToWorld = float4x4(input.modelToWorld1, input.modelToWorld2, input.modelToWorld3, input.modelToWorld4);


    output.position = mul(output.position, meshToModel);
    output.position = mul(output.position, modelToWorld);
    output.tc = input.tc;
    output.start_time = input.time_animation.x;
    output.duration = input.time_animation.y;
    return output;
}
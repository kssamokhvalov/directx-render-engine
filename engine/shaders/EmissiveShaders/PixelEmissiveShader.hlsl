#include "../globals.hlsl"

struct VS_Output
{
    float4 position : SV_POSITION;
    nointerpolation float4 emission : COL;
    float4 worldPos : POS;
    float4 normal : NORMAL;
};

float4 main(VS_Output input) : SV_TARGET
{
    float3 normal = normalize(input.normal);
#ifdef LIGHT_CALCULATION_WORLD_SPACE
    float3 cameraDir = normalize((g_cameraPosition() - input.worldPos).xyz);
#else
    float3 cameraDir = normalize( - input.worldPos.xyz);
#endif
    

    float3 normedEmission = input.emission / max(input.emission.x, max(input.emission.y, max(input.emission.z, 1.0)));

    float NoV = dot(cameraDir, normal);
    float3 ans_color = lerp(normedEmission * 0.33, input.emission.xyz, pow(max(0.0, NoV), 8));
    return float4(ans_color, 1.0f);
}
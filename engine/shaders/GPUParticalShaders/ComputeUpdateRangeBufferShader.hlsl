#include "../globals.hlsl"
RWStructuredBuffer<ParticleGPU> g_GPU_particals : register(u0);
RWByteAddressBuffer g_range_description : register(u1);

[numthreads(1, 1, 1)]
void main(uint3 globalThreadId : SV_DispatchThreadID)
{
    uint max_particles = 0;
    uint stride = 0;
    g_GPU_particals.GetDimensions(max_particles, stride);

    uint number = g_range_description.Load(0);
    uint offset = g_range_description.Load(4);
    uint expired = g_range_description.Load(4 * 2);

    number = clamp(0, max_particles, number);
    number = number - expired;
    offset = (offset + expired) % max_particles;
    expired = 0;

    g_range_description.Store(0, number);
    g_range_description.Store(4, offset);
    g_range_description.Store(4 * 2, expired);
    g_range_description.Store(4 * 3, 6); //Billboard
    g_range_description.Store(4 * 4, number);
    g_range_description.Store(4 * 5, 0);
    g_range_description.Store(4 * 6, offset);

}
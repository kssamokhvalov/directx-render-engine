#include "../../globals.hlsl"
#include "../../utils.hlsl"

static const float RADIUS_PARTICLE = 0.25f;
static const float THICKNESS = 0.0002f;
static const float VANISHING_OFFSET = 0.90f;

struct PS_Input
{
    float4 position : SV_POSITION;
    nointerpolation float3 emission_color : COLOR;
    nointerpolation float3 position_particle : POSITION_PARTICLE;
    nointerpolation float frameFraction : DURATION;
};


float4 main(PS_Input input) : SV_TARGET
{
    float3 surfacePosition;

    surfacePosition.x = input.position.x / g_mainResolution.x;
    surfacePosition.y = (g_mainResolution.y - input.position.y) / g_mainResolution.y; // swap coordinats y
    surfacePosition.z = g_DepthBufferCopy.Load(float3(input.position.xy, 0)).x;
    surfacePosition.xy = 2.0f * surfacePosition.xy - 1.0f;

#ifdef LIGHT_CALCULATION_WORLD_SPACE
#else
    float4x4 worldToViewInv = g_worldToViewInv();
    float4x4 projectionInv = g_projectionInv();
#endif 

#ifdef LIGHT_CALCULATION_WORLD_SPACE
    float4 position = mul(float4(surfacePosition, 1.0f), g_worldToViewProjInv());
    surfacePosition = position.xyz / position.w;
#else
    float4 position = mul(float4(surfacePosition, 1.0f), projectionInv);
    position.xyz = position.xyz / position.w;
    surfacePosition = mul(float4(position.xyz, 0.0f), worldToViewInv);
#endif 
    float3 R = input.position_particle - surfacePosition;
    float distance = length(R);
    R = normalize(R);
    float3 normal = unpackOctahedron(g_gb_normal.Load(float3(input.position.xy, 0)).rg);

    float RoN = max(0.001f, dot(R, normal));
    float vanishing_coff = min(1.0f, (1.0f - input.frameFraction) / (1.0f - VANISHING_OFFSET));
    float alpha = distance / RADIUS_PARTICLE;
    alpha = 1.0f - clamp(0.0f, 1.0f, alpha);
	return float4(input.emission_color, alpha * alpha * vanishing_coff);
}
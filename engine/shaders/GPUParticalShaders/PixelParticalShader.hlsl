#include "../globals.hlsl"
#include "../utils.hlsl"

static const float VANISHING_OFFSET = 0.90f;

struct PS_Input
{
    float4 position : SV_POSITION;
    float2 tc : TEXCOORD;
    nointerpolation float4 color : COLOR;
    nointerpolation float frameFraction : DURATION;
};

Texture2D<float> g_spark : register(t27);

float4 main(PS_Input input) : SV_TARGET
{
    float3 color = input.color.rgb;
    float gray_scale = g_spark.Sample(g_anisotropicWrap, input.tc);
    float alpha = min(1.0f, (1.0f - input.frameFraction) / (1.0f - VANISHING_OFFSET));
	return float4(color, gray_scale * alpha);
}
#include "../globals.hlsl"
#include "../utils.hlsl"

static float REDUCED_SPEED_MAGNITUDE = 0.8f;
static float ACCELERATION_GRAVITY = 9.8f; //random, for EARTH 9.8f

RWStructuredBuffer<ParticleGPU> g_GPU_particals : register(u0);
RWByteAddressBuffer  g_range_description : register(u1);

[numthreads(32, 1, 1)]
void main( uint3 globalThreadId : SV_DispatchThreadID )
{
	ParticleGPU particle = g_GPU_particals[globalThreadId.x];

	if (particle.active == 0)
		return;

	if (particle.start_time + particle.life_time < g_time)
	{
		g_range_description.InterlockedAdd(4 * 2, 1); //g_range_description[0].expired
		g_GPU_particals[globalThreadId.x].active = 0;
		return;
	}

	float delta_t = g_time - g_time_prev_frame;

	float3 position = particle.position + particle.speed * delta_t + float3(0.0f, -ACCELERATION_GRAVITY, 0.0f) * delta_t * delta_t / 2;

	float4 positionView = mul(float4(position, 1.0f), g_worldToViewProj());
	positionView.xyz = positionView.xyz / positionView.w;

	float position_z = positionView.z;
	float2 tc = 0.5 * positionView.xy + 0.5;
	tc.y = 1 - tc.y; //swap y
	uint2 resolution = tc * g_mainResolution.xy;
	float difference_z = g_DepthBufferCopy.Load(int3(resolution, 0)).x;
	if (difference_z > position_z)
	{
		if(length(particle.speed) < 0.025f * ACCELERATION_GRAVITY)  //avoid shaking
			return;

		float3 normal = unpackOctahedron(g_gb_normal.Load(float3(tc * g_mainResolution.xy, 0)).rg);
		particle.speed = reflect(particle.speed, normal) * REDUCED_SPEED_MAGNITUDE;
	}

	particle.position += particle.speed * delta_t + float3(0.0f, -ACCELERATION_GRAVITY, 0.0f) * delta_t * delta_t / 2;
	particle.speed += float3(0.0f, -ACCELERATION_GRAVITY, 0.0f) * delta_t; //gravity

	positionView = mul(float4(particle.position, 1.0f), g_worldToViewProj());
	positionView.xyz = positionView.xyz / positionView.w;

	if (positionView.x > 1.0f || positionView.x < -1.0f || positionView.y > 1.0f || positionView.y < -1.0f) //checking for particle inside screen
	{
		g_range_description.InterlockedAdd(4 * 2, 1); //g_range_description[0].expired
		particle.active = 0;
	}

	g_GPU_particals[globalThreadId.x] = particle;


}
#include "../globals.hlsl"
#include "../utils.hlsl"

static const float SPARK_SIZE = 0.3;

struct VS_Input
{
    uint vertex_id : SV_VertexID;
    uint instance_id : SV_InstanceID;
};

struct VS_Output
{
    float4 position : SV_POSITION;
    float2 tc : TEXCOORD;
    nointerpolation float4 color : COLOR;
    nointerpolation float frameFraction : DURATION;
};

StructuredBuffer<ParticleGPU> g_GPU_particals : register(t27);
ByteAddressBuffer g_range_description : register(t28);

VS_Output main(VS_Input input)
{
    static const int NUM_TEXTURS = 8;

    uint offset = g_range_description.Load(4);
    uint max_particles = 0;
    uint stride = 0;
    g_GPU_particals.GetDimensions(max_particles, stride);

    ParticleGPU particle = g_GPU_particals[(input.instance_id + offset) % max_particles];

    VS_Output output;

#ifdef LIGHT_CALCULATION_WORLD_SPACE
#else
    float4x4 worldToView = g_worldToView();
    float4x4 projection = g_projection();
#endif 

#ifdef LIGHT_CALCULATION_WORLD_SPACE
#else
    particle.position -= g_worldToViewInv()._m30_m31_m32;
#endif 
    float3 right = g_worldToView()._m00_m10_m20;
    float3 top = g_worldToView()._m01_m11_m21;

    right *= SPARK_SIZE / 2;
    top *= SPARK_SIZE;

    float3 position = particle.position - top / 2;

    switch (input.vertex_id)
    {
    case 0:
        output.position = float4(position - right, 1.0f);
        output.tc = float2(0.0f, 1.0f);
        break;
    case 1:
    case 4:
        output.position = float4(position - right + top, 1.0f);
        output.tc = float2(0.0f, 0.0f);
        break;
    case 2:
    case 3:
        output.position = float4(position + right, 1.0f);
        output.tc = float2(1.0f, 1.0f);
        break;
    case 5:
        output.position = float4(position + right + top, 1.0f);
        output.tc = float2(1.0f, 0.0f);
        break;
    default:
        break;
    }

#ifdef LIGHT_CALCULATION_WORLD_SPACE
    output.position = mul(output.position, g_worldToViewProj());
#else
    output.position = mul(float4(output.position.xyz, 0.0f), worldToView);
    output.position = mul(float4(output.position.xyz, 1.0f), projection);
#endif 

    output.color = particle.tint;
    output.frameFraction = (g_time - particle.start_time) / particle.life_time;

	return output;
}
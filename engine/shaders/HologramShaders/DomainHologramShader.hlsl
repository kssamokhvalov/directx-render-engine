

struct VS_Output
{
	float4 position : SV_POSITION;
	nointerpolation float4 color : COL;
	float4 positionLocal : POSITION1;
	float4 positionGlobal : POSITION2;
	float4 normal : NORMAL;
};

struct DS_Output
{
	float4 position : SV_POSITION;
	float4 normal : NORMAL;
};

struct PatchOut
{
	float EdgeTessFactor[3]	: SV_TessFactor;
	float InsideTessFactor : SV_InsideTessFactor;
};

[domain("tri")]
VS_Output main(
	PatchOut input,
	float3 domain : SV_DomainLocation,
	const OutputPatch<VS_Output, 3> patch)
{
	VS_Output Output;

	Output.position = 
		patch[0].position * domain.x + patch[1].position * domain.y + patch[2].position * domain.z;
	Output.positionGlobal =
		patch[0].positionGlobal * domain.x + patch[1].positionGlobal * domain.y + patch[2].positionGlobal * domain.z;
	Output.positionLocal = 
		patch[0].positionLocal * domain.x + patch[1].positionLocal * domain.y + patch[2].positionLocal * domain.z;
	Output.normal =
		patch[0].normal * domain.x + patch[1].normal * domain.y + patch[2].normal * domain.z;

	Output.color = patch[0].color;

	return Output;
}

#include "../globals.hlsl"

struct VS_Output
{
    float4 position : SV_POSITION;
    nointerpolation float4 color : COL;
    float4 positionLocal : POSITION1;
    float4 positionGlobal : POSITION2;
    float4 normal : NORMAL;
};

// BEGIN ShaderToy https://www.shadertoy.com/view/WttcRB
float hash4d(in float4 p) {
    p = frac(p * 0.1031);
    p += dot(p, p.zwyx + 31.32);
    return frac((p.x + p.y) * p.z - p.x * p.w);
}

float noise4d(in float4 p) {
    float4 cell = floor(p);
    float4 local = frac(p);
    local *= local * (3.0 - 2.0 * local);

    float ldbq = hash4d(cell);
    float rdbq = hash4d(cell + float4(1.0, 0.0, 0.0, 0.0));
    float ldfq = hash4d(cell + float4(0.0, 0.0, 1.0, 0.0));
    float rdfq = hash4d(cell + float4(1.0, 0.0, 1.0, 0.0));
    float lubq = hash4d(cell + float4(0.0, 1.0, 0.0, 0.0));
    float rubq = hash4d(cell + float4(1.0, 1.0, 0.0, 0.0));
    float lufq = hash4d(cell + float4(0.0, 1.0, 1.0, 0.0));
    float rufq = hash4d(cell + float4(1.0, 1.0, 1.0, 0.0));
    float ldbw = hash4d(cell + float4(0.0, 0.0, 0.0, 1.0));
    float rdbw = hash4d(cell + float4(1.0, 0.0, 0.0, 1.0));
    float ldfw = hash4d(cell + float4(0.0, 0.0, 1.0, 1.0));
    float rdfw = hash4d(cell + float4(1.0, 0.0, 1.0, 1.0));
    float lubw = hash4d(cell + float4(0.0, 1.0, 0.0, 1.0));
    float rubw = hash4d(cell + float4(1.0, 1.0, 0.0, 1.0));
    float lufw = hash4d(cell + float4(0.0, 1.0, 1.0, 1.0));
    float rufw = hash4d(cell + 1.0);

    return lerp(lerp(lerp(lerp(ldbq, rdbq, local.x),
        lerp(lubq, rubq, local.x),
        local.y),

        lerp(lerp(ldfq, rdfq, local.x),
            lerp(lufq, rufq, local.x),
            local.y),

        local.z),

        lerp(lerp(lerp(ldbw, rdbw, local.x),
            lerp(lubw, rubw, local.x),
            local.y),

            lerp(lerp(ldfw, rdfw, local.x),
                lerp(lufw, rufw, local.x),
                local.y),

            local.z),

        local.w);
}

float noise4d(in float4 p, uniform in float octaves)
{
    float nscale = 1.0;
    float tscale = 0.0;
    float value = 0.0;

    for (float octave = 0.0; octave < octaves; octave++) {
        value += noise4d(p) * nscale;
        tscale += nscale;
        nscale *= 0.5;
        p *= 2.0;
    }

    return value / tscale;
}
// END ShaderToy https://www.shadertoy.com/view/WttcRB

float distanceIntensity(float value, float target, float fade)
{
    return min(1.0, abs(value - target) / fade);
}

float periodIntensity(float value, float period, float fade)
{
    float target = round(value / period) * period;
    return distanceIntensity(value, target, fade);
}

float wave(float3 pos, float waveInterval, float waveYSpeed, float waveThickness, uniform bool distort)
{
    if (distort)
    {
        const float WAVE_XZ_SPEED = 3.0;
        const float WAVE_DISTORTION_SIZE = 0.035;
        const float WAVE_OSCILLATING_TIME = 4;

        float distortionSign = abs(frac(g_time / WAVE_OSCILLATING_TIME) - 0.5) * 4 - 1;
        float2 distortion = sin(pos.xz / WAVE_DISTORTION_SIZE + g_time * WAVE_XZ_SPEED) * WAVE_DISTORTION_SIZE * distortionSign;
        pos.y -= (distortion.x + distortion.y);
    }

    pos.y -= g_time * waveYSpeed;

    float intensity = 1.0 - periodIntensity(pos.y, waveInterval, waveThickness);
    return intensity;
}

// Note: in HLSL global constant is marked with "static const".
// One "const" is not enough, because it will be considered to be a uniform from a constant buffer.
// In HLSL const means that the value can not be changed by the shader.
// Adding "static" to global variable means that it is not visible to application, so doesn't belong to a constant buffer.
// A local constant inside a function can be marked just with "const".
// "static" for locals preserves value during current shader thread execution.

static const float BLUE_WAVE_INTERVAL = 0.8;
static const float BLUE_WAVE_SPEED = 0.25;
static const float BLUE_WAVE_THICKNESS = 0.05;

static const float RED_WAVE_INTERVAL = 10;
static const float RED_WAVE_SPEED = 2;
static const float RED_WAVE_THICKNESS = 0.2;

// called in vertex shader
float3 vertexDistortion(float3 pos, float3 normal)
{
    float3 offset = 0.0;
    offset += normal * 0.025 * wave(pos, BLUE_WAVE_INTERVAL, BLUE_WAVE_SPEED, BLUE_WAVE_THICKNESS, true);
    offset += normal * 0.05 * wave(pos, RED_WAVE_INTERVAL, RED_WAVE_SPEED, RED_WAVE_THICKNESS, false);
    return offset;
}

[maxvertexcount(3)]
void main(
	triangle VS_Output input[3] : SV_POSITION,
	inout TriangleStream< VS_Output > output
)
{
    float4 midle_point = (input[0].positionLocal + input[1].positionLocal + input[2].positionLocal) / 3;

    float3 normal = cross(input[1].positionGlobal.xyz - input[0].positionGlobal.xyz, input[2].positionGlobal.xyz - input[0].positionGlobal.xyz);
    normal = normalize(normal);
    float3 offset = vertexDistortion(midle_point.xyz, normal);

#ifdef LIGHT_CALCULATION_WORLD_SPACE
#else
    float4x4 worldToView = g_worldToView();
    float4x4 projection = g_projection();
#endif 

	for (uint i = 0; i < 3; i++)
	{
        VS_Output element = input[i];
        element.positionGlobal.xyz = (element.positionGlobal.xyz + offset);
#ifdef LIGHT_CALCULATION_WORLD_SPACE
        element.position = mul(element.positionGlobal, g_worldToViewProj());
#else
        element.position = mul(float4(element.positionGlobal.xyz, 0.0f), worldToView);
        element.position = mul(float4(element.position.xyz, 1.0f), projection);
#endif 
		output.Append(element);
	}
}

static const float COFF_EDGE_TESS_FACTOR = 10;

struct VS_Output
{
	float4 position : SV_POSITION;
	nointerpolation float4 color : COL;
	float4 positionLocal : POSITION1;
	float4 positionGlobal : POSITION2;
	float4 normal : NORMAL;
};

struct PatchOut
{
	float EdgeTessFactor[3]			: SV_TessFactor;
	float InsideTessFactor			: SV_InsideTessFactor;
};

PatchOut CalcHSPatchConstants(
	InputPatch<VS_Output, 3> ip,
	uint PatchID : SV_PrimitiveID)
{
	PatchOut Output;

	float lenA = length(ip[0].positionGlobal - ip[1].positionGlobal);
	float lenB = length(ip[0].positionGlobal - ip[2].positionGlobal);
	float lenC = length(ip[2].positionGlobal - ip[1].positionGlobal);


	Output.EdgeTessFactor[0] = COFF_EDGE_TESS_FACTOR * floor(lenA) + 1;
	Output.EdgeTessFactor[1] = COFF_EDGE_TESS_FACTOR * floor(lenB) + 1;
	Output.EdgeTessFactor[2] = COFF_EDGE_TESS_FACTOR * floor(lenC) + 1;
	Output.InsideTessFactor = COFF_EDGE_TESS_FACTOR * floor((lenA + lenB + lenC) / 3) + 1;

	return Output;
}

[outputcontrolpoints(3)]
[domain("tri")]
[outputtopology("triangle_cw")]
[partitioning("integer")]
[patchconstantfunc("CalcHSPatchConstants")]
VS_Output main(
	InputPatch<VS_Output, 3> input,
	uint pointID : SV_OutputControlPointID,
	uint patchID : SV_PrimitiveID )
{
	return input[pointID];
}

#include "../globals.hlsl"

struct VS_Input
{
    float3 pos : POSITION;
    float3 normal : NORMAL;
    float4 modelToWorld1 : MW1;
    float4 modelToWorld2 : MW2;
    float4 modelToWorld3 : MW3;
    float4 modelToWorld4 : MW4;
    float3 color : COLOR;
};

struct VS_Output
{
    float4 position : SV_POSITION;
    nointerpolation float4 color : COL;
    float4 positionLocal : POSITION1;
    float4 positionGlobal : POSITION2;
    float4 normal : NORMAL;
};

cbuffer MeshToModel : register(b2)
{
    float4 meshToModel1;
    float4 meshToModel2;
    float4 meshToModel3;
    float4 meshToModel4;
}

VS_Output main(VS_Input input)
{
    VS_Output output;
    output.positionGlobal = float4(input.pos, 1.0f);
    output.color = float4(input.color, 1.0f);
    float4x4 meshToModel = float4x4(meshToModel1, meshToModel2, meshToModel3, meshToModel4);
#ifdef LIGHT_CALCULATION_WORLD_SPACE
    float4x4 worldToViewProj = g_worldToViewProj();
#else
    float4x4 worldToView = g_worldToView();
    float4x4 projection = g_projection();
#endif 
    
    float4x4 modelToWorld = float4x4(input.modelToWorld1, input.modelToWorld2, input.modelToWorld3, input.modelToWorld4);

    output.positionGlobal = mul(output.positionGlobal, meshToModel);
    output.positionGlobal = mul(output.positionGlobal, modelToWorld);

    float4  basisX = float4(1.0, 0.0, 0.0, 0.0);
    float4  basisY = float4(0.0, 1.0, 0.0, 0.0);
    float4  basisZ = float4(0.0, 0.0, 1.0, 0.0);

    basisX = mul(basisX, modelToWorld);
    basisY = mul(basisY, modelToWorld);
    basisZ = mul(basisZ, modelToWorld);

    basisX = normalize(basisX);
    basisY = normalize(basisY);
    basisZ = normalize(basisZ);

    float4x4 rotationMatrix = float4x4(basisX, basisY, basisZ, float4(0.0, 0.0, 0.0, 1.0));
    float4x4 rotationMatrixInv = transpose(rotationMatrix);

    float4 T = float4(-input.modelToWorld4.xyz,  1.0);
    T = mul( T, rotationMatrixInv);
    rotationMatrix._m30_m31_m32_m33 = input.modelToWorld4;
    rotationMatrixInv._m30_m31_m32_m33 = T;
    output.positionLocal = mul(output.positionGlobal, rotationMatrixInv); ;

    output.positionGlobal.xyz = output.positionGlobal.xyz;
#ifdef LIGHT_CALCULATION_WORLD_SPACE
    output.position = mul(output.positionGlobal, g_worldToViewProj());
#else
    output.position = mul(float4(output.positionGlobal.xyz, 0.0f), worldToView);
    output.position = mul(float4(output.position.xyz, 1.0f), projection);
#endif 
    

    output.normal = mul(float4(input.normal, 0), meshToModel);
    output.normal = normalize(mul(output.normal, rotationMatrix));
    return output;
}
#include "../../globals.hlsl"
#include "../../utils.hlsl"

static const float BLUE_RADIUS = 0.005f;

struct PS_Input
{
    float4 position : SV_POSITION;
    float4 positionView : POSITION;
    float2 tc : TEXCOORD;
    float3 start_position : START_POS;
    nointerpolation float start_time : TIME;
    nointerpolation float velocity : VEL;
    nointerpolation float max_radius : RADIUS;
};

float main(PS_Input input) : SV_Depth
{
    //Masking
    float distance = length(input.positionView - input.start_position);
    float red_radius = input.velocity * (g_time - input.start_time);
    float blue_radius = red_radius - BLUE_RADIUS * input.max_radius;
    distance = (distance - blue_radius) / (red_radius - blue_radius);
    distance = clamp(0.0f, 1.0f, distance);

    if (distance < 0.1f)
        discard;

    float noise = g_noise.Sample(g_anisotropicWrap, input.tc);
    float alpha = distance + noise;
    alpha = derivatives(alpha, 0.90f, 2.0f);
    alpha = smoothstep(0.0f, 1.0f, alpha);
    if (alpha < 0.1f)
        discard;

	return input.position.z;
}
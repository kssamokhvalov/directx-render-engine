#include "../../globals.hlsl"

struct VS_Input
{
    float3 pos : POSITION;
    float3 normal : NORMAL;
    float2 tc : TEXCOORD;
    float4 modelToWorld1 : MW1;
    float4 modelToWorld2 : MW2;
    float4 modelToWorld3 : MW3;
    float4 modelToWorld4 : MW4;
    float3 start_position : START_POS;
    float3 params : PARAMS;
};

struct VS_Output
{
    float4 position : SV_POSITION;
    float4 positionView : POSITION;
    float2 tc : TEXCOORD;
    float3 start_position : START_POS;
    nointerpolation float start_time : TIME;
    nointerpolation float velocity : VEL;
    nointerpolation float max_radius : RADIUS;
};

cbuffer MeshToModel : register(b2)
{
    float4 meshToModel1;
    float4 meshToModel2;
    float4 meshToModel3;
    float4 meshToModel4;
}

VS_Output main(VS_Input input)
{
    VS_Output output;
    output.position = float4(input.pos, 1.0f);
    float4x4 meshToModel = float4x4(meshToModel1, meshToModel2, meshToModel3, meshToModel4);
    float4x4 modelToWorld = float4x4(input.modelToWorld1, input.modelToWorld2, input.modelToWorld3, input.modelToWorld4);


    output.position = mul(output.position, meshToModel);
    output.position = mul(output.position, modelToWorld);

#ifdef LIGHT_CALCULATION_WORLD_SPACE
    output.positionView = output.position;
#else
    output.positionView = float4(output.position.xyz + g_worldToView()._m30_m31_m32, 1.0f);
#endif 

    output.tc = input.tc;
    output.start_time = input.params.x;
    output.velocity = input.params.y;
    output.max_radius = input.params.z;

    output.start_position = float4(input.start_position.xyz, 1.0f);

    return output;
}
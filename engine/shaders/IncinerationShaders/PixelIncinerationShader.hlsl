#include "../globals.hlsl"
#include "../utils.hlsl"

#define EN_DIFFUSE 1
#define EN_SPECLAR 2
#define EN_IBL 4
#define EN_ROUGHNESS_OVERWRITING 8


struct VS_Output
{
    float4 position : SV_POSITION;
    float4 positionView : POSITION;
    float2 tc : TEXCOORD;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    float3 start_position : START_POS;
    nointerpolation float start_time : TIME;
    nointerpolation float velocity : VEL;
    nointerpolation float max_radius : RADIUS;
};

struct Surface
{
    float3 albedo;
    float3 normal;
    float3 geo_normal;
    float3 position;
    float roughnessLinear;
    float roughness2;
    float roughness4;
    float metalness;
    float F0;
};

struct View
{
    float3 reflectionDir;
    float3 V;
    float NoV;
};


float3 addShadowNormalOffset(float3 N, float3 L, float NoL, float shadow_texel)
{
    return shadow_texel * sqrt(2) * (N - 0.9 * L * NoL) / 2;
}

float calculatFalloffFactor(float3 normal, float3 surfacePose, float3 light_pos, float r)
{
    float h = dot(light_pos - surfacePose, normal);
    return saturate((h + r) / (2 * r));
}

float calculatDoubleFalloffFactor(in Surface s, float3 light_pos, float r)
{
    return min(calculatFalloffFactor(s.geo_normal, s.position, light_pos, r), calculatFalloffFactor(s.normal, s.position, light_pos, r));
}

void addEnvironmentReflection(inout float3 diffuseReflection, inout float3 specularReflection, in View v, in Surface s)
{
    diffuseReflection += s.albedo * (1.0 - s.metalness) * g_irradiance.SampleLevel(g_linearClamp, s.normal, 0.0);
    float2 reflectanceLUT = g_reflectance.Sample(g_linearClamp, float2(v.NoV, s.roughnessLinear));
    float3 reflectance = reflectanceLUT.x * s.F0 + reflectanceLUT.y;
    specularReflection += reflectance * g_reflection.SampleLevel(g_linearClamp, v.reflectionDir, s.roughnessLinear * g_reflectionMips);
}

float3 calculatF0(float3 albedo_texture_color, float metalness)
{
    return lerp(float3(TYPICALLY_F0, TYPICALLY_F0, TYPICALLY_F0), albedo_texture_color, metalness);
}

void computeIrradiance(inout float3 diffuseReflection, inout float3 specularReflection, float3 radiance, float3 albedo_texture_color, float rough4, float metalness, float NoV, float NoL, float NoH, float HoL, float w)
{
    float3 F0 = calculatF0(albedo_texture_color, metalness);

    float3 f_d = albedo_texture_color * NoL * w * (1 - metalness) * (1 - fresnel(NoL, F0)) / PI;
    float3 f_s = min(1.0f, ggx(rough4, NoH) * w / (4 * NoV)) * smith(rough4, NoV, NoL) * fresnel(HoL, F0);

    diffuseReflection += radiance * f_d;
    specularReflection += radiance * f_s;
}

float PCF(float3 LightSpacePos, float z)
{
    uint width;
    uint height;
    uint elements;

    g_directional_shadows.GetDimensions(width, height, elements);

    float xOffset = 1.0 / width;
    float yOffset = 1.0 / height;

    float shadow_factor = 0.0;

    for (float x = -0.5f; x <= 0.5f; x++) {
        float2 Offsets = float2(x * xOffset, 0.0f);
        float2 tc = LightSpacePos.xy + Offsets;
        float V = g_directional_shadows.SampleCmp(g_linearWrapCamp, float3(tc, LightSpacePos.z), z);
        shadow_factor += smoothstep(0.0f, 0.66f, V); // Because, 1.0 - no shadow, 0.0 - shadow.
    }

    for (float y = -0.5f; y <= 0.5f; y++) {
        float2 Offsets = float2(0.0f, y * yOffset);
        float2 tc = LightSpacePos.xy + Offsets;
        float V = g_directional_shadows.SampleCmp(g_linearWrapCamp, float3(tc, LightSpacePos.z), z);
        shadow_factor += smoothstep(0.0f, 0.66f, V); // Because, 1.0 - no shadow, 0.0 - shadow.
    }


    return shadow_factor / 4;
}

float checkShadow(in PointLight point_light, in Surface s, int number_light)
{
    uint width;
    uint height;
    uint elements;

    g_point_shadows.GetDimensions(width, height, elements);

    float3 light_position = point_light.position;
    
    float3 L = s.position - light_position;
    float LoGeoN = dot(normalize(L), s.geo_normal);
    
    float z = abs(L.z);
    z = max(z, abs(L.x));
    z = max(z, abs(L.y));

    float W =  2 * z / width;
    L = L - addShadowNormalOffset(s.geo_normal, normalize(L), LoGeoN, W);

    z = (z / (NEAR_PLANE_SHADOW - FAR_PLANE_SHADOW) - FAR_PLANE_SHADOW / (NEAR_PLANE_SHADOW - FAR_PLANE_SHADOW)) * NEAR_PLANE_SHADOW / z; // Finding prespective pprojection for "z"

    return g_point_shadows.SampleCmp(g_linearWrapCamp, float4(L, number_light), z);
}

float checkShadow(in SpotLightWithMask spot_light, in Surface s, float3 L, float LoGeoN, int number_light)
{
    uint width;
    uint height;
    uint elements;

    g_spot_with_mask_shadows.GetDimensions(width, height, elements);

    float4x4 worldToLight = float4x4(spot_light.worldToLight1, spot_light.worldToLight2, spot_light.worldToLight3, spot_light.worldToLight4);
    float4x4 projection = float4x4(spot_light.maskProj1, spot_light.maskProj2, spot_light.maskProj3, spot_light.maskProj4);

    float4 position = float4(s.position , 1.0f);
    position = mul(position, worldToLight);

    float W = 2 * position.z / (projection._m11 * width);
    position.xyz += addShadowNormalOffset(s.geo_normal, L, LoGeoN, W);

    position = mul(position, projection);

    float2 tc = position.xy / position.w;
    tc.y *= -1.0f;
    tc = 0.5f * tc + 0.5f;

    return g_spot_with_mask_shadows.SampleCmp(g_linearWrapCamp, float3(tc, number_light), position.z / position.w);
}

float checkShadow(in DirectionalLight directional_light, in Surface s, float3 L, float LoGeoN, int number_light)
{
    float4x4 worldToLight = float4x4(directional_light.worldToLight1, directional_light.worldToLight2, directional_light.worldToLight3, directional_light.worldToLight4);
    float4x4 projection = float4x4(directional_light.proj1, directional_light.proj2, directional_light.proj3, directional_light.proj4);

    float4 position = float4(s.position + addShadowNormalOffset(s.geo_normal, L, LoGeoN, g_shadow_texel), 1.0f);
    position = mul(position, worldToLight);
    position = mul(position, projection);

    float2 tc = position.xy;
    tc.y *= -1.0f;
    tc = 0.5f * tc + 0.5f;

    return PCF(float3(tc, number_light), position.z);
}

void pointLightCalculation(inout float3 diffuseReflection, inout float3 specularReflection, in PointLight point_light, in View v, in Surface s, int number_light)
{
    float shadow_factor = checkShadow(point_light, s, number_light);

    float3 light_position = point_light.position;

    float3 L = light_position - s.position;
    float D = length(L);
    L = normalize(L);

    float VoN = max(v.NoV, 0.0001f);

    float cosAlpha = sqrt(1 - min(point_light.radius * point_light.radius / (D * D), 1.0f));

    bool intersects;
    float3 R = 2 * VoN * s.normal - v.V;
    L = approximateClosestSphereDir(intersects, R, cosAlpha, light_position - s.position, L, D, point_light.radius);
    float  LoN = dot(L, s.normal);
    clampDirToHorizon(L, LoN, s.normal, 0.0001f);

    float3 H = (L + v.V) / length(L + v.V); //half vector

    float HoN = max(dot(H, s.normal), 0.0001f);
    float NoL = dot(L, s.normal);
    float HoL = max(dot(H, L), 0.0001f);

    float w = computeSolidAngle(point_light.radius, D);
    w *= calculatDoubleFalloffFactor(s, light_position, point_light.radius);

    computeIrradiance(diffuseReflection, specularReflection, point_light.emission, s.albedo, s.roughness4, s.metalness, VoN, LoN, HoN, HoL, w * shadow_factor);
}

void spotLightCalculation(inout float3 diffuseReflection, inout float3 specularReflection, SpotLight spot_light, in View v, in Surface s)
{
    float3 light_position = spot_light.lightToModel4.xyz;

    float3 L = light_position - s.position;
    float4x4 matrix_light_to_world = float4x4(spot_light.lightToModel1, spot_light.lightToModel2, spot_light.lightToModel3, spot_light.lightToModel4);
    float3 S = mul(float4(0.0f, 0.0f, 1.0f, 0.0f), matrix_light_to_world).xyz;
    S = normalize(L);

    float D = length(L);
    L = normalize(L);

    float VoN = max(v.NoV, 0.0001f);

    float cosAlpha = sqrt(1 - min(spot_light.radius * spot_light.radius / (D * D), 1.0f));

    bool intersects;
    float3 R = 2 * VoN * s.normal - v.V;
    L = approximateClosestSphereDir(intersects, R, cosAlpha, light_position - s.position, L, D, spot_light.radius);
    float  LoN = dot(L, s.normal);
    clampDirToHorizon(L, LoN, s.normal, 0.0001f);

    float3 H = (L + v.V) / length(L + v.V); //half vector
    float HoN = max(dot(H, s.normal), 0.0001f);
    float SoL = max(dot(-S, L), 0.0001f);
    float HoL = max(dot(H, L), 0.0001f);

    float w = computeSolidAngle(spot_light.radius, D);
    w *= calculatDoubleFalloffFactor(s, light_position, spot_light.radius);

    float3 spotDiffuseReflection = float3(0.0f, 0.0f, 0.0f);
    float3 spotSpecularReflection = float3(0.0f, 0.0f, 0.0f);

    computeIrradiance(spotDiffuseReflection, spotSpecularReflection, spot_light.emission, s.albedo, s.roughness4, s.metalness, VoN, LoN, HoN, HoL, w);

    diffuseReflection += spotDiffuseReflection * max(pow(SoL, spot_light.p), 0.0001f);
    specularReflection += spotSpecularReflection * max(pow(SoL, spot_light.p), 0.0001f);
}


void spotLightWithMaskCalculation(inout float3 diffuseReflection, inout float3 specularReflection, SpotLightWithMask spot_light, in View v, in Surface s, int number_light)
{
    float3 light_position = spot_light.lightToModel4.xyz;

    float4x4 matrix_light_to_world = float4x4(spot_light.lightToModel1, spot_light.lightToModel2, spot_light.lightToModel3, spot_light.lightToModel4);

    //Inverting Matrix
    float4x4 rotationMatrix = float4x4(matrix_light_to_world._m00_m01_m02_m03, matrix_light_to_world._m10_m11_m12_m13, matrix_light_to_world._m20_m21_m22_m23, float4(0.0, 0.0, 0.0, 1.0));
    float4x4 matrix_light_to_world_inv = transpose(rotationMatrix);
    float4 T = float4(-spot_light.lightToModel4.xyz, 1.0);
    T = mul(T, matrix_light_to_world_inv);
    matrix_light_to_world_inv._m30_m31_m32_m33 = T;

    float3 S = mul(float4(0.0f, 0.0f, 1.0f, 0.0f), matrix_light_to_world).xyz;
    S = normalize(S);

    float3 L = light_position - s.position;
    float D = length(L);
    L = normalize(L);

    float VoN = max(v.NoV, 0.0001f);

    float cosAlpha = sqrt(1 - min(spot_light.radius * spot_light.radius / (D * D), 1.0f));

    bool intersects;
    float3 R = 2 * VoN * s.normal - v.V;
    L = approximateClosestSphereDir(intersects, R, cosAlpha, light_position - s.position, L, D, spot_light.radius);
    float  LoN = dot(L, s.normal);
    clampDirToHorizon(L, LoN, s.normal, 0.0001f);

    float LoGeoN = dot(L, s.geo_normal);
    float shadow_factor = checkShadow(spot_light, s, L, LoGeoN, number_light);

    float3 H = (L + v.V) / length(L + v.V); //half vector
    float HoN = max(dot(H, s.normal), 0.0001f);
    float SoL = max(dot(-S, L), 0.0001f);
    float HoL = max(dot(H, L), 0.0001f);

    float4 positionInFrustrimLight = float4(s.position, 1.0f);

    float4x4 maskProj = float4x4(spot_light.maskProj1, spot_light.maskProj2, spot_light.maskProj3, spot_light.maskProj4);

    positionInFrustrimLight = mul(positionInFrustrimLight, matrix_light_to_world_inv);
    positionInFrustrimLight = mul(positionInFrustrimLight, maskProj);
    float2 tc_mask = positionInFrustrimLight.xy / positionInFrustrimLight.w;
    float lengh_mask = length(tc_mask);
    tc_mask.y *= -1.0f;
    tc_mask = 0.5f * tc_mask + 0.5f;

    const float max_lengh_mask = sqrt(0.5f);

    //This factor allows to avoid illuminating a surface when the spot light is very close to the surface but is not directed at the surface
    float fading_factor = pow(min(1.0f, max_lengh_mask / lengh_mask), 4) * SoL;

    float color_mask = g_flashLightMask.Sample(g_anisotropicClamp, tc_mask) * fading_factor;

    float w = computeSolidAngle(spot_light.radius, D);
    w *= calculatDoubleFalloffFactor(s, light_position, spot_light.radius);

    float3 spotDiffuseReflection = float3(0.0f, 0.0f, 0.0f);
    float3 spotSpecularReflection = float3(0.0f, 0.0f, 0.0f);

    computeIrradiance(spotDiffuseReflection, spotSpecularReflection, spot_light.emission, s.albedo, s.roughness4, s.metalness, VoN, LoN, HoN, HoL, w * shadow_factor);

    diffuseReflection += spotDiffuseReflection * color_mask;
    specularReflection += spotSpecularReflection * color_mask;
}





void directionalLightCalculation(inout float3 diffuseReflection, inout float3 specularReflection, in DirectionalLight directional_light, in View v, in Surface s, int number_light)
{
    float3 L = -directional_light.direction;
    L = normalize(L);

    float NoL = saturate(dot(L, s.normal));
    float LoGeoN = dot(L, s.geo_normal);

    float shadow_factor = checkShadow(directional_light, s, L, LoGeoN, number_light);

    if (NoL < 0.0001f && LoGeoN < 0.0001f)
        return;

    float3 H = (L + v.V) / length(L + v.V); //half vector
    float NoH = max(dot(H, s.normal), 0.0001f);
    float NoV = max(v.NoV, 0.0001f);
    float VoL = max(dot(v.V, L), 0.0001f);
    float HoL = max(dot(H, L), 0.0001f);

    float RoL = 2 * NoL * NoV - VoL;
    if (RoL >= directional_light.cosAlpha)
    {
        NoH = 1;
        HoL = NoL;
        NoV = abs(NoV);
    }

    computeIrradiance(diffuseReflection, specularReflection, directional_light.emission, s.albedo, s.roughness4, s.metalness, NoV, NoL, NoH, HoL, directional_light.w * shadow_factor);
}

float4 main(VS_Output input) : SV_TARGET
{

    Surface surface;

    surface.position = input.positionView.xyz;

    if (g_hasAlbedoTexture() == true)
        surface.albedo = g_albedoTexture.Sample(g_anisotropicWrap, input.tc);
    else
        surface.albedo = g_texture_albedo;

    surface.geo_normal = input.normal;
    float3 normal;
    if (g_hasNormalTexture() == true)
        normal = g_normalTexture.Sample(g_anisotropicWrap, input.tc).xyz;
    else
        normal = g_texture_normal;

    float3x3 TBN = float3x3(input.tangent, input.bitangent, input.normal);
    normal = normal * 2.0f - 1.0f;
    surface.normal = normalize(mul(normal, TBN));



    if (g_flags & EN_ROUGHNESS_OVERWRITING)
    {
        surface.roughnessLinear = g_roughnessSlider;
    }
    else
    {
        if (g_hasRoughnessTexture() == true)
            surface.roughnessLinear = g_roughnessTexture.Sample(g_anisotropicWrap, input.tc);
        else
            surface.roughnessLinear = g_texture_roughness;
        
    }
    surface.roughness4 = max(pow(surface.roughnessLinear, 4.0f), 0.0001f);
    surface.roughness2 = max(pow(surface.roughnessLinear, 2.0f), 0.0001f);

    if (g_hasMetalnessTexture() == true)
        surface.metalness = g_metalnessTexture.Sample(g_anisotropicWrap, input.tc);
    else
        surface.metalness = g_texture_metalness;

    
    surface.F0 = TYPICALLY_F0;


#ifdef LIGHT_CALCULATION_WORLD_SPACE
    float3 V = normalize(g_cameraPosition() - surface.position);
#else
    float3 V = normalize(-surface.position);
#endif 

    if (dot(surface.geo_normal, V) < -0.01f)
    {
        surface.normal = -surface.normal;
        surface.geo_normal = -surface.geo_normal;
    }

    View view;
    view.reflectionDir = 2 * dot(V, surface.normal) * surface.normal - V;
    view.V = V;
    view.NoV = dot(surface.normal, V);
    

    float3 diffuseReflection = float3(0.0f, 0.0f, 0.0f);
    float3 specularReflection = float3(0.0f, 0.0f, 0.0f);

    for (int i = 0; i < g_numPointLights; ++i)
    {
        pointLightCalculation(diffuseReflection, specularReflection, g_pointLights[i], view, surface, i);
    }

    //TODO The calculations are temporarily turned off, because from the physical point of view the spotlight is part of the sphere. 
    //     And we currently calculate solid angle only for spheres.
    /*for (int i = 0; i < g_numSpotLights; ++i)
    {
        ans_color += SpotLightCalculation(g_spotLights[i], albedo_texture_color, roughness, metalness, input.positionView.xyz, normal);
    }*/
    
    for (int i = 0; i < g_numSpotLightsWithMask; ++i)
    {
        spotLightWithMaskCalculation(diffuseReflection, specularReflection, g_spotLightsWithMask[i], view, surface, i);
    }
    
    for (int i = 0; i < g_numDirectionalLights; ++i)
    {
        directionalLightCalculation(diffuseReflection, specularReflection, g_directionalLights[i], view, surface, i);
    }

    if (g_flags & EN_IBL)
        addEnvironmentReflection(diffuseReflection, specularReflection, view, surface);

    float3 ans = float3(0.0f, 0.0f, 0.0f);
    if (g_flags & EN_DIFFUSE)
        ans += diffuseReflection;
    if (g_flags & EN_SPECLAR)
        ans += specularReflection;

    //Masking
    float noise = g_noise.Sample(g_anisotropicWrap, input.tc);
    float alpha = (g_time - input.start_time) / input.velocity + noise;
    alpha = derivatives(alpha, 0.95f, 0.1f);

   
    return float4(ans, alpha);
}
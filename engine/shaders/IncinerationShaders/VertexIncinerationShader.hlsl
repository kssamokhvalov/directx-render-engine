#include "../globals.hlsl"


static const float LIFE_TIME = 10.0f;
static const float VELOCITY = 2.0f;

struct VS_Input
{
    float3 pos : POSITION;
    float3 normal : NORMAL;
    float2 texCoord : TEXCOORD;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    float4 modelToWorld1 : MW1;
    float4 modelToWorld2 : MW2;
    float4 modelToWorld3 : MW3;
    float4 modelToWorld4 : MW4;
    float4 tint : TINT;
    float4 emission : EMISSION;
    float3 start_position : START_POS;
    float3 params : PARAMS;
    uint vertex_id : SV_VertexID;
};

struct VS_Output
{
    float4 position : SV_POSITION;
    float4 positionView : POSITION;
    float2 tc : TEXCOORD;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    float3 start_position : START_POS;
    nointerpolation float start_time : TIME;
    nointerpolation float velocity : VEL;
    nointerpolation float max_radius : RADIUS;
};

cbuffer MeshToModel : register(b2)
{
    float4 meshToModel1;
    float4 meshToModel2;
    float4 meshToModel3;
    float4 meshToModel4;
}

RWStructuredBuffer<ParticleGPU> g_GPU_particals : register(u5);
RWByteAddressBuffer  g_range_description : register(u6);

VS_Output main(VS_Input input)
{
    
    VS_Output output;
    output.position = float4(input.pos, 1.0f);
    float4x4 meshToModel = float4x4(meshToModel1, meshToModel2, meshToModel3, meshToModel4);
#ifdef LIGHT_CALCULATION_WORLD_SPACE
#else
    float4x4 worldToView = g_worldToView();
    float4x4 projection = g_projection();
#endif 
    float4x4 modelToWorld = float4x4(input.modelToWorld1, input.modelToWorld2, input.modelToWorld3, input.modelToWorld4);

    output.position = mul(output.position, meshToModel);
    output.position = mul(output.position, modelToWorld);
    output.positionView = output.position;

#ifdef LIGHT_CALCULATION_WORLD_SPACE
    output.position = mul(output.position, g_worldToViewProj());
#else
    output.position = mul(float4(output.position.xyz, 0.0f), worldToView);
    output.position = mul(float4(output.position.xyz, 1.0f), projection);
#endif 
    

    float3 axisX = normalize(modelToWorld[0].xyz);
    float3 axisY = normalize(modelToWorld[1].xyz);
    float3 axisZ = normalize(modelToWorld[2].xyz);

    output.normal = mul(float4(input.normal, 0.0f), meshToModel).xyz;
    output.normal = output.normal.x * axisX + output.normal.y * axisY + output.normal.z * axisZ;

    output.tangent = mul(float4(input.tangent, 0.0f), meshToModel).xyz;
    output.tangent = output.tangent.x * axisX + output.tangent.y * axisY + output.tangent.z * axisZ;
    output.bitangent = mul(float4(input.bitangent, 0.0f), meshToModel).xyz;
    output.bitangent = output.bitangent.x * axisX + output.bitangent.y * axisY + output.bitangent.z * axisZ;

    output.tc = input.texCoord;
    output.start_time = input.params.x;
    output.velocity = input.params.y;
    output.max_radius = input.params.z;
    output.start_position = input.start_position;
    
    //Particals

    float distance = length(output.positionView - output.start_position);
    distance -= output.velocity * (g_time_prev_frame - output.start_time);
    distance /= output.velocity * (g_time - g_time_prev_frame);
    distance = clamp(0.0f, 1.0f, distance);
    if (distance > 0.1f && distance < 0.9f && input.vertex_id % 2 == 0)
    {
        uint numStructs = 0;
        uint stride = 0;
        g_GPU_particals.GetDimensions(numStructs, stride);
        uint index = g_GPU_particals.IncrementCounter();

        g_GPU_particals[index % numStructs].tint = input.tint;
        g_GPU_particals[index % numStructs].emission_color = input.emission;
#ifdef LIGHT_CALCULATION_WORLD_SPACE
        g_GPU_particals[index % numStructs].position = output.positionView.xyz;
#else
        g_GPU_particals[index % numStructs].position = output.positionView.xyz + g_worldToViewInv()._m30_m31_m32;
#endif 
        g_GPU_particals[index % numStructs].speed = normalize(output.normal) * VELOCITY;
        g_GPU_particals[index % numStructs].start_time = g_time;
        g_GPU_particals[index % numStructs].life_time = LIFE_TIME;
        g_GPU_particals[index % numStructs].active = 1;
        g_range_description.InterlockedAdd(0, 1);
    }
    return output;
}
#include "../globals.hlsl"

struct VS_Output
{
    float4 position : SV_POSITION;
    float4 positionGlobal : POSITION;
    float4 normal : NORMAL;
};

struct DS_Output
{
    float4 position : SV_POSITION;
    nointerpolation float4 color : COL;
    float4 positionGlobal : POSITION;
    float4 normal : NORMAL;
};

static const float GLOBAL_LENGTH_NORMAL = 0.05f;

[maxvertexcount(2)]
void main(
    triangle VS_Output input[3] : SV_POSITION,
    inout LineStream< DS_Output > output
)
{
    DS_Output outputDS;

    float4 midle_point = (input[0].positionGlobal + input[1].positionGlobal + input[2].positionGlobal) / 3;
    float3 normal = cross(input[1].positionGlobal.xyz - input[0].positionGlobal.xyz, input[2].positionGlobal.xyz - input[0].positionGlobal.xyz);
    normal = GLOBAL_LENGTH_NORMAL * normalize(normal);

#ifdef LIGHT_CALCULATION_WORLD_SPACE
#else
    float4x4 worldToView = g_worldToView();
    float4x4 projection = g_projection();
#endif 

    outputDS.color = float4(1.0f, 1.0f, 1.0f, 1.0f);
    outputDS.normal = float4(normal, 0.0f);

    outputDS.positionGlobal = midle_point;

#ifdef LIGHT_CALCULATION_WORLD_SPACE
    outputDS.position = mul(outputDS.positionGlobal, g_worldToViewProj());
#else
    outputDS.position = mul(float4(outputDS.positionGlobal.xyz, 0.0f), worldToView);
    outputDS.position = mul(float4(outputDS.position.xyz, 1.0f), projection);
#endif 

    output.Append(outputDS);

    outputDS.positionGlobal = midle_point + float4(normal, 0.0f);

#ifdef LIGHT_CALCULATION_WORLD_SPACE
    outputDS.position = mul(outputDS.positionGlobal, g_worldToViewProj());
#else
    outputDS.position = mul(float4(outputDS.positionGlobal.xyz, 0.0f), worldToView);
    outputDS.position = mul(float4(outputDS.position.xyz, 1.0f), projection);
#endif 

    output.Append(outputDS);
}
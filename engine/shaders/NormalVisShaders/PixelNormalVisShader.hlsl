#include "../globals.hlsl"

struct DS_Output
{
    float4 position : SV_POSITION;
    nointerpolation float4 color : COL;
    float4 positionGlobal : POSITION;
    float4 normal : NORMAL;
};

float4 main(DS_Output input) : SV_TARGET
{
    return float4(1.0f, 1.0f, 1.0f, 1.0f);
}

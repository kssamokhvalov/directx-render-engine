#include "../../globals.hlsl"
#include "../../utils.hlsl"

#define EN_DIFFUSE 1
#define EN_SPECLAR 2
#define EN_IBL 4
#define EN_ROUGHNESS_OVERWRITING 8


struct VS_Output
{
    float4 position : SV_POSITION;
    float4 positionView : POSITION;
    float2 tc : TEXCOORD;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    uint id_object : ID_OBJECT;
};

struct PS_OUTPUT
{
    float4 albedo: SV_Target0;
    float4 rougness_metalness: SV_Target1;
    float4 normals: SV_Target2;
    float4 emmision : SV_Target3;
    uint id_objects: SV_Target4;
};


PS_OUTPUT main(VS_Output input) : SV_TARGET
{

    PS_OUTPUT output;

    if (g_hasAlbedoTexture() == true)
        output.albedo.xyz = g_albedoTexture.Sample(g_anisotropicWrap, input.tc);
    else
        output.albedo.xyz = g_texture_albedo;

    output.normals.ba = packOctahedron(input.normal);
    float3 normal;
    if (g_hasNormalTexture() == true)
        normal = g_normalTexture.Sample(g_anisotropicWrap, input.tc).xyz;
    else
        normal = g_texture_normal;

    float3x3 TBN = float3x3(input.tangent, input.bitangent, input.normal);
    normal = normal * 2.0f - 1.0f;
    output.normals.rg = packOctahedron(normalize(mul(normal, TBN)));

    if (g_flags & EN_ROUGHNESS_OVERWRITING)
    {
        output.rougness_metalness.x = g_roughnessSlider;
    }
    else
    {
        if (g_hasRoughnessTexture() == true)
            output.rougness_metalness.x = g_roughnessTexture.Sample(g_anisotropicWrap, input.tc);
        else
            output.rougness_metalness.x = g_texture_roughness;

    }


    if (g_hasMetalnessTexture() == true)
        output.rougness_metalness.y = g_metalnessTexture.Sample(g_anisotropicWrap, input.tc);
    else
        output.rougness_metalness.y = g_texture_metalness;

    output.id_objects = input.id_object;

    return output;
}
#include "../globals.hlsl"

struct VS_Input
{
    float3 pos : POSITION;
    float3 normal : NORMAL;
    float2 texCoord : TEXCOORD;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    float4 modelToWorld1 : MW1;
    float4 modelToWorld2 : MW2;
    float4 modelToWorld3 : MW3;
    float4 modelToWorld4 : MW4;
    uint id_object : ID_OBJECT;
};

struct VS_Output
{
    float4 position : SV_POSITION;
    float4 positionView : POSITION;
    float2 tc : TEXCOORD;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    uint id_object : ID_OBJECT;
};

cbuffer MeshToModel : register(b2)
{
    float4 meshToModel1;
    float4 meshToModel2;
    float4 meshToModel3;
    float4 meshToModel4;
}

VS_Output main(VS_Input input)
{
    VS_Output output;
    output.position = float4(input.pos, 1.0f);
    float4x4 meshToModel = float4x4(meshToModel1, meshToModel2, meshToModel3, meshToModel4);
#ifdef LIGHT_CALCULATION_WORLD_SPACE
#else
    float4x4 worldToView = g_worldToView();
    float4x4 projection = g_projection();
#endif 
    float4x4 modelToWorld = float4x4(input.modelToWorld1, input.modelToWorld2, input.modelToWorld3, input.modelToWorld4);

    output.position = mul(output.position, meshToModel);
    output.position = mul(output.position, modelToWorld);
    output.positionView = output.position;

#ifdef LIGHT_CALCULATION_WORLD_SPACE
    output.position = mul(output.position, g_worldToViewProj());
#else
    output.position = mul(float4(output.position.xyz, 0.0f), worldToView);
    output.position = mul(float4(output.position.xyz, 1.0f), projection);
#endif 
    

    float3 axisX = normalize(modelToWorld[0].xyz);
    float3 axisY = normalize(modelToWorld[1].xyz);
    float3 axisZ = normalize(modelToWorld[2].xyz);

    output.normal = mul(float4(input.normal, 0.0f), meshToModel).xyz;
    output.normal = output.normal.x * axisX + output.normal.y * axisY + output.normal.z * axisZ;

    output.tangent = mul(float4(input.tangent, 0.0f), meshToModel).xyz;
    output.tangent = output.tangent.x * axisX + output.tangent.y * axisY + output.tangent.z * axisZ;
    output.bitangent = mul(float4(input.bitangent, 0.0f), meshToModel).xyz;
    output.bitangent = output.bitangent.x * axisX + output.bitangent.y * axisY + output.bitangent.z * axisZ;

    output.tc = input.texCoord;
    output.id_object = input.id_object;
    return output;
}
#include "../globals.hlsl"
#include "../utils.hlsl"

static const float THICKNESS = 0.0002f;

struct PS_Input
{
    float4 position : SV_POSITION;
    float4 positionView : POSITION;
    float2 tc : TEXCOORD;
    nointerpolation float3 normal : NORMAL;
    nointerpolation float4 color : COLOR0;
    nointerpolation float3 emission_color : COLOR1;
    nointerpolation float frameFraction : DURATION;
};

struct Partical
{
    float right;
    float left;
    float top;
    float bottom;
    float back;
    float front;
    float3 basis_x;
    float3 basis_y;
    float3 basis_z;
    float3 emission;
    float alpha;
    float3 position;
    float3 color;
};

float PCF(float3 LightSpacePos, float z)
{
    uint width;
    uint height;
    uint elements;

    g_directional_shadows.GetDimensions(width, height, elements);

    float xOffset = 1.0 / width;
    float yOffset = 1.0 / height;

    float shadow_factor = 0.0;

    for (float x = -0.5f; x <= 0.5f; x++) {
        float2 Offsets = float2(x * xOffset, 0.0f);
        float2 tc = LightSpacePos.xy + Offsets;
        float V = g_directional_shadows.SampleCmp(g_linearWrapCamp, float3(tc, LightSpacePos.z), z);
        shadow_factor += smoothstep(0.0f, 0.66f, V); // Because, 1.0 - no shadow, 0.0 - shadow.
    }

    for (float y = -0.5f; y <= 0.5f; y++) {
        float2 Offsets = float2(0.0f, y * yOffset);
        float2 tc = LightSpacePos.xy + Offsets;
        float V = g_directional_shadows.SampleCmp(g_linearWrapCamp, float3(tc, LightSpacePos.z), z);
        shadow_factor += smoothstep(0.0f, 0.66f, V); // Because, 1.0 - no shadow, 0.0 - shadow.
    }


    return shadow_factor / 4;
}

float checkShadow(in PointLight point_light, in float3 position, int number_light)
{
    uint width;
    uint height;
    uint elements;

    g_point_shadows.GetDimensions(width, height, elements);

    float3 light_position = point_light.position;

    float3 L = position - light_position;

    float z = max(abs(L.x), max(abs(L.z), abs(L.y)));

    float W = 2 * z / width;

    z = (z / (NEAR_PLANE_SHADOW - FAR_PLANE_SHADOW) - FAR_PLANE_SHADOW / (NEAR_PLANE_SHADOW - FAR_PLANE_SHADOW)) * NEAR_PLANE_SHADOW / z; // Finding prespective pprojection for "z"

    return g_point_shadows.SampleCmp(g_linearWrapCamp, float4(L, number_light), z);
}

float checkShadow(in DirectionalLight directional_light, in float3 position, int number_light)
{
    float4x4 worldToLight = float4x4(directional_light.worldToLight1, directional_light.worldToLight2, directional_light.worldToLight3, directional_light.worldToLight4);
    float4x4 projection = float4x4(directional_light.proj1, directional_light.proj2, directional_light.proj3, directional_light.proj4);

    float4 pos = float4(position, 1.0f);
    pos = mul(pos, worldToLight);
    pos = mul(pos, projection);

    float2 tc = pos.xy;
    tc.y *= -1.0f;
    tc = 0.5f * tc + 0.5f;

    return PCF(float3(tc, number_light), pos.z);
}

void pointLightCalculation(inout float3 diffuseReflection, in PointLight point_light, in Partical p, int number_light)
{

    float shadow_factor = checkShadow(point_light, p.position.xyz, number_light);

    float3 light_position = point_light.position;
    float3 L = light_position - p.position;
    float D = length(L);
    L = normalize(L);

    float w = computeSolidAngle(point_light.radius, D);

    float3 E = point_light.emission * w * shadow_factor;

    float RoL = max(dot(p.basis_x, L), 0.0f);
    float ToL = max(dot(p.basis_y, L), 0.0f);
    float FoL = max(dot(p.basis_z, L), 0.0f);

    float3 return_value = p.right * RoL;
    return_value += p.left * (-RoL);
    return_value += p.top * ToL;
    return_value += p.bottom * (-ToL);
    return_value += p.back * (-FoL);
    return_value += p.front * FoL;

    diffuseReflection += return_value * E * p.color + p.emission;
}

void directionalLightCalculation(inout float3 diffuseReflection, in DirectionalLight directional_light, in Partical p, int number_light)
{
    float3 L = -directional_light.direction;
    L = normalize(L);

    float shadow_factor = checkShadow(directional_light, p.position.xyz, number_light);

    float3 E = directional_light.emission * directional_light.w  * shadow_factor;

    float RoL = max(dot(p.basis_x, L), 0.0f);
    float ToL = max(dot(p.basis_y, L), 0.0f);
    float FoL = max(dot(p.basis_z, L), 0.0f);

    float3 return_value = p.right * RoL;
    return_value += p.left * (-RoL);
    return_value += p.top * ToL;
    return_value += p.bottom * (-ToL);
    return_value += p.back * (-FoL);
    return_value += p.front * FoL;

    diffuseReflection += return_value * E * p.color + p.emission;
}


float4 main(PS_Input input) : SV_TARGET
{
    static const int NUM_TEXTURS = 8;
    static const float MV_SCALE = 0.0015; // adjusted for the smoke textures

    float depth = 0.0f;


    if (g_flags & EN_MSAA)
    {
        for (uint i = 0; i < 4; i++)
        {
            depth += g_DepthBufferMSCopy.Load(input.position.xy, i).x;
        }
        depth /= 4.0f;
    }
    else
    {
        depth = g_DepthBufferCopy.Load(float3(input.position.xy, 0)).x ;
    }


    float alpha_fades = smoothstep(depth, depth + THICKNESS, input.position.z);

    float frameFraction = input.frameFraction; // goes from 0.0 to 1.0 between two sequential frames


    int number_texture_left = NUM_TEXTURS * NUM_TEXTURS * NUM_TEXTURS * NUM_TEXTURS * frameFraction;
    
    int row = number_texture_left / NUM_TEXTURS;
    int colum = number_texture_left  - row * NUM_TEXTURS;

    float2 uvThis; // compute motion-vector sample uv for the current frame
    uvThis.x = input.tc.x / 8.0f + colum / 8.0f;
    uvThis.y = (1.0f - input.tc.y) / 8.0f + row / 8.0f;

    number_texture_left = fmod(number_texture_left, NUM_TEXTURS * NUM_TEXTURS);
    row = number_texture_left / NUM_TEXTURS;
    colum = number_texture_left - row * NUM_TEXTURS;

    float2 uvNext; // compute motion-vector sample uv for the next frame
    uvNext.x = input.tc.x / 8.0f + colum / 8.0f;
    uvNext.y = (1.0f - input.tc.y) / 8.0f + row / 8.0f;
    // ----------- sample motion-vectors -----------

    float2 mv0 = 2.0 * g_EMVA.Sample(g_anisotropicWrap, uvThis).gb - 1.0; // current frame motion-vector
    float2 mv1 = 2.0 * g_EMVA.Sample(g_anisotropicWrap, uvNext).gb - 1.0; // next frame motion-vector

    // need to flip motion-vector Y specifically for the smoke textures:
    mv0.y = -mv0.y;
    mv1.y = -mv1.y;

    // ----------- UV flowing along motion-vectors -----------

    float2 uv0 = uvThis; // texture sample uv for the current frame
    uv0 -= mv0 * MV_SCALE * frameFraction; // if MV points in some direction, then UV flows in opposite

    float2 uv1 = uvNext; // texture sample uv for the next frame
    uv1 -= mv1 * MV_SCALE * (frameFraction - 1.f); // if MV points in some direction, then UV flows in opposite

    // ----------- sample textures -----------

    float2 emissionAlpha0 = g_EMVA.Sample(g_anisotropicWrap, uv0).ra;
    float2 emissionAlpha1 = g_EMVA.Sample(g_anisotropicWrap, uv1).ra;

    // .x - right, .y - left, .z - up
    float3 lightmapRLU0 = g_RLT.Sample(g_anisotropicWrap, uv0).rgb;
    float3 lightmapRLU1 = g_RLT.Sample(g_anisotropicWrap, uv1).rgb;

    // .x - down, .y - back, .z - front
    float3 lightmapDBF0 = g_BBF.Sample(g_anisotropicWrap, uv0).rgb;
    float3 lightmapDBF1 = g_BBF.Sample(g_anisotropicWrap, uv1).rgb;

    // ----------- lerp values -----------

    float2 emissionAlpha = lerp(emissionAlpha0, emissionAlpha1, frameFraction);
    float3 lightmapRLU = lerp(lightmapRLU0, lightmapRLU1, frameFraction);
    float3 lightmapDBF = lerp(lightmapDBF0, lightmapDBF1, frameFraction);

    
    Partical partical;

    partical.right = lightmapRLU.r;
    partical.left = lightmapRLU.g;
    partical.top = lightmapRLU.b;

    partical.bottom = lightmapDBF.r;
    partical.back = lightmapDBF.g;
    partical.front = lightmapDBF.b;

    partical.emission = input.emission_color * emissionAlpha.x;
    partical.alpha = emissionAlpha.y * input.color.w * alpha_fades;

    partical.position = input.positionView.xyz;

    partical.basis_z = input.normal;
    basisFromDirPartical(partical.basis_x, partical.basis_y, partical.basis_z);
        
    partical.color = input.color.xyz;

    //Shading

    float3 diffuseReflection = float3(0.0f, 0.0f, 0.0f);

    for (int i = 0; i < g_numPointLights; ++i)
    {
        pointLightCalculation(diffuseReflection, g_pointLights[i], partical, i);
    }

    //for (int i = 0; i < g_numSpotLightsWithMask; ++i)
    //{
    //    spotLightWithMaskCalculation(diffuseReflection, specularReflection, g_spotLightsWithMask[i], view, surface, i);
    //}
    //
    for (int i = 0; i < g_numDirectionalLights; ++i)
    {
        directionalLightCalculation(diffuseReflection, g_directionalLights[i], partical, i);
    }

	return float4(diffuseReflection, partical.alpha);
}
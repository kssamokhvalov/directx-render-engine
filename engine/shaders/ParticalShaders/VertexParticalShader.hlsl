#include "../globals.hlsl"
#include "../utils.hlsl"

struct VS_Input
{
    float4 color : COLOR0;
    float3 emission_color : COLOR1;
    float3 position : POSITION;
    float3 speed : SPEED;
    float rotation_angle : ROTATION;
    float start_time : START_TIME;
    float duration : DURATION;
    float2 size : SIZE;
    uint vertex_id : SV_VertexID;
};

struct VS_Output
{
    float4 position : SV_POSITION;
    float4 positionView : POSITION;
    float2 tc : TEXCOORD;
    nointerpolation float3 normal : NORMAL;
    nointerpolation float4 color : COLOR0;
    nointerpolation float3 emission_color : COLOR1;
    nointerpolation float frameFraction : DURATION;

};

VS_Output main(VS_Input input)
{
    static const int NUM_TEXTURS = 8;
    
    VS_Output output;

    output.frameFraction = (g_time - input.start_time) / (input.duration * NUM_TEXTURS * NUM_TEXTURS); // goes from 0.0 to 1.0 between two sequential frames

#ifdef LIGHT_CALCULATION_WORLD_SPACE
#else
    float4x4 worldToView = g_worldToView();
    float4x4 projection = g_projection();
#endif 

#ifdef LIGHT_CALCULATION_WORLD_SPACE
    float3 V = normalize(g_cameraPosition() - input.position);
#else
    float3 V = normalize(-input.position);
#endif 
    float3 right = g_worldToView()._m00_m10_m20;
    float3 top = g_worldToView()._m01_m11_m21;

    right *= input.size[0] / 2;
    top *= input.size[1];

    float3 position = input.position;

    switch (input.vertex_id) 
    {
    case 0:
        output.positionView = float4(position - right, 1.0f);
        output.tc = float2(0.0f, 1.0f);
        break;
    case 1:
    case 4:
        output.positionView = float4(position - right + top, 1.0f);
        output.tc = float2(0.0f, 0.0f);
        break;
    case 2:
    case 3:
        output.positionView = float4(position + right, 1.0f);
        output.tc = float2(1.0f, 1.0f);
        break;
    case 5:
        output.positionView = float4(position + right + top, 1.0f);
        output.tc = float2(1.0f, 0.0f);
        break;
    default:
        break;
    }

#ifdef LIGHT_CALCULATION_WORLD_SPACE
    output.position = mul(output.positionView, g_worldToViewProj());
#else
    output.position = mul(float4(output.positionView.xyz, 0.0f), worldToView);
    output.position = mul(float4(output.position.xyz, 1.0f), projection);
#endif 

    output.color = input.color;
    output.normal = V;
    output.emission_color = input.emission_color;
    

	return output;
}
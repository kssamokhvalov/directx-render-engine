#include "../globals.hlsl"

static const  float COFF_BLEND = 0.002f;

struct VS_Output
{
	float4 position : SV_POSITION;
};

Texture2D<float4> BloomMap : register(t27);

float4 main(VS_Output input) : SV_TARGET
{
	float3 color_HDR = g_HDR.Load(float3(input.position.xy, 0)).xyz;
	float3 color_Bloom = BloomMap.Load(float3(input.position.xy, 0)).xyz;

	float3 ans_color = (1.0f - COFF_BLEND) * color_HDR + COFF_BLEND * color_Bloom;

	return float4(ans_color, 1);
}
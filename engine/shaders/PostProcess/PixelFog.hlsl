#include "../globals.hlsl"
#include "../utils.hlsl"

static const  float  INV_4_PI = 1.0f / (4.0f  * PI);
static const  float  G = 0.05;

struct PS_Input
{
	float4 position : SV_POSITION;
};

float phaseHG(float cosTheta, float g) {
	float denom = 1 + g * g + 2 * g * cosTheta;
	return INV_4_PI * (1 - g * g) / (denom * sqrt(denom));
}

float4 main(PS_Input input) : SV_TARGET
{
    float4 Ib = g_HDR.Load(float3(input.position.xy, 0));
    float4 Isun = g_directionalLights[0].emission;
    float w = g_directionalLights[0].w;
    //Sun direction
	float3 L = g_directionalLights[0].direction;
	L = normalize(L);

    //Position direction
    float3 surf_position;
    surf_position.x = input.position.x / g_mainResolution.x;
    surf_position.y = (g_mainResolution.y - input.position.y) / g_mainResolution.y; // swap coordinats y
    surf_position.z = g_DepthBufferCopy.Sample(g_anisotropicWrap, float2(surf_position.x, 1.0f - surf_position.y)).x;
    surf_position.xy = 2.0f * surf_position.xy - 1.0f;

    float4x4 worldToViewInv = g_worldToViewInv();
    float4x4 projectionInv = g_projectionInv();

    float distance;

    float4 position = mul(float4(surf_position, 1.0f), projectionInv);
    position.xyz = position.xyz / position.w;

    float3 pos_dir = mul(float4(position.xyz, 0.0f), worldToViewInv);
    distance = length(pos_dir);
    pos_dir = normalize(pos_dir);

    float cosTheta = dot(pos_dir, L);

    //Extinction and inscattering
    float Fex = exp(-g_coffAbsorption * distance);
    float pHG = phaseHG(cosTheta, g_asymmetryParameter);

    float4 Iin = Isun * min(pHG * w, 1.0f) * (1.0f - Fex) / g_coffAbsorption;

    float4 Iout = Ib * Fex + Iin;

	return float4(Iout.rgb, 1.0f);
}
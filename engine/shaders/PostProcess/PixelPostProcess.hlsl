#include "../globals.hlsl"

struct VS_Output
{
	float4 position : SV_POSITION;
};

float3 acesHdr2Ldr(float3 hdr)
{
	float3x3 m1 = float3x3(
		float3(0.59719f, 0.07600f, 0.02840f),
		float3(0.35458f, 0.90834f, 0.13383f),
		float3(0.04823f, 0.01566f, 0.83777f)
		);
	float3x3 m2 = float3x3(
		float3(1.60475f, -0.10208, -0.00327f),
		float3(-0.53108f, 1.10813, -0.07276f),
		float3(-0.07367f, -0.00605, 1.07602f)
		);

	float3 v = mul(hdr, m1);
	float3 a = v * (v + float3(0.0245786f, 0.0245786f, 0.0245786f)) - float3(0.000090537f, 0.000090537f, 0.000090537f);
	float3 b = v * (float3(0.983729f, 0.983729f, 0.983729f) * v + float3(0.4329510f, 0.4329510f, 0.4329510f)) + float3(0.238081f, 0.238081f, 0.238081f);
	float3 ldr = saturate(mul(a / b, m2));

	return ldr;
}

float3 adjustExposure(float3 color, float EV100)
{
	float LMax = (78.0f / (0.65f * 100.0f)) * pow(2.0f, EV100);
	return color * (1.0f / LMax);
}

float3 correctGamma(float3 color, float gamma)
{
	return pow(color, 1.0f / gamma);
}


float4 main(VS_Output input) : SV_TARGET
{
	float3 ans_color = float3(0.0f, 0.0f, 0.0f);

	if (g_flags & EN_MSAA)
	{
		for (uint i = 0; i < 4; i++)
		{
			float3 subsample = g_HDR_MS.Load(input.position.xy, i).xyz;
			subsample = adjustExposure(subsample, g_EV100);
			ans_color += acesHdr2Ldr(subsample);
		}
		ans_color /= 4;
	}
	else
	{
		ans_color = g_HDR.Load(float3(input.position.xy, 0)).xyz;
		ans_color = adjustExposure(ans_color, g_EV100);
		ans_color = acesHdr2Ldr(ans_color);
	}
	ans_color = correctGamma(ans_color, g_gamma);
	float luma = dot(ans_color, float3(0.2126f, 0.7152f, 0.0722f));

	return float4(ans_color, luma);
}
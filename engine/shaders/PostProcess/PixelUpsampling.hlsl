#include "../globals.hlsl"

struct PS_Input
{
    float4 position : SV_POSITION;
};

float4 main(PS_Input input) : SV_TARGET
{
    float2 texture_coordinats = float2(input.position.x / (g_resolutions.x + 2), input.position.y / (g_resolutions.y + 2)) / 2;

    // The filter kernel is applied with a radius, specified in texture
// coordinates, so that the radius will vary across mip resolutions.
    float x = 1 / g_resolutions.x;
    float y = 1 / g_resolutions.y;

    // Take 9 samples around current texel:
    // a - b - c
    // d - e - f
    // g - h - i
    // === ('e' is the current texel) ===
    float3 a = g_HDR.Sample(g_linearClamp, float2(texture_coordinats.x - x, texture_coordinats.y + y)).rgb;
    float3 b = g_HDR.Sample(g_linearClamp, float2(texture_coordinats.x, texture_coordinats.y + y)).rgb;
    float3 c = g_HDR.Sample(g_linearClamp, float2(texture_coordinats.x + x, texture_coordinats.y + y)).rgb;

    float3 d = g_HDR.Sample(g_linearClamp, float2(texture_coordinats.x - x, texture_coordinats.y)).rgb;
    float3 e = g_HDR.Sample(g_linearClamp, float2(texture_coordinats.x, texture_coordinats.y)).rgb;
    float3 f = g_HDR.Sample(g_linearClamp, float2(texture_coordinats.x + x, texture_coordinats.y)).rgb;

    float3 g = g_HDR.Sample(g_linearClamp, float2(texture_coordinats.x - x, texture_coordinats.y - y)).rgb;
    float3 h = g_HDR.Sample(g_linearClamp, float2(texture_coordinats.x, texture_coordinats.y - y)).rgb;
    float3 i = g_HDR.Sample(g_linearClamp, float2(texture_coordinats.x + x, texture_coordinats.y - y)).rgb;

    // Apply weighted distribution, by using a 3x3 tent filter:
    //  1   | 1 2 1 |
    // -- * | 2 4 2 |
    // 16   | 1 2 1 |
    float3 upsample = e * 4.0;
    upsample += (b + d + f + h) * 2.0;
    upsample += (a + c + g + i);
    upsample *= 1.0 / 16.0;

    return float4(upsample, 1.0f);
}
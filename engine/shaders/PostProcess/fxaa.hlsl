/*-----------------------------------------------------------------------------
	Integrating NVidia FXAA code
-----------------------------------------------------------------------------*/

#define FXAA_PC 1 // PC platform
#define FXAA_HLSL_5 1 // Shader Model 5
#define FXAA_QUALITY__PRESET 39
    // 10 to 15 - default medium dither (10=fastest, 15=highest quality)
    // 20 to 29 - less dither, more expensive (20=fastest, 29=highest quality)
    // 39       - no dither, very expensive 

#include "Fxaa3_11.hlsl"

/*---------------------------------------------------------------------------*/
#include "../globals.hlsl"
// .rgb = LDR sRGB color (after gamma-correction), .a = color luma

struct PSIn
{
	float4 pos : SV_POSITION;
};

float4 main(PSIn pin) : SV_TARGET
{
	FxaaTex TextureAndSampler;
	TextureAndSampler.tex = g_LDR;
	TextureAndSampler.smpl = g_linearClamp;
	TextureAndSampler.UVMinMax = float4(0, 0, 1, 1); // fullscreen uv

	return FxaaPixelShader(
		pin.pos.xy * g_mainResolution.zw, // map pixel coordinate to [0; 1] range
		0, // unused, for consoles
		TextureAndSampler,
		TextureAndSampler,
		TextureAndSampler,
		g_mainResolution.zw,
		0, // unused, for consoles
		0, // unused, for consoles
		0, // unused, for consoles
		g_qualitySubpix,
		g_qualityEdgeThreshold,
		g_qualityEdgeThresholdMin,
		0, // unused, for consoles
		0, // unused, for consoles
		0, // unused, for consoles
		0 // unused, for consoles
	);
}
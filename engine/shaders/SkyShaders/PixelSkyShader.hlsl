#include "../globals.hlsl"

struct VS_Output
{
	float4 position : SV_POSITION;
	float4 positionGlobal : POSITION;
};

float4 main(VS_Output input) : SV_TARGET
{
	return float4(g_skyBox.Sample(g_anisotropicWrap, input.positionGlobal).rgb, 1.0f);
}
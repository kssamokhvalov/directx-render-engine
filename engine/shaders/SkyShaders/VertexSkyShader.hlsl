#include "../globals.hlsl"
struct VS_Output
{
	float4 position : SV_POSITION;
	float4 positionGlobal : POSITION;
};

VS_Output main(uint vertex_id : SV_VertexID)
{
	VS_Output output;

	const float4 triangle_positions[3] = { float4(-1,-1, 0, 1),  float4(-1, 3, 0, 1), float4(3, -1, 0, 1) };

	output.positionGlobal = g_frustrim_directions[vertex_id];
	output.position = triangle_positions[vertex_id];

	return output;
}
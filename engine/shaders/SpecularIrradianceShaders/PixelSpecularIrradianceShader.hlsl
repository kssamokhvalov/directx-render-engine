#include "../utils.hlsl"
#include "../globals.hlsl"

static const int NUM_SAMPLES = 1024;

struct VS_Output
{
	float4 position : SV_POSITION;
	float3 normal : POSITION;
};

float3 main(VS_Output input) : SV_TARGET
{

	uint width;
	uint height;

	g_skyBox.GetDimensions(width, height);
	float resolution = width;

	float3 N = normalize(input.normal);
	float3 V = N;
	float epsilon = 0.0001f;
	float total_weight = 0.0f; //page 6 https://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf

	const float3x3 R = basisFromDir(normalize(input.normal));

	float3 ans_color = float3(0.0f, 0.0f, 0.0f);
	float NoH = 0.0f;
	float rough = g_roughness * g_roughness;
	for (int i = 0; i < NUM_SAMPLES; ++i)
	{
		float3 H = ImportanceSampleGGX(NoH, i, NUM_SAMPLES, rough * rough, R);

		float D = ggx(rough * rough, NoH);
		float pdf = (D / 4.0f) + epsilon; //because N = V = R
		float saTexel = 4.0f * PI / (6.0f * resolution * resolution);
		float saSample = 1.0f / (float(NUM_SAMPLES) * pdf + epsilon);
		float mipLevel = g_roughness == 0.0 ? 0.0 : 0.5 * log2(saSample / saTexel);
 
		float3 L = 2 * NoH * H - V; //because N = V = R

		float NoL = saturate(dot(N, L));
		if (NoL > 0.0001f)
		{
			float3 Ei = g_skyBox.SampleLevel(g_linearWrap, L, mipLevel).rgb * NoL;

			ans_color += Ei;
			total_weight += NoL;
		}
	}

	return ans_color / total_weight;
}


//float3 main(VS_Output input) : SV_TARGET
//{
//	float3 N = normalize(input.normal);
//	float3 V = N;
//	float epsilon = 0.001f;
//	float total_weight = 0.0f; //page 6 https://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf
//
//	const float3x3 R = basisFromDir(normalize(input.normal));
//
//	float3 ans_color = float3(0.0f, 0.0f, 0.0f);
//	float NoH = 0.0f;
//	float rough = g_roughness * g_roughness;
//	float S = 2.0f / (PI * NUM_SAMPLES * ggx(rough * rough, 1.0f)); // 1.0f because N = V = H
//	float mip_level = max(0.0f, 0.5f * log2(3 * g_width_tex * g_hight_tex * S));
//	int num_samples = 0;
//	for (int i = 0; i < NUM_SAMPLES; ++i)
//	{
//		float3 H = randomGGX(NoH, i, NUM_SAMPLES, rough * rough, R);
//		if (NoH < 0.0001f)
//			continue;
//		float3 L = 2 * dot(V, H) * H - V;
//
//		float NoL = saturate(dot(N, L));
//		if (NoL > 0.0001f)
//		{
//			float3 Ei = g_skyBox.SampleLevel(g_linearWrap, L, mip_level).rgb;
//
//			ans_color += Ei;
//			total_weight += NoL;
//			num_samples++;
//		}
//	}
//
//	return ans_color / num_samples;
//}
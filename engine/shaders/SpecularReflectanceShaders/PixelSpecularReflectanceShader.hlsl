#include "../utils.hlsl"
#include "../globals.hlsl"

static const int NUM_SAMPLES = 1024;

struct VS_Output
{
	float4 position : SV_POSITION;
};

float2 main(VS_Output input) : SV_TARGET
{
	float2  ans_color = float2(0.0f, 0.0f);
	float NoV = input.position.x / g_width_tex;
	float3 V = float3(sqrt(1 - NoV * NoV), 0.0f, NoV);
	float rough = 1 - input.position.y / g_hight_tex; //flip Y
	rough = rough * rough;
	float NoH = 0.0f;
	float3 N = float3(0.0f, 0.0f, 1.0f);
	int num_samples = 0;
	for (int i = 0; i < NUM_SAMPLES; ++i)
	{
		float3 H = ImportanceSampleGGX(randomHammersley(i, NUM_SAMPLES), rough * rough);
		float3 L = 2 * dot(V, H) * H - V;
		float NoL = L.z;
		NoH = max(H.z, 0.0001f);
		float HoV = max(dot(H, V), 0.0001f);
		if (NoL > 0.0001f && NoH > 0.0001f)
		{
			float G_Vis = smith(rough * rough, NoV, NoL) * HoV / (NoV * NoH);
			float Fc = pow(1 - HoV, 5.0f);
			ans_color.r += G_Vis * (1 - Fc);
			ans_color.g += G_Vis * Fc;
			num_samples++;
		}
	}

	return ans_color / num_samples;
}
#include "../globals.hlsl"

struct VS_Output
{
    float4 position : SV_POSITION;
    float2 tc : TEXCOORD;
    float4 normal : NORMAL;
};

float4 main(VS_Output input) : SV_TARGET
{
	return g_albedoTexture.Sample(g_pointWrap, input.tc);
}
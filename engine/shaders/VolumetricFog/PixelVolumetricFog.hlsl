#include "../globals.hlsl"
#include "../utils.hlsl"

static const  float  INV_4_PI = 1.0f / (4.0f * PI);
static const  float  G = 0.05;
static const  int  N = 10;

Texture3D<float> simplexNoise3D : register(t27);

float phaseHG(float cosTheta, float g) {
    float denom = 1 + g * g + 2 * g * cosTheta;
    return INV_4_PI * (1 - g * g) / (denom * sqrt(denom));
}

void swap(inout float a, inout float b)
{
    float  c = a;
    a = b;
    b = c;
}

bool intersects(float3 dir, float3 origin, float3 min, float3 max, out float tmin, out float tmax)
{

    tmin = (min.x - origin.x) / dir.x;
    tmax = (max.x - origin.x) / dir.x;

    if (tmin > tmax) swap(tmin, tmax);

    float tymin = (min.y - origin.y) / dir.y;
    float tymax = (max.y - origin.y) / dir.y;

    if (tymin > tymax) swap(tymin, tymax);

    if ((tmin > tymax) || (tymin > tmax))
        return false;

    if (tymin > tmin)
        tmin = tymin;

    if (tymax < tmax)
        tmax = tymax;

    float tzmin = (min.z - origin.z) / dir.z;
    float tzmax = (max.z - origin.z) / dir.z;

    if (tzmin > tzmax) swap(tzmin, tzmax);

    if ((tmin > tzmax) || (tzmin > tmax))
        return false;

    if (tzmin > tmin)
        tmin = tzmin;

    if (tzmax < tmax)
        tmax = tzmax;

    return true;
}

bool intersects(float3 dir, float3 origin, float3 min, float3 max, out float tmax)
{
    float tmin = 0.0f;
    return intersects( dir, origin, min, max, tmin, tmax);
}

struct PS_Intput
{
    float4 position : SV_POSITION;
    float4 positionView : POSITION;
    float4x4 fogToWorld : MATRIX_ONE;
    float4x4 worldToFog : MATRIX_TWO;
    float3 min : MIN;
    float3 max : MAX;
};

float4 main(PS_Intput input) : SV_TARGET
{
    float4 Ib = g_HDR.Load(float3(input.position.xy, 0));
    float4 Isun = g_directionalLights[0].emission;
    float w = g_directionalLights[0].w;

    float3 dir = normalize(input.positionView.xyz);
    float3 origin = float3(0.0f, 0.0f, 0.0f);
    float tmin = 0.0f;
    float tmax = 0.0f;

    intersects(dir, origin, input.min, input.max, tmin, tmax);

    float3 surf_position;
    surf_position.x = input.position.x / g_mainResolution.x;
    surf_position.y = (g_mainResolution.y - input.position.y) / g_mainResolution.y; // swap coordinats y
    surf_position.z = g_DepthBufferCopy.Sample(g_anisotropicWrap, float2(surf_position.x, 1.0f - surf_position.y)).x;
    surf_position.xy = 2.0f * surf_position.xy - 1.0f;

    float4x4 worldToViewInv = g_worldToViewInv();
    float4x4 projectionInv = g_projectionInv();

    float distance;

    float4 position = mul(float4(surf_position, 1.0f), projectionInv);
    position.xyz = position.xyz / position.w;

    surf_position = mul(float4(position.xyz, 0.0f), worldToViewInv);

    float t_surf = (surf_position.z - origin.z) / dir.z;

    if (tmin > t_surf)
        discard;

    if (tmax > t_surf)
    {
        tmax = t_surf;
    }

    float t = 0.0f;

    if (tmin > 0.0f)
    {
        origin = origin + dir * tmin;
        t = tmax - tmin;
    }
    else
    {
        t = tmax;
    }

    float max_distance = t;

    //Sun direction
    float3 L = g_directionalLights[0].direction;
    L = normalize(L);

    float cosTheta = dot(dir, L);

    float dD = max_distance / N;
    float dt = t / N;
    float3 sizeBox = input.max - input.min;
    float3 originBox = origin - input.min;
    originBox = float3(originBox.x / sizeBox.x, originBox.y / sizeBox.y, originBox.z / sizeBox.z);
    float3 dirBox = float3(dir.x / sizeBox.x, dir.y / sizeBox.y, dir.z / sizeBox.z);
    float3 dirSunBox = -float3(L.x / sizeBox.x, L.y / sizeBox.y, L.z / sizeBox.z);
    float coffAbsorption = 0.0f;

    float coff_Iin = 0.0f;

    for (int i = 1; i < N + 1; i++)
    {
        float ti = dt * i;
        float3 pos_i = originBox + dirBox * ti;
        coffAbsorption += simplexNoise3D.Sample(g_linearClamp, pos_i).r + 0.001f;

        //Oin

        float t_sun = 0.0f;
        intersects(dirSunBox, pos_i, float3(0.0f, 0.0f, 0.0f), float3(1.0f, 1.0f, 1.0f), t); //in box coordinats
        float dD_sun = length(pos_i + dirSunBox  * t_sun) / N;
        float dt_sun = t_sun / N;

        float coffAbsorptionSun = 0.0f;

        for (int k = 1; k < N + 1; k++)
        {
            float tk = dt_sun * k;
            float3 pos_k = pos_i + dirSunBox * tk;
            coffAbsorptionSun += simplexNoise3D.Sample(g_linearClamp, pos_k).r + 0.001f;
        }
        coff_Iin += exp(-(coffAbsorption * dD + coffAbsorptionSun * dD_sun));
    }

    //Extinction and inscattering
    float Fex = exp(-coffAbsorption * dD);
    float pHG = phaseHG(cosTheta, 0.5f);

    float4 Iin = Isun * min(pHG * w, 1.0f) * coff_Iin * max_distance / N;

    float4 Iout = Ib * Fex + Iin;

    return float4(Iout.rgb, 1.0f);
}
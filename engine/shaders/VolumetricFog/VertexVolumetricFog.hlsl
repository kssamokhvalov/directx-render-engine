#include "../globals.hlsl"

struct VS_Input
{
    float3 pos : POSITION;

    float4 fogToWorld1 : MW1;
    float4 fogToWorld2 : MW2;
    float4 fogToWorld3 : MW3;
    float4 fogToWorld4 : MW4;

    float4 worldToFog1 : MW5;
    float4 worldToFog2 : MW6;
    float4 worldToFog3 : MW7;
    float4 worldToFog4 : MW8;

    float3 min : MIN;
    float3 max : MAX;
};

struct VS_Output
{
    float4 position : SV_POSITION;
    float4 positionView : POSITION;
    nointerpolation float4x4 fogToWorld : MATRIX_ONE;
    nointerpolation float4x4 worldToFog : MATRIX_TWO;
    nointerpolation float3 min : MIN;
    nointerpolation float3 max : MAX;
};

VS_Output main(VS_Input input)
{
    VS_Output output;
    output.position = float4(input.pos, 1.0f);
#ifdef LIGHT_CALCULATION_WORLD_SPACE
#else
    float4x4 worldToView = g_worldToView();
    float4x4 projection = g_projection();
#endif 
    output.fogToWorld = float4x4(input.fogToWorld1, input.fogToWorld2, input.fogToWorld3, input.fogToWorld4);

    output.position = mul(output.position, output.fogToWorld);
    output.positionView = output.position;

#ifdef LIGHT_CALCULATION_WORLD_SPACE
    output.position = mul(output.position, g_worldToViewProj());
#else
    output.position = mul(float4(output.position.xyz, 0.0f), worldToView);
    output.position = mul(float4(output.position.xyz, 1.0f), projection);
#endif 
    output.worldToFog = float4x4(input.worldToFog1, input.worldToFog2, input.worldToFog3, input.worldToFog4);

    output.min = mul(float4(input.min, 1.0f), output.fogToWorld);
    output.max = mul(float4(input.max, 1.0f), output.fogToWorld);

    return output;
}
//#define LIGHT_CALCULATION_WORLD_SPACE
#define EN_DIFFUSE 1
#define EN_SPECLAR 2
#define EN_IBL 4
#define EN_ROUGHNESS_OVERWRITING 8
#define EN_MSAA 16

// all const values must be equal to const values in LightSystem.h
static const int MAX_NUM_SPOT_LIGHTS = 10;
static const int MAX_NUM_POINT_LIGHTS = 10;
static const int MAX_NUM_DIRETIONAL_LIGHTS = 2;
static const int MAX_NUM_SPOT_LIGHTS_WITH_MASK = 1;
static const float TYPICALLY_F0 = 0.04f;

static const int HAS_ALBEDO = 1;
static const int HAS_NORMAL = 1 << 1;
static const int HAS_ROUGHNESS = 1 << 2;
static const int HAS_METALNESS = 1 << 3;

static const float NEAR_PLANE_SHADOW = 0.01f;
static const float FAR_PLANE_SHADOW = 100.0f;

struct SpotLight
{
    float4 lightToModel1;
    float4 lightToModel2;
    float4 lightToModel3;
    float4 lightToModel4;
    float4 emission;
    float p;
    float radius;
    float2 _pad;
};

struct SpotLightWithMask
{
    float4 worldToLight1;
    float4 worldToLight2;
    float4 worldToLight3;
    float4 worldToLight4;
    float4 lightToModel1;
    float4 lightToModel2;
    float4 lightToModel3;
    float4 lightToModel4;
    float4 maskProj1;
    float4 maskProj2;
    float4 maskProj3;
    float4 maskProj4;
    float4 emission;
    float p;
    float radius;
    float2 _pad1;
    float4 _pad2;
};

struct PointLight
{
    float3 position;
    float radius;
    float4 emission;
    float4 _pad;
};

struct DirectionalLight
{
    float4 worldToLight1;
    float4 worldToLight2;
    float4 worldToLight3;
    float4 worldToLight4;
    float4 proj1;
    float4 proj2;
    float4 proj3;
    float4 proj4;
    float3 direction;
    float w;            //solid angle
    float4 emission;
    float cosAlpha;
    float3 _pad;
};

struct ParticleGPU
{
    float4 tint;
    float3 emission_color;
    float3 position;
    float3 speed;
    float start_time;
    float life_time;
    uint active;
};

struct RangeDescription
{
    uint number;
    uint offset;
    uint expired;
    uint VertexCountPerInstance;
    uint InstanceCount;
    uint StartVertexLocation;
    uint StartInstanceLocation;
};

cbuffer PerFrame : register(b0)
{
    float4 g_mainResolution;
    float  g_time;
    float  g_time_prev_frame;
    int g_flags;
    float g_roughnessSlider;
    float g_qualitySubpix;				//   FXAA_QUALITY__SUBPIX, range [0.0; 1.0], default 0.75
    //   Choose the amount of sub-pixel aliasing removal. This can effect sharpness.
    //   1.00 - upper limit (softer)
    //   0.75 - default amount of filtering
    //   0.50 - lower limit (sharper, less sub-pixel aliasing removal)
    //   0.25 - almost off
    //   0.00 - completely off

    float g_qualityEdgeThreshold;  		//   FXAA_QUALITY__EDGE_THRESHOLD, range [0.063; 0.333], best quality 0.063
    //   The minimum amount of local contrast required to apply algorithm.
    //   0.333 - too little (faster)
    //   0.250 - low quality
    //   0.166 - default
    //   0.125 - high quality 
    //   0.063 - overkill (slower)

    float g_qualityEdgeThresholdMin; 	//   FXAA_QUALITY__EDGE_THRESHOLD_MIN, range [0.0; 0.0833], best quality 0.0312
    //   Trims the algorithm from processing darks.
    //   0.0833 - upper limit (default, the start of visible unfiltered edges)
    //   0.0625 - high quality (faster)
    //   0.0312 - visible limit (slower)

    float g_coffAbsorption;
    float g_asymmetryParameter;
}

cbuffer PerView : register(b1)
{
    float4 g_worldToViewProj1;
    float4 g_worldToViewProj2;
    float4 g_worldToViewProj3;
    float4 g_worldToViewProj4;

    float4 g_worldToViewProjInv1;
    float4 g_worldToViewProjInv2;
    float4 g_worldToViewProjInv3;
    float4 g_worldToViewProjInv4;

    float4 g_worldToView1;
    float4 g_worldToView2;
    float4 g_worldToView3;
    float4 g_worldToView4;

    float4 g_worldToViewInv1;
    float4 g_worldToViewInv2;
    float4 g_worldToViewInv3;
    float4 g_worldToViewInv4;

    float4 g_projection1;
    float4 g_projection2;
    float4 g_projection3;
    float4 g_projection4;

    float4 g_projectionInv1;
    float4 g_projectionInv2;
    float4 g_projectionInv3;
    float4 g_projectionInv4;

    float2 _pad_b1;
    float g_shadow_texel;
    float g_reflectionMips;
    float4 g_frustrim_directions[3];
}

cbuffer LightSources : register(b3)
{
    SpotLight g_spotLights[MAX_NUM_SPOT_LIGHTS];
    SpotLightWithMask g_spotLightsWithMask[MAX_NUM_SPOT_LIGHTS_WITH_MASK];
    PointLight g_pointLights[MAX_NUM_POINT_LIGHTS];
    DirectionalLight g_directionalLights[MAX_NUM_DIRETIONAL_LIGHTS];
    int g_numSpotLights;
    int g_numSpotLightsWithMask;
    int g_numPointLights;
    int g_numDirectionalLights;
    float4 g_ambiend_light;
}

cbuffer PostProcess : register(b4)
{
    float4 g_resolutions; //.xy original .za render target
    float g_gamma;
    float g_EV100;
}

cbuffer ReflectionCapture : register(b5)
{
    float4 g_frustrim_directions_tex[3]; 
    float g_width_tex;
    float g_hight_tex;
    float g_roughness;
}

cbuffer MaterialData : register(b6)
{
    float3 g_texture_albedo;
    float g_texture_roughness;
    float3 g_texture_normal;
    float g_texture_metalness;
    int g_texture_state;
    float3 _pad;
}

//Textures
TextureCube<float4> g_skyBox : register(t0);
Texture2D<float4> g_albedoTexture : register(t1);
Texture2D<float4> g_normalTexture : register(t2);
Texture2D<float4> g_roughnessTexture : register(t3);
Texture2D<float4> g_metalnessTexture : register(t4);
Texture2D<float4> g_flashLightMask : register(t5);
TextureCube<float4> g_irradiance : register(t6);
TextureCube<float4> g_reflection : register(t7);
Texture2D<float2> g_reflectance : register(t8);

//Shadows
Texture2DArray<float> g_directional_shadows : register(t9);
Texture2DArray<float> g_spot_with_mask_shadows : register(t10);
TextureCubeArray<float> g_point_shadows : register(t11);

//PostProcess
Texture2DMS<float4> g_HDR_MS : register(t12);

//Noises
Texture2D<float> g_noise : register(t13);

//Smoke
Texture2D<float3> g_BBF : register(t14);
Texture2D<float3> g_RLT : register(t15);
Texture2D<float4> g_EMVA : register(t16);

//PostProcess
Texture2DMS<float> g_DepthBufferMSCopy : register(t17);
Texture2D<float2> g_DepthBufferCopy : register(t18);
Texture2D<float4> g_HDR : register(t19);

//GBuffers
Texture2D<float4> g_gb_albedo : register(t20);
Texture2D<float4> g_gb_rough_metal : register(t21);
Texture2D<float4> g_gb_normal : register(t22);
Texture2D<float4> g_gb_emmision : register(t23);
Texture2D<uint> g_gb_objectID : register(t24);

Texture2D<uint2> g_stencilBuffer : register(t25);

Texture2D<float4> g_LDR : register(t26);


//t27-t28 reserved


//Samplers
SamplerState g_pointWrap : register(s0);
SamplerState g_linearWrap : register(s1);
SamplerState g_anisotropicWrap : register(s2);
SamplerState g_linearBorder : register(s3);
SamplerState g_linearClamp : register(s4);
SamplerState g_anisotropicClamp : register(s5);

SamplerComparisonState g_linearWrapCamp : register(s6);

float4x4 g_worldToView()
{
    return float4x4(g_worldToView1, g_worldToView2, g_worldToView3, g_worldToView4);
}

float4x4 g_worldToViewInv()
{
    return float4x4(g_worldToViewInv1, g_worldToViewInv2, g_worldToViewInv3, g_worldToViewInv4);
}

float4x4 g_worldToViewProj()
{
    return float4x4(g_worldToViewProj1, g_worldToViewProj2, g_worldToViewProj3, g_worldToViewProj4);
}

float4x4 g_worldToViewProjInv()
{
    return float4x4(g_worldToViewProjInv1, g_worldToViewProjInv2, g_worldToViewProjInv3, g_worldToViewProjInv4);
}

float4x4 g_projection()
{
    return float4x4(g_projection1, g_projection2, g_projection3, g_projection4);
}

float4x4 g_projectionInv()
{
    return float4x4(g_projectionInv1, g_projectionInv2, g_projectionInv3, g_projectionInv4);
}

float3 g_cameraPosition()
{
    return g_worldToViewInv4.xyz;
}

bool g_hasAlbedoTexture()
{
    return g_texture_state & HAS_ALBEDO;
}
bool g_hasNormalTexture()
{
    return g_texture_state & HAS_NORMAL;
}
bool g_hasRoughnessTexture()
{
    return g_texture_state & HAS_ROUGHNESS;
}
bool g_hasMetalnessTexture()
{
    return g_texture_state & HAS_METALNESS;
}
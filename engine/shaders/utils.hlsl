static const float PI = 3.14159265f;


// Height-correlated Smith G2 for GGX,
// Filament, 4.4.2 Geometric shadowing
float smith(float rough4, float NoV, float NoL)
{
	NoV *= NoV;
	NoL *= NoL;
	return 2.0 / (sqrt(1 + rough4 * (1 - NoV) / NoV) + sqrt(1 + rough4 * (1 - NoL) / NoL));
}

// GGX normal distribution,
// Real-Time Rendering 4th Edition, page 340, equation 9.41
float ggx(float rough4, float NoH)
{
	float denom = NoH * NoH * (rough4 - 1.0) + 1.0;
	denom = PI * denom * denom;
	return rough4 / denom;
}

// ---------------- SAMPLE GENERATION ----------------


// Schlick's approximation of Fresnel reflectance,
float3 fresnel(float NdotL, float3 F0)
{
	return F0 + (1 - F0) * pow(1 - NdotL, 5);
}

// Fibonacci hemisphere point uniform distribution
float3 randomHemisphere(out float NdotV, float i, float N)
{
	const float GOLDEN_RATIO = (1.0 + sqrt(5.0)) / 2.0;
	float theta = 2.0 * PI * i / GOLDEN_RATIO;
	float phiCos = NdotV = 1.0 - (i + 0.5) / N;
	float phiSin = sqrt(1.0 - phiCos * phiCos);
	float thetaCos, thetaSin;
	sincos(theta, thetaSin, thetaCos);
	return float3(thetaCos * phiSin, thetaSin * phiSin, phiCos);
}

// Frisvad with z == -1 problem avoidance
void basisFromDir(out float3 right, out float3 top, in float3 dir)
{
	float k = 1.0 / max(1.0 + dir.z, 0.00001);
	float a = dir.y * k;
	float b = dir.y * a;
	float c = -dir.x * a;
	right = float3(dir.z + b, c, -dir.x);
	top = float3(c, 1.0 - b, -dir.y);
}

void basisFromDirPartical(out float3 right, out float3 top, in float3 dir)
{
	float z = -1.0f;
	if (dir.x - dir.y * dir.y / dir.x < 0)
	{
		z = - z;
	}

	right = normalize(float3(-z * dir.z / dir.x, 0.0f, z));
	top = normalize(cross(dir, right));
}

// Frisvad with z == -1 problem avoidance
float3x3 basisFromDir(float3 dir)
{
	float3x3 rotation;
	rotation[2] = dir;
	basisFromDir(rotation[0], rotation[1], dir);
	return rotation;
}

float randomVanDeCorput(uint bits)
{
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}

// random2D from random1D
float2 randomHammersley(float i, float N)
{
	return float2(i / N, randomVanDeCorput(i));
}

// GGX importance sampling, returns microsurface normal (half-vector)
// rough4 is initial roughness value in power of 4 
float3 ImportanceSampleGGX(float2 random, float rough4)
{
	float phi = 2.0 * PI * random.x;
	float cosTheta = sqrt((1.0 - random.y) / (1.0 + (rough4 - 1.0) * random.y));
	float sinTheta = sqrt(1.0 - cosTheta * cosTheta);

	float3 dir;
	dir.x = cos(phi) * sinTheta;
	dir.y = sin(phi) * sinTheta;
	dir.z = cosTheta;
	return dir;
}

// GGX importance sampling, returns microsurface normal (half-vector)
// rough4 is initial roughness value in power of 4
float3 ImportanceSampleGGX(out float NdotH, uint index, uint N, float rough4, float3x3 rotation)
{
	float3 H = ImportanceSampleGGX(randomHammersley(index, N), rough4);
	NdotH = H.z;
	return mul(H, rotation);
}


// Determing which mip level to read in cubemap sampling with uniform/importance sampling
float hemisphereMip(float sampleProbability, float cubemapSize)
{
	float hemisphereTexels = cubemapSize * cubemapSize * 3;
	float log4 = 0.5 * log2(sampleProbability * hemisphereTexels);
	return log4;
}

// ---------------- SPHERELIGHTS ----------------

// May return direction pointing beneath surface horizon (dot(N, dir) < 0), use clampDirToHorizon to fix it.
// sphereCos is cosine of light sphere solid angle.
// sphereRelPos is position of a sphere relative to surface:
// 'sphereDir == normalize(sphereRelPos)' and 'sphereDir * sphereDist == sphereRelPos'
float3 approximateClosestSphereDir(out bool intersects, float3 reflectionDir, float sphereCos,
	float3 sphereRelPos, float3 sphereDir, float sphereDist, float sphereRadius)
{
	float RoS = dot(reflectionDir, sphereDir);

	intersects = RoS >= sphereCos;
	if (intersects) return reflectionDir;
	if (RoS < 0.0) return sphereDir;

	float3 closestPointDir = normalize(reflectionDir * sphereDist * RoS - sphereRelPos);
	return normalize(sphereRelPos + sphereRadius * closestPointDir);
}

// Input dir and NoD is N and NoL in a case of lighting computation 
void clampDirToHorizon(inout float3 dir, inout float NoD, float3 normal, float minNoD)
{
	if (NoD < minNoD)
	{
		dir = normalize(dir + (minNoD - NoD) * normal);
		NoD = minNoD;
	}
}

// [ de Carpentier 2017, "Decima Engine: Advances in Lighting and AA" ]
void SphereMaxNoH(float NoV, inout float NoL, inout float VoL, float SinAlpha, float CosAlpha, bool bNewtonIteration, out float NoH, out float VoH)
{
	float RoL = 2 * NoL * NoV - VoL;
	if (RoL >= CosAlpha)
	{
		NoH = 1;
		VoH = abs(NoV);
	}
	else
	{
		float rInvLengthT = SinAlpha * rsqrt(1 - RoL * RoL);
		float NoTr = rInvLengthT * (NoV - RoL * NoL);
		float VoTr = rInvLengthT * (2 * NoV * NoV - 1 - RoL * VoL);

		if (bNewtonIteration && SinAlpha != 0)
		{
			// dot( cross(N,L), V )
			float NxLoV = sqrt(saturate(1 - pow(NoL, 2) - pow(NoV, 2) - pow(VoL, 2) + 2 * NoL * NoV * VoL));

			float NoBr = rInvLengthT * NxLoV;
			float VoBr = rInvLengthT * NxLoV * 2 * NoV;

			float NoLVTr = NoL * CosAlpha + NoV + NoTr;
			float VoLVTr = VoL * CosAlpha + 1 + VoTr;

			float p = NoBr * VoLVTr;
			float q = NoLVTr * VoLVTr;
			float s = VoBr * NoLVTr;

			float xNum = q * (-0.5 * p + 0.25 * VoBr * NoLVTr);
			float xDenom = p * p + s * (s - 2 * p) + NoLVTr * ((NoL * CosAlpha + NoV) * pow(VoLVTr, 2) + q * (-0.5 * (VoLVTr + VoL * CosAlpha) - 0.5));
			float TwoX1 = 2 * xNum / (pow(xDenom, 2) + pow(xNum, 2));
			float SinTheta = TwoX1 * xDenom;
			float CosTheta = 1.0 - TwoX1 * xNum;
			NoTr = CosTheta * NoTr + SinTheta * NoBr;
			VoTr = CosTheta * VoTr + SinTheta * VoBr;
		}

		NoL = NoL * CosAlpha + NoTr;
		VoL = VoL * CosAlpha + VoTr;

		float InvLenH = rsqrt(2 + 2 * VoL);
		NoH = saturate((NoL + NoV) * InvLenH);
		VoH = saturate(InvLenH + InvLenH * VoL);
	}
}

float derivatives(float alpha, float threshold, float falloff)
{
	return saturate(((alpha - threshold) / max(fwidth(alpha), 0.0001f)) / falloff);
}

float computeSolidAngle(float radius_source, float distance)
{
	return 2 * PI * (1 - sqrt(1 - min(radius_source * radius_source / (distance * distance), 1.0f)));
}

float2 nonZeroSign(float2 v)
{
	return float2(v.x >= 0.0 ? 1.0 : -1.0, v.y >= 0.0 ? 1.0 : -1.0);
}

float2 packOctahedron(float3 v)
{
	float2 p = v.xy / (abs(v.x) + abs(v.y) + abs(v.z));
	return v.z <= 0.0 ? (1.0 - abs(p.yx)) * nonZeroSign(p) : p;
}

float3 unpackOctahedron(float2 oct)
{
	float3 v = float3(oct, 1.0 - abs(oct.x) - abs(oct.y));
	if (v.z < 0) v.xy = (1.0 - abs(v.yx)) * nonZeroSign(v.xy);
	return normalize(v);
}

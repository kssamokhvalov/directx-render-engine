#pragma once
#include "math/Vector3.h"
#include "vector"
#include "cmath"

namespace engine
{
	struct BoundingBox
	{
		Vector3 min;
		Vector3 max;

		static BoundingBox empty() {
			BoundingBox empty_box;
			empty_box.min = Vector3(0.0f, 0.0f, 0.0f);
			empty_box.max = Vector3(0.0f, 0.0f, 0.0f);
			return empty_box;
		}

		bool createBox(const std::vector<Vector3>& vertices) 
		{
			if (vertices.empty()) 
			{
				return false;
			}
			float min_x = vertices[0].x();
			float min_y = vertices[0].y();
			float min_z = vertices[0].z();

			float max_x = vertices[0].x();
			float max_y = vertices[0].y();
			float max_z = vertices[0].z();

			for (const auto& vertex : vertices)
			{
				min_x = fmin(min_x, vertex.x());
				min_y = fmin(min_y, vertex.y());
				min_z = fmin(min_z, vertex.z());
				max_x = fmax(max_x, vertex.x());
				max_y = fmax(max_y, vertex.y());
				max_z = fmax(max_z, vertex.z());
			}
			min = Vector3(min_x, min_y, min_z);
			max = Vector3(max_x, max_y, max_z);

			return true;
		}
		bool contains(const Vector3& vertex) 
		{
			return ((min < vertex) && (vertex < max));
		}

		Vector3 size() 
		{
			return (max - min);
		}


	};
}
#pragma once

struct MeshInstanceID
{
    struct IdComponent
    {
        int id_shading;
        int id_model;
        int id_material;
        int id_instance;
    };

	IdComponent id_component_main;
    IdComponent id_component_normalVis;
};

struct ModelInstanceID
{
    std::vector<MeshInstanceID> meshInstanceIDs;
};
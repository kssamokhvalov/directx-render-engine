#pragma once
#include "math/Triangle.h"
#include "math/Matrix4x4.h"
#include "BoundingBox.h"
#include "TriangleOctree.h"

#include <vector>
#include <algorithm>
#include <numeric>
#include <string>
	
namespace engine
{
	struct  Mesh 
	{
		struct Float2
		{
			float x;
			float y;

			Float2 operator-(const Float2& vector2)
			{
				return { x - vector2.x, y - vector2.y };
			}
		};

		struct Vertex
		{
			Vector3 pos;
			Vector3 normal;
			Float2 tc;
			Vector3 tangent;
			Vector3	bitangent;

			static Vertex initial()
			{
				Vertex ans;

				ans.pos = Vector3(0.0f, 0.0f, 0.0f);
				ans.normal = Vector3(0.0f, 0.0f, 0.0f);
				ans.tc = { 0.0f, 0.0f };
				ans.tangent = Vector3(0.0f, 0.0f, 0.0f);
				ans.bitangent = Vector3(0.0f, 0.0f, 0.0f);

				return ans;
			}
		};

		std::vector<math::Triangle> triangles;
		std::vector<Vertex> vertices;
		std::vector<Matrix4x4> instances;
		std::vector<Matrix4x4> instancesInv;

		BoundingBox box;
		TriangleOctree octree;
		std::string name;

		static Mesh unitCube()
		{
			//Clockwise
			Mesh unit_cube;

			unit_cube.name = "unitCube";

			//bottom
			unit_cube.vertices.push_back({ Vector3(-0.5f, -0.5f, -0.5f), Vector3(0, -1.0f, 0), {0.0f, 0.0f} });
			unit_cube.vertices.push_back({ Vector3(-0.5f, -0.5f, 0.5f), Vector3(0, -1.0f, 0), {1.0f, 0.0f} });
			unit_cube.vertices.push_back({ Vector3(0.5f, -0.5f, -0.5f), Vector3(0, -1.0f, 0), {0.0f, 1.0f} });
			unit_cube.vertices.push_back({ Vector3(0.5f, -0.5f, 0.5f), Vector3(0, -1.0f, 0), {1.0f, 1.0f} });

			//front
			unit_cube.vertices.push_back({ Vector3(-0.5f, -0.5f, -0.5f), Vector3(0, 0, -1.0f), {0.0f, 0.0f} });
			unit_cube.vertices.push_back({ Vector3(0.5f, -0.5f, -0.5f), Vector3(0, 0, -1.0f), {1.0f, 0.0f} });
			unit_cube.vertices.push_back({ Vector3(-0.5f, 0.5f, -0.5f), Vector3(0, 0, -1.0f), {0.0f, 1.0f} });
			unit_cube.vertices.push_back({ Vector3(0.5f, 0.5f, -0.5f), Vector3(0, 0, -1.0f), {1.0f, 1.0f} });

			//left
			unit_cube.vertices.push_back({ Vector3(-0.5f, -0.5f, -0.5f), Vector3(-1.0f, 0, 0), {0.0f, 0.0f} });
			unit_cube.vertices.push_back({ Vector3(-0.5f, -0.5f, 0.5f), Vector3(-1.0f, 0, 0), {1.0f, 0.0f} });
			unit_cube.vertices.push_back({ Vector3(-0.5f, 0.5f, -0.5f), Vector3(-1.0f, 0, 0), {0.0f, 1.0f} });
			unit_cube.vertices.push_back({ Vector3(-0.5f, 0.5f, 0.5f), Vector3(-1.0f, 0, 0), {1.0f, 1.0f} });

			//right
			unit_cube.vertices.push_back({ Vector3(0.5f, -0.5f, -0.5f), Vector3(1.0f, 0, 0), {0.0f, 0.0f} });
			unit_cube.vertices.push_back({ Vector3(0.5f, -0.5f, 0.5f), Vector3(1.0f, 0, 0), {1.0f, 0.0f} });
			unit_cube.vertices.push_back({ Vector3(0.5f, 0.5f, -0.5f), Vector3(1.0f, 0, 0), {0.0f, 1.0f} });
			unit_cube.vertices.push_back({ Vector3(0.5f, 0.5f, 0.5f), Vector3(1.0f, 0, 0), {1.0f, 1.0f} });

			//back
			unit_cube.vertices.push_back({ Vector3(-0.5f, -0.5f, 0.5f), Vector3(0, 0, 1.0f), {0.0f, 0.0f} });
			unit_cube.vertices.push_back({ Vector3(-0.5f, 0.5f, 0.5f), Vector3(0, 0, 1.0f), {1.0f, 0.0f} });
			unit_cube.vertices.push_back({ Vector3(0.5f, 0.5f, 0.5f), Vector3(0, 0, 1.0f), {0.0f, 1.0f} });
			unit_cube.vertices.push_back({ Vector3(0.5f, -0.5f, 0.5f), Vector3(0, 0, 1.0f), {1.0f, 1.0f} });

			//top
			unit_cube.vertices.push_back({ Vector3(-0.5f, 0.5f, -0.5f), Vector3(0, 1.0f, 0), {0.0f, 0.0f} });
			unit_cube.vertices.push_back({ Vector3(-0.5f, 0.5f, 0.5f), Vector3(0, 1.0f, 0), {1.0f, 0.0f} });
			unit_cube.vertices.push_back({ Vector3(0.5f, 0.5f, -0.5f), Vector3(0, 1.0f, 0), {0.0f, 1.0f} });
			unit_cube.vertices.push_back({ Vector3(0.5f, 0.5f, 0.5f), Vector3(0, 1.0f, 0), {1.0f, 1.0f} });

			//bottom
			unit_cube.triangles.push_back({ 0, 2, 1 });
			unit_cube.triangles.push_back({ 1, 2, 3 });

			//front
			unit_cube.triangles.push_back({ 6, 5, 4 });
			unit_cube.triangles.push_back({ 6, 7, 5 });

			//left
			unit_cube.triangles.push_back({ 8, 9, 10 });
			unit_cube.triangles.push_back({ 10, 9, 11 });

			//right
			unit_cube.triangles.push_back({ 12, 14, 13 });
			unit_cube.triangles.push_back({ 14, 15, 13 });

			//back
			unit_cube.triangles.push_back({ 16, 18, 17 });
			unit_cube.triangles.push_back({ 16, 19, 18 });

			//top
			unit_cube.triangles.push_back({ 20, 21, 22 });
			unit_cube.triangles.push_back({ 21, 23, 22 });

			unit_cube.instances.push_back(Matrix4x4(
				1.0f, 0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f));

			unit_cube.instancesInv.push_back(Matrix4x4(
				1.0f, 0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f));

			unit_cube.init();

			return unit_cube;
		}

		void init() 
		{ 
			std::vector<Vector3> vertex_pos;
			vertex_pos.resize(vertices.size());
			for (int i = 0; i < vertices.size(); i++)
			{
				vertex_pos[i] = vertices[i].pos;
			}
			if (triangles.empty())
			{
				triangles.resize(vertices.size() / 3);
				for (int index_triangle = 0; index_triangle < triangles.size(); ++index_triangle)
				{
					triangles[index_triangle].indices[0] = index_triangle * 3;
					triangles[index_triangle].indices[1] = index_triangle * 3 + 1;
					triangles[index_triangle].indices[2] = index_triangle * 3 + 2;
				}
			}
			box.createBox(vertex_pos);
			octree.initialize(*this); 
		}
		bool valid() { return vertices.size() > 2; }
		const Vector3& getPos(int index_triangles, int index_vertex) const 
		{
			return vertices[triangles[index_triangles].indices[index_vertex]].pos;
		}

		void updateOctree()
		{
			octree.initialize(*this);
		}
	};
}
#pragma once
#include <stdint.h>
#include "Mesh.h"
#include "BoundingBox.h"
#include <string>
#include "../render/d3d_utils/VertexBuffer.h"
#include "../render/d3d_utils/IndexBuffer.h"

struct Model
{
	struct MeshRange
	{
		uint32_t vertexOffset; // offset in vertices
		uint32_t indexOffset; // offset in indices
		uint32_t vertexNum; // num of vertices
		uint32_t indexNum; // num of indices
	};


	std::string name;
	engine::BoundingBox box;
	std::vector<engine::Mesh> meshes;
	std::vector<std::string> albedo_texture_paths;
	std::vector<MeshRange> m_ranges; // where each mesh data is stored in m_vertices
	VertexBuffer<engine::Mesh::Vertex> m_vertices; // stores vertices of all meshes of this Model
	IndexBuffer m_indices; // stores vertex indices of all meshes of this Model

	VertexBuffer<engine::Mesh::Vertex>& vertexBuffer()
	{
		return m_vertices;
	}

	IndexBuffer& indexBuffer()
	{
		return m_indices;
	}

	void bindBuffers()
	{
		m_vertices.bind();
		if (m_indices.empty() == false)
		{
			m_indices.bind();
		}
	}

	void init()
	{
		uint32_t num_vertex = 0;
		uint32_t num_trianles = 0;
		for (const auto& mesh : meshes)
		{
			num_vertex += mesh.vertices.size();
			num_trianles += mesh.triangles.size();
		}

		m_ranges.resize(meshes.size());
		std::vector<engine::Mesh::Vertex> vertices;
		std::vector<uint32_t> indices;
		std::vector<Vector3> vertices_pos;

		vertices.resize(num_vertex);
		vertices_pos.resize(num_vertex);
		indices.resize(num_trianles * 3);

		uint32_t offset_vertex = 0;
		uint32_t offset_index = 0;

		for (size_t i = 0; i < meshes.size(); ++i)
		{
			m_ranges[i].indexOffset = offset_index;
			m_ranges[i].indexNum = meshes[i].triangles.size() * 3;
			m_ranges[i].vertexOffset = offset_vertex;
			m_ranges[i].vertexNum = meshes[i].vertices.size();

			

			for (size_t j = 0; j < meshes[i].triangles.size(); ++j)
			{
				indices[offset_index + 3 * j] = meshes[i].triangles[j].indices[0];
				indices[offset_index + 3 * j + 1] = meshes[i].triangles[j].indices[1];
				indices[offset_index + 3 * j + 2] = meshes[i].triangles[j].indices[2];
			}

			for (size_t j = 0; j < meshes[i].vertices.size(); ++j)
			{
				vertices[offset_vertex + j] = meshes[i].vertices[j];
				vertices_pos[offset_vertex + j] = meshes[i].vertices[j].pos;
			}

			offset_index += m_ranges[i].indexNum;
			offset_vertex += m_ranges[i].vertexNum;
		}


		m_vertices.init(vertices.size(), D3D11_USAGE_IMMUTABLE, vertices.data());
		if(indices.size() > 0)
			m_indices.init(indices.size(), D3D11_USAGE_IMMUTABLE, indices.data());

		box.createBox(vertices_pos);
	}

	void deinit()
	{
		m_vertices.deinit();
		m_indices.deinit();
	}
};


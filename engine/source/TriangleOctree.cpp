#include "TriangleOctree.h"
#include "Mesh.h"
#include "math/Ray.h"
#include "math/Intersection.h"
#include <algorithm>

namespace engine
{
	const int TriangleOctree::PREFFERED_TRIANGLE_COUNT = 36;
	const float TriangleOctree::MAX_STRETCHING_RATIO = 1.05f;

	void engine::TriangleOctree::initialize(const Mesh& mesh)
	{
		m_triangles.clear();
		m_triangles.shrink_to_fit();

		m_mesh = &mesh;
		m_children = nullptr;

		const Vector3 eps(1e-5f, 1e-5f, 1e-5f);
		m_box = m_initialBox = { mesh.box.min - eps, mesh.box.max + eps };

		for (uint32_t i = 0; i < mesh.triangles.size(); ++i)
		{
			const Vector3& V1 = mesh.getPos( i, 0);
			const Vector3& V2 = mesh.getPos( i, 1);
			const Vector3& V3 = mesh.getPos( i, 2);

			Vector3 P = (V1 + V2 + V3) / 3.f;

			bool inserted = addTriangle(i, V1, V2, V3, P);
		}

	}

	bool TriangleOctree::intersect(const math::Ray& ray, math::Intersection& outNearest) const
	{
		float t;
		if (!ray.intersects(m_box, t)) 
		{
			return false;
		}

		return intersectInternal(ray, outNearest);
	}

	bool engine::TriangleOctree::addTriangle(uint32_t triangleIndex, const Vector3& V1, const Vector3& V2, const Vector3& V3, const Vector3& center)
	{
		if (!m_initialBox.contains(center) ||
			!m_box.contains(V1) ||
			!m_box.contains(V2) ||
			!m_box.contains(V3))
		{
			return false;
		}

		if (m_children == nullptr)
		{
			if (m_triangles.size() < PREFFERED_TRIANGLE_COUNT)
			{
				m_triangles.emplace_back(triangleIndex);
				return true;
			}
			else
			{
				Vector3 C = (m_initialBox.min + m_initialBox.max) / 2.f;

				m_children.reset(new std::array<TriangleOctree, 8>());
				for (int i = 0; i < 8; ++i)
				{
					(*m_children)[i].initialize(*m_mesh, m_initialBox, C, i);
				}

				std::vector<uint32_t> newTriangles;

				for (uint32_t index : m_triangles)
				{
					const Vector3& P1 = m_mesh->getPos( index, 0);
					const Vector3& P2 = m_mesh->getPos( index, 1);
					const Vector3& P3 = m_mesh->getPos( index, 2);

					Vector3 P = (P1 + P2 + P3) / 3.f;

					int i = 0;
					for (; i < 8; ++i)
					{
						if ((*m_children)[i].addTriangle(index, P1, P2, P3, P))
							break;
					}

					if (i == 8)
						newTriangles.emplace_back(index);
				}

				m_triangles = std::move(newTriangles);
			}
		}

		int i = 0;
		for (; i < 8; ++i)
		{
			if ((*m_children)[i].addTriangle(triangleIndex, V1, V2, V3, center))
				break;
		}

		if (i == 8)
			m_triangles.emplace_back(triangleIndex);

		return true;

	}

	void TriangleOctree::initialize(const Mesh& mesh, const BoundingBox& parentBox, const Vector3& parentCenter, int octetIndex)
	{
		m_mesh = &mesh;
		m_children = nullptr;

		const float eps = 1e-5f;

		if (octetIndex % 2 == 0)
		{
			m_initialBox.min.ref_x() = parentBox.min.x();
			m_initialBox.max.ref_x() = parentCenter.x();
		}
		else
		{
			m_initialBox.min.ref_x() = parentCenter.x();
			m_initialBox.max.ref_x() = parentBox.max.x();
		}

		if (octetIndex % 4 < 2)
		{
			m_initialBox.min.ref_y() = parentBox.min.y();
			m_initialBox.max.ref_y() = parentCenter.y();
		}
		else
		{
			m_initialBox.min.ref_y() = parentCenter.y();
			m_initialBox.max.ref_y() = parentBox.max.y();
		}

		if (octetIndex < 4)
		{
			m_initialBox.min.ref_z() = parentBox.min.z();
			m_initialBox.max.ref_z() = parentCenter.z();
		}
		else
		{
			m_initialBox.min.ref_z() = parentCenter.z();
			m_initialBox.max.ref_z() = parentBox.max.z();
		}

		m_box = m_initialBox;
		Vector3 elongation = m_box.size() * (MAX_STRETCHING_RATIO - 1.f);

		if (octetIndex % 2 == 0) m_box.max.ref_x() += elongation.x();
		else m_box.min.ref_x() -= elongation.x();

		if (octetIndex % 4 < 2) m_box.max.ref_y() += elongation.y();
		else m_box.min.ref_y() -= elongation.y();

		if (octetIndex < 4) m_box.max.ref_z() += elongation.z();
		else m_box.min.ref_z() -= elongation.z();

	}

	bool TriangleOctree::intersectInternal(const math::Ray& ray, math::Intersection& nearest) const
	{
		
		{
			float t = nearest.t;
			if (!ray.intersects(m_box, t))
				return false;
		}

		bool found = false;

		for (uint32_t i = 0; i < m_triangles.size(); ++i)
		{
			const Vector3& V1 = m_mesh->getPos(m_triangles[i], 0);
			const Vector3& V2 = m_mesh->getPos(m_triangles[i], 1);
			const Vector3& V3 = m_mesh->getPos(m_triangles[i], 2);

			if (ray.intersects(V1, V2, V3, nearest))
			{
				found = true;
			}
		}
		if (!m_children) return found;

		struct OctantIntersection
		{
			int index;
			float t;
		};

		std::array<OctantIntersection, 8> boxIntersections;

		for (int i = 0; i < 8; ++i)
		{
			if ((*m_children)[i].m_box.contains(ray.origin))
			{
				boxIntersections[i].index = i;
				boxIntersections[i].t = 0.f;
			}
			else
			{
				float boxT = nearest.t;
				if (ray.intersects((*m_children)[i].m_box, boxT))
				{
					boxIntersections[i].index = i;
					boxIntersections[i].t = boxT;
				}
				else
				{
					boxIntersections[i].index = -1;
				}
			}
		}

		std::sort(boxIntersections.begin(), boxIntersections.end(),
			[](const OctantIntersection& A, const OctantIntersection& B) -> bool { return A.t < B.t; });

		for (int i = 0; i < 8; ++i)
		{
			if (boxIntersections[i].index < 0 || boxIntersections[i].t > nearest.t)
				continue;

			if ((*m_children)[boxIntersections[i].index].intersectInternal(ray, nearest))
			{
				found = true;
			}
		}

		return found;

	}

}
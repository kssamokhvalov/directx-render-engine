#pragma once
#include <memory>
#include <vector>
#include <array>
#include "BoundingBox.h"

namespace math
{
	struct Ray;
	struct Intersection;
}

namespace engine
{
	struct Mesh;

	class TriangleOctree
	{
	public:
		TriangleOctree() = default;

		const static int PREFFERED_TRIANGLE_COUNT;
		const static float MAX_STRETCHING_RATIO;

		void initialize(const Mesh& mesh);
		bool intersect(const math::Ray& ray, math::Intersection& outNearest) const;
	private:
		const Mesh* m_mesh = nullptr;
		std::vector<uint32_t> m_triangles;

		BoundingBox m_box;
		BoundingBox m_initialBox;

		std::unique_ptr<std::array<TriangleOctree, 8>> m_children;
		bool addTriangle(uint32_t triangleIndex, const Vector3& V1, const Vector3& V2, const Vector3& V3, const Vector3& center);
		void initialize(const Mesh& mesh, const BoundingBox& parentBox, const Vector3& parentCenter, int octetIndex);
		bool intersectInternal(const math::Ray& ray, math::Intersection& nearest) const;
	};
}
#pragma once
#include "../math/Vector3.h"
namespace math
{
	struct Ray;

	class IDragger
	{
	public:
		IDragger() = default;
		explicit IDragger(const Vector3& old_point, float t):
			m_old_point(old_point), m_t(t){}
		virtual void drag(const math::Ray& ray) = 0;
		void setOldPoint(const Vector3& old_point) { m_old_point = m_old_point; }
		float t() const { return m_t; }
	protected:
		float m_t;
		Vector3 m_old_point;
	};
}


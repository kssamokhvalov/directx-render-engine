#include "MatrixMover.h"
#include "../../render/systems_and_managers/MeshSystem.h"
#include "../../render/systems_and_managers/LightSystem.h"
#include "../math/Ray.h"

void math::MatrixMover::move(const math::Ray& ray)
{
	Vector3 offset = ray.getPoint(m_t);
	Vector3 diff = offset - m_old_point;
	Vector3 pos_model = TransformSystem::instance().getMatrix(m_matrices[0]).rowAsVector3(3);
	pos_model = pos_model + diff;
	Vector4  pos4_model(pos_model, 1.0f);
	for (const auto id_matrix : m_matrices)
	{
		auto matrix = TransformSystem::instance().getMatrix(id_matrix);
		matrix.setRow(3, pos4_model);
		TransformSystem::instance().changeMatrix(matrix, id_matrix);
	}
	m_old_point = offset;
	//TODO
	MeshSystem::instance().update();
	LightSystem::instance().update();
}

#pragma once
#include "../math/Vector3.h"
#include "../math/Matrix4x4.h"
#include "../../render/rendered_objects/ObjRef.h"
#include "../../utils/SolidVector.h"
#include <vector>

namespace math
{
	struct Ray;

	class MatrixMover
	{
	public:
		MatrixMover() = default;
		explicit MatrixMover(const Vector3& old_point, float t, internal::IntersectedType typeGroupe) :
			m_old_point(old_point), m_t(t), m_typeGroupe(typeGroupe){}
		void move(const math::Ray& ray);
		std::vector<engine::ID>& getMatricesID() { return m_matrices; }
		void setOldPoint(const Vector3& old_point) { m_old_point = m_old_point; }
		float t() const { return m_t; }
	protected:
		float m_t;
		Vector3 m_old_point;
		internal::IntersectedType m_typeGroupe = internal::IntersectedType::NUM;
		std::vector<engine::ID> m_matrices;
	};
}
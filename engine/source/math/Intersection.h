#pragma once
#include <limits>
#include <cmath>
#include "Vector3.h"

namespace math
{
	struct Intersection
	{
		float t;
		Vector3 pos;
		Vector3 normal;

		bool valid() const { return std::isfinite(t); }
		void reset() { t = std::numeric_limits<float>::infinity(); }
	};
}

#include "Matrix4x4.h"

Matrix4x4::Matrix4x4(float _11, float _12, float _13, float _14, float _21, float _22, float _23, float _24, float _31, float _32, float _33, float _34, float _41, float _42, float _43, float _44)
{
    m_matrix[0][0] = _11;
    m_matrix[0][1] = _12;
    m_matrix[0][2] = _13;
    m_matrix[0][3] = _14;

    m_matrix[1][0] = _21;
    m_matrix[1][1] = _22;
    m_matrix[1][2] = _23;
    m_matrix[1][3] = _24;

    m_matrix[2][0] = _31;
    m_matrix[2][1] = _32;
    m_matrix[2][2] = _33;
    m_matrix[2][3] = _34;

    m_matrix[3][0] = _41;
    m_matrix[3][1] = _42;
    m_matrix[3][2] = _43;
    m_matrix[3][3] = _44;
}

Matrix4x4 Matrix4x4::createZeroMatrix()
{
    Matrix4x4 ans_matrix;

    for (int i = 0; i < 4; i++) 
    {
        for (int j = 0; j < 4; j++) 
        {
            ans_matrix.m_matrix[i][j] = 0;
        }
    }

    return ans_matrix;
}

Matrix4x4 Matrix4x4::identity()
{
    Matrix4x4 ans_matrix = createZeroMatrix();

    ans_matrix.m_matrix[0][0] = 1.0f;
    ans_matrix.m_matrix[1][1] = 1.0f;
    ans_matrix.m_matrix[2][2] = 1.0f;
    ans_matrix.m_matrix[3][3] = 1.0f;

    return ans_matrix;
}

Matrix4x4 Matrix4x4::createFrustumPerspectiveMatrix(float angle_FOV, float aspect, float near_plate, float far_plate)
{
    Matrix4x4 ans_matrix = createZeroMatrix();
    ans_matrix.m_matrix[0][0] = aspect / tanf(angle_FOV / 2);
    ans_matrix.m_matrix[1][1] = 1 / tanf(angle_FOV / 2);
    ans_matrix.m_matrix[2][2] = near_plate / (near_plate - far_plate);
    ans_matrix.m_matrix[2][3] = 1.0f;
    ans_matrix.m_matrix[3][2] = -far_plate * near_plate / (near_plate - far_plate);

    return ans_matrix;
}

Matrix4x4 Matrix4x4::createFrustumOrthographicMatrix(float right, float left, float top, float bottom, float near, float far)
{
    Matrix4x4 ans_matrix = createZeroMatrix();

    ans_matrix.m_matrix[0][0] = 2.0f / (right - left);
    ans_matrix.m_matrix[1][1] = 2.0f / (top - bottom);
    ans_matrix.m_matrix[2][2] = 1.0f / (near - far);
    ans_matrix.m_matrix[3][0] = (right + left) / (left - right);
    ans_matrix.m_matrix[3][1] = (top + bottom) / (bottom - top);
    ans_matrix.m_matrix[3][2] = far / (far - near);
    ans_matrix.m_matrix[3][3] = 1.0f;

    return ans_matrix;
}

Matrix4x4 Matrix4x4::createFrustumProjectionMatrixInf(float angle_FOV, float aspect)
{
    Matrix4x4 ans_matrix = createZeroMatrix();

    ans_matrix.m_matrix[0][0] = tanf(angle_FOV / 2) * aspect;
    ans_matrix.m_matrix[1][1] = tanf(angle_FOV / 2);
    ans_matrix.m_matrix[2][3] = 1.0f;
    ans_matrix.m_matrix[3][2] = 1.0f;

    return ans_matrix;
}

Matrix4x4 Matrix4x4::createTransformationMatrix(const Quaternion& quaternion, const Vector3& T)
{
    Matrix4x4 ans;

    ans.m_matrix[0][0] = 1 - 2 * quaternion.j() * quaternion.j() - 2 * quaternion.k() * quaternion.k();
    ans.m_matrix[0][1] = 2 * quaternion.i() * quaternion.j() - 2 * quaternion.w() * quaternion.k();
    ans.m_matrix[0][2] = 2 * quaternion.i() * quaternion.k() + 2 * quaternion.w() * quaternion.j();
    ans.m_matrix[0][3] = 0;

    ans.m_matrix[1][0] = 2 * quaternion.i() * quaternion.j() + 2 * quaternion.w() * quaternion.k(); 
    ans.m_matrix[1][1] = 1 - 2 * quaternion.i() * quaternion.i() - 2 * quaternion.k() * quaternion.k();
    ans.m_matrix[1][2] = 2 * quaternion.j() * quaternion.k() - 2 * quaternion.w() * quaternion.i();
    ans.m_matrix[1][3] = 0;

    ans.m_matrix[2][0] = 2 * quaternion.i() * quaternion.k() - 2 * quaternion.w() * quaternion.j(); 
    ans.m_matrix[2][1] = 2 * quaternion.j() * quaternion.k() + 2 * quaternion.w() * quaternion.i(); 
    ans.m_matrix[2][2] = 1 - 2 * quaternion.i() * quaternion.i() - 2 * quaternion.j() * quaternion.j();
    ans.m_matrix[2][3] = 0;

    ans.m_matrix[3][0] = T.x();
    ans.m_matrix[3][1] = T.y();
    ans.m_matrix[3][2] = T.z();
    ans.m_matrix[3][3] = 1;

    return ans;
}

Matrix4x4 Matrix4x4::createRotationMatrix(const Vector3& dir)
{
    // Frisvad with z == -1 problem avoidance
    auto basisFromDir = [&](Vector3& right, Vector3& top, Vector3 dir)
    {
        float k = 1.0 / fmaxf(1.0 + dir.z(), 0.00001);
        float a = dir.y() * k;
        float b = dir.y() * a;
        float c = -dir.x() * a;
        right = Vector3(dir.z() + b, c, -dir.x());
        top = Vector3(c, 1.0 - b, -dir.y());
    };

    Matrix4x4 ans = createZeroMatrix();
    Vector3 right;
    Vector3 top;

    basisFromDir(right, top, dir);

    ans.setColom(0, right);
    ans.setColom(1, top);
    ans.setColom(2, dir);
    ans.m_matrix[3][3] = 1.0f;

    return ans;
}

Matrix4x4 Matrix4x4::createScaleMatrix(float scale)
{
    return Matrix4x4(scale, 0.0f, 0.0f, 0.0f,
        0.0f, scale, 0.0f, 0.0f,
        0.0f, 0.0f, scale, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f);
}

void Matrix4x4::scaleMatrix(Matrix4x4& matrix, float scale)
{
    Matrix4x4 scaleMatrix = Matrix4x4::createScaleMatrix(scale);
    Vector4 T = matrix.rowAsVector4(3);
    matrix.setRow(3, Vector4(0.0f, 0.0f, 0.0f, 1.0f));
    matrix = matrix * scaleMatrix;
    matrix.setRow(3, T);
}

Matrix4x4 Matrix4x4::createTransformationMatrix(const Quaternion& quaternion, const Vector3& T, float scale)
{
    Matrix4x4 ans;

    ans.m_matrix[0][0] = 1 - 2 * quaternion.j() * quaternion.j() - 2 * quaternion.k() * quaternion.k();
    ans.m_matrix[0][1] = 2 * quaternion.i() * quaternion.j() - 2 * quaternion.w() * quaternion.k();
    ans.m_matrix[0][2] = 2 * quaternion.i() * quaternion.k() + 2 * quaternion.w() * quaternion.j();
    ans.m_matrix[0][3] = 0;

    ans.m_matrix[1][0] = 2 * quaternion.i() * quaternion.j() + 2 * quaternion.w() * quaternion.k();
    ans.m_matrix[1][1] = 1 - 2 * quaternion.i() * quaternion.i() - 2 * quaternion.k() * quaternion.k();
    ans.m_matrix[1][2] = 2 * quaternion.j() * quaternion.k() - 2 * quaternion.w() * quaternion.i();
    ans.m_matrix[1][3] = 0;

    ans.m_matrix[2][0] = 2 * quaternion.i() * quaternion.k() - 2 * quaternion.w() * quaternion.j();
    ans.m_matrix[2][1] = 2 * quaternion.j() * quaternion.k() + 2 * quaternion.w() * quaternion.i();
    ans.m_matrix[2][2] = 1 - 2 * quaternion.i() * quaternion.i() - 2 * quaternion.j() * quaternion.j();
    ans.m_matrix[2][3] = 0;

    ans.m_matrix[3][3] = 1;

    ans = ans * Matrix4x4::createScaleMatrix(scale);

    ans.m_matrix[3][0] = T.x();
    ans.m_matrix[3][1] = T.y();
    ans.m_matrix[3][2] = T.z();
    

    return ans;
}

Vector4 Matrix4x4::multiply(const Vector4& vector, const Matrix4x4& matrix)
{
    float x = vector.x() * matrix.m_matrix[0][0] + vector.y() * matrix.m_matrix[1][0] + vector.z() * matrix.m_matrix[2][0] + vector.w() * matrix.m_matrix[3][0];
    float y = vector.x() * matrix.m_matrix[0][1] + vector.y() * matrix.m_matrix[1][1] + vector.z() * matrix.m_matrix[2][1] + vector.w() * matrix.m_matrix[3][1];
    float z = vector.x() * matrix.m_matrix[0][2] + vector.y() * matrix.m_matrix[1][2] + vector.z() * matrix.m_matrix[2][2] + vector.w() * matrix.m_matrix[3][2];
    float w = vector.x() * matrix.m_matrix[0][3] + vector.y() * matrix.m_matrix[1][3] + vector.z() * matrix.m_matrix[2][3] + vector.w() * matrix.m_matrix[3][3];

    return Vector4(x, y, z, w);
}

Vector4 Matrix4x4::multiply(const Matrix4x4& matrix, const Vector4& vector)
{
    float x = vector.x() * matrix.m_matrix[0][0] + vector.y() * matrix.m_matrix[0][1] + vector.z() * matrix.m_matrix[0][2] + vector.w() * matrix.m_matrix[0][3];
    float y = vector.x() * matrix.m_matrix[1][0] + vector.y() * matrix.m_matrix[1][1] + vector.z() * matrix.m_matrix[1][2] + vector.w() * matrix.m_matrix[1][3];
    float z = vector.x() * matrix.m_matrix[2][0] + vector.y() * matrix.m_matrix[2][1] + vector.z() * matrix.m_matrix[2][2] + vector.w() * matrix.m_matrix[2][3];
    float w = vector.x() * matrix.m_matrix[3][0] + vector.y() * matrix.m_matrix[3][1] + vector.z() * matrix.m_matrix[3][2] + vector.w() * matrix.m_matrix[3][3];

    return Vector4(x, y, z, w);
}


Matrix4x4 Matrix4x4::operator*(const Matrix4x4& input_matrix) const
{
    Matrix4x4 ans_matrix = Matrix4x4::createZeroMatrix();

    for (int i = 0; i < 4; i++) 
    {
        for (int j = 0; j < 4; j++) 
        {
            for (int k = 0; k < 4; k++) 
            {
                ans_matrix.m_matrix[j][i] += m_matrix[j][k] * input_matrix.m_matrix[k][i];
            }
        }
    }

    return ans_matrix;
}

Matrix4x4 Matrix4x4::operator/=(float value)
{
    for (int i = 0; i < 4; i++) 
    {
        for (int j = 0; j < 4; j++) 
        {
            m_matrix[i][j] /= value;
        }
    }

    return *this;
}

Vector3 Matrix4x4::colomAsVector3(int colom) const
{
    return Vector3(m_matrix[0][colom], m_matrix[1][colom], m_matrix[2][colom]);
}

Vector3 Matrix4x4::rowAsVector3(int row) const
{
    return Vector3(m_matrix[row][0], m_matrix[row][1], m_matrix[row][2]);
}

Vector4 Matrix4x4::rowAsVector4(int row) const
{
    return Vector4(m_matrix[row][0], m_matrix[row][1], m_matrix[row][2], m_matrix[row][3]);
}

void Matrix4x4::setRow(int rowIndex, const Vector4 row)
{
    m_matrix[rowIndex][0] = row.x();
    m_matrix[rowIndex][1] = row.y();
    m_matrix[rowIndex][2] = row.z();
    m_matrix[rowIndex][3] = row.w();
}

void Matrix4x4::setColom(int colomIndex, const Vector3 colom)
{
    m_matrix[0][colomIndex] = colom.x();
    m_matrix[1][colomIndex] = colom.y();
    m_matrix[2][colomIndex] = colom.z();
}

float Matrix4x4::getDeterminant() const
{
    return m_matrix[0][0] * m_matrix[1][1] * m_matrix[2][2] * m_matrix[3][3] + m_matrix[0][0] * m_matrix[2][1] * m_matrix[3][2] * m_matrix[1][3]
        + m_matrix[0][0] * m_matrix[3][1] * m_matrix[1][2] * m_matrix[2][3] - m_matrix[0][0] * m_matrix[3][1] * m_matrix[2][2] * m_matrix[1][3]
        - m_matrix[0][0] * m_matrix[2][1] * m_matrix[1][2] * m_matrix[3][3] - m_matrix[0][0] * m_matrix[1][1] * m_matrix[3][2] * m_matrix[2][3]
        - m_matrix[1][0] * m_matrix[0][1] * m_matrix[2][2] * m_matrix[3][3] - m_matrix[2][0] * m_matrix[0][1] * m_matrix[3][2] * m_matrix[1][3]
        - m_matrix[3][0] * m_matrix[0][1] * m_matrix[1][2] * m_matrix[2][3] + m_matrix[3][0] * m_matrix[0][1] * m_matrix[2][2] * m_matrix[1][3]
        + m_matrix[2][0] * m_matrix[0][1] * m_matrix[1][2] * m_matrix[3][3] + m_matrix[1][0] * m_matrix[0][1] * m_matrix[3][2] * m_matrix[2][3]
        + m_matrix[1][0] * m_matrix[2][1] * m_matrix[0][2] * m_matrix[3][3] + m_matrix[2][0] * m_matrix[3][1] * m_matrix[0][2] * m_matrix[1][3]
        + m_matrix[3][0] * m_matrix[1][1] * m_matrix[0][2] * m_matrix[2][3] - m_matrix[3][0] * m_matrix[2][1] * m_matrix[0][2] * m_matrix[1][3]
        - m_matrix[2][0] * m_matrix[1][1] * m_matrix[0][2] * m_matrix[3][3] - m_matrix[1][0] * m_matrix[3][1] * m_matrix[0][2] * m_matrix[2][3]
        - m_matrix[1][0] * m_matrix[2][1] * m_matrix[3][2] * m_matrix[0][3] - m_matrix[2][0] * m_matrix[3][1] * m_matrix[1][2] * m_matrix[0][3]
        - m_matrix[3][0] * m_matrix[1][1] * m_matrix[2][2] * m_matrix[0][3] + m_matrix[3][0] * m_matrix[2][1] * m_matrix[1][2] * m_matrix[0][3]
        + m_matrix[2][0] * m_matrix[1][1] * m_matrix[3][2] * m_matrix[0][3] + m_matrix[1][0] * m_matrix[3][1] * m_matrix[2][2] * m_matrix[0][3];
}

void Matrix4x4::toInverseMatrix()
{
    Matrix4x4 ans_matrix;

    ans_matrix.m_matrix[0][0] = m_matrix[1][1] * m_matrix[2][2] * m_matrix[3][3] + m_matrix[2][1] * m_matrix[3][2] * m_matrix[1][3]
        + m_matrix[3][1] * m_matrix[1][2] * m_matrix[2][3] - m_matrix[3][1] * m_matrix[2][2] * m_matrix[1][3]
        - m_matrix[2][1] * m_matrix[1][2] * m_matrix[3][3] - m_matrix[1][1] * m_matrix[3][2] * m_matrix[2][3];

    ans_matrix.m_matrix[0][1] = -m_matrix[0][1] * m_matrix[2][2] * m_matrix[3][3] - m_matrix[2][1] * m_matrix[3][2] * m_matrix[0][3]
        - m_matrix[3][1] * m_matrix[0][2] * m_matrix[2][3] + m_matrix[3][1] * m_matrix[2][2] * m_matrix[0][3]
        + m_matrix[2][1] * m_matrix[0][2] * m_matrix[3][3] + m_matrix[0][1] * m_matrix[3][2] * m_matrix[2][3];

    ans_matrix.m_matrix[0][2] = m_matrix[0][1] * m_matrix[1][2] * m_matrix[3][3] + m_matrix[1][1] * m_matrix[3][2] * m_matrix[0][3]
        + m_matrix[3][1] * m_matrix[0][2] * m_matrix[1][3] - m_matrix[3][1] * m_matrix[1][2] * m_matrix[0][3]
        - m_matrix[1][1] * m_matrix[0][2] * m_matrix[3][3] - m_matrix[0][1] * m_matrix[3][2] * m_matrix[1][3];

    ans_matrix.m_matrix[0][3] = -m_matrix[0][1] * m_matrix[1][2] * m_matrix[2][3] - m_matrix[1][1] * m_matrix[2][2] * m_matrix[0][3]
        - m_matrix[2][1] * m_matrix[0][2] * m_matrix[1][3] + m_matrix[2][1] * m_matrix[1][2] * m_matrix[0][3]
        + m_matrix[1][1] * m_matrix[0][2] * m_matrix[2][3] + m_matrix[0][1] * m_matrix[2][2] * m_matrix[1][3];



    ans_matrix.m_matrix[1][0] = -m_matrix[1][0] * m_matrix[2][2] * m_matrix[3][3] - m_matrix[2][0] * m_matrix[3][2] * m_matrix[1][3]
        - m_matrix[3][0] * m_matrix[1][2] * m_matrix[2][3] + m_matrix[3][0] * m_matrix[2][2] * m_matrix[1][3]
        + m_matrix[2][0] * m_matrix[1][2] * m_matrix[3][3] + m_matrix[1][0] * m_matrix[3][2] * m_matrix[2][3];

    ans_matrix.m_matrix[1][1] = m_matrix[0][0] * m_matrix[2][2] * m_matrix[3][3] + m_matrix[2][0] * m_matrix[3][2] * m_matrix[0][3]
        + m_matrix[3][0] * m_matrix[0][2] * m_matrix[2][3] - m_matrix[3][0] * m_matrix[2][2] * m_matrix[0][3]
        - m_matrix[2][0] * m_matrix[0][2] * m_matrix[3][3] - m_matrix[0][0] * m_matrix[3][2] * m_matrix[2][3];

    ans_matrix.m_matrix[1][2] = -m_matrix[0][0] * m_matrix[1][2] * m_matrix[3][3] - m_matrix[1][0] * m_matrix[3][2] * m_matrix[0][3]
        - m_matrix[3][0] * m_matrix[0][2] * m_matrix[1][3] + m_matrix[3][0] * m_matrix[1][2] * m_matrix[0][3]
        + m_matrix[1][0] * m_matrix[0][2] * m_matrix[3][3] + m_matrix[0][0] * m_matrix[3][2] * m_matrix[1][3];

    ans_matrix.m_matrix[1][3] = m_matrix[0][0] * m_matrix[1][2] * m_matrix[2][3] + m_matrix[1][0] * m_matrix[2][2] * m_matrix[0][3]
        + m_matrix[2][0] * m_matrix[0][2] * m_matrix[1][3] - m_matrix[2][0] * m_matrix[1][2] * m_matrix[0][3]
        - m_matrix[1][0] * m_matrix[0][2] * m_matrix[2][3] - m_matrix[0][0] * m_matrix[2][2] * m_matrix[1][3];



    ans_matrix.m_matrix[2][0] = m_matrix[1][0] * m_matrix[2][1] * m_matrix[3][3] + m_matrix[2][0] * m_matrix[3][1] * m_matrix[1][3]
        + m_matrix[3][0] * m_matrix[1][1] * m_matrix[2][3] - m_matrix[3][0] * m_matrix[2][1] * m_matrix[1][3]
        - m_matrix[2][0] * m_matrix[1][1] * m_matrix[3][3] - m_matrix[1][0] * m_matrix[3][1] * m_matrix[2][3];

    ans_matrix.m_matrix[2][1] = -m_matrix[0][0] * m_matrix[2][1] * m_matrix[3][3] - m_matrix[2][0] * m_matrix[3][1] * m_matrix[0][3]
        - m_matrix[3][0] * m_matrix[0][1] * m_matrix[2][3] + m_matrix[3][0] * m_matrix[2][1] * m_matrix[0][3]
        + m_matrix[2][0] * m_matrix[0][1] * m_matrix[3][3] + m_matrix[0][0] * m_matrix[3][1] * m_matrix[2][3];

    ans_matrix.m_matrix[2][2] = m_matrix[0][0] * m_matrix[1][1] * m_matrix[3][3] + m_matrix[1][0] * m_matrix[3][1] * m_matrix[0][3]
        + m_matrix[3][0] * m_matrix[0][1] * m_matrix[1][3] - m_matrix[3][0] * m_matrix[1][1] * m_matrix[0][3]
        - m_matrix[1][0] * m_matrix[0][1] * m_matrix[3][3] - m_matrix[0][0] * m_matrix[3][1] * m_matrix[1][3];

    ans_matrix.m_matrix[2][3] = -m_matrix[0][0] * m_matrix[1][1] * m_matrix[2][3] - m_matrix[1][0] * m_matrix[2][1] * m_matrix[0][3]
        - m_matrix[2][0] * m_matrix[0][1] * m_matrix[1][3] + m_matrix[2][0] * m_matrix[1][1] * m_matrix[0][3]
        + m_matrix[1][0] * m_matrix[0][1] * m_matrix[2][3] + m_matrix[0][0] * m_matrix[2][1] * m_matrix[1][3];



    ans_matrix.m_matrix[3][0] = -m_matrix[1][0] * m_matrix[2][1] * m_matrix[3][2] - m_matrix[2][0] * m_matrix[3][1] * m_matrix[1][2]
        - m_matrix[3][0] * m_matrix[1][1] * m_matrix[2][2] + m_matrix[3][0] * m_matrix[2][1] * m_matrix[1][2]
        + m_matrix[2][0] * m_matrix[1][1] * m_matrix[3][2] + m_matrix[1][0] * m_matrix[3][1] * m_matrix[2][2];

    ans_matrix.m_matrix[3][1] = m_matrix[0][0] * m_matrix[2][1] * m_matrix[3][2] + m_matrix[2][0] * m_matrix[3][1] * m_matrix[0][2]
        + m_matrix[3][0] * m_matrix[0][1] * m_matrix[2][2] - m_matrix[3][0] * m_matrix[2][1] * m_matrix[0][2]
        - m_matrix[2][0] * m_matrix[0][1] * m_matrix[3][2] - m_matrix[0][0] * m_matrix[3][1] * m_matrix[2][2];

    ans_matrix.m_matrix[3][2] = -m_matrix[0][0] * m_matrix[1][1] * m_matrix[3][2] - m_matrix[1][0] * m_matrix[3][1] * m_matrix[0][2]
        - m_matrix[3][0] * m_matrix[0][1] * m_matrix[1][2] + m_matrix[3][0] * m_matrix[1][1] * m_matrix[0][2]
        + m_matrix[1][0] * m_matrix[0][1] * m_matrix[3][2] + m_matrix[0][0] * m_matrix[3][1] * m_matrix[1][2];

    ans_matrix.m_matrix[3][3] = m_matrix[0][0] * m_matrix[1][1] * m_matrix[2][2] + m_matrix[1][0] * m_matrix[2][1] * m_matrix[0][2]
        + m_matrix[2][0] * m_matrix[0][1] * m_matrix[1][2] - m_matrix[2][0] * m_matrix[1][1] * m_matrix[0][2]
        - m_matrix[1][0] * m_matrix[0][1] * m_matrix[2][2] - m_matrix[0][0] * m_matrix[2][1] * m_matrix[1][2];

    ans_matrix /= getDeterminant();

    *this = ans_matrix;
}

Matrix4x4 Matrix4x4::getInversedMatrix() const
{
    Matrix4x4 ans_matrix = *this;
    ans_matrix.toInverseMatrix();

    return ans_matrix;
}

void Matrix4x4::invertOrthonormal(const Matrix4x4& matrix)
{
    Vector3 pos = matrix.rowAsVector3(3);
    m_matrix[1][0] = matrix.m_matrix[0][1];
    m_matrix[2][0] = matrix.m_matrix[0][2];
    m_matrix[2][1] = matrix.m_matrix[1][2];

    m_matrix[0][1] = matrix.m_matrix[1][0];
    m_matrix[0][2] = matrix.m_matrix[2][0];
    m_matrix[1][2] = matrix.m_matrix[2][1];

    m_matrix[0][0] = matrix.m_matrix[0][0];
    m_matrix[1][1] = matrix.m_matrix[1][1];
    m_matrix[2][2] = matrix.m_matrix[2][2];

    m_matrix[0][3] = 0;
    m_matrix[1][3] = 0;
    m_matrix[2][3] = 0;
    m_matrix[3][3] = 1;

    pos = matrix.colomAsVector3(0) * (-pos.x())
        + matrix.colomAsVector3(1) * (-pos.y())
        + matrix.colomAsVector3(2) * (-pos.z());

    m_matrix[3][0] = pos.x();
    m_matrix[3][1] = pos.y();
    m_matrix[3][2] = pos.z();

}



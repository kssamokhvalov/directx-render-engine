#pragma once
#include "Vector4.h"
#include "Quaternion.h"

class Matrix4x4
{
public:
	Matrix4x4() = default;
	Matrix4x4(float _11, float _12, float _13, float _14,
		float _21, float _22, float _23, float _24,
		float _31, float _32, float _33, float _34,
		float _41, float _42, float _43, float _44);

	static Matrix4x4 createZeroMatrix();
	static Matrix4x4 identity();
	static Matrix4x4 createFrustumPerspectiveMatrix(float angle_FOV, float aspect, float near_plate, float far_plate);
	static Matrix4x4 createFrustumOrthographicMatrix(float right, float left, float top, float bottom, float near, float far);
	static Matrix4x4 createFrustumProjectionMatrixInf(float angle_FOV, float aspect);
	static Matrix4x4 createTransformationMatrix(const Quaternion& quaternion, const Vector3& T);
	static Matrix4x4 createRotationMatrix(const Vector3& dir);
	static Matrix4x4 createScaleMatrix(float scale);
	static void scaleMatrix(Matrix4x4& matrix, float scale);
	static Matrix4x4 createTransformationMatrix(const Quaternion& quaternion, const Vector3& T, float scale);
	static Vector4 multiply(const Vector4& vector, const Matrix4x4& matrix);
	static Vector4 multiply(const Matrix4x4& matrix, const Vector4& vector);

	Matrix4x4 operator*(const Matrix4x4& input_matrix) const;
	Matrix4x4 operator/=(float value);

	Vector3 colomAsVector3(int colom) const;
	Vector3 rowAsVector3(int row) const;

	Vector4 rowAsVector4(int row) const;
	void setRow(int rowIndex, const Vector4 row) ;
	void setColom(int colomIndex, const Vector3 colom);
	float getDeterminant() const;
	void toInverseMatrix();
	Matrix4x4 getInversedMatrix() const;
	void invertOrthonormal(const Matrix4x4& matrix);

private:
	float m_matrix[4][4];
};


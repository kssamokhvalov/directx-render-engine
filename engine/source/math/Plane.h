#pragma once
#include "Vector3.h"

namespace math
{
	class Plane
	{
	public:
		Plane() = default;
		Plane(const Vector3& normal, const Vector3& p0) :
			m_normal(normal), m_p0(p0) {}

		const Vector3& normal() const { return m_normal; }
		const Vector3& p0() const { return m_p0; }
		float D() const { return m_D; }
	protected:
		Vector3 m_normal;
		Vector3 m_p0;
		float m_D;
	};
}

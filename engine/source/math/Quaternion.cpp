#include"Quaternion.h"

Quaternion::Quaternion(const Vector3& A, float angle_in_radians)
{
	m_w = cosf(angle_in_radians / 2);
	m_i = A.x() * sinf(angle_in_radians / 2);
	m_j = A.y() * sinf(angle_in_radians / 2);
	m_k = A.z() * sinf(angle_in_radians / 2);
}

void Quaternion::operator=(const Quaternion& quaterion)
{
	m_w = quaterion.m_w;
	m_i = quaterion.m_i;
	m_j = quaterion.m_j;
	m_k = quaterion.m_k;
}

Quaternion Quaternion::operator+(const Quaternion& quaterion) const
{
	return Quaternion(m_w + quaterion.m_w, m_i + quaterion.m_i, m_j + quaterion.m_j, m_k + quaterion.m_k);
}

Quaternion Quaternion::operator+(float value) const
{
	return Quaternion(m_w + value, m_i + value, m_j + value, m_k + value);
}

Quaternion& Quaternion::operator+=(const Quaternion& quaterion)
{
	m_w + quaterion.m_w;
	m_i + quaterion.m_i;
	m_j + quaterion.m_j;
	m_k + quaterion.m_k;
	return *this;
}

Quaternion& Quaternion::operator+=(float value)
{
	m_w + value;
	m_i + value;
	m_j + value;
	m_k + value;
	return *this;
}

Quaternion Quaternion::operator-(const Quaternion& quaterion) const
{
	return Quaternion(m_w - quaterion.m_w, m_i - quaterion.m_i, m_j - quaterion.m_j, m_k - quaterion.m_k);
}

Quaternion Quaternion::operator-(float value) const
{
	return Quaternion(m_w - value, m_i - value, m_j - value, m_k - value);
}

Quaternion& Quaternion::operator-=(const Quaternion& quaterion)
{
	m_w - quaterion.m_w;
	m_i - quaterion.m_i;
	m_j - quaterion.m_j;
	m_k - quaterion.m_k;
	return *this;
}

Quaternion& Quaternion::operator-=(float value)
{
	m_w - value;
	m_i - value;
	m_j - value;
	m_k - value;
	return *this;
}

Quaternion Quaternion::operator*(const Quaternion& quaterion) const
{
	return Quaternion(m_w * quaterion.m_w - m_i * quaterion.m_i - m_j * quaterion.m_j - m_k * quaterion.m_k,
		m_w * quaterion.m_i + m_i * quaterion.m_w + m_j * quaterion.m_k - m_k * quaterion.m_j,
		m_w * quaterion.m_j - m_i * quaterion.m_k + m_j * quaterion.m_w + m_k * quaterion.m_i,
		m_w * quaterion.m_k + m_i * quaterion.m_j - m_j * quaterion.m_i + m_k * quaterion.m_w);
}

Quaternion Quaternion::operator*(float value) const
{
	return Quaternion(m_w * value, m_i * value, m_j * value, m_k * value);
}

Quaternion& Quaternion::operator*=(const Quaternion& quaterion)
{
	float w = m_w * quaterion.m_w - m_i * quaterion.m_i - m_j * quaterion.m_j - m_k * quaterion.m_k;
	float i = m_w * quaterion.m_i + m_i * quaterion.m_w + m_j * quaterion.m_k - m_k * quaterion.m_j;
	float j = m_w * quaterion.m_j - m_i * quaterion.m_k + m_j * quaterion.m_w + m_k * quaterion.m_i;
	float k = m_w * quaterion.m_k + m_i * quaterion.m_j - m_j * quaterion.m_i + m_k * quaterion.m_w;

	m_w = w;
	m_i = i;
	m_j = j;
	m_k = k;

	return *this;
}

Quaternion& Quaternion::operator*=(float value)
{
	m_w* value;
	m_i* value;
	m_j* value;
	m_k* value;
	return *this;
}

Quaternion Quaternion::operator/(const Quaternion& quaterion) const
{
	return *this * toInverse(quaterion);
}

Quaternion Quaternion::operator/(float value) const
{
	return Quaternion(m_w / value, m_i / value, m_j / value, m_k / value);
}

Quaternion& Quaternion::operator/=(const Quaternion& quaterion)
{
	*this *= toInverse(quaterion);
	return *this;
}

Quaternion& Quaternion::operator/=(float value)
{
	m_w / value;
	m_i / value;
	m_j / value;
	m_k / value;
	return *this;
}

Quaternion Quaternion::toInverse(const Quaternion& quaterion)
{
	return toConjugate(quaterion) / quaterion.getSquareMagnitude();
}

Quaternion Quaternion::toConjugate(const Quaternion& quaterion)
{
	return Quaternion(quaterion.m_w, -quaterion.m_i, -quaterion.m_j, -quaterion.m_k);
}

void Quaternion::toInverse()
{
	*this = toConjugate(*this) / getSquareMagnitude();
}

void Quaternion::toConjugate()
{
	m_i = -m_i;
	m_j = -m_j;
	m_k = -m_k;
}

float Quaternion::getSquareMagnitude() const
{
	return m_w * m_w + m_i * m_i + m_j * m_j + m_k * m_k;
}

void Quaternion::normalize()
{
	*this /= sqrtf(getSquareMagnitude());
}

#pragma once
#include "Vector3.h"
class Quaternion
{
public:
	Quaternion() = default;
	explicit Quaternion(float w, float i, float j, float k):
		m_w(w),  m_i(i), m_j(j), m_k(k)
	{};
	explicit Quaternion(const Vector3& A, float angle_in_radians);

	void operator=(const Quaternion& quaterion);

	Quaternion operator+(const Quaternion& quaterion) const;
	Quaternion operator+(float value) const;
	Quaternion& operator+=(const Quaternion& quaterion);
	Quaternion& operator+=(float value);

	Quaternion operator-(const Quaternion& quaterion) const;
	Quaternion operator-(float value) const;
	Quaternion& operator-=(const Quaternion& quaterion);
	Quaternion& operator-=(float value);

	Quaternion operator*(const Quaternion& quaterion) const;
	Quaternion operator*(float value) const;
	Quaternion& operator*=(const Quaternion& quaterion);
	Quaternion& operator*=(float value);

	Quaternion operator/(const Quaternion& quaterion) const;
	Quaternion operator/(float value) const;
	Quaternion& operator/=(const Quaternion& quaterion);
	Quaternion& operator/=(float value);

	static Quaternion toInverse(const Quaternion& quaterion);
	static Quaternion toConjugate(const Quaternion& quaterion);

	void toInverse();
	void toConjugate();
	float getSquareMagnitude() const;
	void normalize();

	float w() const { return m_w; };
	float i() const { return m_i; };
	float j() const { return m_j; };
	float k() const { return m_k; };

private:

	float m_w;
	float m_i;
	float m_j;
	float m_k;
};


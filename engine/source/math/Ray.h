#pragma once

#include "Vector3.h"
#include "Sphere.h"
#include "Intersection.h"
#include "Plane.h"
#include "../BoundingBox.h"

namespace math
{
	struct Ray {

		Vector3 origin;
		Vector3 direction;

		Vector3 getPoint(float t) const { return (direction * t + origin); }

		bool intersects(const math::Sphere& sphere, math::Intersection& outNearest) const
		{
			Vector3 OC = origin - sphere.getPosition();
			float a = Vector3::Dot(direction, direction);
			float b = 2.0f * Vector3::Dot(OC, direction);
			float c = Vector3::Dot(OC, OC) - sphere.getRadius() * sphere.getRadius();
			float D = (b * b - 4.0f * a * c);

			if (D < 0.0f)
				return false;
			float t = (-b - sqrtf(D)) / (2.0f * a);

			if (t >= 0.0f)
			{
				outNearest.t = t;
				return true;
			}
			return false;
		}

		bool intersects(const math::Plane& plane, math::Intersection& outNearest) const
		{
			float t;
			float denom = Vector3::Dot(plane.normal(), direction);
			if (denom > -1e-6f) 
			{
				return false;
			}

			Vector3 p0l0 = plane.p0() - origin;
			t = Vector3::Dot(p0l0, plane.normal()) / denom;


			if (t >= 0.0f)
			{
				outNearest.t = t;
				return true;
			}
			return false;
		}

		bool intersects(const engine::BoundingBox& box, float& t) const
		{

			float tmin = (box.min.x() - origin.x()) / direction.x();
			float tmax = (box.max.x() - origin.x()) / direction.x();

			if (tmin > tmax) std::swap(tmin, tmax);

			float tymin = (box.min.y() - origin.y()) / direction.y();
			float tymax = (box.max.y() - origin.y()) / direction.y();

			if (tymin > tymax) std::swap(tymin, tymax);

			if ((tmin > tymax) || (tymin > tmax))
				return false;

			if (tymin > tmin)
				tmin = tymin;

			if (tymax < tmax)
				tmax = tymax;

			float tzmin = (box.min.z() - origin.z()) / direction.z();
			float tzmax = (box.max.z() - origin.z()) / direction.z();

			if (tzmin > tzmax) std::swap(tzmin, tzmax);

			if ((tmin > tzmax) || (tzmin > tmax))
				return false;

			if (tzmin > tmin)
				tmin = tzmin;

			if (tzmax < tmax)
				tmax = tzmax;
			t = tmin;
			return true;
		}

		bool intersects(const Vector3& V1, const Vector3& V2, const Vector3& V3, math::Intersection& outNearest) const
		{
			Vector3 unit_direction = direction.normalized();

			float t;

			Vector3 v0v1 = V2 - V1;
			Vector3 v0v2 = V3 - V1;

			Vector3 N = -Vector3::Cross(v0v1, v0v2); //left-handedness
			N.makeUnitVector();
			float area2 = N.lenght();

			float NdotRayUnitDirection = Vector3::Dot(N, unit_direction); // if direction short
			float NdotRayDirection = Vector3::Dot(N, direction);
			if (NdotRayUnitDirection < 0.001f)
				return false; 

			float d = -Vector3::Dot(N, V1);

			t = -(Vector3::Dot(N, origin) + d) / NdotRayDirection;

			if (t < 0.0f) return false;

			Vector3 P = getPoint(t);

			Vector3 edge0 = V2 - V1;
			Vector3 vp0 = P - V1;
			Vector3 C = -Vector3::Cross(edge0, vp0);
			if (Vector3::Dot(N, C) < 0.0f) return false;


			Vector3 edge1 = V3 - V2;
			Vector3 vp1 = P - V2;
			C = -Vector3::Cross(edge1, vp1);
			if (Vector3::Dot(N, C) < 0.0f)  return false;


			Vector3 edge2 = V1 - V3;
			Vector3 vp2 = P - V3;
			C = -Vector3::Cross(edge2, vp2);
			if (Vector3::Dot(N, C) < 0.0f) return false;

			if (t < outNearest.t) 
			{
				outNearest.t = t;
				outNearest.pos = P;
				N.makeUnitVector();
				outNearest.normal = N;

			}
			return true;
		}
	};
}
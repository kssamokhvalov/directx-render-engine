#pragma once

#include "..\math\Vector3.h"


namespace math
{
	class Sphere
	{
	public:
		Sphere() = default;
		explicit Sphere(const Vector3& position, float radius) :
			m_position(position), m_radius(radius) {}

		Vector3 getPosition() const { return m_position; }
		float getRadius() const { return m_radius; }

		void setPosition(const Vector3& position) { m_position = position; }
	protected:
		Vector3 m_position;
		float m_radius;
	};
}


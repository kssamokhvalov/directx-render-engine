#pragma once
#include "Vector3.h"

float Vector3::Dot(const Vector3& first_vector, const Vector3& second_vector)
{
	return first_vector.m_elements[0] * second_vector.m_elements[0] + first_vector.m_elements[1] * second_vector.m_elements[1] + first_vector.m_elements[2] * second_vector.m_elements[2];
}

Vector3 Vector3::Cross(const Vector3& first_vector, const Vector3& second_vector)
{
	return Vector3(first_vector.m_elements[1] * second_vector.m_elements[2] - first_vector.m_elements[2] * second_vector.m_elements[1],
		first_vector.m_elements[2] * second_vector.m_elements[0] - first_vector.m_elements[0] * second_vector.m_elements[2],
		first_vector.m_elements[0] * second_vector.m_elements[1] - first_vector.m_elements[1] * second_vector.m_elements[0]);
}

Vector3 Vector3::getHalfwayVector(const Vector3& first_vector, const Vector3& second_vector)
{
	return (first_vector + second_vector) / (first_vector + second_vector).lenght();
}

Vector3 Vector3::cross(const Vector3& second_vector)
{
	return Vector3(m_elements[1] * second_vector.m_elements[2] - m_elements[2] * second_vector.m_elements[1],
		m_elements[2] * second_vector.m_elements[0] - m_elements[0] * second_vector.m_elements[2],
		m_elements[0] * second_vector.m_elements[1] - m_elements[1] * second_vector.m_elements[0]);
}

Vector3 Vector3::operator+(const Vector3& v2) const
{
	Vector3 ans;

	ans.m_elements[0] = m_elements[0] + v2.m_elements[0];
	ans.m_elements[1] = m_elements[1] + v2.m_elements[1];
	ans.m_elements[2] = m_elements[2] + v2.m_elements[2];

	return ans;
}

Vector3 Vector3::operator-(const Vector3& v2) const
{
	Vector3 ans;

	ans.m_elements[0] = m_elements[0] - v2.m_elements[0];
	ans.m_elements[1] = m_elements[1] - v2.m_elements[1];
	ans.m_elements[2] = m_elements[2] - v2.m_elements[2];

	return ans;
}

Vector3 Vector3::operator-() const
{
	Vector3 ans;

	ans.m_elements[0] = -m_elements[0];
	ans.m_elements[1] = -m_elements[1];
	ans.m_elements[2] = -m_elements[2];

	return ans;
}

Vector3 Vector3::operator*(const Vector3& v2) const
{
	Vector3 ans;

	ans.m_elements[0] = m_elements[0] * v2.m_elements[0];
	ans.m_elements[1] = m_elements[1] * v2.m_elements[1];
	ans.m_elements[2] = m_elements[2] * v2.m_elements[2];

	return ans;
}

Vector3 Vector3::operator+(float t) const
{
	Vector3 ans;

	ans.m_elements[0] = m_elements[0] + t;
	ans.m_elements[1] = m_elements[1] + t;
	ans.m_elements[2] = m_elements[2] + t;

	return ans;
}

Vector3 Vector3::operator*(float t) const
{
	Vector3 ans;

	ans.m_elements[0] = m_elements[0] * t;
	ans.m_elements[1] = m_elements[1] * t;
	ans.m_elements[2] = m_elements[2] * t;

	return ans;
}

Vector3 Vector3::operator/(float t) const
{
	Vector3 ans;

	ans.m_elements[0] = m_elements[0] / t;
	ans.m_elements[1] = m_elements[1] / t;
	ans.m_elements[2] = m_elements[2] / t;

	return ans;
}

bool Vector3::operator<(const Vector3& v2) const
{
	return ((m_elements[0] < v2.m_elements[0]) && (m_elements[1] < v2.m_elements[1]) && (m_elements[2] < v2.m_elements[2]));
	
}

Vector3 Vector3::operator+=(const Vector3& v2)
{
	m_elements[0] += v2.m_elements[0];
	m_elements[1] += v2.m_elements[1];
	m_elements[2] += v2.m_elements[2];

	return *this;
}

Vector3 Vector3::operator-=(const Vector3& v2)
{
	m_elements[0] -= v2.m_elements[0];
	m_elements[1] -= v2.m_elements[1];
	m_elements[2] -= v2.m_elements[2];

	return *this;
}

Vector3 Vector3::operator*=(const Vector3& v2)
{
	m_elements[0] *= v2.m_elements[0];
	m_elements[1] *= v2.m_elements[1];
	m_elements[2] *= v2.m_elements[2];

	return *this;
}

Vector3 Vector3::operator*=(float t)
{
	m_elements[0] *= t;
	m_elements[1] *= t;
	m_elements[2] *= t;

	return *this;
}

Vector3 Vector3::operator/=(float t)
{
	m_elements[0] /= t;
	m_elements[1] /= t;
	m_elements[2] /= t;

	return *this;
}

Vector3 Vector3::normalized() const
{
	Vector3 ans = *this;
	return ans /= lenght();
}

void Vector3::makeUnitVector()
{
	*this /= lenght();
}

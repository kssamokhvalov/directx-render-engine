#pragma once

#include <math.h>

class Vector3 {
public:
	Vector3() = default;
	explicit Vector3(float x, float y, float z)
	{
		m_elements[0] = x;
		m_elements[1] = y;
		m_elements[2] = z;
	}
		
	Vector3(const Vector3& vec)
	{
		m_elements[0] = vec.m_elements[0];
		m_elements[1] = vec.m_elements[1];
		m_elements[2] = vec.m_elements[2];
	}

	static float Dot(const Vector3& first_vector, const Vector3& second_vector);
	static Vector3 Cross(const Vector3& first_vector, const Vector3& second_vector);
	static Vector3 getHalfwayVector(const Vector3& first_vector, const Vector3& second_vector);

	Vector3 cross(const Vector3& second_vector);

	float x() const { return m_elements[0]; }
	float y() const { return m_elements[1]; }
	float z() const { return m_elements[2]; }

	float& ref_x() { return m_elements[0]; }
	float& ref_y() { return m_elements[1]; }
	float& ref_z() { return m_elements[2]; }

	float& operator[](int i) { return m_elements[i]; }

	Vector3 operator+(const Vector3& v2) const;
	Vector3 operator-(const Vector3& v2) const;
	Vector3 operator-() const;
	Vector3 operator*(const Vector3& v2) const;
	Vector3 operator+(float t) const;
	Vector3 operator*(float t) const;
	Vector3 operator/(float t) const;

	bool operator<(const Vector3& v2) const;

	Vector3 operator+=(const Vector3& v2);
	Vector3 operator-=(const Vector3& v2);
	Vector3 operator*=(const Vector3& v2);
	Vector3 operator*=(float t);
	Vector3 operator/=(float t);

	Vector3 normalized() const;

	float lenght() const
	{
		return sqrtf(m_elements[0] * m_elements[0] + m_elements[1] * m_elements[1] + m_elements[2] * m_elements[2]);
	}
	void makeUnitVector();
private:
	float m_elements[3];
};
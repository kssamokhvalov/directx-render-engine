#include "Vector4.h"

Vector4::Vector4(const Vector3& vector3, float w)
{
	m_x = vector3.x();
	m_y = vector3.y();
	m_z = vector3.z();
	m_w = w;
}

Vector4::Vector4(float x, float y, float z, float w)
{
	m_x = x;
	m_y = y;
	m_z = z;
	m_w = w;
}

Vector4 Vector4::operator+(const Vector4& vector4) const
{
	return Vector4(m_x + vector4.m_x, m_y + vector4.m_y, m_z + vector4.m_z, m_w + vector4.m_w);
}

Vector4 Vector4::operator+(float value) const
{
	return Vector4(m_x + value, m_y + value, m_z + value, m_w + value);
}

Vector4& Vector4::operator+=(const Vector4& Vector4)
{
	m_x += Vector4.m_x;
	m_y += Vector4.m_y;
	m_z += Vector4.m_z;
	m_w += Vector4.m_w;

	return *this;
}

Vector4& Vector4::operator+=(float value)
{
	m_x += value;
	m_y += value;
	m_z += value;
	m_w += value;

	return *this;
}

Vector4 Vector4::operator-()
{
	return Vector4(-m_x, -m_y, -m_z, -m_w);
}

Vector4 Vector4::operator-(const Vector4& vector4) const
{
	return Vector4(m_x - vector4.m_x, m_y - vector4.m_y, m_z - vector4.m_z, m_w - vector4.m_w);
}

Vector4 Vector4::operator-(float value) const
{
	return Vector4(m_x - value, m_y - value, m_z - value, m_w - value);
}

Vector4& Vector4::operator-=(const Vector4& vector4)
{
	m_x -= vector4.m_x;
	m_y -= vector4.m_y;
	m_z -= vector4.m_z;
	m_w -= vector4.m_w;

	return *this;
}

Vector4& Vector4::operator-=(float value)
{
	m_x -= value;
	m_y -= value;
	m_z -= value;
	m_w -= value;

	return *this;
}


Vector4 Vector4::operator*(float value) const
{
	return Vector4(m_x * value, m_y * value, m_z * value, m_w * value);
}

Vector4& Vector4::operator*=(float value)
{
	m_x *= value;
	m_y *= value;
	m_z *= value;
	m_w *= value;

	return *this;
}

Vector4 Vector4::operator/(float value) const
{
	return Vector4(m_x / value, m_y / value, m_z / value, m_w / value);
}

Vector4& Vector4::operator/=(float value)
{
	m_x /= value;
	m_y /= value;
	m_z /= value;
	m_w /= value;

	return *this;
}

float Vector4::dot(const Vector4& firstVector4, const Vector4& secondVector4)
{
	return firstVector4.m_x * secondVector4.m_x + firstVector4.m_y * secondVector4.m_y + firstVector4.m_z * secondVector4.m_z + firstVector4.m_w * secondVector4.m_w;
}

float Vector4::getMagnitude() const
{
	return sqrtf(m_x * m_x + m_y * m_y + m_z * m_z + m_w * m_w);
}

float Vector4::getSquareMagnitude() const
{
	return m_x * m_x + m_y * m_y + m_z * m_z;
}

void Vector4::toNormalize()
{
	*this /= getMagnitude();
}
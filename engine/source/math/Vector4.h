#pragma once
#include "Vector3.h"

class Vector4
{
public:
	Vector4()  = default;
	explicit Vector4(const Vector3& vector3, float w);
	explicit Vector4(float x, float y, float z, float w);

	Vector4 operator+(const Vector4& vector4) const;
	Vector4 operator+(float value) const;
	Vector4& operator+=(const Vector4& vector4);
	Vector4& operator+=(float value);

	Vector4 operator-();
	Vector4 operator-(const Vector4& vector4) const;
	Vector4 operator-(float value) const;
	Vector4& operator-=(const Vector4& vector4);
	Vector4& operator-=(float value);
	Vector4 operator*(float value) const;
	Vector4& operator*=(float value);

	Vector4 operator/(float value) const;
	Vector4& operator/=(float value);

	static float dot(const Vector4& firstVector4, const Vector4& secondVector4);

	float x() const { return m_x; };
	float y() const { return m_y; };
	float z() const { return m_z; };
	float w() const { return m_w; };

	float& ref_x() { return m_x; };
	float& ref_y() { return m_y; };
	float& ref_z() { return m_z; };
	float& ref_w() { return m_w; };

	Vector3 xyz() { return Vector3(m_x, m_y, m_z); }
	Vector3 xyzProj() { return Vector3(m_x, m_y, m_z) / m_w; }
	float getMagnitude() const;
	float getSquareMagnitude() const;
	void toNormalize();

private:

	float m_x;
	float m_y;
	float m_z;
	float m_w;
};


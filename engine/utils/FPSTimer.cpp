#include "FPSTimer.h"

bool Timer::frameElapsed()
{
	namespace t = std::chrono;

	
	Time frame_duration = (Time::now() - m_start_time_frame);
	if (frame_duration > m_min_duration_frame)
	{
		m_rendering_time_frame = frame_duration;
		m_start_time_frame = Time::now();
		return true;
	}
	else
	{
		return false;
	}
}

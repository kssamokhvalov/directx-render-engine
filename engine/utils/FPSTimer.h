#pragma once
#include <chrono>
#include "time.h"

class Timer
{
public:
	Timer()
		: m_min_duration_frame(1000000 / 60), m_rendering_time_frame(0), m_start_time_frame(std::chrono::high_resolution_clock::now())
	{}

	bool frameElapsed();
	Time getRenderingTimeFrame() const { return m_rendering_time_frame; }
private:
	Time m_min_duration_frame; //microsecond
	Time m_rendering_time_frame; //microsecond
	Time m_start_time_frame;
};

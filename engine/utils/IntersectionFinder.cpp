#include "IntersectionFinder.h"
bool utility::intersects(const math::Ray& ray, const Matrix4x4& obj, internal::ObjRef& outRef, const engine::Mesh& mesh, math::Intersection& outNearest)
{
	Matrix4x4 objInv = obj.getInversedMatrix();
	math::Ray ray_obj{ Matrix4x4::multiply(Vector4(ray.origin, 1.0f), objInv).xyz(),
		Matrix4x4::multiply(Vector4(ray.direction, 0.0f), objInv).xyz() };

	math::Intersection inter_box;
	inter_box.t = outNearest.t;
	if (mesh.octree.intersect(ray_obj, inter_box))
	{
		if (inter_box.t < outNearest.t)
		{
			outNearest.t = inter_box.t;
			outNearest.normal = Matrix4x4::multiply(Vector4(inter_box.normal, 0.0f), obj).xyz();
			outNearest.pos = Matrix4x4::multiply(Vector4(ray_obj.getPoint(inter_box.t), 1), obj).xyz();
			return true;
		}
	}
	return false;
}
#pragma once
#include "../source/math/Ray.h"
#include "../render/systems_and_managers/TransformSystem.h"
#include "../render/rendered_objects/ObjRef.h"
#include "../source/math/Matrix4x4.h"
#include "../source/Mesh.h"

namespace utility
{
	bool intersects(const math::Ray& ray, const Matrix4x4& obj, internal::ObjRef& outRef, const engine::Mesh& mesh, math::Intersection& outNearest);

    template<typename TGroup>
    bool findIntersection(TGroup& group, const math::Ray& ray, internal::ObjRef& objref, math::Intersection& outNearest, internal::IntersectedType intersected_type)
    {
		bool intersection_exist = false;
		for (const auto& perModel : group.m_perModel)
		{
			for (uint32_t meshIndex = 0; meshIndex < perModel.perMesh.size(); ++meshIndex)
			{
				const engine::Mesh& mesh = perModel.model->meshes[meshIndex];
				const Matrix4x4& meshToModel = perModel.model->meshes[meshIndex].instances[0];

				for (const auto& perMaterial : perModel.perMesh[meshIndex].perMaterial)
				{
					auto& instances = perMaterial.instances;

					uint32_t numModelInstances = instances.sizeMax();
					for (uint32_t index = 0; index < numModelInstances; ++index)
					{
						if (instances.occupied(index) == false)
							continue;
						Matrix4x4 obj = meshToModel * TransformSystem::instance().getMatrix(instances[index].id_modelToWorld);
						if (intersects(ray, obj, objref, mesh, outNearest))
						{
							objref.id = instances[index].id_entity;
							objref.type = intersected_type;
							intersection_exist = true;
						}
					}
				}
			}
		}
		return intersection_exist;
    }

}
#include "time.h"

float Time::toSeconds() const
{
    return m_microseconds.count() / 1000000.0f;
}

float Time::toMilliseconds() const
{
    return m_microseconds.count() / 1000.0f;
}

Time Time::operator+(const Time& rhs) const
{
    Time ans;

    ans.m_microseconds = m_microseconds + rhs.m_microseconds;

    return ans;
}

Time& Time::operator+=(const Time& rhs)
{
    m_microseconds += rhs.m_microseconds;

    return *this;
}

Time Time::operator-(const Time& rhs) const
{
    Time ans;

    ans.m_microseconds = m_microseconds - rhs.m_microseconds;

    return ans;
}

Time& Time::operator-=(const Time& rhs)
{
    m_microseconds -= rhs.m_microseconds;

    return *this;
}

bool Time::operator<(const Time& rhs) const
{
    return m_microseconds < rhs.m_microseconds;
}

bool Time::operator>(const Time& rhs) const
{
    return m_microseconds > rhs.m_microseconds;
}

Time Time::now()
{
    Time ans(std::chrono::high_resolution_clock::now());
    return ans;
}

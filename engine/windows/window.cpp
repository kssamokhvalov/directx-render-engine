﻿#include "Window.h"
#include "../source/math/Ray.h"
#include "../utils/ParallelExecutor.h"


std::vector<Pixel>* const Window::getRenderBuffer()
{
	return &m_render_buffer;
}

int Window::createWindow(HINSTANCE appHandle, WNDPROC WinProc, int windowShowParams)
{
    
    m_lpbmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    m_lpbmi.bmiHeader.biWidth = m_width_render;
    m_lpbmi.bmiHeader.biHeight = m_height_render;
    m_lpbmi.bmiHeader.biPlanes = 1;
    m_lpbmi.bmiHeader.biBitCount = sizeof(Pixel) * 8;
    m_lpbmi.bmiHeader.biCompression = BI_RGB;

    const wchar_t CLASS_NAME[] = L"Sample Window Class";

    WNDCLASSEXW wcex = { };

    wcex.lpfnWndProc = WinProc;
    wcex.hInstance = appHandle;
    wcex.lpszClassName = CLASS_NAME;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);

    RegisterClassEx(&wcex);

    

    m_handle = CreateWindowEx(
        0,                              
        CLASS_NAME,                     
        L"Render",    
        WS_OVERLAPPEDWINDOW,            
        CW_USEDEFAULT, CW_USEDEFAULT, m_width_window, m_height_window,
        NULL,           
        NULL,       
        appHandle,  
        NULL        
    );


    if (m_handle == NULL)
    {
        return 0;
    }

    m_hDC = GetDC(m_handle);
    ShowWindow(m_handle, windowShowParams);

    initSwapchain();
    initBackBuffer();
	return 1;
}

void Window::releaseWindow()
{
    ReleaseDC(m_handle, m_hDC);

    m_swapchain.release();
    m_backbuffer.release();
    m_render_target.deinit();
}


void Window::resize(int width_window, int height_window)
{
	m_width_window = width_window;
	m_height_window = height_window;

    m_width_render = width_window * m_buffer_size_multiplier;
    m_height_render = height_window * m_buffer_size_multiplier;
    m_render_buffer.resize(m_width_render * m_height_render);

    m_lpbmi.bmiHeader.biWidth = m_width_render;
    m_lpbmi.bmiHeader.biHeight = m_height_render;


    if (m_swapchain.ptr() != nullptr)
    {
        initBackBuffer();
    }
}

void Window::flush()
{
    m_swapchain->Present(1, 0);
}

void Window::initSwapchain()
{
    DXGI_SWAP_CHAIN_DESC1 desc;

    // clear out the struct for use
    memset(&desc, 0, sizeof(DXGI_SWAP_CHAIN_DESC1));

    // fill the swap chain description struct
    desc.AlphaMode = DXGI_ALPHA_MODE::DXGI_ALPHA_MODE_UNSPECIFIED;
    desc.BufferCount = 2;
    desc.BufferUsage = DXGI_USAGE_BACK_BUFFER | DXGI_USAGE_RENDER_TARGET_OUTPUT;
    desc.Flags = 0;
    desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    desc.SampleDesc.Count = 1;                               // how many multisamples
    desc.SampleDesc.Quality = 0;                             // ???
    desc.Scaling = DXGI_SCALING_NONE;
    desc.Stereo = false;
    desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;

    HRESULT res = engine::s_factory->CreateSwapChainForHwnd(engine::s_device, m_handle, &desc, NULL, NULL, m_swapchain.reset());
    ALWAYS_ASSERT(res >= 0 && "CreateSwapChainForHwnd");

}

void Window::initBackBuffer()
{
    if (m_backbuffer.ptr() != nullptr)
    {
        m_backbuffer.release();
        m_render_target.deinit();
        m_swapchain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0);
    }

    HRESULT result = m_swapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)m_backbuffer.reset());
    ALWAYS_ASSERT(result >= 0);

    ID3D11Texture2D* pTextureInterface = 0;
    m_backbuffer->QueryInterface<ID3D11Texture2D>(&pTextureInterface);
    pTextureInterface->GetDesc(&m_backbufferDesc);
    pTextureInterface->Release();

    m_height_render = m_backbufferDesc.Height;
    m_width_render = m_backbufferDesc.Width;
    m_render_target.init(m_backbuffer);
}



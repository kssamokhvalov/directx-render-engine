#pragma once
#include <windows.h>
#include <vector>
#include "../render/primitives.h"
#include "../render/d3d_utils/RenderTarget.h"
#include "../render/DxRes.h"
#include "../render/d3d.h"

class Window
{
public:
    Window() :
        m_width_window(837),
        m_height_window(442), 
        m_buffer_size_multiplier(.5f),
        m_width_render(m_width_window * m_buffer_size_multiplier),
        m_height_render(m_height_window * m_buffer_size_multiplier)
    {
        m_render_buffer.resize(m_width_render * m_height_render);
    }

    explicit Window(int width_window, int height_window, const float& buffer_size_multiplier) :
        m_width_window(width_window), m_height_window(height_window), 
        m_buffer_size_multiplier(buffer_size_multiplier),
        m_width_render(m_width_window * m_buffer_size_multiplier),
        m_height_render(m_width_window * m_buffer_size_multiplier)
    {
        m_render_buffer.resize(m_width_render * m_height_render);
    }

    std::vector<Pixel>* const getRenderBuffer();

    int getWidthWindow() const { return m_width_window; }
    int getHeightWindow() const { return m_height_window; }
    int getWidthRender() const { return m_width_render; }
    int getHeightRender() const { return m_height_render; }
    int createWindow(HINSTANCE appHandle, WNDPROC WinProc, int windowShowParams);
    void releaseWindow();
    void resize(int width, int height);

    void flush();

    const HWND& getHWND() { return m_handle; }
    IDXGISwapChain1* const getSwapchain() { return m_swapchain.ptr(); }
    RenderTarget* getRenderTarget() { return &m_render_target; }
    const D3D11_TEXTURE2D_DESC& getBackbufferDesc() { return m_backbufferDesc; }
private:
    void initSwapchain();
    void initBackBuffer();

    float m_buffer_size_multiplier;

    int m_width_window;
    int m_height_window;

    int m_width_render;
    int m_height_render;

    std::vector<Pixel> m_render_buffer;
    BITMAPINFO m_lpbmi = {};

    HDC m_hDC;
    HWND m_handle = nullptr;

    engine::DxResPtr<IDXGISwapChain1> m_swapchain;
    engine::DxResPtr<ID3D11Texture2D> m_backbuffer;
    D3D11_TEXTURE2D_DESC m_backbufferDesc;

    RenderTarget m_render_target;
};


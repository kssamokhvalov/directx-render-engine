#include "application.h"

#include <imgui/imgui_impl_dx11.h>
#include <imgui/imgui_impl_win32.h>

const uint32_t ParallelExecutor::MAX_THREADS = std::max(1u, std::thread::hardware_concurrency());
const uint32_t ParallelExecutor::HALF_THREADS = std::max(1u, std::thread::hardware_concurrency() / 2);

int Application::init(HINSTANCE appHandle, WNDPROC WinProc, int windowShowParams)
{

	uint32_t numThreads = std::max(1u, std::max(ParallelExecutor::MAX_THREADS > 4u ? ParallelExecutor::MAX_THREADS - 4u : 1u, ParallelExecutor::HALF_THREADS));
	m_flash_light_attached = true;
	m_parallel_executor.reset(new ParallelExecutor(numThreads));
	m_window.createWindow(appHandle, WinProc, windowShowParams);
	//init imgui
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	m_io = &ImGui::GetIO(); (void)m_io;
	m_io->ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

	// Setup Dear ImGui style
	ImGui::StyleColorsDark();
	//ImGui::StyleColorsLight();

	// Setup Platform/Renderer backends
	ImGui_ImplWin32_Init(m_window.getHWND());
	ImGui_ImplDX11_Init(engine::s_device, engine::s_devcon);

	initScene();

	m_start_time = Time::now();

	return 0;
}

void Application::deinit()
{
	// Cleanup
	ImGui_ImplDX11_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();
	m_window.releaseWindow();
}

void Application::tick()
{
	if (m_timer.frameElapsed())
	{
		m_application_time = (Time::now() - m_start_time).toSeconds();
		RenderManager::instance().setTime(m_application_time);
		ImGui_ImplDX11_NewFrame();
		ImGui_ImplWin32_NewFrame();
		ImGui::NewFrame();

		{
			ImGui::Begin("DebugInfo");

			ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / m_io->Framerate, m_io->Framerate);

			ImGui::Checkbox("Diffuse reflection", &m_en_diffuse);
			ImGui::Checkbox("Specular reflection", &m_en_specular);
			ImGui::Checkbox("IBL", &m_en_IBL);
			ImGui::Checkbox("Deferred rendering", &m_deferred_rendering);
			ImGui::Checkbox("Roughness overwriting", &m_en_roughness_overwriting);
			
			ImGui::SliderFloat("Roughness", &m_roughnessSlider, 0.0f, 1.0f);
			ImGui::SliderInt("DepthBias", &m_depth_bias, -500.0f, 2.0f);
			ImGui::SliderFloat("SlopeScaledDepthBias", &m_slope_scaled_depth_bias, -10.0f, 0.0f);
			ImGui::SliderFloat("Quality Subpix", &m_qualitySubpix, 0.0f, 1.0f);
			ImGui::SliderFloat("Quality Edge Threshold", &m_qualityEdgeThreshold, 0.063f, 0.333f);
			ImGui::SliderFloat("Quality Edge Threshold Min", &m_qualityEdgeThresholdMin, 0.0f, 0.0833f);
			ImGui::Text("Fog");
			ImGui::SliderFloat("Coefficient of absorption", &m_coffAbsorption, 0.001f, 0.2f);
			ImGui::SliderFloat("Asymmetry parameter(g)", &m_asymmetryParameter, -1.0f, 1.0f);
			
			

			ImGui::End();
		}
		ImGui::Render();

		RenderManager::instance().updateSettigns(m_roughnessSlider, m_depth_bias, m_slope_scaled_depth_bias, m_en_diffuse, m_en_specular, m_en_IBL, m_en_roughness_overwriting,
			m_qualitySubpix, m_qualityEdgeThreshold, m_qualityEdgeThresholdMin, m_coffAbsorption, m_asymmetryParameter);

		processInput();																																							

		checkDissolutionAnimation();
		checkIncinerationAnimation();
		if(RenderManager::instance().getCamera()->needUpdatedMatrices())
			flashLightProcess();
		if(m_deferred_rendering == true)
			RenderManager::instance().deferredRender(m_window.getRenderTarget());
		else
			RenderManager::instance().render(m_window.getRenderTarget());

		ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
		m_window.flush();
	}
}

void Application::processInput()
{
	bool shift_pressed = m_buttons_pressed.contains(ButtonsEvents::SHIFT);
	float speed_cam_multiplyer = shift_pressed ? 5.0f : 1.0f;
	float duration_frame = m_timer.getRenderingTimeFrame().toSeconds();
	speed_cam_multiplyer *= duration_frame;

	for (const auto& button : m_buttons_pressed) {
		switch (button)
		{
		case LBUTTON_MOUSE:
		{
			float diff_x = m_diff_x_mouse * SPEED_ROTATE_CAMERA * duration_frame;
			float diff_y = m_diff_y_mouse * SPEED_ROTATE_CAMERA * duration_frame;

			RenderManager::instance().getCamera()->addRelativeAngles({ 0.0f, -diff_y, -diff_x });
			break;
		}
		case RBUTTON_MOUSE:
		case SPACE:
		case CTRL:
			break;
		case W:
			RenderManager::instance().getCamera()->addRelativeOffset(Vector3(0.f, 0.f, m_speed_camera * speed_cam_multiplyer));
			break;
		case A:
			RenderManager::instance().getCamera()->addRelativeOffset(Vector3(-m_speed_camera * speed_cam_multiplyer, 0.f, 0.f));
			break;
		case S:
			RenderManager::instance().getCamera()->addRelativeOffset(Vector3(0.f, 0.f, -m_speed_camera * speed_cam_multiplyer));
			break;
		case D:
			RenderManager::instance().getCamera()->addRelativeOffset(Vector3(m_speed_camera * speed_cam_multiplyer, 0.f, 0.f));
			break;
		case Q:
			RenderManager::instance().getCamera()->addWorldOffset(Vector3(0.f, -m_speed_camera * speed_cam_multiplyer, 0.f));
			break;
		case E:
			RenderManager::instance().getCamera()->addWorldOffset(Vector3(0.f, m_speed_camera * speed_cam_multiplyer, 0.f));
			break;
		case ADD:
			{
				float EV100 = RenderManager::instance().getPostPocess()->getEV100();
				RenderManager::instance().getPostPocess()->setEV100(EV100 + SPEED_CHANGING_EV100 * duration_frame);
			}
			break;
		case SUBTRACT:
			{
				float EV100 = RenderManager::instance().getPostPocess()->getEV100();
				RenderManager::instance().getPostPocess()->setEV100(EV100 - SPEED_CHANGING_EV100 * duration_frame);
			}
			break;
		default:
			break;
		}
	}
	//It is necessary that the dragging of objects takes after all manipulations with the camera
	if(m_buttons_pressed.contains(ButtonsEvents::RBUTTON_MOUSE))
	{
		if (m_dragger.get() != nullptr)
			{
				math::Ray ray_mouse = RenderManager::instance().getCamera()->getRayForPixelPos(m_pos_x_mouse, m_pos_y_mouse,
					m_window.getWidthWindow(),
					m_window.getHeightWindow());
				m_dragger->move(ray_mouse);
			}
	}
}

void Application::initScene()
{
	auto texture_sky = TextureManager::instance().getTexture(m_sky_cube_path);
	RenderManager::instance().getSkyRender()->setTextureSky(texture_sky);

	RenderManager::instance().setIrradiance(
		TextureManager::instance().getDiffuseIrradianCubeMap(m_sky_cube_path));
	RenderManager::instance().setReflection(
		TextureManager::instance().getSpecularIrradianCubeMap(m_sky_cube_path));
	RenderManager::instance().setReflectance(
		TextureManager::instance().getSpecularReflectanceCubeMap(m_sky_cube_path));
	RenderManager::instance().getVolumetricFog()->setNoise(
		TextureManager::instance().getSimplexNoise3D("assets/textures/PerlinNoise3D.dds"));

	RenderManager::instance().setIrradiance(
		TextureManager::instance().getDiffuseIrradianCubeMap(m_sky_cube_path));
	RenderManager::instance().setReflection(
		TextureManager::instance().getSpecularIrradianCubeMap(m_sky_cube_path));
	RenderManager::instance().setReflectance(
		TextureManager::instance().getSpecularReflectanceCubeMap(m_sky_cube_path));
	

	OpaqueGroup::Material brick = {
		TextureManager::instance().getTexture("assets/textures/brick/Brick_albedo.dds"),
		TextureManager::instance().getTexture("assets/textures/brick/Brick_normal.dds"),
		TextureManager::instance().getTexture("assets/textures/brick/Brick_roughness.dds"),
		TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
		{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.0f, 0.0f, 1.0f), 0.0f, 7}
	};

	OpaqueGroup::Material mud = {
		TextureManager::instance().getTexture("assets/textures/Mud/Mud_Albedo.dds"),
		TextureManager::instance().getTexture("assets/textures/Mud/Mud_Normal.dds"),
		TextureManager::instance().getTexture("assets/textures/Mud/Mud_Roughness.dds"),
		TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
		{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 7} //7
	};

	std::vector<OpaqueGroup::Material> horse = {
		{
			TextureManager::instance().getTexture("assets/models/KnightHorse/dds/Armor_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/KnightHorse/dds/Armor_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/KnightHorse/dds/Armor_Roughness.dds"),
			TextureManager::instance().getTexture("assets/models/KnightHorse/dds/Armor_Metallic.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 15}
		},
		{
			TextureManager::instance().getTexture("assets/models/KnightHorse/dds/Horse_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/KnightHorse/dds/Horse_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/KnightHorse/dds/Horse_Roughness.dds"),
			TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 7}
		},
		{
			TextureManager::instance().getTexture("assets/models/KnightHorse/dds/Tail_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/KnightHorse/dds/Tail_Normal.dds"),
			TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
			TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 3}
		}
	};

	std::vector<OpaqueGroup::Material> knight = {
		{
			TextureManager::instance().getTexture("assets/models/Knight/dds/Fur_BaseColor.dds"), //TODO problem with fur, maybe texture is not correct or fur should be rendered as a transparent/semitransparent object
			TextureManager::instance().getTexture("assets/models/Knight/dds/Fur_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Fur_Roughness.dds"),
			TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 7}
		},
		{
			TextureManager::instance().getTexture("assets/models/Knight/dds/Pants_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Pants_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Pants_Roughness.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Pants_Metallic.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 15}
		},
		{
			TextureManager::instance().getTexture("assets/models/Knight/dds/Torso_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Torso_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Torso_Roughness.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Torso_Metallic.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 15}
		},
		{
			TextureManager::instance().getTexture("assets/models/Knight/dds/Head_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Head_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Head_Roughness.dds"),
			TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 7}
		},
		{
			TextureManager::instance().getTexture("assets/models/Knight/dds/Eyes_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Eyes_Normal.dds"),
			TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
			TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 0.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 3}
		},
		{
			TextureManager::instance().getTexture("assets/models/Knight/dds/Helmet_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Helmet_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Helmet_Roughness.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Helmet_Metallic.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 15}
		},
		{
			TextureManager::instance().getTexture("assets/models/Knight/dds/Skirt_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Skirt_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Skirt_Roughness.dds"),
			TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 7}
		},
		{
			TextureManager::instance().getTexture("assets/models/Knight/dds/Cloak_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Cloak_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Cloak_Roughness.dds"),
			TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 7}
		},
		{
			TextureManager::instance().getTexture("assets/models/Knight/dds/Gloves_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Gloves_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Gloves_Roughness.dds"),
			TextureManager::instance().getTexture("assets/models/Knight/dds/Gloves_Metallic.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 15}
		}
	};

	std::vector<OpaqueGroup::Material> samurai = {
		{
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Sword_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Sword_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Sword_Roughness.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Sword_Metallic.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 15}
		},
		{
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Head_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Head_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Head_Roughness.dds"),
			TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 7}
		},
		{
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Eyes_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Eyes_Normal.dds"),
			TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
			TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 3}
		},
		{
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Helmet_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Helmet_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Helmet_Roughness.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Helmet_Metallic.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 15}
		},
		{
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Decor_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Decor_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Decor_Roughness.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Decor_Metallic.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 15}
		},
		{
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Pants_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Pants_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Pants_Roughness.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Pants_Metallic.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 15}
		},
		{
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Hands_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Hands_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Hands_Roughness.dds"),
			TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 7}
		},
		{
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Torso_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Torso_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Torso_Roughness.dds"),
			TextureManager::instance().getTexture("assets/models/Samurai/dds/Torso_Metallic.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 15}
		}
	};

	std::vector<OpaqueGroup::Material> tower = {
		{
			TextureManager::instance().getTexture("assets/models/EastTower/dds/CityWalls_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/EastTower/dds/CityWalls_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/EastTower/dds/CityWalls_Roughness.dds"),
			TextureManager::instance().getTexture("assets/models/EastTower/dds/CityWalls_Metallic.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 15}
		},
		{
			TextureManager::instance().getTexture("assets/models/EastTower/dds/Marble_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
			TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
			TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 1}
		},
		{
			TextureManager::instance().getTexture("assets/models/EastTower/dds/Trims_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/EastTower/dds/Trims_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/EastTower/dds/Trims_Roughness.dds"),
			TextureManager::instance().getTexture("assets/models/EastTower/dds/Trims_Metallic.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 15}
		},
		{
			TextureManager::instance().getTexture("assets/models/EastTower/dds/Statue_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/EastTower/dds/Statue_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/EastTower/dds/Statue_Roughness.dds"),
			TextureManager::instance().getTexture("assets/models/EastTower/dds/Statue_Metallic.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 15}
		},
		{
			TextureManager::instance().getTexture("assets/models/EastTower/dds/StoneWork_BaseColor.dds"),
			TextureManager::instance().getTexture("assets/models/EastTower/dds/StoneWork_Normal.dds"),
			TextureManager::instance().getTexture("assets/models/EastTower/dds/StoneWork_Roughness.dds"),
			TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
			{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 7}
		}
	};

	OpaqueGroup::Material wood = {
		TextureManager::instance().getTexture("assets/textures/wood/Wood_albedo.dds"),
		TextureManager::instance().getTexture("assets/textures/wood/Wood_normal.dds"),
		TextureManager::instance().getTexture("assets/textures/wood/Wood_roughness.dds"),
		TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
		{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 7}
	};

	OpaqueGroup::Material stones = {
		TextureManager::instance().getTexture("assets/textures/stones/Wall_Stone_albedo.dds"),
		TextureManager::instance().getTexture("assets/textures/stones/Wall_Stone_normal.dds"),
		TextureManager::instance().getTexture("assets/textures/stones/Wall_Stone_roughness.dds"),
		TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
		{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 7}
	};

	auto model = ModelManager::instance().getUnitCube();
	Matrix4x4 modelToWorld = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vector3(2.0f, -2.0f, 5.0f));
	MeshSystem::instance().addOpaqueInstance(model, stones, modelToWorld);



	modelToWorld = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vector3(0.0f, -2.0f, 5.0f));
	MeshSystem::instance().addOpaqueInstance(model, wood, modelToWorld);


	
	modelToWorld = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vector3(-4.0f, -2.0f, 5.0f));
	MeshSystem::instance().addOpaqueInstance(model, brick, modelToWorld);


	modelToWorld = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vector3(-2.0f, -2.0f, 5.0f));
	MeshSystem::instance().addOpaqueInstance(model, wood, modelToWorld);
	model = ModelManager::instance().getUnitCube();
	//modelToWorld = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vector3(0.0f, -26.0f, 0.0f), 40.0f);
	//MeshSystem::instance().addOpaqueInstance(model, mud, modelToWorld);

	//floor
	float size = 40.0f;
	int N = 10;
	for (int step_x = 0; step_x < N; step_x++)
	{
		for (int step_z = 0; step_z < N; step_z++)
		{
			float x = -size / 2 + step_x * size / N;
			float z = -size / 2 + step_z * size / N;
			modelToWorld = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vector3(x, -size / (2 * N) - 3.0f, z), size / N);
			MeshSystem::instance().addOpaqueInstance(model, mud, modelToWorld);
		}

	}

	addPointLightSource(Vector3(3.8f, -0.2f, 9.3f), Vector3(1.0f, 0.0f, 0.0f), 1000.0f, 0.05f);
	addPointLightSource(Vector3(5.0f, -0.2f, 9.0f), Vector3(0.0f, 1.0f, 0.0f), 1000.0f, 0.05f);
	addPointLightSource(Vector3(4.0f, -0.5f, 7.9f), Vector3(1.0f, 1.0f, 1.0f), 100.0f, 0.25f);
	addPointLightSource(Vector3(0.0f, 0.0f, 5.0f), Vector3(0.0f, 1.0f, 1.0f), 2000.0f, 0.125f);

	
	modelToWorld = Matrix4x4::createTransformationMatrix(Quaternion(0.7f, -0.7f, 0.0f, 0.0f), Vector3(2.0f, 4.0f, 5.0f), 0.1f);

	m_id_matrix_flash_light = TransformSystem::instance().addMatrix(modelToWorld);
	//LightSystem::instance().addSpotLight(modelToWorld, 10.0f, Vector3(0.0f, 0.7f, 0.7f), 20.0f);
	addSpotWithMaskLightSource(m_id_matrix_flash_light, Vector3(1.0f, 1.0f, 1.0f), 10000.0f, 15.0f);
	const float w4Moon = 0.00006f; // 0.5f degrees
	constexpr float angularDiameter4Moon = 0.5f * 3.14 / 180; // 0.5f degrees
	LightSystem::instance().addDirectionalLight(Vector3(-1.0f, -1.0f, 1.0f), 200000.0f, Vector3(1.0f, 1.0f, 1.0f), w4Moon, cosf(angularDiameter4Moon / 2));

	modelToWorld = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vector3(6.0f, -2.0f, 10.0f), 1.0f);
	model = ModelManager::instance().getModel("assets\\models\\EastTower\\EastTower.fbx");
	MeshSystem::instance().addOpaqueInstance(model, tower, modelToWorld);

	modelToWorld = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vector3(4.0f, -2.2f, 9.2f), 1.0f);
	model = ModelManager::instance().getModel("assets\\models\\Samurai\\Samurai.fbx");
	MeshSystem::instance().addOpaqueInstance(model, samurai, modelToWorld);
	
	modelToWorld = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vector3(4.0f, -2.0f, 5.0f));
	MeshSystem::instance().addHologramInstance(model, modelToWorld, { 1.0f, 0.0f, 0.5f });

	modelToWorld = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vector3(5.0f, -2.0f, 9.0f), 1.0f);
	model = ModelManager::instance().getModel("assets\\models\\KnightHorse\\KnightHorse.fbx");
	MeshSystem::instance().addOpaqueInstance(model, horse, modelToWorld);

	modelToWorld = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vector3(4.3f, -1.9f, 9.5f), 1.0f);
	model = ModelManager::instance().getModel("assets\\models\\Knight\\Knight.fbx");
	MeshSystem::instance().addOpaqueInstance(model, knight, modelToWorld);

	//TEST
	modelToWorld = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vector3(0.0f, -2.0f, 9.5f), 2.0f);
	engine::ID id_matrix = TransformSystem::instance().addMatrix(modelToWorld);
	MeshInstanceID entity;
	RenderManager::instance().getVolumetricFog()->add(VolumetricFog::Material{}, { id_matrix, 0 }, entity);


	//addEmitter(Vector3(6.0f, -2.5f, 4.0f), 1.0f / 5.0f, 1.0f, 14.0f, Vector4(0.1f, 0.1f, 0.1f, 0.0f), Vector3(1.0f, 0.5f, 0.0f));
}

void Application::updateDiffMouse(int x, int y)
{
	m_diff_x_mouse = (static_cast<float>(x) - m_pos_x_mouse_lmouse_press) / (m_window.getWidthWindow() / 2);
	m_diff_y_mouse = (static_cast<float>(y) - m_pos_y_mouse_lmouse_press) / (m_window.getHeightWindow() / 2);
}

void Application::flashLightProcess()
{
	if (m_flash_light_attached == true)
	{
		RenderManager::instance().updateCameraMatrices();
		Matrix4x4 matrix_flashlight = RenderManager::instance().getCamera()->getViewInv();
		Matrix4x4::scaleMatrix(matrix_flashlight, 0.25f);
		TransformSystem::instance().changeMatrix(matrix_flashlight, m_id_matrix_flash_light);
		MeshSystem::instance().update();
		LightSystem::instance().update();
	}
}

void Application::addPointLightSource(const Vector3& position, const Vector3& color, float I, float radius)
{
	Matrix4x4 lightToModel = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, .0f, .0f, .0f), Vector3(0.0f, 0.0f, 0.0f), 1.0);
	engine::ID id_matrix_local = TransformSystem::instance().addMatrix(lightToModel);

	Matrix4x4 modelToWorld = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, .0f, .0f, .0f), position, radius);
	engine::ID id_matrix = TransformSystem::instance().addMatrix(modelToWorld);
	Vector4 emission = Vector4(color, 1.0f) * I;
	LightSystem::instance().addPointLight(id_matrix, id_matrix_local, emission, radius);

	auto model = ModelManager::instance().getUnitSphere();
	MeshSystem::instance().addEmissiveInstance(model, id_matrix, emission);
}

void Application::addSpotLightSource(const Matrix4x4& modelToWorld, const Vector3& color, float I, float p)
{
	Matrix4x4 lightToModel = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, .0f, .0f, .0f), Vector3(0.0f, 0.0f, 0.0f), 1.0);
	engine::ID id_matrix_local = TransformSystem::instance().addMatrix(lightToModel);

	Vector4 Q(1.0f, 0.0f, 0.0f, 0.0f);
	engine::ID id_matrix = TransformSystem::instance().addMatrix(modelToWorld);
	float radius = Matrix4x4::multiply(Q, modelToWorld).getMagnitude();
	Vector4 emission = Vector4(color, 1.0f) * I;
	LightSystem::instance().addSpotLight(id_matrix, id_matrix_local, p, emission, radius);
	auto model = ModelManager::instance().getUnitSphere();
	MeshSystem::instance().addEmissiveInstance(model, id_matrix, emission);
}

void Application::addSpotWithMaskLightSource(const Matrix4x4& modelToWorld, const Vector3& color, float I, float p)
{
	Matrix4x4 lightToModel = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, .0f, .0f, .0f), Vector3(0.0f, 0.0f, 0.0f), 1.0);
	engine::ID id_matrix_local = TransformSystem::instance().addMatrix(lightToModel);

	Vector4 Q(1.0f, 0.0f, 0.0f, 0.0f);
	engine::ID id_matrix = TransformSystem::instance().addMatrix(modelToWorld);
	float radius = Matrix4x4::multiply(Q, modelToWorld).getMagnitude();
	Vector4 emission = Vector4(color, 1.0f) * I;
	LightSystem::instance().addSpotLightWithMask(id_matrix, id_matrix_local, p, emission, radius);
	auto model = ModelManager::instance().getUnitSphere();
	MeshSystem::instance().addEmissiveInstance(model, id_matrix, emission);
}

void Application::addSpotWithMaskLightSource(engine::ID id_modelToWorld, const Vector3& color, float I, float p)
{
	Matrix4x4 lightToModel = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, .0f, .0f, .0f), Vector3(0.0f, 0.0f, 0.0f), 1.0);
	engine::ID id_matrix_local = TransformSystem::instance().addMatrix(lightToModel);

	Vector4 Q(1.0f, 0.0f, 0.0f, 0.0f);
	Matrix4x4 modelToWorld = TransformSystem::instance().getMatrix(id_modelToWorld);
	float radius = Matrix4x4::multiply(Q, modelToWorld).getMagnitude();
	Vector4 emission = Vector4(color, 1.0f) * I;
	LightSystem::instance().addSpotLightWithMask(id_modelToWorld, id_matrix_local, p, emission, radius);
	auto model = ModelManager::instance().getUnitSphere();
	MeshSystem::instance().addEmissiveInstance(model, id_modelToWorld, emission);
}

void Application::addEmitter(Vector3 position, float spawn_rate_per_second, float spawn_circle_radius, float life_time_particle, Vector4 spawned_particle_color, Vector3 emission_color)
{
	Matrix4x4 emitterToWorld = Matrix4x4::createTransformationMatrix(Quaternion(1.0f, .0f, .0f, .0f), position, 0.1f);
	engine::ID id_matrix_world = TransformSystem::instance().addMatrix(emitterToWorld);

	ParticleSystem::instance().addSmokeEmitter( position, spawn_rate_per_second, spawn_circle_radius, life_time_particle, spawned_particle_color, emission_color, id_matrix_world);

	auto model = ModelManager::instance().getUnitSphere();
	MeshSystem::instance().addEmissiveInstance(model, id_matrix_world, spawned_particle_color);
}

void Application::checkDissolutionAnimation()
{
	for (auto& perModel : MeshSystem::instance().getDissolutioInstances().getPerModel())
	{
		for (auto& perMesh : perModel.perMesh)
		{
			for (auto& perMaterial : perMesh.perMaterial)
			{
				if (perMaterial.instances.size() == 0) continue;

				uint32_t numInstances = uint32_t(perMaterial.instances.size());
				//TODO the loop reversed for easy way to remove Instance
				for (int indexInstance = numInstances - 1; indexInstance > -1; --indexInstance)
				{
					if (perMaterial.instances[indexInstance].start_time + perMaterial.instances[indexInstance].duration < m_application_time)
					{
						OpaqueGroup::Material* opaque = reinterpret_cast<OpaqueGroup::Material*>(&(perMaterial.material));
						MeshSystem::instance().addOpaqueInstance(perModel.model, *opaque, perMaterial.instances[indexInstance].id_modelToWorld);
						MeshSystem::instance().remove(perMaterial.instances[indexInstance].id_entity);
					}
				}
			}
		}
	}
}

void Application::checkIncinerationAnimation()
{
	for (auto& perModel : MeshSystem::instance().getIncinerationInstances().getPerModel())
	{
		for (auto& perMesh : perModel.perMesh)
		{
			for (auto& perMaterial : perMesh.perMaterial)
			{
				if (perMaterial.instances.size() == 0) continue;

				uint32_t numInstances = uint32_t(perMaterial.instances.sizeMax());
				//TODO the loop reversed for easy way to remove Instance
				for (int indexInstance = numInstances - 1; indexInstance > -1; --indexInstance)
				{
					if (perMaterial.instances.occupied(indexInstance) == false)
						continue;
					float  delta_t = m_application_time - perMaterial.instances[indexInstance].start_time;
					float radius = perMaterial.instances[indexInstance].velocity * delta_t;
					if (perMaterial.instances[indexInstance].max_radius < radius)
					{
						MeshSystem::instance().remove(perMaterial.instances[indexInstance].id_entity);
					}
				}
			}
		}
	}
}

void Application::buttonPressed(ButtonsEvents button, int x, int y)
{

	if (m_io->WantCaptureMouse || m_io->WantCaptureKeyboard)
		return;

	switch (button)
	{
	case RBUTTON_MOUSE:
	{
		math::Intersection inter;
		MeshSystem::instance().findIntersection(RenderManager::instance().getCamera()->getRayForPixelPos(x, y,
			m_window.getWidthWindow(),
			m_window.getHeightWindow()),
			inter,
			m_dragger);
		break;
	}
	case LBUTTON_MOUSE:
	{
		m_pos_x_mouse_lmouse_press = x;
		m_pos_y_mouse_lmouse_press = y;
		updateDiffMouse(x, y);
		break;
	}
	case N:
		if(m_buttons_pressed.contains(button) == false)
			MeshSystem::instance().toggleRenderNormalVis();
		break;
	case F:
		if (m_buttons_pressed.contains(button) == false)
		{
			m_flash_light_attached = !m_flash_light_attached;
			flashLightProcess();
		}
		break;
	case M:
		if (m_buttons_pressed.contains(button) == false)
		{
			DissolutionGroup::Material stones = {
				TextureManager::instance().getTexture("assets/textures/stones/Wall_Stone_albedo.dds"),
				TextureManager::instance().getTexture("assets/textures/stones/Wall_Stone_normal.dds"),
				TextureManager::instance().getTexture("assets/textures/stones/Wall_Stone_roughness.dds"),
				TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
				{ Vector3(1.0f, 1.0f, 1.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 7}
			};

			Vector3 pos = RenderManager::instance().getCamera()->getRayForPixelPos(
				m_window.getWidthWindow() / 2, 
				m_window.getHeightWindow() / 2,
				m_window.getWidthWindow(),
				m_window.getHeightWindow()
			).getPoint(DISTANCE_SPAWN_OBJECT * 100);

			Quaternion orentation = RenderManager::instance().getCamera()->getQuaternion();

			auto model = ModelManager::instance().getUnitCube();
			Matrix4x4 modelToWorld = Matrix4x4::createTransformationMatrix(orentation, pos);
			MeshSystem::instance().addDissolutionInstance(model, stones, modelToWorld, m_application_time, DISSOLUTION_TIME);
		}
		break;
	case B:
		if (m_buttons_pressed.contains(button) == false)
		{
			DecalSystem::Material splatter = {
				TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
				TextureManager::instance().getTexture("assets/textures/Decal/splatter/splatter_normal.dds"),
				TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
				TextureManager::instance().getTexture("assets/textures/missingTexture.dds"),
				{ Vector3(1.0f, 0.0f, 0.0f), 1.0f, Vector3(0.5f, 0.5f, 1.0f), 0.0f, 2}
			};
			internal::ObjRef objref;
			math::Intersection inter;
			math::Ray ray = RenderManager::instance().getCamera()->getRayForPixelPos(
				m_window.getWidthWindow() / 2,
				m_window.getHeightWindow() / 2,
				m_window.getWidthWindow(),
				m_window.getHeightWindow());

			MeshSystem::instance().findIntersection(ray, objref, inter);

			Vector3 pos = ray.getPoint(inter.t);

			Quaternion orentation = RenderManager::instance().getCamera()->getQuaternion();

			auto model = ModelManager::instance().getUnitCube();
			Matrix4x4 modelToWorld = Matrix4x4::createTransformationMatrix(orentation, pos);
			MeshSystem::instance().addDecalInstance(splatter, modelToWorld, objref.id);
		}
		break;
	case DEL:
		if (m_buttons_pressed.contains(button) == false)
		{
			internal::ObjRef objref;
			math::Intersection inter;
			math::Ray ray = RenderManager::instance().getCamera()->getRayForPixelPos(m_pos_x_mouse, m_pos_y_mouse,
				m_window.getWidthWindow(),
				m_window.getHeightWindow());

			if (MeshSystem::instance().findIntersection(ray, objref, inter) == true)
			{
				ModelInstanceID* entity = &MeshSystem::instance().getInstances()[objref.id];
				//TODO need use CRT here
				MeshInstanceID::IdComponent& id_component_main = entity->meshInstanceIDs[0].id_component_main;
				switch (id_component_main.id_shading)
				{
				case internal::IntersectedType::Opaque:
				{
					Vector3 pos = ray.getPoint(inter.t);
					std::vector<OpaqueGroup::PerModel>& perModels = MeshSystem::instance().getOpaqueInstances().getPerModel();
					size_t num_meshes = perModels[id_component_main.id_model].perMesh.size();
					std::vector<IncinerationGroup::Material> matrials;
					matrials.resize(num_meshes);
					for (int id_mesh = 0; id_mesh < num_meshes; id_mesh++)
					{
						IncinerationGroup::Material* incineration = reinterpret_cast<IncinerationGroup::Material*>(&(perModels[id_component_main.id_model].perMesh[id_mesh].perMaterial[id_component_main.id_material].material));
						matrials[id_mesh] = *incineration;
					}
					
					const std::shared_ptr<Model>& model = perModels[id_component_main.id_model].model;
					Vector3 size = model->box.size();

					MeshSystem::instance().addIncinerationInstance(model,
						matrials,
						perModels[id_component_main.id_model].perMesh[0].perMaterial[id_component_main.id_material].instances[id_component_main.id_instance].id_modelToWorld,
						Vector4(float(id_component_main.id_instance % 2), 1.0f, 1.0f, 1.0f),
						Vector4(float(id_component_main.id_instance % 2), 1.0f, 1.0f, 1.0f),
						pos,
						m_application_time,
						0.2f,
						size.lenght());

					MeshSystem::instance().remove(objref.id);
					break;
				}
				case internal::IntersectedType::Hologram:
				case internal::IntersectedType::Dissolution:
				case internal::IntersectedType::Emessive:
				case internal::IntersectedType::Incineration:
				{
					MeshSystem::instance().remove(objref.id);
					break;
				}
				case internal::IntersectedType::NUM:
					break;
				default:
					break;
				}

			}

		}
		break;
	default:
		break;
	}
	m_buttons_pressed.emplace(button);
}

void Application::buttonReleased(ButtonsEvents button)
{
	//TODO maybe this 'if' need only in Application::buttonPressed()
	if (m_io->WantCaptureMouse || m_io->WantCaptureKeyboard)
		return;

	switch (button)
	{
	case RBUTTON_MOUSE:
	{
		m_dragger.release();
		break;
	}
	default:
		break;
	}
	m_buttons_pressed.erase(button);
}

void Application::onMouseMove(int x, int y)
{
	updateDiffMouse(x, y);

	m_pos_x_mouse = x;
	m_pos_y_mouse = y;
}

void Application::onMouseWheelScroll(int delta)
{
	m_speed_camera *= (1.0f + (0.05f * delta / WHEEL_DELTA));
}

void Application::resize(int width, int height)
{
	m_window.resize(width, height);
	RenderManager::instance().setResolution(width, height);
	RenderManager::instance().getCamera()->setPerspective(FOV * K_DEG_TO_RAD, static_cast<float>(m_window.getHeightRender()) / m_window.getWidthRender(), 0.01, 50);
}

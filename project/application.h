#pragma once

#define NOMINMAX

#include "engine.h"
#include <memory>
#include <set>
#include <algorithm>
#include <imgui/imgui.h>

enum ButtonsEvents {
	LBUTTON_MOUSE = VK_LBUTTON,
	RBUTTON_MOUSE = VK_RBUTTON,
	SPACE = VK_SPACE,
	CTRL = VK_CONTROL,
	SHIFT = VK_SHIFT,
	A = 0x41,
	B,
	C,
	D,
	E,
	F,
	G,
	H,
	I,
	J,
	K,
	L,
	M,
	N,
	O,
	P,
	Q,
	R,
	S,
	T,
	U,
	V,
	W,
	X,
	Y,
	Z = 0x5A,
	ADD = VK_ADD,
	SUBTRACT = VK_SUBTRACT,
	DEL  = VK_DELETE
};

class Application
{
public:
	int init(HINSTANCE appHandle, WNDPROC WinProc, int windowShowParams);
	void deinit();
	void tick();
	void buttonPressed(ButtonsEvents button, int x, int y);
	void buttonReleased(ButtonsEvents button);
	void onMouseMove(int x, int y);
	void onMouseWheelScroll(int delta);
	void resize(int width, int height);
	
private:
	const float PI = 3.14159265f;
	const float FOV = 90.0f;
	const float K_DEG_TO_RAD = PI / 180;
	const float SPEED_ROTATE_CAMERA = PI; // 180 deg per 1 second
	const float SPEED_CHANGING_EV100 = 0.5f;
	const float DISTANCE_SPAWN_OBJECT = 4.0f;
	const float DISSOLUTION_TIME = 14.0f;

	void processInput();
	void initScene();
	void updateDiffMouse(int x, int y);
	void flashLightProcess();

	void addPointLightSource(const Vector3& position, const Vector3& color, float I, float radius);
	void addSpotLightSource(const Matrix4x4& modelToWorld, const Vector3& color, float I, float p);
	void addSpotWithMaskLightSource(const Matrix4x4& modelToWorld, const Vector3& color, float I, float p);
	void addSpotWithMaskLightSource(engine::ID id_modelToWorld, const Vector3& color, float I, float p);
	void addEmitter(Vector3 position, float spawn_rate_per_second, float spawn_circle_radius, float life_time_particle, Vector4 spawned_particle_color, Vector3 emission_color);

	void checkDissolutionAnimation();
	void checkIncinerationAnimation();

	Window m_window;
	Timer m_timer;
	Time m_start_time;
	float m_application_time;



	std::unique_ptr <ParallelExecutor> m_parallel_executor;
	std::set<ButtonsEvents> m_buttons_pressed;
	ImGuiIO* m_io;

	std::string m_sky_cube_path = "assets/textures/cubemaps/mountains.dds";

	std::unique_ptr<math::MatrixMover> m_dragger;

	int m_pos_x_mouse = 0;
	int m_pos_y_mouse = 0;

	int m_pos_x_mouse_lmouse_press = 0;
	int m_pos_y_mouse_lmouse_press = 0;

	float m_diff_x_mouse = 0;
	float m_diff_y_mouse = 0;

	engine::ID m_id_matrix_flash_light;

	bool m_flash_light_attached;

	float m_speed_camera = 0.2f;

	float m_roughnessSlider;
	int m_depth_bias = -250;
	float m_slope_scaled_depth_bias = - 2.1f;
	bool m_en_diffuse = true;
	bool m_en_specular = true;
	bool m_en_IBL = true;
	bool m_en_roughness_overwriting = false;
	bool m_deferred_rendering = true;

	float m_qualitySubpix = 0.75f;
	float m_qualityEdgeThreshold = 0.063f;
	float m_qualityEdgeThresholdMin = 0.0312f;
	float m_coffAbsorption = 0.2;
	float m_asymmetryParameter = 0.962;
};


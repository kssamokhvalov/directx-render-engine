// project.cpp : Defines the entry point for the application.
//

#include <windows.h>
#include <vector>
#include <memory>
#include <chrono>
#include <thread>
#include <imgui/imgui.h>
#include <imgui/imgui_impl_win32.h>
#include "application.h"

const float PI = 3.14159265f;

void initConsole()
{
    AllocConsole();
    FILE* dummy;
    auto s = freopen_s(&dummy, "CONOUT$", "w", stdout); // stdout will print to the newly created console
}

Vector3 randomHemisphere(float& NdotV, float i, float N)
{
    
    const float GOLDEN_RATIO = (1.0f + sqrtf(5.0f)) / 2.0f;
    float theta = 2.0f * PI * i / GOLDEN_RATIO;
    float phiCos = NdotV = 1.0f - (i + 0.5f) / N;
    float phiSin = sqrtf(1.0f - phiCos * phiCos);
    float thetaCos = cosf(theta);
    float thetaSin = sinf(theta);
    return Vector3(thetaCos * phiSin, thetaSin * phiSin, phiCos);
}

bool testIntegralHemisphere()
{
    const int NUM_SAMPLES = 1024;
    float NoV = 0.0f;
    float interal = 0.0f;
    for (int i = 0; i < NUM_SAMPLES; ++i)
    {
        randomHemisphere(NoV, i, NUM_SAMPLES);
        interal += NoV;
    }
    interal *= 2 * PI / NUM_SAMPLES;
    std::cout << "Integral of cosine over the hemisphere: " << interal << std::endl;

    return interal - PI < 0.01f;
}

static Application s_application;


LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE appHandle, HINSTANCE, LPSTR cmdLine, int windowShowParams)
{
    MSG msg;

    initConsole();
    testIntegralHemisphere();

    Engine::init();
    s_application.init(appHandle, WindowProc, windowShowParams);

    while (true)
    {
        while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) > 0)
        {
            if (msg.message == WM_QUIT) {
                s_application.deinit();
                Engine::deinit();
                return 0;
            }
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }

        s_application.tick();
        std::this_thread::yield();
	}
}

// Forward declare message handler from imgui_impl_win32.cpp
extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    if (ImGui_ImplWin32_WndProcHandler(hWnd, message, wParam, lParam))
        return true;

    switch (message)
    {
    case WM_DESTROY:
    {
        PostQuitMessage(0);
        return 0;
    }
    break;

    case WM_RBUTTONDOWN:
        s_application.buttonPressed(ButtonsEvents::RBUTTON_MOUSE, LOWORD(lParam), HIWORD(lParam));
        return 0;
    case WM_RBUTTONUP:
        s_application.buttonReleased(ButtonsEvents::RBUTTON_MOUSE);
        return 0;

    case WM_LBUTTONDOWN:
        s_application.buttonPressed(ButtonsEvents::LBUTTON_MOUSE, LOWORD(lParam), HIWORD(lParam));
        return 0;
    case WM_LBUTTONUP:
        s_application.buttonReleased(ButtonsEvents::LBUTTON_MOUSE);
        return 0;

    case WM_MOUSEWHEEL: {
        s_application.onMouseWheelScroll(GET_WHEEL_DELTA_WPARAM(wParam));
        return 0;
    }
    case WM_MOUSEMOVE:
        s_application.onMouseMove(LOWORD(lParam), HIWORD(lParam));
        return 0;

    case WM_KEYDOWN:
        s_application.buttonPressed(static_cast<ButtonsEvents>(wParam), LOWORD(lParam), HIWORD(lParam));
        return 0;
    case WM_KEYUP:
        s_application.buttonReleased(static_cast<ButtonsEvents>(wParam));
        return 0;
    case WM_SIZE:
        s_application.resize(LOWORD(lParam), HIWORD(lParam));
        return 0;
        break;
    }
    return DefWindowProc(hWnd, message, wParam, lParam);
}